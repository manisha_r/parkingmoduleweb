package com.traffsys.parkingsolution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
//import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.traffsys.parkingsolution.components.MyProperties;
import com.traffsys.parkingsolution.security.MySessionListener;

@SpringBootApplication
@Configuration
@ComponentScan
@EnableAutoConfiguration
//@EnableEurekaClient
//@EnableDiscoveryClient
@ComponentScan

public class ParkingSolutionWebApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(ParkingSolutionWebApplication.class, args);
	}
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(ParkingSolutionWebApplication.class);
	}
	
	
	@Bean
	public CorsFilter corsFilter() {
		try (ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class,
				MyProperties.class);) {
			MyProperties dbProperties = context.getBean(MyProperties.class);
			System.out.println("Error =" + dbProperties.getAllowedDomains());
			List<String> domain = dbProperties.getAllowedDomains();
			String domaineds = domain.get(0);
			String domainArray[] = domaineds.split(",");
			System.out.println("Value = " + domainArray[0]);

			List<String> newDomain = new ArrayList<String>();

			for (String m : domainArray) {
				newDomain.add(m);
			}

			final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
			final CorsConfiguration config = new CorsConfiguration();
			config.setAllowCredentials(true);
			config.setAllowedOrigins(newDomain);
			config.setAllowedHeaders(Arrays.asList("Origin", "Content-Type", "Accept", "Authorization"));
			config.setAllowedMethods(Arrays.asList("GET", "POST"));

			source.registerCorsConfiguration("/**", config);
			return new CorsFilter(source);

		}
	}
	
	/*
	 * @Bean public WebServerFactoryCustomizer<TomcatServletWebServerFactory>
	 * tomcatCustomizer() { return customizer ->
	 * customizer.addConnectorCustomizers(connector -> {
	 * connector.setAllowTrace(false); }); }
	 */
	
	
	@Bean
    public MySessionListener mySessionListener() {
        return new MySessionListener();
    }
 
}