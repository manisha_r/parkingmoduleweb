package com.traffsys.parkingsolution;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.traffsys.parkingsolution.components.MyProperties;
import com.traffsys.parkingsolution.utils.Util;

@Configuration
public class AppConfig {

	@Bean
	public static PropertySourcesPlaceholderConfigurer properties() throws MalformedURLException {
		Path rootLocation = Util.initConfigurationRoot();
		Path p = rootLocation.resolve("configuration.properties");
 		PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
		propertySourcesPlaceholderConfigurer.setLocations(new UrlResource("file:" + p.toAbsolutePath()));
		return propertySourcesPlaceholderConfigurer;
	}

	@Bean
	public WebMvcConfigurer securityConfigurerAdapter() {
		return new WebMvcConfigurer() {
			@Override
			public void addInterceptors(
					org.springframework.web.servlet.config.annotation.InterceptorRegistry registry) {
				// registry.addInterceptor(new
				// HtmlEscapeRequestInterceptor()).addPathPatterns("/**");
				registry.addInterceptor(new SecurityHeadersInterceptor());
			}
		};
	}

	private static class SecurityHeadersInterceptor extends HandlerInterceptorAdapter {
		
		ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class,
				MyProperties.class);
		
		@Override
		public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
				throws IOException {
			
			response.setHeader("X-FRAME-OPTIONS", "DENY");
			response.setHeader("Strict-Transport-Security", "max-age=7776000");
			response.setHeader("Pragma", "No-cache");
			response.setHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
			response.setDateHeader("Expires", 0);

			response.setHeader("X-XSS-Protection", "1; mode=block");
			response.setHeader("X-Powered-By", "");
			//response.setHeader("Content-Security-Policy",
				//	"default-src *; script-src 'self' 'unsafe-inline' 'unsafe-eval' *; style-src 'self' 'unsafe-inline' *; img-src 'self' blob: *;");
			response.setHeader("Content-Security-Policy", "default-src *; script-src 'self' 'unsafe-inline' 'unsafe-eval' *; style-src 'self' 'unsafe-inline' *; img-src 'self' blob: * data:;");

			response.setHeader("X-Content-Type-Options", "nosniff");
			response.setHeader("Content-Type", " text/html; charset=utf-8 ");
  			response.setHeader("Allow", "GET,POST");
  			response.setHeader("Referrer-Policy", "strict-origin-when-cross-origin");
			
 

 				MyProperties dbProperties = context.getBean(MyProperties.class);

 				String allowedIPs = dbProperties.getWhitelistip();
 	 			if (allowedIPs.isEmpty()) {
 				    System.err.println("Add an IP in the whitelist of the Config file");
 	 			}
 				
 	 			List<String> allowedIPList = Arrays.asList(allowedIPs.split(","));
 	  			
 				// Retrieve the Host and X-Forwarded-Host headers from the HTTP request
 				String host = request.getHeader("host");
 				String xForwardedHost = request.getHeader("X-Forwarded-Host");

 				if (xForwardedHost != null && !xForwardedHost.equalsIgnoreCase("null"))
 				{
 					 if (!xForwardedHost.equalsIgnoreCase(host)) {
 			                sendErrorResponse(response, HttpStatus.FORBIDDEN, "Invalid X-Forwarded-Host");
 					        return false;
 					    }
 				}
 	 			
 				// Check if the request's Host is in the whitelist
 				if (!allowedIPList.contains(host)) {
 		            sendErrorResponse(response, HttpStatus.FORBIDDEN, "Host not allowed");
 	 			    return false;
 				}
 	 		
  			
  			if (HttpMethod.GET.matches(request.getMethod()) || HttpMethod.POST.matches(request.getMethod())) {
				return true;
			} else {
 				return false;
			}
		}

		@Override
		public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
				Exception ex) throws Exception {
			response.setHeader("X-FRAME-OPTIONS", "DENY");
			response.setHeader("Strict-Transport-Security", "max-age=7776000");
			response.setHeader("Pragma", "No-cache");
			response.setHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
			response.setDateHeader("Expires", 0);

			response.setHeader("X-XSS-Protection", "1; mode=block");
			response.setHeader("X-Powered-By", "");
			response.setHeader("Content-Security-Policy", "default-src *; script-src 'self' 'unsafe-inline' 'unsafe-eval' *; style-src 'self' 'unsafe-inline' *; img-src 'self' blob: *;");

			response.setHeader("X-Content-Type-Options", "nosniff");
 			response.setHeader("Content-Type", " text/html; charset=utf-8 ");
			response.setHeader("Allow", "GET,POST");
			response.setHeader("Referrer-Policy", "strict-origin-when-cross-origin");

			//if (response.getStatus() == HttpStatus.METHOD_NOT_ALLOWED.value()) {
				//response.setHeader("Allow", null);
 			//}
		}
		
	}
	//---------------SET ERROR RESPONSE TO CLIENT-----------------------------------//
	 private static void sendErrorResponse(HttpServletResponse response, HttpStatus status, String message) throws IOException {
	        response.setStatus(status.value());
	        response.getWriter().write(message); // Write the custom message to the response body
	    }
	
	
	 //---------------SET COMMON RESPONSE HEADERS-----------------------------------//
	  private static void setCommonResponseHeaders(HttpServletResponse response) {
	        response.setHeader("X-FRAME-OPTIONS", "DENY");
	        response.setHeader("Strict-Transport-Security", "max-age=7776000");
	        response.setHeader("Pragma", "No-cache");
	        response.setHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
	        response.setDateHeader("Expires", 0);
	        response.setHeader("X-XSS-Protection", "1; mode=block");
	        response.setHeader("X-Powered-By", "");
	        response.setHeader("Content-Security-Policy", "default-src *; script-src 'self' 'unsafe-inline' 'unsafe-eval' *; style-src 'self' 'unsafe-inline' *; img-src 'self' blob: *;");
	        response.setHeader("X-Content-Type-Options", "nosniff");
	        response.setHeader("Content-Type", "text/html; charset=utf-8");
			response.setHeader("Allow", "GET,POST");
	        response.setHeader("Referrer-Policy", "strict-origin-when-cross-origin");
	    }
}
