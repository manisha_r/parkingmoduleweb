<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
<meta charset="ISO-8859-1">
<title>City | VPark</title>
        <link href="${pageContext.request.contextPath}/resources/css/application.min.css" rel="stylesheet">
         <link rel="shortcut icon" type="image/x-icon"  href="${pageContext.request.contextPath}/resources/img/logo.png">
       
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/line-awesome/dist/line-awesome/css/line-awesome.css" rel="stylesheet">
             
        <link href="${pageContext.request.contextPath}/resources/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
		
		
		<!----------------------------------------------progressbar--------------------------------------------------------->
		<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
		
  		<!----------------------------------------------Jquery------------------------------------------------------>
            <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>   

        <!-----------------------------------------dataTable------------------------------------------------------>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>
        <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        
        <!-----------------------------------------------ANGUARJS----------------------------------------------->
<!--         <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
 -->	
        <!---------------------------------- API URL ------------------------------------------------------>
        <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script> 
	
        <!-- Datatables css-->
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"rel="stylesheet">
	    <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
	    rel="stylesheet">
	    <!-- Datatables js-->
        <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

        <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>

         <!---------------------------------------DATEPICKER-------------------------------------------------------->
		 <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

       
		  <!-------------------------------- Load Leaflet from CDN----------------------------------------------->
		  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
		    integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
		    crossorigin=""/>
		  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
		    integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
		    crossorigin=""></script>
		
		  <!------------------------------ Load Esri Leaflet from CDN -------------------------------------------->
		  <script src="https://unpkg.com/esri-leaflet@2.5.0/dist/esri-leaflet.js"
		    integrity="sha512-ucw7Grpc+iEQZa711gcjgMBnmd9qju1CICsRaryvX7HJklK0pGl/prxKvtHwpgm5ZHdvAil7YPxI1oWPOWK3UQ=="
		    crossorigin=""></script>
		
		  <!------------------------------- Load Esri Leaflet Geocoder from CDN ----------------------------------->
		  <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
		    integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
		    crossorigin="">
		  <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
		    integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
		    crossorigin=""></script>
             
             <!-------------------------------------------markercluster  ------------------------------------------>
	        <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.0.4/dist/MarkerCluster.Default.css">
			<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.0.4/dist/MarkerCluster.css">
			<script src="https://unpkg.com/leaflet.markercluster@1.0.4/dist/leaflet.markercluster.js"></script>
			<script src="${pageContext.request.contextPath}/resources/MovingMarker.js"></script>
			
			<!-- Load Clustered Feature Layer from CDN -->
	        <script src="https://unpkg.com/esri-leaflet-cluster@2.0.0/dist/esri-leaflet-cluster.js"></script>
	        
	       <script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>
	        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/webmain.css">
	    
    	  <!---------------------------------- SweetAlert  ------------------------------------------------------>
          <script src="${pageContext.request.contextPath}/resources/sweetAlert.js"></script> 
	  

	<style>
	
   .previous {
  text-decoration: none;
  display: inline-block;
  padding: 8px 16px;
  background-color: #f1f1f1;
   cursor:pointer;
  /*  border-radius: 50%; */
   }
 </style>
    
    <script>
    
    
    $(document).ready(function(){

        $("#confi").addClass("active");
        $("#confi").addClass("open");
        $("#sidebar-ui").addClass("show");
        $("#kcities").addClass("active");

		 });
    
    var uid = '<%=session.getAttribute("uid")%>';
	var userN = '<%=session.getAttribute("user")%>';

	var status ='<%= session.getAttribute("isLoggedIn")%>';
	var token = '<%=session.getAttribute("token") %>';
	var options = {
	 		   headers: {
	 		     Authorization: 'Bearer ' + '${jwtToken}' // Assuming 'jwtToken' holds the JWT
	 		   }
	 		 };
	
	var jDataTable;
	var jData;
	var jDatanew;
	var jDataTablenew;
	var incilat=0.0; 
  	var incilon=0.0;
       var app = angular.module("app", []);
       app.controller("GetDataController", ['$scope','$http','$window', function(scope,http,window) {
    	   document.getElementById('overl').style.visibility = "hidden";
    	   document.getElementById('overlid').style.visibility = "hidden";
    	   
   		//******************************SITE LIST******************************//
    		scope.sitedata = function () { 
   		
   	        http.get(apiURL+'getlocation/'+userN+'/doGetLocation',options).
   					then(function (response) {
   	
   						if(response.data != null)
   						{
   					    $(function() {
   				 		    Pace.on("done", function(){
   				 		   });
   				 		});
   				  	    document.getElementById('overl').style.visibility = "hidden";
   				  	   
   						if(response.data.status==1){
   							       	jData = [];
   				             		jData= response.data.myObjectList;
        				            jDataTable.clear().rows.add(jData).draw();
   				         }else if(response.data.status==21){
		   		  				 
			  		        } else{
   				        	 jData = [];
   				       	     jDataTable.clear().rows.add(jData).draw();
   				               }  
   				   }else{
   				     	 jData = [];
   				   	     jDataTable.clear().rows.add(jData).draw();
   				      }   
   						
   					}, function (response) {
   					});
   			 
   	};  	
   	/*********************************SITE DATA*************************************/
   	$(document).ready(function() {
   		 jDataTable = $('#siteTable').DataTable({ 
   	 		 	data: jData,
   		            columns: [
   		         	 {'data':'id'},
   		                
   		             {'data':'locationName', 'render':function(locationName){
	         				
 	             		 if(locationName!=null)
	             			 {
 	             			if(locationName!=""){
  	         					return locationName;
   	         				}else{
   	         					return '<p>__</p>';
   	         				}
	             			 }else{
	             				return '<p>__</p>';
	             			 }
	         			}},
   		        
	         			{'data':'lat', 'render':function(lat){
 	         				
    	             		 if(lat!=null)
	             			 {
    	             			if(lat!=""){
     	         					return lat;
      	         				}else{
      	         					return '<p>__</p>';
      	         				}
	             			 }else{
	             				return '<p>__</p>';
	             			 }
	         			}},
	         	 		{'data':'lon', 'render':function(lon){
 	         				
   	             		 if(lon!=null)
	             			 {
   	             			if(lon!=""){
    	         					return lon;
     	         				}else{
     	         					return '<p>__</p>';
     	         				}
	             			 }else{
	             				return '<p>__</p>';
	             			 }
	         			}},
   		               {'data': "id", 'render':function(id){
	         				
	         				if(id!=null){
	         						return '<u id="edit" style="color: #1f94f4;padding-left: 14px; cursor:pointer; font-size:18px;"\><span class="glyphicon glyphicon-edit"></span></u>';
	         				   
	         				}
	         			}}  
   		             	
   		       
   		            ],
   		           buttons: [
   		        	    'copy', 'excel', 'pdf'
   		        	  ],
   	
   		 	             "ordering": false,
   		 	             "searching": false,
   		 	             "deferRender": true,
   		 	             "autoWidth": false,
   		 	            "lengthChange": false,
   		 	             "language": {
   		     	    		"infoEmpty": "No records to show",
   		         	    	"emptyTable": "not record found",
   		     	    	    }
   		    	        });
   		  
   	  }); 
   	
    	   
    
	 //**************SELECT DESELECT TABLE ROW******************************//
	 $('#siteTable').on('click', 'tr', function () {
	 	  
		 if ( $(this).hasClass('row_selected'))
	 	  		{  
	 			   $("#siteTable tbody tr").removeClass('row_selected');
	 	  	       $(this).removeClass('row_selected');
	 	  	    }else
	 	  	        {
	 	  	        $("#siteTable tbody tr").removeClass('row_selected');
	 	  	        $(this).addClass('row_selected');
	 	  	
	 	  	    }
	 }); 
	 

	   //*************************UPDATE CLICK*******************************//
	  	$('#siteTable').on('click','#edit', function () {
	   		
	  		scope.UserModel = "";
	  		scope.$apply();
	  		var tr = $(this).closest('tr');
	  		var selectedData = jDataTable.row(tr).data();
	  	    scope.UserModel =selectedData;
	  	    $("#edit_tab").show();
	  		$("#user_tab").hide();
			scope.viewOnMap(); 

	  		scope.$apply();
	  		

		 });

	  	 //BACK TO TABLE
		    scope.gobackto = function(){
		   	 $("#edit_tab").hide();
		   	 $("#user_tab").show();
		     };	
	   
	   
        /******************UPDATE Location**************************************************/
    	scope.updateLocation=function(UserModel)
    	{
    		 scope.submitted = true;
    		 incilat=document.getElementById('lat').value;
    		 incilon=document.getElementById('lon').value ;    
    		   scope.locationdata = {
		      			
						"id":UserModel.id,
						"locationName":UserModel.locationName,
						"lon":incilon,
						"lat":incilat
			      	};	
    		   scope.requestedData = {
							"username":userN,
							"functionName" : "doUpdateLocationInfo",
				  			 "data" : JSON.stringify(scope.locationdata)
		
						};
		     	  
		      	
		      	    /***********************VALIDATION**********************/
					if (scope.addUser.$valid) 
					{	
					  if(UserModel.locationName != undefined &&  document.getElementById('lon').value !=""&&  document.getElementById('lat').value !="")
					   {    
						  document.getElementById('overlid').style.visibility = "visible";
						  			
					       http({
							method :'POST',
							url :apiURL+'updatelocation',
							data :scope.requestedData,
						    headers: options.headers

							}).then(function (response) {
			 					if(response.data.status==1){
			 						
			 						scope.response_message = response.data.message;
			 						var x = document.getElementById("snackbar_success")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 3000);
					 				 setTimeout(function() {	
			 							 location.reload();
			  						}, 1500); 
								}else if(response.data.status==0)
								{
									scope.message = response.data.message;
									var x = document.getElementById("snackbar_error")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 3000);
					 			
					 				document.getElementById('overlid').style.visibility = "hidden";
									
								}else if(response.data.status==21){
			   		  				 
				  		        } else{
									scope.message = response.data.message;
									document.getElementById('overlid').style.visibility = "hidden";
									
							    } 
									
			  					      
							}, function (response) {
							});
						 
			     	  } else  {
			   				
						  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		              
						 } 
					  }// end of validate for angularjs
			     	  
					 else  {
			   				
						  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		              
						 } 
					 
		     	
    	};
    	
		//-------MAP JS---------//
		
    	var marker;
    	var map;
    	var geocodeService1;
  	  var mapZoom;
		map= L.map('map').setView([21.7679,78.8718], 4);
    	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    	  attribution: 'Map'
    	}).addTo(map);

    	 var smallIcon = new L.Icon({
	            iconSize: [27, 27],
	            iconAnchor: [13, 27],
	            popupAnchor: [1, -24],
	            iconUrl: 'https://cdn4.iconfinder.com/data/icons/small-n-flat/24/map-marker-512.png'
	        });


    	var arcgisOnline = L.esri.Geocoding.arcgisOnlineProvider();

    	var searchControl = L.esri.Geocoding.geosearch({
    	  providers: [
    	    arcgisOnline,
    	    L.esri.Geocoding.mapServiceProvider({
    	      label: 'States and Counties',
    	      url: 'https://sampleserver6.arcgisonline.com/arcgis/rest/services/Census/MapServer',
    	      layers: [2, 3],
    	      searchFields: ['NAME', 'STATE_NAME']
    	    })
    	  ]
    	}).addTo(map);

    	  
  	  mapZoom = L.featureGroup().addTo(map);
  	  setInterval(function (){
  	         map.invalidateSize();
  	       }, 100);
    	geocodeService1 = L.esri.Geocoding.geocodeService();

    	function onMapClick(e) {
    	incilat=e.latlng.lat;
    	incilon=e.latlng.lng;

    	//document.getElementById('gatlat').value = e.latlng.lat;
    	//document.getElementById('gatlon').value = e.latlng.lng;

    	scope.UserModel.lat = e.latlng.lat;
    	scope.UserModel.lon = e.latlng.lng;

		
    	scope.$apply();

    	geocodeService1.reverse().latlng(e.latlng).run(function (error, result) {	
    	   if (marker != undefined) {
    	       map.removeLayer(marker);
    	        
    	 };
    	   marker=  L.marker(result.latlng, {icon: smallIcon}).addTo(map).bindPopup(result.address.Match_addr).openPopup();
    	   map.fitBounds([[e.latlng.lat, e.latlng.lng]]); 
    	 });
    	 
    	}

    	map.on('click', onMapClick);


    	//----------------VIEW ON MAP BUTTON CLICK----------------//
    	scope.viewOnMap = function()
    	{ 
    		scope.viewsubmitted = true;
    	 
    		incilat = scope.UserModel.lat;
    		incilon = scope.UserModel.lon;
    		
    		if(incilat !== undefined && incilon !== undefined)
    		   {
    			
    		     var latlng ={"lat":incilat,"lng":incilon};
    	  	     if (marker) {
    		    	 map.removeLayer(marker);
    	 	       }
    		     
    		    
    		    
    	 	    geocodeService1.reverse().latlng(latlng).run(function (error, result) {
    		         if (marker != undefined) {
    				   	 map.removeLayer(marker);
    				   	 if (error) {			 	   		  	  
    				  	       return;
    				  	     }	        
    				  }
    				    marker=  L.marker(latlng, {icon: smallIcon}).addTo(map).bindPopup(result.address.Match_addr).openPopup();
    				    map.fitBounds([[incilat,incilon]]);
    				  });
    	 	    
    	 	    
    	 	    
    		   }//end of lat lon check
    	 	     
    	} //--end of view on map click
    	 
             
   }]);
</script> 

</head>
 <body class="" ng-controller="GetDataController" ng-init="sitedata()">
 
  <%@ include file="Sidebar.jsp"%>	
  
    <div class="content-wrap" >
      <!-- main page content. the place to put widgets in. usually consists of .row > .col-lg-* > .widget.  -->
      <main id="contentt" class="content" role="main" style="  padding-bottom: 70px;" class="ng-cloak">
        <!-- Page content -->
        <!-- add Site data form -->

     
  <div id="snackbar_success">{{response_message}}</div>
  <div id="snackbar_error">{{message}}</div>
    <div  id="user_tab"> 
    <div class="row">
            <div class="col-lg-12">
                <section class="widget">
                    <header>
                         <h5>
                            All Cities 
                           <span class="fw-semi-bold"></span>
                        </h5>
                    </header>
                    <div class="widget-body">
                    <div class="table-responsive">
                      <table class="table table-striped dataTable  table-lg mt-lg mb-0" id="siteTable">
                      <thead class=" text-primary">
                      <tr>
                                	<th data-visible="false">ID</th>
                                	       <th style=" text-transform: revert; font-size: 15px;">City Name</th>
                                	       	<th style=" text-transform: revert; font-size: 15px;">Latitude</th>
											<th style=" text-transform: revert; font-size: 15px;">Longitude </th>
											<th style=" text-transform: revert; font-size: 15px;">Action</th>
						 </tr>
                         </thead>
                       </table>
                        </div>
                        <div class="clearfix">
                     </div>
                    </div>
                </section>
            </div>
        </div>
        </div>
       <div class="row" id="edit_tab" style="display:none;">
         <div class="col-lg-12">
          <section class="widget">
            <div class="widget-body">
               <form class="form-horizontal"  name="addUser" novalidate autocomplete="off">
                    <fieldset>
                        <legend style="padding-bottom: 12px;font-size: 18px;" id="allp"><strong>Update City</strong> 
                                               <a class="previous" ng-click="gobackto()" style="float: right;">� Back</a>
                        
                                  </legend>
                          
                        <!-- Site Name -->
                       <div class="form-group row">
                         <label for="siteName" class="col-md-4 form-control-label ">City Name</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="siteName"	placeholder="Enter City Name" ng-model="UserModel.locationName"
											id="siteName" onkeyup="alphaNumericChecker();"   required="required" />
											
									 <span style="color: red" ng-show="(addUser.siteName.$dirty || submitted)&& addUser.siteName.$error.required">
											City Name  is required !</span> 
											
									 
										  </div>
                        </div>
                       
                        <div class="form-group row">
                          <label for="lat" class="col-md-4 form-control-label ">City Location</label>
                            <div class="col-md-3">
                                	<input type="text" class="form-control  " name="lat"	placeholder="Enter latitude" ng-model="UserModel.lat" onkeyup="alphaNumericChecker();" 
											id="lat"/>
											
											<!-- <span style="color: red" ng-show="(addUser.latitude.$dirty || submitted)&& addUser.latitude.$error.required">
											Latitude is required !</span> --> 
									
									 <div style="margin-top: 15px;">
									 
									 <input type="text" class="form-control  " name=lat	placeholder="Enter longitude" ng-model="UserModel.lon" onkeyup="alphaNumericChecker();" 
											id="lon" />
							
							         <!--  <span style="color: red" ng-show="(addUser.latitude.$dirty || submitted)&& addUser.latitude.$error.required">
											Longitude  is required !</span>  -->
                             
									</div>
									
									   <div style="margin-top: 15px;"> <button class="btn btn-primary" style="background-color: #002B49;" ng-click="viewOnMap()"  >Preview on Map</button></div>
					      </div>
					     
                        <div class="col-md-4" id="mpb" style=" padding-left: 0px;">
                           <div id="map" class="mapCard"></div>
 	                     </div>	                       
				         </div>
                        
                     
                    </fieldset>
                    <div class="form-actions bg-transparent">
                        <div class="row">
                            <div class="offset-md-4 col-md-7 col-12">
                          	<button type="submit" ng-click="updateLocation(UserModel)"class="btn btn-primary" id="updatebtn" style="background-color: #002B49;">Update </button>
    				 
                            </div>
                        </div>
                    </div>
                     </form>
              </div>
           </section>
        </div>
     </div>
        
        
    
        
        

    		
		</main>
</div>
        
  <!-- spinner -->
  <div class="loader-wrap" style="background-color: rgba(0, 0, 0, 0.4);" id="overlid">
		<i class="fa fa-spinner fa-spin"></i>
   </div>
   
   
         <!-- The Loader. Is shown when pjax happens -->
      <div class="loader-wrap" id="overl">
            <i class="fa fa-circle-o-notch fa-spin-fast"></i>
        </div>
        <!-- common libraries. required for every page
         <script src="${pageContext.request.contextPath}/resources/node_modules/jquery/dist/jquery.min.js"></script> 
      -->  <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-pjax/jquery.pjax.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/popper.js/dist/umd/popper.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/dist/js/bootstrap.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/js/dist/util.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/widgster/widgster.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" data-pace-options='{ "target": ".content-wrap", "ghostTime": 1000 }'></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/hammerjs/hammer.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-hammerjs/jquery.hammer.js"></script>


        <!-- common app js -->
        <script src="${pageContext.request.contextPath}/resources/js/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/app.js"></script>

        <!-- Page scripts -->
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.animator/jquery.flot.animator.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.selection.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.time.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.pie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.stack.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.crosshair.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.symbol.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.dashes/jquery.flot.dashes.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap_calendar/bootstrap_calendar/js/bootstrap_calendar.min.js"></script>
       <!-- bootstrap-daterangepicker -->	
       <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
         
<script>
function alphaNumericChecker() {

var gName = document.getElementById('siteName');
gName.value = gName.value.replace(/[^a-zA-Z0-9 ]+/, '');

var glat = document.getElementById('lat');
glat.value = glat.value.replace(/[^0-9. ]+/, '');

var glon = document.getElementById('lon');
glon.value = glon.value.replace(/[^0-9. ]+/, '');

}

</script>


</body>

</html>


