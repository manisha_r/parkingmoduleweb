package com.traffsys.parkingsolution.DAO;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.traffsys.parkingsolution.utils.AppConstant;
import com.traffsys.parkingsolution.utils.NetworkResponse;


  
 
@Component
public class RemoteDAO {
	//-----------------SEND POST WIHTOUT BODY----------//
    public static String sendPostRequest(String url) throws Exception {
        URL apiUrl = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) apiUrl.openConnection();
        // Set the request method to POST
        connection.setRequestMethod("POST");
        // Set request headers
        connection.setRequestProperty("Content-Type", "application/json"); // Set appropriate content type if needed
        // Add any other headers if needed
        // Get the response code
        int responseCode = connection.getResponseCode();
        System.out.println("Response Code: " + responseCode);
        // Read the response body
        StringBuilder response = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        }
        // Disconnect the connection
        connection.disconnect();
        // Return the response body
        return response.toString();
    }

	public static JSONObject doPost(final JSONObject mJsonObject, final String methodURL) {
		JSONObject apiResponse = null;
		HttpURLConnection urlConnection = null;
		URL url;
		InputStream inputStream;
		try {
			url = new URL(methodURL);
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod("POST");
			urlConnection.setConnectTimeout(10000);
//			urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			urlConnection.setRequestProperty("Content-Type", "application/json");
			PrintWriter pw = new PrintWriter(urlConnection.getOutputStream());
			pw.write(mJsonObject.toString());
			pw.flush();
			pw.close();
			int statusCode = urlConnection.getResponseCode();
			if (statusCode == 200) {
				inputStream = new BufferedInputStream(urlConnection.getInputStream());
				String response = readIncomingData(inputStream);
				if (response != null && !response.isEmpty()) {
					apiResponse = new JSONObject(response);
				} else {
					System.out.println("Response is null" + RemoteDAO.class.getSimpleName());
					apiResponse = getErrorJSON("Response is null", AppConstant.ERROR_STATUS_CODE);
				}
			} else {
				String statusMSG = NetworkResponse.networkResponse(statusCode);
				apiResponse = getErrorJSON(statusMSG, statusCode);
			}
		} catch (IOException exe) {
			apiResponse = null;
			apiResponse = getErrorJSON(exe.getMessage(), AppConstant.ERROR_STATUS_CODE);
			System.err.println("Execption = " + exe.getMessage());

		} catch (JSONException jsonExe) {
			apiResponse = getErrorJSON(jsonExe.getMessage(), AppConstant.ERROR_STATUS_CODE);
			System.out.println("Execption" + jsonExe.getMessage());

		} finally {
			if (urlConnection != null)
				urlConnection.disconnect();
		}
		return apiResponse;
	}

	private static String readIncomingData(InputStream _instream) {
		StringBuilder sb = null;
		try {
			sb = new StringBuilder();
			BufferedReader r = new BufferedReader(new InputStreamReader(_instream));
			for (String line = r.readLine(); line != null; line = r.readLine()) {
				sb.append(line);
			}
			_instream.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return sb.toString();
	}

	public static JSONObject getErrorJSON(String message, int statusCode) {
		JSONObject errorJSON = new JSONObject();
		try {
			errorJSON.put("status", statusCode);
			errorJSON.put("message", message);
		} catch (JSONException e) {
			System.out.println(e.getMessage()+ RemoteDAO.class.getSimpleName()+ AppConstant.ERROR+
					e.getStackTrace()[0].getLineNumber());
		}
		return errorJSON;
	}

	// testing
	public static String doPostBinary() {
		String response = null;
		JSONObject apiResponse = null;
		HttpURLConnection urlConnection = null;
		URL url;
		InputStream inputStream;
		String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.
		String CRLF = "\r\n";
		try {
			File f = new File("E:/a.png");
			FileInputStream fis = new FileInputStream(f);
			BufferedInputStream inp = new BufferedInputStream(fis);
			byte[] imageBytes = new byte[(int) f.length()];

			/*
			 * url = new URL("http://192.168.1.164:30001/getColor"); urlConnection =
			 * (HttpURLConnection) url.openConnection(); urlConnection.setDoOutput(true);
			 * urlConnection.setRequestMethod("POST");
			 * urlConnection.setConnectTimeout(10000);
			 * urlConnection.setRequestProperty("Content-Type",
			 * "multipart/form-data; boundary==44100"); PrintWriter pw = new
			 * PrintWriter(urlConnection.getOutputStream());
			 * pw.write(mJsonObject.toString()); pw.flush(); pw.close();
			 */

			URLConnection connection = new URL("http://192.168.1.161:30001/getColor").openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "binary; boundary=" + boundary);

			// connection.setRequestProperty("Accept-Charset", "UTF-8");
			connection.setRequestProperty("Accept", "*/*");
			OutputStream output = connection.getOutputStream();
			PrintWriter pw = new PrintWriter(new OutputStreamWriter(output, "UTF-8"), true);
			pw.append("--" + boundary).append(CRLF);
			// pw.append("Content-Disposition: form-data; name=\"file\"; filename=\"" +
			// f.getName() + "\"").append(CRLF);
			pw.append("Content-Type: " + URLConnection.guessContentTypeFromName(f.getName())).append(CRLF);
			pw.append("Content-Transfer-Encoding: binary").append(CRLF);
			pw.append(CRLF).flush();
			Files.copy(f.toPath(), output);
			output.flush(); // Important before continuing with writer!
			pw.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.

			// End of multipart/form-data.
			pw.append("--" + boundary + "--").append(CRLF).flush();
			int responseCode = ((HttpURLConnection) connection).getResponseCode();
			String responseMessage = ((HttpURLConnection) connection).getResponseMessage();
			System.err.println("Response = " + responseMessage + " code = " + responseCode);
			response = "Response = " + responseMessage + " code = " + responseCode;

			// int statusCode = urlConnection.getResponseCode();
			/*
			 * if (statusCode == 200) { inputStream = new
			 * BufferedInputStream(urlConnection.getInputStream()); String response =
			 * readIncomingData(inputStream); if(response != null && !response.isEmpty()) {
			 * apiResponse = new JSONObject(response); }else {
			 * Logger.getPathInstance().log("Response is null",
			 * RemoteDAO.class.getSimpleName(), AppConstant.INFO,0); apiResponse =
			 * getErrorJSON("Response is null", AppConstant.ERROR_STATUS_CODE); } } else {
			 * String statusMSG = NetworkResponse.networkResponse(statusCode); apiResponse =
			 * getErrorJSON(statusMSG, statusCode); }
			 */
		} catch (IOException exe) {
			response = exe.getMessage();
			apiResponse = null;
			apiResponse = getErrorJSON(exe.getMessage(), AppConstant.ERROR_STATUS_CODE);
			
			System.out.println("Execption" + exe.getMessage());

		} catch (Exception jsonExe) {
			response = jsonExe.getMessage();
			apiResponse = getErrorJSON(jsonExe.getMessage(), AppConstant.ERROR_STATUS_CODE);
			
			System.out.println("Execption" + jsonExe.getMessage());

		} finally {
			if (urlConnection != null)
				urlConnection.disconnect();
		}
		return response;
	}
}
