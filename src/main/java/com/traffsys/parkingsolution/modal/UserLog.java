package com.traffsys.parkingsolution.modal;

import java.sql.Timestamp;

/**
 * 
 * @author @$hok@
 * @Description : to save user's log into DB.
 */
public class UserLog {

	private int id;
	
	private long userId;
	
	private String username;
	
	private String ip;
		
	private Timestamp lastAccessTime;

	private String status;
	
	private String description;
	
	public int getId() {
		return id;
	}

	public UserLog setId(int id) {
		this.id = id;
		return this;
	}

	public long getUserId() {
		return userId;
	}

	public UserLog setUserId(long userId) {
		this.userId = userId;
		return this;
	}

	public String getUsername() {
		return username;
	}

	public UserLog setUsername(String username) {
		this.username = username;
		return this;
	}

	public String getIp() {
		return ip;
	}

	public UserLog setIp(String ip) {
		this.ip = ip;
		return this;
	}

	public Timestamp getLastAccessTime() {
		return lastAccessTime;
	}

	public UserLog setLastAccessTime(Timestamp lastAccessTime) {
		this.lastAccessTime = lastAccessTime;
		return this;
	}

	public String getStatus() {
		return status;
	}

	public UserLog setStatus(String status) {
		this.status = status;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public UserLog setDescription(String description) {
		this.description = description;
		return this;
	}
	
}
