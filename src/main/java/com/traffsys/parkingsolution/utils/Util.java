package com.traffsys.parkingsolution.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.traffsys.parkingsolution.components.Logger;
import com.traffsys.parkingsolution.modal.ResponseModel;

public class Util {
	public static final String SECURITY_KEY = "98766543210";
	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	public static final String AES_ECB_PKCS5_PADDING = "AES/ECB/PKCS5Padding";
	public static final String UTF_8 = "UTF-8";

	
	
	public static ResponseModel setResponse(final Integer status_code, final String response_message,
			final Object myObjectList) {
		ResponseModel response = new ResponseModel();
		response.setStatus(status_code);
		response.setMessage(response_message);
		response.setMyObjectList(myObjectList);
		return response;
	}

	public static String getCurrentDate(final String date_format) {
		DateFormat dateFormat = new SimpleDateFormat(date_format);
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public static java.sql.Timestamp getCurrentSQLTimeStamp() {
		java.util.Date date = new java.util.Date();
		java.sql.Timestamp timestamp = new java.sql.Timestamp(date.getTime());
		return timestamp;
	}

	public static Path initConfigurationRoot() {
		Path rootLocation = null;
		String s = System.getProperty("os.name");
		if (s.contains(AppConstant.WINDOW)) {
			rootLocation = Paths.get("./" + AppConstant.ROOT_DIRECTORY + "/" + AppConstant.CONFIG_DIRECTORY);
		} else if (s.equalsIgnoreCase(AppConstant.LINUX)) {
			rootLocation = Paths.get(
					AppConstant.CONFIG_DIR + "/" + AppConstant.ROOT_DIRECTORY + "/" + AppConstant.CONFIG_DIRECTORY);
		}
		return rootLocation;
	}
	public static JSONObject convertPOJOToJSON(final Object mModelObject) {
		ObjectMapper objectMapper = new ObjectMapper();
		JSONObject mJsonObject = null;
		String jsonObject;
		try {
			jsonObject = objectMapper.writeValueAsString(mModelObject);
			mJsonObject = new JSONObject(jsonObject);
		} catch (JSONException e) {
			Logger.getPathInstance().log(e.getMessage(), Util.class.getSimpleName(), AppConstant.ERROR,
					e.getStackTrace()[0].getLineNumber());
			e.printStackTrace();
		} catch (JsonProcessingException e1) {
			Logger.getPathInstance().log(e1.getMessage(), Util.class.getSimpleName(), AppConstant.ERROR,
					e1.getStackTrace()[0].getLineNumber());
			e1.printStackTrace();
		}
		return mJsonObject;
	}
	//-----------------SEND POST WIHTOUT BODY----------//
    public static String sendHTTPRequest(String url,String ReuestMethod) throws Exception {
        URL apiUrl = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) apiUrl.openConnection();
         // Set the request method
        connection.setRequestMethod(ReuestMethod);
         connection.setRequestProperty("Content-Type", "application/json"); // Set appropriate content type if needed
        // Add any other headers if needed
        // Read the response body
        StringBuilder response = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        }
        // Disconnect the connection
        connection.disconnect();
        // Return the response body
        return response.toString();
    }













}
