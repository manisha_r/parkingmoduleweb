package com.traffsys.parkingsolution.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.traffsys.parkingsolution.modal.RequestModel;
import com.traffsys.parkingsolution.modal.ResponseModel;
import com.traffsys.parkingsolution.utils.AppConstant;
import com.traffsys.parkingsolution.utils.Util;


@RestController
public class CheckSessionController {

 	@RequestMapping(value = "/doCheckSession", method = RequestMethod.POST)
	public ResponseModel doCheckUserSession(HttpServletRequest mHttpServletRequest, HttpServletResponse mHttpServletResponse) throws Exception {
		
 		ResponseModel mResponseModel = null;
  		
 		String reqURL = AppConstant.API_URL+"checkJWT/"+mHttpServletRequest.getSession().getAttribute("jwtToken")+'/'+mHttpServletRequest.getSession().getAttribute("user");
   		
 		
 	    System.out.println("Username from session: " + mHttpServletRequest.getSession().getAttribute("user"));

 		//mHttpServletRequest.getSession().getAttribute("uname")
		String responsefromapi = Util.sendHTTPRequest(reqURL,"POST");
 		
		JSONObject mjsonobj = new JSONObject(responsefromapi);
  		 
		mResponseModel = Util.setResponse(mjsonobj.getInt("status"), mjsonobj.getString("message"), null);
		
 		return mResponseModel;

	}
}
