<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html  ng-app="app">
<head>
<meta charset="ISO-8859-1">
 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <title>Dashboard | VPark</title>

         <link href="${pageContext.request.contextPath}/resources/css/application.min.css" rel="stylesheet">
         <link rel="shortcut icon" type="image/x-icon"  href="${pageContext.request.contextPath}/resources/img/logo.png">
         
         <!--angular 1.3.14  -->
	      <script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>
       
         <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
         <link href="${pageContext.request.contextPath}/resources/node_modules/line-awesome/dist/line-awesome/css/line-awesome.css" rel="stylesheet">
         <link href="${pageContext.request.contextPath}/resources/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
       
		  <!-------------------------------- Load Leaflet from CDN----------------------------------------------->
		  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
		    integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
		    crossorigin=""/>
		  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
		    integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
		    crossorigin=""></script>
		
		  <!------------------------------ Load Esri Leaflet from CDN -------------------------------------------->
		  <script src="https://unpkg.com/esri-leaflet@2.5.0/dist/esri-leaflet.js"
		    integrity="sha512-ucw7Grpc+iEQZa711gcjgMBnmd9qju1CICsRaryvX7HJklK0pGl/prxKvtHwpgm5ZHdvAil7YPxI1oWPOWK3UQ=="
		    crossorigin=""></script>
		
		  <!------------------------------- Load Esri Leaflet Geocoder from CDN ----------------------------------->
		  <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
		    integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
		    crossorigin="">
		  <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
		    integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
		    crossorigin=""></script>
             
             <!-------------------------------------------markercluster  ------------------------------------------>
	        <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.0.4/dist/MarkerCluster.Default.css">
			<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.0.4/dist/MarkerCluster.css">
			<script src="https://unpkg.com/leaflet.markercluster@1.0.4/dist/leaflet.markercluster.js"></script>
			<script src="${pageContext.request.contextPath}/resources/MovingMarker.js"></script>
			
			<!-- Load Clustered Feature Layer from CDN -->
	        <script src="https://unpkg.com/esri-leaflet-cluster@2.0.0/dist/esri-leaflet-cluster.js"></script>
		
          <!---------------------------------- API URL ------------------------------------------------------>
	      <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script> 
	 
	         <!----------------------------------------------Jquery------------------------------------------------------>
      
         <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>   
            <!-- Bootstrap v4.5.3 -->
 	      <script src="${pageContext.request.contextPath}/resources/bootstrap/bootstrap.min.js"></script>
	       
    	  <script src="${pageContext.request.contextPath}/resources/charts/chart.js"></script>
    	
          <script src="${pageContext.request.contextPath}/resources/fontawesome/fontawesome.js"></script>
    	
    	  <!---------------------------------- SweetAlert  ------------------------------------------------------>
          <script src="${pageContext.request.contextPath}/resources/sweetAlert.js"></script> 
	     
    	
<style type="text/css">
		    	
		.modal {
		    position: fixed;
		    top: 60px;
		    left: 0;
		    z-index: 1050;
		    display: none;
		    width: 100%;
		    height: 100%;
		    overflow: hidden;
		    outline: 0;
		}
		 .fontmodel {
		    margin: 0 0 10px;
		    padding-top: 20PX;
		    font-size: 13px;
		} 
		
		img {
		   vertical-align: middle;
		    margin-top: 50px;
		}
		
		.analytics .slots {
		    display: flex;
		    overflow: hidden;
		    justify-content: center;
		    min-width: calc(100% - 150px);
		}
		
		.leaflet-popup-content {
            margin: 13px 19px;
            width: 339px !important;
            line-height: 1.4;
        }
        
 
 hr {
    clear: both;
    visibility: hidden;
}
</style>
       
        <script>
        $(document).ready(function(){

             $("#khome").addClass("active");

     		 });
        
       ///////////////////////////////////SESSION///////////////////////////////////////
       var uid = '<%=session.getAttribute("uid")%>';
       var userN = '<%=session.getAttribute("user")%>';
       

       var options = {
     		   headers: {
     		     Authorization: 'Bearer ' + '${jwtToken}' // Assuming 'jwtToken' holds the JWT
     		   }
     		 };       
       var map;
       var markersLayer;
 	   var addsitedata=[];
       var markers= null;
	   var mapZoom;
		 
       var app = angular.module("app", []);
       app.controller("GetDataController", ['$scope','$http','$window', function(scope,http,window) {
       	
    	   /* Recent Register Vehicles DATE */
    	   scope.formatDate = function(cdate){
	   			return new Date(cdate)
 	   		}  
    	   
             
           
             /********************************GET PARKING***************************************/
             http.get(apiURL+'getParking/'+userN+'/doGetParkingInfo',options).
           		then(function (response) {
           			
           			if(response.data != null)
           			{
           			if(response.data.status==1){
           		              
           				scope.locationlist= response.data.myObjectList;
           			}else if(response.data.status==21){
   		  				 
	  		         }
           			else{
           	        	
           				scope.locationlist =[];
           	             }  
           	   }else{
           		    scope.locationlist =[];
           	      }      		       		
           		}, function (response) {
				});
             
             scope.sitearray = [];
             scope.amountarray = [];
             scope.allSite=[];
            //---------------------ALL SITE REVENUE---------------------------// 
             http.get(apiURL+'getSiteCharges/'+userN+'/doGetAllSiteRevenue',options).
        		then(function (response) {
        			
        			if(response.data != null)
        			{
        			if(response.data.status==1){
        		              
        				scope.allSite= response.response.data.myObjectList;
        				for(var i=0;i<scope.allSite.length;i++)
        					{
         					scope.sitearray.push(scope.allSite[i].sitename);
         					scope.amountarray.push(scope.allSite[i].total_amount);
         					}
        			    	$('#messagechart').show();
        			    	$('#message').hide();
        			    	
        				  scope.loadDonutData(scope.sitearray, scope.amountarray); //calling of function to load revenue response.response.data
        				 
        			}else if(response.data.status==21){
   		  				 
	  		        }
        			else{
        				scope.allSite="";
        				$('#messagechart').hide();
    			    	$('#message').show();
        	        }  
        	   }else{
        		   scope.allSite="";
        	      }      		       		
        		}, function (response) {
				});
            
            
            //-----------------GET OCCUPANCY DATA--------------------------------//
            scope.siteList =[];
            scope.occupancypercentage =[];
            scope.occupancydata = [];
            
           	http.get(apiURL+'getOccupancyAllSite/'+userN+'/doGetOccupancyAllSites',options).
        		then(function (response) {
        			
        			if(response.data != null)
        			{
        			if(response.data.status==1){
        		              
        				scope.occupancydata= response.data.myObjectList;
         				for(var i=0;i<scope.occupancydata.length;i++)
        					{
         					if(scope.occupancydata[i].occupancy>0){
          						scope.siteList.push(scope.occupancydata[i].siteName);
             					scope.occupancypercentage.push(scope.occupancydata[i].occupancy);
          					}
         					}
         				
         				  scope.loadOccupancyData(scope.siteList, scope.occupancypercentage);
        				 
        			}else if(response.data.status==21){
   		  				 
	  		         }
        			else{
        				scope.occupancydata="";
        	        }  
        	   }else{
        		   scope.occupancydata="";
        	      }      		       		
        		}, function (response) {
				});
           
           	
           	/********************************GET COUNT OF PARKING REGISTER***************************************/
           	http.get(apiURL+'getParkCount/'+userN+'/doGetParkCount',options).
			then(function (response) {
				if(response.data != null)
				{
				if(response.data.status==1){
					  scope.parkingcount=response.data.myObjectList;
					  
				 
                 }else if(response.data.status==21){
		  				 
  		         }else{
		        	  }  
		       }else{
		     	
		   	
		       }      		       		
			}, function (response) {
			});
           	
             scope.citylat= 0;
        	 scope.citylon= 0;
            
            /********************************GET LOCATION***************************************/
               http.get(apiURL+'getlocation/'+userN+'/doGetLocation',options).
    			then(function (response) {
    				
    				if(response.data != null)
    				{
    				if(response.data.status==1){
    					scope.locationData= response.data.myObjectList;
      					scope.citylat=scope.locationData[0].lat;
      				    scope.citylon=scope.locationData[0].lon;
     				    
    				   // renderMarker(scope.citylat, scope.citylon); //default view as per location
     				    
     					Pace.stop();
     		         }else if(response.data.status==21){
   		  				 
	  		         }else{
      		        	 //renderMarker(30.3255579, 77.9469222);
      		         }  
    		   }else{
    		     	
    		      }      		       		
    			}, function (response) {
				});
           	
 
    	  /********************Site Info*********************************/
    	  scope.locationdata=[]; 
     	 scope.sitedata=[];
     	 scope.GetParking = function () { 
     			  
     		 http.get(apiURL+'getSite/'+userN+'/doGetSiteInfo',options).
     				then(function (response) {
     					 
     					if(response.data != null)
     					{
     					if(response.data.status==1){
     						 scope.sitedata= response.data.myObjectList;
     						 for(var i=0; i<scope.sitedata.length;i++)
       							  {
       							 
       					    	  scope.siteStatus = scope.sitedata[i].siteStatus

       							  }
       						 
           			      		   scope.arraydata= addsitedata;
             		               

       			      		  
       			      		 $(function() {
      			    		    Pace.on("done", function(){
      			    		   });
      			    		});
      			     	   document.getElementById('overl').style.visibility = "hidden";
     	                 }else if(response.data.status==21){
        		  				 
    	  		         }else{
     	                	 
     	                	 $(function() {
       			    		    Pace.on("done", function(){
       			    		   });
       			    		});
       			     	   document.getElementById('overl').style.visibility = "hidden";
       			     	   
     			        	  }  
     			       }else{
     			     	
     			    	   
     			    	   $(function() {
     			    		    Pace.on("done", function(){
     			    		   });
     			    		});
     			     	   document.getElementById('overl').style.visibility = "hidden";
     			   	
     			       }      		       		
     				}, function (response) {
					});
      	        	
     	 };
    	 
    	 

         /*******************************GET MAP SLOT INFO***************************************/
    		
    	
    	 scope.soltinfo=[];
    	 http.get(apiURL+'getSlotDetails/'+userN+'/doGetSlotInfo',options).
				    				then(function (response) {
    					 
    					if(response.data != null)
    					{
    					if(response.data.status==1){
    						 scope.soltinfo= response.data.myObjectList;
    				 
                       addMarker(scope.soltinfo); 	
     			     	   document.getElementById('overl').style.visibility = "hidden";
    	                 }else if(response.data.status==21){
    	   		  				 
    	  		         }else{
    	                	 
    	                   scope.soltinfo= "";
      			     	   document.getElementById('overl').style.visibility = "hidden";
      			     	   
    			        	  }  
    			       }else{
    			     	
    			    	   
    			    	   scope.soltinfo= "";
    			     	   document.getElementById('overl').style.visibility = "hidden";
    			   	
    			       }      		       		
				    				}, function (response) {
									}); 
     	        	
    	
    	 
    	 
    	 //--------------TOTAL CLICK-------------//
    	 
    	 scope.totalClick = function(){
    		 
    		 window.location.href = '${pageContext.servletContext.contextPath}/getVehicle';
    		 

     	 };
    	 
    	 
    	 
   
    	 
	 /* MAP JS */
		    
	 
	 
	/*********************DEFAULT MAP********************************/
  	 var center;
	 var markersArray =[];
	 
 	//  function renderMarker(lat, lon)
		//{
       	map = L.map('map').setView([scope.citylat, scope.citylon],12);
   		var markerClusters = L.markerClusterGroup();
 	    markersLayer = new L.LayerGroup(); 
		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
			 attribution: 'Map',
			 maxZoom: 18
		}).addTo(map);		
 		
		var arcgisOnline = L.esri.Geocoding.arcgisOnlineProvider();
 		var searchControl = L.esri.Geocoding.geosearch({
		providers: [
		  arcgisOnline,
		  L.esri.Geocoding.mapServiceProvider({
		    label: 'States and Counties',
		    url: 'https://sampleserver6.arcgisonline.com/arcgis/rest/services/Census/MapServer',
		    layers: [2, 3],
		    searchFields: ['NAME', 'STATE_NAME']
		  })
		]
		}).addTo(map); 
  		 
  	   mapZoom = L.featureGroup().addTo(map);
  		  
   	//}
    	   
  	//-------------------------------------------------BY API CALL------------------------------------------//
  	  	scope.carlen=0;
  	  	scope.buslen=0;
  	  	scope.twowheelerlen=0;
  	  	scope.availableCar=0;
  	  	scope.availableBus=0;
  	  	scope.availableBike=0;
  	  	
  	    //GET SITE DATA ON MARKER  	                      
  	  	function addMarker(addsitedata) //dynamic site list
  		{  
  			var parkinglocation;
  			 var contentpop= '';
  			
  			 var lat=0;
  			 var lng=0;
  		  
  			 mapZoom = new L.featureGroup().addTo(map); //to get bounds
  			 
  					 var sensorIcon = L.icon({
  					    iconUrl: '${pageContext.request.contextPath}/resources/img/offstreetparking_icon.png',
  					    iconSize:     [40, 50], // size of the icon			   
  					    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
  					    shadowAnchor: [4, 62],  // the same for the shadow
  					    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
  					});
  					 
  					 var motionsensorIcon = L.icon({
  			  			 iconUrl: '${pageContext.request.contextPath}/resources/img/onstreetparking_icon.png',
  			  			  iconSize:     [40,50], // size of the icon			   
  						    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
  						    shadowAnchor: [4, 62],  // the same for the shadow
  						    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
  						 });
  					 
  					 
  					 var parkingfullicon = L.icon({
  			  			 iconUrl: '${pageContext.request.contextPath}/resources/img/parkingfull_icon.png',
  			  			  iconSize:     [40,50], // size of the icon			   
  						    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
  						    shadowAnchor: [4, 62],  // the same for the shadow
  						    popupAnchor:  [-3, -76] // point from which the popup should open relative3 to the iconAnchor
  						 });
  					 
  					popup = L.popup({
  		        		  pane: 'fixed',
  		        		  className: 'popup-fixed',
  		          		  autoPan: true,
  		          		  keepInView: true,
  		             	  closeButton: true 
  		         		});
  					
  		    	       //  var contentpop='<div class="row"  ><h4  class="pull-left" style="padding-left: 7px;padding-right: 7px;color:#73879C;"><strong>'+siteName+'</strong></h4></div><div class="row"><div class="col-sm-4"><b>VEHICLE TYPES</b><div class="d-flex  align-items-stretch"><div class="p-2 bd-highlight">Flex item 1</div><div class="p-2 bd-highlight">Flex item 2</div><div class="p-2 bd-highlight">Flex item 3</div></div></div><div class="col-sm-4"><b>OCCUPIED</b><div class="d-flex  align-items-stretch"><div class="p-2 bd-highlight">Flex item 1</div><div class="p-2 bd-highlight">Flex item 2</div><div class="p-2 bd-highlight">Flex item 3</div></div></div><div class="col-sm-4"><b>AVAILABILITY</b><div class="d-flex  align-items-stretch"><div class="p-2 bd-highlight">Flex item 1</div><div class="p-2 bd-highlight">Flex item 2</div><div class="p-2 bd-highlight">Flex item 3</div></div></div></div></div>'
  		    	     
  	  			
  		    	    
  		    for(var a=0;a<addsitedata.length;a++)
  			 { 
  		    	parkinglocation = addsitedata[a].mSiteModel;
  		    	
  	 	    	 if(parkinglocation.type=="offstreet")
  		    		{
  	 	    	    lat = addsitedata[a].mSiteModel.latitude;
  				    lng = addsitedata[a].mSiteModel.longitude;
  				   markers= L.marker([lat, lng], {icon: sensorIcon}).addTo(mapZoom);	
  			       // map.setView(mapZoom.getBounds().getCenter());
  		    	    
  		    		
  		    		} 
  	 	    
  		    	if(parkinglocation.type=="onstreet")
  	    		{ 
  		    	    lat = addsitedata[a].mSiteModel.latitude;
  				    lng = addsitedata[a].mSiteModel.longitude;
  		    	   // var contentpop='<div class="row"  ><h5  class="pull-left" style="padding-left: 7px;padding-right: 7px;color:#73879C;"><strong>'+parkinglocation.siteName+'</strong></h5></div><div class="row"></div></div>'
  				   // var contentpop='<div class="row"  ><h4  class="pull-left" style="padding-left: 7px;padding-right: 7px;color:#73879C;"><strong>'+parkinglocation.siteName+'</strong></h4></div><div class="row"><div class="col-sm-4"><b>VEHICLE TYPES</b><div class="d-flex  align-items-stretch"><div class="p-2 bd-highlight">Flex item 1</div><div class="p-2 bd-highlight">Flex item 2</div><div class="p-2 bd-highlight">Flex item 3</div></div></div><div class="col-sm-4"><b>OCCUPIED</b><div class="d-flex  align-items-stretch"><div class="p-2 bd-highlight">Flex item 1</div><div class="p-2 bd-highlight">Flex item 2</div><div class="p-2 bd-highlight">Flex item 3</div></div></div><div class="col-sm-4"><b>AVAILABILITY</b><div class="d-flex  align-items-stretch"><div class="p-2 bd-highlight">'+scope.circleBike+'</div><div class="p-2 bd-highlight">'+scope.circleCar+'</div><div class="p-2 bd-highlight">'+scope.circleBus+'</div></div></div></div></div>'
  		    	      
  				    markers= L.marker([lat, lng], {icon: motionsensorIcon}).addTo(mapZoom);	
  					
  	    		}
  		    	
  		    	/*****************to add marker for slot is full **********************/
  		    	 if(parkinglocation.availableSlots==0)
  		    		{
  		  	 	    
  				    // var contentpop='<div class="row"  ><h4  class="pull-left" style="padding-left: 7px;padding-right: 7px;color:#73879C;"><strong>'+parkinglocation.siteName+'</strong></h4></div><div class="row"></div></div>'
  			        // var contentpop='<div class="row"  ><h4  class="pull-left" style="padding-left: 7px;padding-right: 7px;color:#73879C;"><strong>'+parkinglocation.siteName+'</strong></h4></div><div class="row"><div class="col-sm-4"><b>VEHICLE TYPES</b><div class="d-flex  align-items-stretch"><div class="p-2 bd-highlight">Flex item 1</div><div class="p-2 bd-highlight">Flex item 2</div><div class="p-2 bd-highlight">Flex item 3</div></div></div><div class="col-sm-4"><b>OCCUPIED</b><div class="d-flex  align-items-stretch"><div class="p-2 bd-highlight">Flex item 1</div><div class="p-2 bd-highlight">Flex item 2</div><div class="p-2 bd-highlight">Flex item 3</div></div></div><div class="col-sm-4"><b>AVAILABILITY</b><div class="d-flex  align-items-stretch"><div class="p-2 bd-highlight">'+scope.circleBike+'</div><div class="p-2 bd-highlight">'+scope.circleCar+'</div><div class="p-2 bd-highlight">'+scope.circleBus+'</div></div></div></div></div>'
  		    	    markers= L.marker([lat, lng], {icon: parkingfullicon}).addTo(mapZoom);	
  	 	    		} 
  		    	
  		    	
  		    	
   		   	for(var k=0; k<addsitedata.length; k++)
  	        {
  	    		 if(addsitedata[k].mSiteModel.sid == addsitedata[a].mSiteModel.sid)
  	    			{
  	    			 if(addsitedata[k].mVehicleClassModel.className=="CAR")
  	    			 {
  	    				scope.carlen= addsitedata[k].occupiedSlot;
  	    				scope.availableCar=addsitedata[k].availableVehicleSlot;
  	    		 	 } 
  	    		     else if(addsitedata[k].mVehicleClassModel.className=="BUS")
  	    			 {
  	    			 scope.buslen=addsitedata[k].occupiedSlot;
  	    			 scope.availableBus=addsitedata[k].availableVehicleSlot;
  	    			 }
  	    		    else if(addsitedata[k].mVehicleClassModel.className=="TWO WHEELER")
    			         {
    			         scope.twowheelerlen=addsitedata[k].occupiedSlot;
    			         scope.availableBike=addsitedata[k].availableVehicleSlot;
  	    			 } 
  	    		     else{
  	    				scope.twowheelerlen=0;
  	    				scope.buslen=0;
  	    				scope.carlen=0;
  	    			 	scope.availableCar=0;
  	    			  	scope.availableBus=0;
  	    			  	scope.availableBike=0;
  	    			 }  
  	    			}
  	    		}
  	    	
  	    	scope.status="";
  	    	//STATUS CHECKING
  	    	if(addsitedata[a].mSiteModel.siteStatus==1){
  	    		scope.status='<span class="badge badge-success">Open</span>';
  	    	}
  	    	else if(addsitedata[a].mSiteModel.siteSatus==0){
  	    		scope.status='<span class="badge badge-warning">Closed</span>';
  	    	}
  	    	else{
  	    		scope.status="";
  	    	}
  	    	
  	    	//----------setting availability icon---------------------//
  	 
		    	 	if(scope.availableCar>0)
  	    		{
  	    		 scope.circleCar='<img src="${pageContext.request.contextPath}/resources/img/available.png" style="height: 13px;width: 13px;  margin-top: 0px;" alt="..."> '+ scope.availableCar;
	 	    	    	
  	    		}
  	    	else
  	    		{
  	    		 scope.circleCar='<img src="${pageContext.request.contextPath}/resources/img/unavailable.png" style="height: 13px;width: 13px;  margin-top: 0px;" alt="..."> '+ scope.availableCar;
	 	    	    	
  	    		}
  	    	if(scope.availableBus>0)
  	    		{
  	    		  scope.circleBus='<img src="${pageContext.request.contextPath}/resources/img/available.png" style="height: 13px;width: 13px;  margin-top: 0px;" alt="..."> '+ scope.availableBus;
	  	    	    	
  	    		}
  	    	else
  	    		{
  	    		  scope.circleBus='<img src="${pageContext.request.contextPath}/resources/img/unavailable.png" style="height: 13px;width: 13px;  margin-top: 0px;" alt="..."> '+ scope.availableBus;
	  	    	    	
  	    		}
  	    	if(scope.availableBike>0)
  	    		{
  	    		  scope.circleBike='<img src="${pageContext.request.contextPath}/resources/img/available.png" style="height: 13px;width: 13px;  margin-top: 0px;" alt="..."> '+ scope.availableBike;
  	  			  
  	    		}
  	    	else
  	    		{
  	    		  scope.circleBike='<img src="${pageContext.request.contextPath}/resources/img/available.png" style="height: 13px;width: 13px;  margin-top: 0px;" alt="..."> '+ scope.availableBike;
  	  			  
  	    		}

   		    	
  		    	  var contentpop='<div class="row"  ><h4  class="pull-left" style="padding-left: 7px;padding-right: 7px;color:#73879C;"><strong>'+addsitedata[a].mSiteModel.siteName+'</strong></h4></div><div class="row" ><div class="col-sm-4" style="padding-left: 10px;"><b>TYPES</b><div class="  align-items-stretch"><div class="p-2 bd-highlight" ng-if="addsitedata[a].mVehicleClassModel.className=="TWO WHEELER"" ><i class="fa fa-motorcycle text-primary" style="font-size:15px;"></i></div><div class="p-2 bd-highlight" ng-if="addsitedata[a].mVehicleClassModel.className=="CAR""><i class="fa fa-car text-primary" style="font-size:15px;"></i></div><div class="p-2 bd-highlight"ng-if="addsitedata[a].mVehicleClassModel.className=="BUS"" ><i class="fa fa-bus text-primary" style="font-size:15px;"></i></div></div></div><div class="col-sm-4"><b>OCCUPIED</b><div class="  align-items-stretch"><div class="p-2 bd-highlight" ng-if="addsitedata[a].mVehicleClassModel.className=="TWO WHEELER"" >'+scope.twowheelerlen+'</div><div class="p-2 bd-highlight" ng-if="addsitedata[a].mVehicleClassModel.className=="CAR"">'+scope.carlen+'</div><div class="p-2 bd-highlight" ng-if="addsitedata[a].mVehicleClassModel.className=="BUS"">'+scope.buslen+'</div></div></div><div class="col-sm-4"><b>AVAILABILITY</b><div class="  align-items-stretch"><div class="p-2 bd-highlight" ng-if="addsitedata[a].mVehicleClassModel.className=="TWO WHEELER"">'+scope.circleBike+'</div><div class="p-2 bd-highlight" ng-if="addsitedata[a].mVehicleClassModel.className=="CAR"">'+scope.circleCar+'</div><div class="p-2 bd-highlight" ng-if="addsitedata[a].mVehicleClassModel.className=="BUS"">'+scope.circleBus+'</div></div></div></div></div>'
   		    	  markers.bindPopup(contentpop, {
  		    	    maxWidth : 260
  		    	});
  		    	  markers.myID = a;
  		    	  
  	              /* markers.on('click', function(e) {
  		    	      this.openPopup();
  		           }); */
  		           
  		    	 markers.on('mouseover', function(e) {
  		              this.openPopup();
  		     	    	    	
  		              });
  		              
  		              markers.on('mouseout', function (e) {
  		                  this.closePopup();
  		              });
  	         
  		    	 
  			 } 
 	  		 map.setView(mapZoom.getBounds().getCenter());
 	  		 map.fitBounds(mapZoom.getBounds());
  	  		 
	}
  	
  	
  	
  	/************************************SITE STATUS**********************************/
  	 var siteIndex=0;
   	 scope.sitedata=[];
   	 
   	 //-----------RENDER FIRST TIME WITHIN 1 SECOND---------------//
   	 
   	 setTimeout(function(){
   		
   		scope.sitelocation=[];

  		 if(scope.sitedata.length>0)
         {
  		    scope.sitelocation=[];
  		    
             if(siteIndex==scope.sitedata.length)
  		       {
  		        siteIndex=0;
  		       }else{
  		
  			     scope.sitelocation.push(scope.sitedata[siteIndex]);
  			     siteIndex++;
  			     scope.$apply();
   			  }
       }
   		 
   	 },500); 
   	 
   	 
//------------REFRESH EVERY 5 SECOND---------------//

   	 setInterval(function() {
   		 scope.sitelocation=[];

   		 if(scope.sitedata.length>0)
          {
   		    scope.sitelocation=[];
   		    
              if(siteIndex==scope.sitedata.length)
   		       {
   		        siteIndex=0;
   		       }else{
   		
   			     scope.sitelocation.push(scope.sitedata[siteIndex]);
   			     siteIndex++;
   			     scope.$apply();
    			  }
        }
    }, 6000);
    
    
   
   	
   	//-----------calling doughnut chart-------------//
   	
   	scope.loadDonutData = function(sitearray, amountarray){
   		
   			var data = {
   		   		    datasets: [{
   		   		        data: amountarray,
   		                backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
    		   		  }],
    		   		labels: sitearray
   		   		};
   			
   		
   	  var ctx = document.getElementById('revenuechart').getContext('2d');
     	var chart = new Chart(ctx, {
      	    type: 'doughnut',
     	    tooltipFillColor: "rgba(51, 51, 51, 0.55)",
      	    data:data, 
      	     options: {
     	     responsive: true,
      		 maintainAspectRatio : false,
   			   legend: {
				  //display: true,
 			      position: 'bottom',
 			      labels: {
		                boxWidth: 10,
		                fontSize: 11,
		                usePointStyle: true,
 		                }
			   },
			   
			   tooltips: {
		            callbacks: {
		                label: function(tooltipItems, data) {
		                    return data.labels[tooltipItems.index] + 
		                    " : " + 
 		                    ' Rs. '+data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index] ;
		                }
		            }
		        } //end tooltip
			   
			}
     	});
     	 
   	}; 
   	
   	
   	//-------------------LOAD OCCUPANCY DATA------------------------//
   	
   	scope.loadOccupancyData = function(sitename, occupancy)
		{
   		
   		var testdata = {
	   		    datasets: [{
	   		    	data: occupancy,
	                backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]

	   		    }],
	   		    
  	   		    labels: sitename
	   		};
 	 
	 	var occupancychart = document.getElementById('occupancychart').getContext('2d');
	 	
	 	var loadchart = new Chart(occupancychart, {
	 	    // The type of chart we want to create
	 	    type: 'doughnut',
	 	    tooltipFillColor: "rgba(51, 51, 51, 0.55)",
	 	    data:testdata, 
	  	    options: {
	  	    responsive: true,
 	  		maintainAspectRatio : false,
 	  		legends: false,
  	  		     legend: {
				  //display: true,
				   position: 'bottom',
 			      labels: {
		                boxWidth: 10,
		                fontSize: 11,
		                usePointStyle: true,
 		                }
			   }, //end label
			   
			   tooltips: {
		            callbacks: {
		                label: function(tooltipItems, data) {
		                    return data.labels[tooltipItems.index] + 
		                    " : " + 
		                    data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index] +
		                    ' %';
		                }
		            }
		        } //end tooltip
			   
			   
			} //end options
	 	}); 
 	
		
   		
   		}   	
   	
   	
 
		
       }]);
       
       
       </script>
     
</head> 
 <body class="" ng-controller="GetDataController" ng-init="GetParking()">
 
  <%@ include file="Sidebar.jsp"%>	
    
  
   <div class="content-wrap"  >
     <!-- main page content. the place to put widgets in. usually consists of .row > .col-lg-* > .widget.  -->
     <!--  <main id="content" class="content" role="main">  -->
     <main id="content" class="content" role="main" class="ng-cloak">
     <div class="row">
     <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
      <div id="map"  style="width: 100%; height: 300px;"></div>
      </div>
      </div>
      <hr>  
       
             
                <div class="row">
                       <!-----------------TOTAL Occupancy------------------------>
                       <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12">
                       <div class="pb-xlg h-100">
                              <section class="widget h-100">
                                <header>
                                    <h5> Occupancy </h5>
                                </header>
                                <div class="widget-body">
                                     <div class="row">
                                     <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                      <div style="height: 300px">
                                       <canvas id="occupancychart"></canvas>
                                        </div>
                                      </div>    
                                         
                                    </div>
                                    
                                    <!-- NO RECORD FOUND -->
                                   <div class="row" ng-if="siteList.length==0">
                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                            <div class="text-center" style="padding: 100px 0;"><small class="text-danger">No record Found !</small></div> 
                                         </div>    
                                     </div>  
                                    
                                    
                                </div>
                           </section>
                            </div>
                     </div>   
                    
                    <!---------------------REVENUE------------------------------>
                    <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12">
                    <div class="pb-xlg h-100">
                              <section class="widget h-100">
                                <header>
                                  <h5> Site Revenue</h5>
                                 </header>
                                <div class="widget-body">
                                    <div class="row" id="messagechart">
                                         <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12">
                                        <div style="height: 300px">
                                          <canvas id="revenuechart"></canvas>
                                               <div ng-if="allSite == null" class="text-center" style="padding: 100px 0;"><small class="text-danger">No record Found !</small></div> 
                                		 </div>
                                        </div>   
                                     </div>
                                     
                                      <!-- NO RECORD FOUND -->
                                      <div class="row" id="message" >
                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                            <div class="text-center" style="padding: 100px 0;"><small class="text-danger">No record Found !</small></div> 
                                         </div>    
                                     </div> 
                                    
                                     
                                </div>
                            </section>
                             </div>
                     </div>
                    <!--------------------------------SITE STATUS--------------------------------------->
                      <div class="col-sm-3 col-lg-3 col-md-3 col-xs-12">
                      <div class="pb-xlg h-100">
                              <section class="widget h-100">
                                <header>
                                <div class="row">
                                <div class="col-sm-6 col-md-6 col-xs-12">
                                <h5> Site Info</h5>
                                 </div>
                                
                                </div>
                                 </header>
                                  <div class="widget-body" ng-repeat="i in sitelocation">
                                    <!-- site name --> 
                                   <div class="row">
                                      <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                        <p><i class="fi flaticon-placeholder text-info"></i> <b class="text-info"> {{i.siteName}}</b></p>
                                       </div>
                                   </div>     
                                     
                                     <!--total slots  -->
                                        
                                       <div class="row">
                                       <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                       <p>Total Slots</p>
                                       </div>
                                       
                                       <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                       <p class="pull-right"><small><span class="badge badge-info">{{i.slots}}</span></small></p>
                                       </div>
                                       </div>
                                       
                                       <hr>
                                       
                                     <!--occupied slots  -->
                                    
                                    <div class="row">
                                       <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                                       <p>Occupied Slots</p>
                                       </div>
                                       
                                       <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                                       <p class="pull-right"><small><span class="badge badge-success">{{i.slots-i.availableSlots}}</span></small></p>
                                       </div>
                                       </div>
                                       
                                        <hr>
                                       
                                       <!--available slots  --> 
                                       
                                    <div class="row">
                                       <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                                       <p>Available Slots</p>
                                       </div>
                                       
                                       <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12">
                                       <p class="pull-right"><small><span class="badge badge-danger">{{i.availableSlots}}</span></small></p>
                                       </div>
                                       </div>
                                       
                                        <hr>
                                       
                                        
                                </div>
                                
                                 <!-- NO RECORD FOUND -->
                                     <div class="row" ng-if="sitedata.length==0">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                            <div class="text-center" style="padding: 120px 0;"><small class="text-danger">No Record Found !</small></div> 
                                         </div>    
                                     </div>
                             </section>
                          </div>
                    </div>
                    
                    <!------------------------REGISTER VEHICLES------------------------------------------->
                      <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12">
                       <div class="pb-xlg h-100">
                              <section class="widget h-100">
                                <header>
                                    <h5>Recent Registered</h5>
                                 </header>
                                
                                <div class="widget-body" ng-repeat="i in locationlist | orderBy:'-inTime' | limitTo : 3  ">
                                   <div class="row">
                                  
                                      <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                      
                                       <p><small><span ng-if="i.mVehicleClass.className=='CAR'"><i class="fa fa-car text-info"></i></span> <span ng-if="i.mVehicleClass.className=='TWO WHEELER'"><i class="fa fa-motorcycle text-info"></i></span> <span ng-if="i.mVehicleClass.className=='BUS'"><i class="fa fa-bus text-info"></i></span> {{i.vehicleNumber}}</small> <small class="pull-right"><i class="fi flaticon-placeholder text-info"></i> {{i.mSiteModel.siteName }}</small></p>
                                     
                                       </div>
                                       
                                        
                                       <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                      
                                       <small><i class="fi flaticon-clock-1 text-info"></i> {{formatDate(i.inTime) | date:'dd MMM,  hh:mm a'}}</small> <span ng-if=" i.status==0 && i.userType==10 || i.status==0 && i.userType==11"><small class="pull-right text-danger">{{formatDate(i.outTime) | date:'dd MMM,  hh:mm a'}} </small></span><span ng-if="i.status==1 && i.userType==10 || i.status==1 && i.userType==11"><small class="pull-right"><span class="badge badge-success">Parked</span></small></span>
                                     
                                       
                                       </div>
                                      
                                 </div>
                                 
                                  <hr> 
                                  
                               </div>
                                
                                <!-- NO RECORD FOUND -->
                                     <div class="row" ng-if="locationlist.length ==0">
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                            <div class="text-center" style="padding: 120px 0;"><small class="text-danger">No Record Found !</small></div> 
                                         </div>    
                                     </div>
                                
                            </section>
                            </div>
                      </div>
                    
                    </div>
                
                 
            </main>  <!-- main -->   
                
            </div> <!--  div content wrap-->

                  
    <!--------------------------- VIEW LIVE MODAL ---------------------------------->
    <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content"  style=" padding-left: 17px;padding-right: 17px;">
                        <div class="modal-header"  style="border-bottom: none;">
                             <h5 class="modal-title"> Parking Status</h5>
                         </div>
                         <div class="modal-body"style=" padding-left: 17px;padding-right: 17px;" id="datamodel">
						  </div>
					      <div class="modal-footer"  style="border-top: none;">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" id="submitbtn" data-dismiss="modal">Open Parking Space</button>
                          </div> 
                </div>
           </div>
   </div><!-- END MODAL -->
   
 <!--    </main> -->
<!-- </div> -->

        <!-- The Loader. Is shown when pjax happens -->
         <div class="loader-wrap" id="overl">
            <i class="fa fa-circle-o-notch fa-spin-fast"></i>
        </div> 

        <!-- common libraries. required for every page-->
      
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery/dist/jquery.min.js"></script>
      
      
<%--         <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-pjax/jquery.pjax.js"></script>
 --%>       
 
        <script src="${pageContext.request.contextPath}/resources/node_modules/popper.js/dist/umd/popper.js"></script>
         <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/dist/js/bootstrap.js"></script>
       <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/js/dist/util.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/widgster/widgster.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" ></script>
       <script src="${pageContext.request.contextPath}/resources/node_modules/hammerjs/hammer.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-hammerjs/jquery.hammer.js"></script>
     <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" data-pace-options='{ "target": ".content-wrap", "ghostTime": 1000 }'></script>
     

        <!-- common app js -->
        <script src="${pageContext.request.contextPath}/resources/js/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/app.js"></script>
       
        <!-- Page scripts -->
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.animator/jquery.flot.animator.min.js"></script>
         <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap_calendar/bootstrap_calendar/js/bootstrap_calendar.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script type="text/javascript">
         
       
      
         </script>
         
    </body>

</html>

