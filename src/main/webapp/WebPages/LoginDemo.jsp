<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="com.traffsys.parkingsolution.utils.AppConstant"%>
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
    
   
<!DOCTYPE html>
<html ng-app="app">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/logindemo/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/css/util.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/css/main.css">
 <!---------------------------------- API URL ------------------------------------------------------>
    <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script>
          

<!--===============================================================================================-->

 <link rel="stylesheet" type="text/css"
	href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
		
	<script src="${pageContext.request.contextPath}/resources/aes.js"></script>
   	<script src="${pageContext.request.contextPath}/resources/secureLogin.js"></script>
   	   	<script src="${pageContext.request.contextPath}/resources/pbkdf2.js"></script>
  
	
 <script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>
 
 <style>
  .overlay {
	position: absolute;
	top: 0;
	left: 0;
	height: inherit;
	width: 100%;
	background-color: rgba(0, 0, 0, 0.8);
	z-index: 10;
}

#ico {
	font-size: 56px;
	color: white;
	position: fixed;
	top: 50%;
	left: 50%;
	width: 300px;
	line-height: 200px;
	height: 200px;
	margin-left: -150px;
	margin-top: -100px;
	background-color: rgba(0, 0, 0, 0.5);
	text-align: center;
	z-index: 10;
	outline: 9999px solid rgba(0, 0, 0, 0.5);
} 
.errormsg
{
    /* font-family: Raleway-Black; */
    font-family: inherit;
    font-size: 16px;
    color: red;
    line-height: 1.2;
    padding-bottom: 5px;
    /* text-transform: uppercase; */
    text-align: center;
    width: 100%;
    display: block;
    }
    #captchaBox
    {
    display: none;
    }
</style>
 
<script>
var captchaEnable='${captchaEnable}';

var _key= "1234567891234567";

	var app = angular.module("app", []);
	app.controller("postDataController", [ '$scope', '$http', '$window',
			function(scope, http, window) {
		
		 document.getElementById('overl').style.visibility = "hidden";
		 
		 
		 
			if(captchaEnable=='true' )
    		{
    		 
    		 document.getElementById('captchaBox').style.display = 'block';
    		 
    		}else
    			{
    	 
    			 document.getElementById('captchaBox').style.display = 'none';
    		 
    	    }
		 
		 
		 
		 
		 
		 var pushLogin=false;

				scope.submitForm = function() {
					var username = $("#username").val();
					var password = $("#inputPassword").val();


		 			/*Aes En*/
					 var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
			          var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);

			          var aesUtil = new AesUtil(128, 1000);
			          var ciphertext = aesUtil.encrypt(salt, iv, _key, password);
			          var usern = aesUtil.encrypt(salt, iv, _key, username);

			          var aesPassword = (iv + "::" + salt + "::" + ciphertext);
			          var userName = (iv + "::" + salt + "::" + usern);

			          var password = btoa(aesPassword);
			          var userName = btoa(userName);
		 			
		 			
			          
			          scope.loginData = {
								"username" : username,
								"password" : userName
							};

							scope.requestedData = {
								"sessionTokken" : "",
								"functionName" : "doLogin",
		 		  			    "data" : JSON.stringify({"username" :userName,"password" : password,"pushLogin" : pushLogin})

							};
							
					if(username != "" && password!= ""){
						
						if(captchaEnable=='true')
				 		{	
				 		if(ValidCaptcha())
			   			{
				 			
					          
					          
				 			document.getElementById('overl').style.visibility = "visible";
				     		  
				    		   http({
								method :'POST',
								url :"${pageContext.servletContext.contextPath}/login",
								data :scope.requestedData
								}).then(function (response) {
									if(response.data.status==1){
										window.location.href = '${pageContext.servletContext.contextPath}/map';
	 								}else if(response.data.status==0)
									{
										scope.errormsg = response.data.message;
				 						document.getElementById('overl').style.visibility = "hidden";
				 				 
									}else{
										
										scope.errormsg = response.data.message;
										document.getElementById('overl').style.visibility = "hidden";
									}
										
				  					      
								}, function (response) {
								});	
			   			}
				 		else
				   		{
				   		scope.message="Enter Valid Captcha";
					  
						var x = document.getElementById("snackbar_error")
						x.className = "show";
						setTimeout(function() {
							x.className = x.className.replace("show", "");
						}, 3000);
				   		DrawCaptcha();
				   		}
				 		}
						else
							
							{
							document.getElementById('overl').style.visibility = "visible";
				     		  
				    		   http({
								method :'POST',
								url :"${pageContext.servletContext.contextPath}/login",
								data :scope.requestedData
								}).then(function (response) {
									if(response.data.status==1){
										window.location.href = '${pageContext.servletContext.contextPath}/map';
	 								}else if(response.data.status==0)
									{
										scope.errormsg = response.data.message;
				 						document.getElementById('overl').style.visibility = "hidden";
				 				 
									}else{
										
										scope.errormsg = response.data.message;
										document.getElementById('overl').style.visibility = "hidden";
									}
										
				  					      
								}, function (response) {
								});	
							
							}
						
			     		  
			     	  } else  {
			   				
						  scope.message = "Enter username and password !";
						  
							var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
					   		DrawCaptcha();

						 }
					
					 

				};/*end function  */
				
				
				/************************GET VERSION************************/
			 	scope.requestedData = {
						"sessionTokken" : "",
						"functionName" : "doGetParkingVersionDetails",
						    "data" : ""
					};
				
			 	  http({
						method :'POST',
						url :apiURL+'version',
							data :scope.requestedData
						}).then(function (response) {	
							
							if(response.data.status==1)
								{
								scope.version=response.data.myObjectList.buildVersion;
								}
							else if(response.data.status==0)
								{
								scope.version=response.data.myObjectList.buildVersion;
								}
							else{
								
							}
						}, function (response) {
						}); 
			}]);
</script>

</head>
<body ng-controller="postDataController"  onload=" DrawCaptcha();">
	<div class="limiter">
	
		<div class="container-login100" >
		  <!-- spinner div -->
		 <div class="overlay" id="overl">
			<div id="ico">
				<i class="fa fa-spinner fa-spin"></i>
			</div>
		</div>
		
		
			<div class="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-77">
				<form class="login100-form validate-form" autocomplete="off">
					<span class="login100-form-title p-b-55">
						Log in to your Web App
					</span>
					  <span  class="errormsg">{{errormsg}}</span>

					<div class="wrap-input100 validate-input m-b-16" >
						<input class="input100" type="text" id="username" name="username" placeholder="Username">
						<span class="focus-input100"></span>
						 
					</div>

					<div class="wrap-input100 validate-input m-b-16" >
						<input class="input100" type="password" id="inputPassword" name="pass" placeholder="Password">
						<span class="focus-input100"></span>
 					</div>
 
			        <div class="text-center text-info">
			        <a href="${pageContext.servletContext.contextPath}/resetpassword" id="forgot_pswd">Forgot Password?</a>
			         </div>          
					
					  <div id="captchaBox">
						<div >
                         <div class="wrap-input100 validate-input m-b-16 p-t-20" >	
                         					 <input type="text" id="txtCaptcha"  style=" height: 35px;border-radius: 3px;padding: 0 30px 0 20px; text-align:center; border:none; background-color:#363746; color:#fff; font-size:large; font-weight:bold; font-family:cursive;" disabled/>
					 
                       </div>						
                       </div>
						<div class="row">
							  <div class="col-sm-1 col-sm-1 col-md-1 col-xs-1">
							  </div>
							  <div class="col-sm-8 col-sm-8 col-md-8 col-xs-8">
							  <input type="text" class="form-control" placeholder="Enter above captcha" style=" text-align:center;  font-family:sans-serif; border-radius:5px;" id="txtInput"/> 
							  
							  </div>
							  <div class="col-sm-2 col-sm-2 col-md-2 col-xs-2">
							  <button  class="btn btn-primary" id="btnrefresh" onclick="DrawCaptcha();"><i class="fa fa-refresh"></i></button>
							  </div>
							  <div class="col-sm-1 col-sm-1 col-md-1 col-xs-1">
							  </div>
						
						</div>
						</div>
						 <br>
					
					<div class="container-login100-form-btn p-t-40">
					
						<button class="login100-form-btn" id="loginbtn" ng-click="submitForm()">
							Login
						</button>
						
						 
					</div>
	                   <span class="p-t-10" style="width: 100%; color: #666666; text-align: center;">Version {{version}}</span>
						 <span  style="width: 100%; text-align: center; color: #666666;">	 Powered by AABmatica 
</span>
							
					

		<%-- 			<div class="text-center w-full p-t-42 p-b-22">
						<span class="txt1">
							Or login with
						</span>
					</div>

					<a href="#" class="btn-face m-b-10">
						<i class="fa fa-facebook-official"></i>
						Facebook
					</a>

					<a href="#" class="btn-google m-b-10">
						<img src="${pageContext.request.contextPath}/resources/logindemo/images/icons/icon-google.png" alt="GOOGLE">
						Google
					</a>

					<div class="text-center w-full p-t-115">
						<span class="txt1">
							Not a member?
						</span>

						<a class="txt1 bo1 hov1" href="#">
							Sign up now							
						</a>
					</div> --%>
				</form>
			</div>
		</div>
	</div>
		<div id="snackbar_success">{{response.message}}</div>
	 <div id="snackbar_error">{{message}}</div> 
 
<!--===============================================================================================-->	
	<script src="${pageContext.request.contextPath}/resources/logindemo/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="${pageContext.request.contextPath}/resources/logindemo/vendor/bootstrap/js/popper.js"></script>
	<script src="${pageContext.request.contextPath}/resources/logindemo/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="${pageContext.request.contextPath}/resources/logindemo/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="${pageContext.request.contextPath}/resources/logindemo/js/main.js"></script>
	
	<script type="text/javascript">
	
    function DrawCaptcha()
    {
        var a = Math.ceil(Math.random() * 10)+ '';
        var b = Math.ceil(Math.random() * 10)+ '';       
        var c = Math.ceil(Math.random() * 10)+ '';  
        var d = Math.ceil(Math.random() * 10)+ '';  
        var e = Math.ceil(Math.random() * 10)+ '';  
        var f = Math.ceil(Math.random() * 10)+ '';  
        var g = Math.ceil(Math.random() * 10)+ '';  
        var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' '+ f + ' ' + g;
        document.getElementById("txtCaptcha").value = code
    }

    // Validate the Entered input aganist the generated security code function   
    function ValidCaptcha(){
        var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
        var str2 = removeSpaces(document.getElementById('txtInput').value);
        if (str1 == str2) 
    	{
    	return true;
    	}else
    		{
    		document.getElementById('txtInput').value="";
    		 return false;
    		}
        
    }
    // Remove the spaces from the entered and generated code
    function removeSpaces(string)
    {
        return string.split(' ').join('');
    }
	</script>
</body>
</html>