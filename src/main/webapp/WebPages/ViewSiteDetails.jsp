<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
<meta charset="ISO-8859-1">
<title>Site Information | VPark</title>
   <link href="${pageContext.request.contextPath}/resources/css/application.min.css" rel="stylesheet">
         <link rel="shortcut icon" type="image/x-icon"  href="${pageContext.request.contextPath}/resources/img/logo.png">
       
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/line-awesome/dist/line-awesome/css/line-awesome.css" rel="stylesheet">
             
        <link href="${pageContext.request.contextPath}/resources/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
		
		
		<!----------------------------------------------progressbar--------------------------------------------------------->
		<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
		
  		<!----------------------------------------------Jquery------------------------------------------------------>
      
        <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>   
       <!-----------------------------------------dataTable------------------------------------------------------>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>
        <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        
        <!-----------------------------------------------ANGUARJS----------------------------------------------->
<!--         <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
 -->	
        <!---------------------------------- API URL ------------------------------------------------------>
        <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script> 
	
        <!-- Datatables css-->
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"rel="stylesheet">
	    <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
	    rel="stylesheet">
	    <!-- Datatables js-->
        <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

        <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>

         <!---------------------------------------DATEPICKER-------------------------------------------------------->
		 <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

       
		  <!-------------------------------- Load Leaflet from CDN----------------------------------------------->
		  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
		    integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
		    crossorigin=""/>
		  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
		    integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
		    crossorigin=""></script>
		
		  <!------------------------------ Load Esri Leaflet from CDN -------------------------------------------->
		  <script src="https://unpkg.com/esri-leaflet@2.5.0/dist/esri-leaflet.js"
		    integrity="sha512-ucw7Grpc+iEQZa711gcjgMBnmd9qju1CICsRaryvX7HJklK0pGl/prxKvtHwpgm5ZHdvAil7YPxI1oWPOWK3UQ=="
		    crossorigin=""></script>
		
		  <!------------------------------- Load Esri Leaflet Geocoder from CDN ----------------------------------->
		  <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
		    integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
		    crossorigin="">
		  <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
		    integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
		    crossorigin=""></script>
             
             <!-------------------------------------------markercluster  ------------------------------------------>
	        <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.0.4/dist/MarkerCluster.Default.css">
			<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.0.4/dist/MarkerCluster.css">
			<script src="https://unpkg.com/leaflet.markercluster@1.0.4/dist/leaflet.markercluster.js"></script>
			<script src="${pageContext.request.contextPath}/resources/MovingMarker.js"></script>
			
			<!-- Load Clustered Feature Layer from CDN -->
	        <script src="https://unpkg.com/esri-leaflet-cluster@2.0.0/dist/esri-leaflet-cluster.js"></script>
	        
	        <script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>
	        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/webmain.css">
	    
	        <!---------------------------------- SweetAlert  ------------------------------------------------------>
            <script src="${pageContext.request.contextPath}/resources/sweetAlert.js"></script> 
	     
 
<script>
         $(document).ready(function(){

              $("#kviewSite").addClass("active");

	      });








	/**********************************SESSION ********************************************************** */
	var uid = '<%=session.getAttribute("uid")%>';
	var userN = '<%=session.getAttribute("user")%>';
	
	var options = {
	 		   headers: {
	 		     Authorization: 'Bearer ' + '${jwtToken}' // Assuming 'jwtToken' holds the JWT
	 		   }
	 };	
	
	
	
	
	var jDataTable;
	var jData;
	var app = angular.module("app", []);
	app.controller("GetDataController", ['$scope','$http','$window', function(scope,http,window) {
	  
		scope.requestData =
	    {
			"data" : "",
			"username" : uid,
			"basicAuth" : ""
	     };
		
		document.getElementById('overlid').style.visibility = "hidden";
		scope.formdate =moment().format('YYYY-MM-DD HH:mm:ss');
		
		
		//******************************PARKING LIST******************************//
		   scope.GetParking = function () { 
				  
			   http.get(apiURL+'getSite/'+userN+'/doGetSiteInfo',options).
				then(function (response) {
					if(response.data != null)
					{
						if(response.data.status==1){
						        Pace.stop();
				                document.getElementById('overl').style.visibility = "hidden";   
                               jData = [];
			             		jData= response.data.myObjectList;
				                jDataTable.clear().rows.add(jData).draw();
			         }else if(response.data.status==21){
	   		  				 
		  		        }else{
			        	 jData = [];
			       	     jDataTable.clear().rows.add(jData).draw();
			               } 
					
			      }
					else{
			        	 jData = [];
			       	     jDataTable.clear().rows.add(jData).draw();
			       	     
			               } 
				}, function (response) {
				});
	};  	
	/*********************************PARKING DATA*************************************/
	$(document).ready(function() {
		 jDataTable = $('#users').DataTable({ 
	 		 	data: jData,
	 		 	columns: [
		         	    {'data':'sid'},
		                
		             {'data':'locationDetail', 'render':function(locationDetail){
         				
	             		 if(locationDetail!=null)
             			 {
	             			if(locationDetail!=""){
	         					//return locationDetail.locationName;
	         					
 	         					return '<i class="fi flaticon-placeholder-2 text-info" style="font-size:13px;" aria-hidden="true"></i> ' +locationDetail.locationName;

	         				}else{
	         					return '<p>__</p>';
	         				}
             			 }else{
             				return '<p>__</p>';
             			 }
         			}},
		         		{'data':'siteName', 'render':function(siteName){
	         				
	             		 if(siteName!=null)
             			 {
	             			if(siteName!=""){
 	         					return '<i class="fi flaticon-placeholder-3 text-info" style="font-size:13px;" aria-hidden="true"></i> ' +siteName;
  	         				}else{
  	         					return '<p>__</p>';
  	         				}
             			 }else{
             				return '<p>__</p>';
             			 }
         			}},
         			{'data':'operatingCompany', 'render':function(operatingCompany){
         				
	             		 if(operatingCompany!=null)
            			 {
	             			if(operatingCompany!=""){
	         					return operatingCompany;
 	         				}else{
 	         					return '<p>__</p>';
 	         				}
            			 }else{
            				return '<p>__</p>';
            			 }
        			}},
        			{'data':'type', 'render':function(type){
         				
	             		 if(type!=null)
            			 {
	             			if(type!=""){
	         					return type;
 	         				}else{
 	         					return '<p>__</p>';
 	         				}
            			 }else{
            				return '<p>__</p>';
            			 }
        			}},
        			{'data':'availableSlots', 'render':function(availableSlots){
         				
	             		 if(availableSlots !=null)
            			 {
	             			if(availableSlots >0){
	             				
	         					return availableSlots;
	         					
 	         				}else{
 	         					return availableSlots;
 	         				}
            			 }else{
            				return '<p>__</p>';
            			 }
        			}},
        			
        			{'data':'slots', 'render':function(slots){
         				
	             		 if(slots !=null)
            			 {
	             			if(slots >0){
	             				
	         					return slots;
	         					
	         				}else{
	         					return slots;
	         				}
            			 }else{
            				return '<p>__</p>';
            			 }
        			}},
        			
        			
        			{'data':'siteStatus', 'render':function(siteStatus){
         				
	             		 if(siteStatus!=null)
            			 {
	             			if(siteStatus!=""){
	             				if(siteStatus == 1){
	             					return '<i class="fa fa-toggle-on text-success" style="font-size:23px;" aria-hidden="true"></i>'
	             				}
	             				else{
	             					return '<i class="fa fa-toggle-off" style="font-size:23px;" aria-hidden="true"></i>';
	             					
	             				}
	             				//<i class="fa fa-toggle-on text-success" style="font-size:18px;" aria-hidden="true"></i>
	         					//return siteStatus;
 	         				}else{
 	         					return '<p>__</p>';
 	         				}
            			 }else{
            				return '<p>__</p>';
            			 }
        			}}
		             	   
         			
		       
		            ],
		           buttons: [
		        	    'copy', 'excel', 'pdf'
		        	  ],
	
		 	             "ordering": false,
		 	             "searching": false,
		 	             "deferRender": true,
		 	             "autoWidth": false,
		 	            "lengthChange": false,
		 	             "language": {
		     	    		"infoEmpty": "No records to show",
		         	    	"emptyTable": "no record found",
		     	    	    }
		    	        }); 
	  }); 
	
	
	   
	   
	//**************SELECT DESELECT TABLE ROW******************************//
	$('#users').on('click', 'tr', function () {
		
			 if ( $(this).hasClass('row_selected'))
		  		{  
				   $("#users tbody tr").removeClass('row_selected');
		  	       $(this).removeClass('row_selected');
		      }else
		  	        {
		  	        $("#users tbody tr").removeClass('row_selected');
		  	        $(this).addClass('row_selected');
		  	 
		  	    }
		}); 
	
		scope.formatDate = function(date){
			  return new Date(date)
		}
	  
	 
	}]);
</script>
</head>
<body class="" ng-controller="GetDataController" ng-init="GetParking()">
 
  <%@ include file="Sidebar.jsp"%>	
    
  
  
  <div class="content-wrap">
    
    <!-- main page content. the place to put widgets in. usually consists of .row > .col-lg-* > .widget.  -->
    <main id="contentt" class="content" role="main" class="ng-cloak">
      <!-- Page content -->
        <div class="row">
            <div class="col-lg-12">
                <section class="widget">
                    <header>
                         <h5>Site Information<span class="fw-semi-bold"></span>  
                        </h5>
                    </header>
                    <div class="widget-body" style="margin-top: 0px !important;">
                    <div class="table-responsive">
                      <table class="table table-striped dataTable  table-lg mt-lg mb-0" id="users">
                      <thead class=" text-primary">
                      <tr>
                                	        <th data-visible="false">ID</th>
                                	        <th style=" text-transform: revert; font-size: 15px;">Location</th>
										    <th style=" text-transform: revert; font-size: 15px;">Site </th>
											<th style=" text-transform: revert; font-size: 15px;">Operating Company</th>
											<th style=" text-transform: revert; font-size: 15px;"> Type</th>
											<th style=" text-transform: revert; font-size: 15px;"> Available Slots</th>
											<th style=" text-transform: revert; font-size: 15px;">Total Slots</th>
											<th style=" text-transform: revert; font-size: 15px;">Site Status</th>
											
						 </tr>
                         </thead>
                       </table>
                        </div>
                        <div class="clearfix">
                     </div>
                    </div>
                </section>
            </div>

            
        </div>
        <div id="snackbar_success">{{response_message}} </div>
		<div id="snackbar_error">{{message}} </div>
		
		<!-- spinner -->
        <div class="loader-wrap" style="background-color: rgba(0, 0, 0, 0.4);" id="overlid">
		<i class="fa fa-spinner fa-spin"></i>
        </div>
        
        </main>
        </div>
              
        <div class="loader-wrap"id="overl">
            <i class="fa fa-circle-o-notch fa-spin-fast"></i>
        </div> 
        
        
        
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-pjax/jquery.pjax.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/popper.js/dist/umd/popper.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/dist/js/bootstrap.js"></script>
      
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/js/dist/util.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/widgster/widgster.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" data-pace-options='{ "target": ".content-wrap", "ghostTime": 1000 }'></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/hammerjs/hammer.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-hammerjs/jquery.hammer.js"></script>


        <!-- common app js -->
        <script src="${pageContext.request.contextPath}/resources/js/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/app.js"></script>

        <!-- Page scripts -->
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.animator/jquery.flot.animator.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.selection.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.time.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.pie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.stack.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.crosshair.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.symbol.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.dashes/jquery.flot.dashes.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap_calendar/bootstrap_calendar/js/bootstrap_calendar.min.js"></script>
       <!-- bootstrap-daterangepicker -->	
       <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
       
    </body>

</html>










