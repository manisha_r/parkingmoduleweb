<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
<meta charset="ISO-8859-1">
<title>Parking Map | VPark</title>
          <!-- main css -->
          <link href="${pageContext.request.contextPath}/resources/css/application.min.css" rel="stylesheet">
          
          <link rel="shortcut icon" type="image/x-icon"  href="${pageContext.request.contextPath}/resources/img/logo.png">
       
          <meta name='viewport' content='width=device-width, initial-scale=1'>
          <script src='https://kit.fontawesome.com/a076d05399.js'></script>
          <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png">
          <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
          <link href="${pageContext.request.contextPath}/resources/node_modules/line-awesome/dist/line-awesome/css/line-awesome.css" rel="stylesheet">
        
          <link href="${pageContext.request.contextPath}/resources/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
		
		  <!------------------------------- Load Leaflet from CDN --------------------------------------------------->
		  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
		    integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
		    crossorigin=""/>
		  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
		    integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
		    crossorigin=""></script>
		
		  <!--------------------------------- Load Esri Leaflet from CDN ---------------------------------------------->
		  <script src="https://unpkg.com/esri-leaflet@2.5.0/dist/esri-leaflet.js"
		    integrity="sha512-ucw7Grpc+iEQZa711gcjgMBnmd9qju1CICsRaryvX7HJklK0pGl/prxKvtHwpgm5ZHdvAil7YPxI1oWPOWK3UQ=="
		    crossorigin=""></script>
		
		  <!----------------------------- Load Esri Leaflet Geocoder from CDN ----------------------------------------->
		  <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
		    integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
		    crossorigin="">
		  <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
		    integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
		    crossorigin=""></script>
		    
		  <!-------------------------------------- MARKER CLUSTER------------------------------------------------- -->
          <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.0.4/dist/MarkerCluster.Default.css">
		  <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.0.4/dist/MarkerCluster.css">
		  <script src="https://unpkg.com/leaflet.markercluster@1.0.4/dist/leaflet.markercluster.js"></script>
		
		  <!------------------------- Load Clustered Feature Layer from CDN ----------------------------------------------->
          <script src="https://unpkg.com/esri-leaflet-cluster@2.0.0/dist/esri-leaflet-cluster.js"></script>
	      <script src="${pageContext.request.contextPath}/resources/MovingMarker.js"></script>
	
          <!--------------------------------------ANGULARJS-------------------------------------------------------------->
		  <!--angular 1.3.14  -->
	     <script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>         
          
           <!----------------------------------------------Jquery------------------------------------------------------>
      
         <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>   
      
          <!---------------------------------- API URL ------------------------------------------------------>
          <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script> 
	     
	     	
          <!---------------------------------- SweetAlert  ------------------------------------------------------>
          <script src="${pageContext.request.contextPath}/resources/sweetAlert.js"></script> 
	     
		  <!----------------------------------------------PROGRESS BAR--------------------------------------------------------->
		  <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
		
		<style type="text/css">
		
		.content-map {
		    position: absolute;
		    top: 0;
		    bottom: 0;
		    left: 0;
		    right: 0;
		    margin-top: 61px;
		 }
		 .modal {
		    position: fixed;
		    top: 60px;
		    left: 0;
		    z-index: 1050;
		    display: none;
		    width: 100%;
		    height: 100%;
		    overflow: hidden;
		    outline: 0;
		}
		
		 p {
		    margin: 0 0 10px;
		    padding-top: 10PX;
		    font-size: 13px;
		    font-weight: 500;
            font-family: "Montserrat",sans-serif;
		}
		
		img.parkingimage {
		 vertical-align: middle;
		    width: 100px;
            height: inherit;
		    margin-top: 50px;
       }
       
		
		</style>
	
    <script>
    
    
       /**************** USER PROFILE ************/
       var uid = '<%=session.getAttribute("uid")%>';
       var userN = '<%=session.getAttribute("user")%>';

       var options = {
    		   headers: {
    		     Authorization: 'Bearer ' + '${jwtToken}' // Assuming 'jwtToken' holds the JWT
    		   }
    		 };
      
       
       /****Initial value*****/
       var map;
	   var markersLayer;
	   var pane;
	   var popup;
	   var addsitedata=[];
	   var markers= null;
	   var mapZoom;
		 
	   /****sidebar active****/
	   $(document).ready(function(){   
        $("#kmap").addClass("active");
    	});
	   
       /************************************************* Angular Controller ******************************************************************************/

       var app = angular.module("app", []);
       app.controller("GetDataController", ['$scope','$http','$window', function(scope,http,window) {
       	
    	
             /****Initial value*****/
             scope.ParkingSite=[];
             scope.citylat= 0;
         	 scope.citylon= 0;
            
         	/********************************GET LOCATION***************************************/
           //  http.get(apiURL+'getlocation/'+jwtToken+'/'+userN+'/doGetLocation').
           http.get(apiURL + 'getSlotDetails/' + userN + '/doGetSlotInfo', options).
  
           then(function (response) {
  				
  			  if(response.data != null)
  				 {
  				 if(response.data.status==1){
  			              
  					scope.locationlist= response.data.myObjectList;
  					scope.citylat=scope.locationlist[0].lat;
  				    scope.citylon=scope.locationlist[0].lon;
				       /*PROGRESS BAR  */   
					  // Pace.stop();
  		           }else if(response.data.status==21){
  		           } //end of jwt check 
  		           
   	
   		        }else{}      		       		
             }, function (response) {
				});
          

          
             
            
           //******************************LOCATION LIST******************************//
        	
            scope.locationdata=""; 
        	 scope.sitedata=[];
        	 scope.GetParking = function () { 
        		

        		 //http.get(apiURL+'getSlotDetails/'+jwtToken+'/'+userN+'/doGetSlotInfo').
 				 http.get(apiURL+'getSlotDetails/'+userN+'/doGetSlotInfo',options).

        		 then(function (response) {
 					 
 					if(response.data != null)
 					{
 					if(response.data.status==1){
 					    scope.sitedata= response.data.myObjectList;
 					    scope.locationdata=scope.sitedata[0];
  					     addMarker(scope.sitedata);
                        // document.getElementById('overl').style.visibility = "hidden";
 	                 }else if(response.data.status==21){
 	  		         }
 	                 else{
 	                	 scope.sitedata="";
 			        	  }  
 			       
 					}
 					else{
 			    	   scope.sitedata="";
 			   	 }      		       		
        		 }, function (response) {
					});
        	 };
    	
           
	/********************************DEFAULT MAP*******************************************/
	
  	/* function renderMarker(lat, lon)
		{ */
  	//	console.log(lat, lon);
  		
    	map = L.map('map').setView([scope.citylat, scope.citylon],12);
 		var markerClusters = L.markerClusterGroup();
 	    markersLayer = new L.LayerGroup(); 
		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);		
		pane = map.createPane('fixed', document.getElementById('map'));	 
		
		var arcgisOnline = L.esri.Geocoding.arcgisOnlineProvider();
 		var searchControl = L.esri.Geocoding.geosearch({
		providers: [
		  arcgisOnline,
		  L.esri.Geocoding.mapServiceProvider({
		    label: 'States and Counties',
		    url: 'https://sampleserver6.arcgisonline.com/arcgis/rest/services/Census/MapServer',
		    layers: [2, 3],
		    searchFields: ['NAME', 'STATE_NAME']
		  })
		]
		}).addTo(map); 
 		 
 		 mapZoom = L.featureGroup().addTo(map);
 		 
   	/* } */
	 
	
	//-------------------------------------------------BY API CALL------------------------------------------//
  	scope.carlen=0;
  	scope.buslen=0;
  	scope.twowheelerlen=0;
  	scope.availableCar=0;
  	scope.availableBus=0;
  	scope.availableBike=0;
  	
    //GET SITE DATA ON MARKER  	                      
  	function addMarker(addsitedata) //dynamic site list
	{  
		var parkinglocation;
		 var contentpop= '';
		
		 var lat=0;
		 var lng=0;
	  
		 
		 mapZoom = new L.featureGroup().addTo(map); //to get bounds
		 
				 var sensorIcon = L.icon({
				    iconUrl: '${pageContext.request.contextPath}/resources/img/offstreetparking_icon.png',
				    iconSize:     [40, 50], // size of the icon			   
				    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
				    shadowAnchor: [4, 62],  // the same for the shadow
				    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
				});
				 
				 var motionsensorIcon = L.icon({
		  			 iconUrl: '${pageContext.request.contextPath}/resources/img/onstreetparking_icon.png',
		  			  iconSize:     [40,50], // size of the icon			   
					    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
					    shadowAnchor: [4, 62],  // the same for the shadow
					    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
					 });
				 
				 
				 var parkingfullicon = L.icon({
		  			 iconUrl: '${pageContext.request.contextPath}/resources/img/parkingfull_icon.png',
		  			  iconSize:     [40,50], // size of the icon			   
					    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
					    shadowAnchor: [4, 62],  // the same for the shadow
					    popupAnchor:  [-3, -76] // point from which the popup should open relative3 to the iconAnchor
					 });
				 
				popup = L.popup({
	        		  pane: 'fixed',
	        		  className: 'popup-fixed',
	          		  autoPan: true,
	          		  keepInView: true,
	             	  closeButton: true 
	         		});
				
				
		
	    for(var a=0;a<addsitedata.length;a++)
		 { 
	    	parkinglocation = addsitedata[a].mSiteModel;
	    	
 	    	 if(parkinglocation.type=="offstreet")
	    		{
   			    
 	    		var totalslots = addsitedata[a].slot;
  			    var occupiedSlot =  addsitedata[a].occupiedSlot
  			    var availablevehicleSlot=  addsitedata[a].availableVehicleSlot
 	 		    lat = addsitedata[a].mSiteModel.latitude;
			    lng = addsitedata[a].mSiteModel.longitude;
			    
			    var contentpop='<div class="row"  ><h4  class="pull-left" style="padding-left: 7px;padding-right: 7px;color:#73879C;"><strong>'+parkinglocation.siteName+'</strong></h4></div><div class="row"></div></div>'
		       
			    markers= L.marker([lat, lng], {icon: sensorIcon}).addTo(mapZoom);	
	    	    
	    		} 
	    	if(parkinglocation.type=="onstreet")
    		{
	    		
	    		var totalslots = addsitedata[a].slot;
  			    var occupiedSlot =  addsitedata[a].occupiedSlot
  			    var availablevehicleSlot=  addsitedata[a].availableVehicleSlot
  			    siteName = addsitedata[a].mSiteModel.siteName;
 	 		    lat = addsitedata[a].mSiteModel.latitude;
			    lng = addsitedata[a].mSiteModel.longitude;
			  
			    var contentpop='<div class="row"  ><h5  class="pull-left" style="padding-left: 7px;padding-right: 7px;color:#73879C;"><strong>'+parkinglocation.siteName+'</strong></h5></div><div class="row"></div></div>'
			   
			    markers= L.marker([lat, lng], {icon: motionsensorIcon}).addTo(mapZoom);	
				   			    
    		}
	    	
	    	 /* to add marker for slot is full */
	    	 if(parkinglocation.availableSlots==0)
	    		{
	    		
 			    var totalslots = addsitedata[a].slot;
			    var occupiedSlot =  addsitedata[a].occupiedSlot
			    var availablevehicleSlot=  addsitedata[a].availableVehicleSlot
			    siteName = addsitedata[a].mSiteModel.siteName;
  	 		    lat = addsitedata[a].mSiteModel.latitude;
			    lng = addsitedata[a].mSiteModel.longitude;
			    var contentpop='<div class="row"  ><h4  class="pull-left" style="padding-left: 7px;padding-right: 7px;color:#73879C;"><strong>'+parkinglocation.siteName+'</strong></h4></div><div class="row"></div></div>'
		        markers= L.marker([lat, lng], {icon: parkingfullicon}).addTo(mapZoom);	
 	    		} 
	    	
	    	  markers.myID = a;
	    	 
              markers.on('click', function(e) {
              	    scope.twowheelerlen=0;
	    				scope.buslen=0;
	    				scope.carlen=0;
	    			 	scope.availableCar=0;
	    			  	scope.availableBus=0;
	    			  	scope.availableBike=0;
	    	    	    var i = e.target.myID;
	    	    	    
	    	    	//FOR PARKING AVAILABLE AND SPACE DATA IN PARKING INFO IN MODEL  
	    	    	for(var k=0; k<addsitedata.length; k++)
	    	        {
	    	    		 
	    	    		 if(addsitedata[k].mSiteModel.sid == addsitedata[i].mSiteModel.sid)
	    	    			{
	    	    			 
	    	    			 
	    	    			 if(addsitedata[k].mVehicleClassModel.className=="CAR")
	    	    			 {
	    	    				scope.carlen= addsitedata[k].occupiedSlot;
	    	    				scope.availableCar=addsitedata[k].availableVehicleSlot;
	    	    		 	} 
	    	    		     else if(addsitedata[k].mVehicleClassModel.className=="BUS")
	    	    			 {
	    	    			 scope.buslen=addsitedata[k].occupiedSlot;
	    	    			 scope.availableBus=addsitedata[k].availableVehicleSlot;
	    	    				
	    	    			 }
	    	    		    else if(addsitedata[k].mVehicleClassModel.className=="TWO WHEELER")
  	    			        {
  	    			         scope.twowheelerlen=addsitedata[k].occupiedSlot;
  	    			         scope.availableBike=addsitedata[k].availableVehicleSlot;
	    	    			 } 

	    	    		     else{
	    	    				scope.twowheelerlen=0;
	    	    				scope.buslen=0;
	    	    				scope.carlen=0;
	    	    			 	scope.availableCar=0;
	    	    			  	scope.availableBus=0;
	    	    			  	scope.availableBike=0;
	    	    			 }  
	    	    			}
	    	    			
	    	    		}
	    	    	
	    	    	scope.status="";
	    	    	
	    	    	//STATUS CHECKING
	    	    	if(addsitedata[i].mSiteModel.siteStatus==1){
	    	    		scope.status='<span class="badge badge-success">Open</span>';
	    	    	}
	    	    	else if(addsitedata[i].mSiteModel.siteSatus==0){
	    	    		scope.status='<span class="badge badge-warning">Closed</span>';
	    	    	}
	    	    	else{
	    	    		scope.status="";
	    	    	}
	    	    	
	    	    	//----------setting availability icon---------------------//
	    	 
			    	if(scope.availableCar>0)
	    	    		{
	    	    		 scope.circleCar='<img src="${pageContext.request.contextPath}/resources/img/available.png" style="height: 13px;width: 13px;  margin-top: 0px;" alt="..."> '+ scope.availableCar;
	    	    		}
	    	    	else
	    	    		{
	    	    		 scope.circleCar='<img src="${pageContext.request.contextPath}/resources/img/unavailable.png" style="height: 13px;width: 13px;  margin-top: 0px;" alt="..."> '+ scope.availableCar;
	    	    		}
	    	    	if(scope.availableBus>0)
	    	    		{
	    	    		  scope.circleBus='<img src="${pageContext.request.contextPath}/resources/img/available.png" style="height: 13px;width: 13px;  margin-top: 0px;" alt="..."> '+ scope.availableBus;
	    	    		}
	    	    	else
	    	    		{
	    	    		  scope.circleBus='<img src="${pageContext.request.contextPath}/resources/img/unavailable.png" style="height: 13px;width: 13px;  margin-top: 0px;" alt="..."> '+ scope.availableBus;
	    	    		}
	    	    	if(scope.availableBike>0)
	    	    		{
	    	    		  scope.circleBike='<img src="${pageContext.request.contextPath}/resources/img/available.png" style="height: 13px;width: 13px;  margin-top: 0px;" alt="..."> '+ scope.availableBike;
	    	    		}
	    	    	else
	    	    		{
	    	    		  scope.circleBike='<img src="${pageContext.request.contextPath}/resources/img/available.png" style="height: 13px;width: 13px;  margin-top: 0px;" alt="..."> '+ scope.availableBike;
	    	    		}
	
                      scope.imageUrl="data:image/png;base64,"+addsitedata[i].mSiteModel.siteImage;

	    	    	  $('#datamodel').empty();
	    	          $('#modal-title').empty();
	    	          $('#myModal').modal('show');
	    	          $('#modal-title').append(addsitedata[i].mSiteModel.siteName);
	    	          $("#datamodel").append(' <div class="row"> <div class="col-sm-5 "><p >PARKING STATUS</p><p >PARKING TYPE</p><p >TOTAL SLOTS</p><p > AVAILABLE SLOTS</p></div><div class="col-sm-3"> <p >'+scope.status+'</p><p >'+addsitedata[i].mSiteModel.type+'</p> <p >'+addsitedata[i].mSiteModel.slots+' Vehicle</p><p >'+addsitedata[i].mSiteModel.availableSlots+' Vehicle</p> </div><div class="col-sm-4"  style="padding-left: 0px;"><img src="'+scope.imageUrl+'" class="thumbnail img-fluid"/> </div></div><div class="row"><div class="col-sm-5 "><br> <b >VEHICLE TYPES</b><p ><i style="padding-left: 28%;font-size: 25px;"class="fa fa-motorcycle"></i></p> <p ><i style="padding-left: 28%;font-size: 25px; "class="fa fa-automobile"></i></p><p > <i style=" padding-left: 28%;font-size: 25px; " class="fa fa-bus"></i></p></div><div class="col-sm-4 "><br><b >OCCUPIED</b> <p style=" padding-top: 20px;">'+scope.twowheelerlen+' bike</p><p >'+scope.carlen+' car</p> <p>'+scope.buslen+' bus</p></div><div class="col-sm-3 "style=" padding-left: 0px;"> <br><b >AVAILABILITY</b><p style="padding-top: 6px;margin-top: 13px;margin-bottom: 0px;">'+scope.circleBike+'</p><p style="padding-top: 8px;margin-top: 15px;margin-bottom: 0px;">'+scope.circleCar+'</p><p style="margin-top: 16px;">'+scope.circleBus+'</p></div></div></div>');
	        	      scope.$apply();
	            });
              
              
              markers.bindPopup(contentpop);
              markers.on('mouseover', function(e) {
              this.openPopup();
     	    	    	
              });
              
              markers.on('mouseout', function (e) {
                  this.closePopup();
              });
              
              document.getElementById('overl').style.visibility = "hidden";
	    	 
		   } 
	        
	      map.setView(mapZoom.getBounds().getCenter());
 	  	  map.fitBounds(mapZoom.getBounds());
	  	
	    }

      }]);
</script> 

</head>
 <body class="" ng-controller="GetDataController" ng-init="GetParking()">
 
  <%@ include file="Sidebar.jsp"%>	

    <div class="loader-wrap " id="overl">
         <i class="fa fa-circle-o-notch fa-spin-fast"></i>
    </div>

    <div class="content-wrap">

      <main id="contentt" class="content" role="main"  class="ng-cloak">
      
        <!-- Leaflet Map -->
        <div id="map" class="content-map"></div> 
       	  
       	<!-- VIEW LIVE MODAL -->
        <div class="modal fade" id="myModal" role="dialog">
         <div class="modal-dialog">
               <!-- Modal content-->
               <div class="modal-content"  style=" padding-left: 17px;padding-right: 17px;background: #f9fbfd;">
                 
                  <div class="modal-header"  style="border-bottom: none;">
                      <h4 class="modal-title" style="font-weight: 700;font-family: inherit;" id="modal-title"> </h4>
                  </div>
                  
                  <div class="modal-body"style=" padding-left: 17px;padding-right: 17px;" id="datamodel"></div>
		   
                 <div class="modal-footer"  style="border-top: none; padding-bottom: 10px;"> </div>
                 
         </div>
      </div>
     </div>
     
     
   </main>
  </div>
       
        <!-- common libraries. required for every page-->
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery/dist/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-pjax/jquery.pjax.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/popper.js/dist/umd/popper.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/dist/js/bootstrap.js"></script>
      <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/js/dist/util.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/widgster/widgster.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" data-pace-options='{ "target": ".content-wrap", "ghostTime": 1000 }'></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/hammerjs/hammer.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-hammerjs/jquery.hammer.js"></script>


        <!-- common app js -->
        <script src="${pageContext.request.contextPath}/resources/js/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/app.js"></script>

        <!-- Page scripts -->
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.animator/jquery.flot.animator.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.selection.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.time.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.pie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.stack.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.crosshair.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.symbol.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.dashes/jquery.flot.dashes.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap_calendar/bootstrap_calendar/js/bootstrap_calendar.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

     </body>

</html>
