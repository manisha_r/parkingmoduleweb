package com.traffsys.parkingsolution.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.traffsys.parkingsolution.DAO.RemoteDAO;
import com.traffsys.parkingsolution.modal.RequestModel;
import com.traffsys.parkingsolution.utils.AppConstant;
import com.traffsys.parkingsolution.utils.Util;


@Controller
public class ViewMappingController {

	
	// Home Page
	@RequestMapping(value = "/Home", method = RequestMethod.GET)
	public String OpenHomePage(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {
	
       if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}
       
       String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		//request.changeSessionId();
		
		
		
		
		return "Home";
	}

	// Home Map Page
	@RequestMapping(value = "/map", method = RequestMethod.GET)
	public String OpenParkingMap(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {
		System.out.print("check:-"+request.getSession().getAttribute("jwtToken"));

		if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}
	      
       String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

		return "map";
	}
	
	
	// Parking map
	@RequestMapping(value = "/viewsite", method = RequestMethod.GET)
		public String openSitedetails(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {
		
        if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}   
        
       String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

			return "ViewSiteDetails";
	}

	// addVehicle Page
	@RequestMapping(value = "/addVehicle", method = RequestMethod.GET)
	public String addVehicle(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {

		if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}
	      
       String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
	
		return "addVehicle";
	}

	// user Page
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public String getalluser(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {

		if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}      
       
		
		
		
		
		
		String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

		return "user";
	}

	// add user page
	@RequestMapping(value = "/addUser", method = RequestMethod.GET)
	public String adduser(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {

		if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}      
       
		
		
		
		
		
		String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

		return "addUser";
	}

	// get Vehicle Page
	@RequestMapping(value = "/getVehicle", method = RequestMethod.GET)
	public String getVehicle(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {

		if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}      
       
		
		
		
		
		
		String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

		return "getVehicle";
	}

	// get Vehicle Page
	@RequestMapping(value = "/Location", method = RequestMethod.GET)
	public String addlocationMaster(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {

		if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}      
       
		
		
		
		
		
		String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

		return "MasterLocation";
	}

	@RequestMapping(value = "/Site", method = RequestMethod.GET)
	public String addlocation(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {

		if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}      
       
		
		
		
		
		
		String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

		return "MasterSite";
	}

	@RequestMapping(value = "/parkingcharges", method = RequestMethod.GET)
	public String parkingcharges(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {
		if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}      
       
		
		
		
		
		
		String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

		return "parkingcharges";
	}
	

	@RequestMapping(value = "/SiteLogs", method = RequestMethod.GET)
	public String openReportoccupancyPage(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {

		if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}      
       
		
		
		
		
		
		String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

		return "SiteLogs";
	}
	
	//OperatorWise
	@RequestMapping(value = "/OperatorWise", method = RequestMethod.GET)
	public String openOperatorWiseReport(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {

		if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}      
       
		
		
		
		
		
		String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

		return "OperaterWiseReport";
	}
	
	
	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public String openReportPage(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {

		if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}      
       
		
		
		
		
		
		String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

		return "Reports";
	}
	
	@RequestMapping(value = "/allreport", method = RequestMethod.GET)
	public String openbasicreport(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {

		if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}      
       
		
		
		
		
		
		String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

		return "AllBasicReport";
	}

	
	@RequestMapping(value = "/payment", method = RequestMethod.GET)
	public String openpaymentpage(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {
		
		if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}      
       
		
		
		
		
		
		String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

		return "ReportPaymentCollection";
	}
	
	@RequestMapping(value = "/City", method = RequestMethod.GET)
	public String openUpdateLocation(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {

		if (request.getSession().getAttribute("jwtToken") == null) {
			
			return"redirect:/signin";
		}      
       
		
		
		
		
		
		String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

		return "UpdateLocation";
	}
	
	
	//ReserveParking Information
	@RequestMapping(value="/ReserveParking" ,method=RequestMethod.GET)
	public String openReserveParking(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) 
	{
		if (request.getSession().getAttribute("isLoggedIn") == null) {
			System.out.print(request.getSession().getAttribute("isLoggedIn"));
			return"redirect:/signin";
		}      
       
		
		
		
		
		
		String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		

		

		return "ReserveParking";
	}
	

	//MonthlyPass Information
	@RequestMapping(value="/MonthlyPass" ,method=RequestMethod.GET)
	public String openMonthlyPass(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) 
	{
		if (request.getSession().getAttribute("isLoggedIn") == null) {
			System.out.print(request.getSession().getAttribute("isLoggedIn"));
			return"redirect:/signin";
		}
	      
       
		
		
		
		
		
		String sessionid = request.getSession().getId();
 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
  		request.changeSessionId();
		
		
		
		

		return "Monthlypass";
	}
	
	//MonthlyPass Information
		@RequestMapping(value="/Device" ,method=RequestMethod.GET)
		public String openDeviceStatus(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) 
		{

			if (request.getSession().getAttribute("jwtToken") == null) {
				
				return"redirect:/signin";
			}      
	        
			
			
			
			
			
			String sessionid = request.getSession().getId();
	 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
	  		request.changeSessionId();
			
			
			
			

			
			return "DeviceStatus";
		}
	
		
	// signin
	@GetMapping({ "/signin", "/login" })
	public String opnlogin(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {
        HttpSession session = request.getSession();	
 			
		//request.chngeSessionId();
		session.invalidate();
  
		return "LoginDemo";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public String opnlogout(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) throws JSONException {
        HttpSession session = request.getSession();	
        
        HttpSession sessions = (HttpSession) request.getSession(false);
		if (session != null) {
			
	  	String reqURL = AppConstant.API_URL+"invalid/"+request.getSession().getAttribute("jwtToken");

			try {
				String resjson= RemoteDAO.sendPostRequest(reqURL);
				System.out.print(resjson+":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");

				session.invalidate();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
		}
		
			return "LoginDemo";
	}

	
	// reset password
		@RequestMapping(value = "/resetpassword", method = RequestMethod.GET)
		public String resetpass(ModelMap model, HttpServletRequest request,HttpServletResponse mHttpServletResponse) {

			    
				
				
				
				
				
				String sessionid = request.getSession().getId();
		 		//mHttpServletResponse.setHeader("SET-COOKIE","JSESSIONID=" + sessionid + ";Path=/<eTmcsWeb>; Secure; HttpOnly");
		  		request.changeSessionId();
				
				
				

			    return "resetpassword";
		}
	
}
