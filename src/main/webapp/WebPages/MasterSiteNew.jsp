<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
<meta charset="ISO-8859-1">
<title>Add Site | VPark</title>
        <link href="${pageContext.request.contextPath}/resources/css/application.min.css" rel="stylesheet">
       
        <link rel="shortcut icon" type="image/x-icon"  href="${pageContext.request.contextPath}/resources/img/logo.png">
       
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/line-awesome/dist/line-awesome/css/line-awesome.css" rel="stylesheet">
             
        <link href="${pageContext.request.contextPath}/resources/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
		
		
		<!----------------------------------------------progressbar--------------------------------------------------------->
		<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
		
  		<!----------------------------------------------Jquery------------------------------------------------------>
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script> 
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery/dist/jquery.min.js"></script> 
 
        <!-----------------------------------------dataTable------------------------------------------------------>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>
        <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        
        <!-----------------------------------------------ANGUARJS----------------------------------------------->
        <script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>
	
        <!---------------------------------- API URL ------------------------------------------------------>
        <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script> 
	
        <!-- Datatables css-->
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"rel="stylesheet">
	    <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
	    rel="stylesheet">
	    <!-- Datatables js-->
        <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

        <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>

         <!---------------------------------------DATEPICKER-------------------------------------------------------->
		 <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

       
		  <!-------------------------------- Load Leaflet from CDN----------------------------------------------->
		  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
		    integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
		    crossorigin=""/>
		  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
		    integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
		    crossorigin=""></script>
		
		  <!------------------------------ Load Esri Leaflet from CDN -------------------------------------------->
		  <script src="https://unpkg.com/esri-leaflet@2.5.0/dist/esri-leaflet.js"
		    integrity="sha512-ucw7Grpc+iEQZa711gcjgMBnmd9qju1CICsRaryvX7HJklK0pGl/prxKvtHwpgm5ZHdvAil7YPxI1oWPOWK3UQ=="
		    crossorigin=""></script>
		
		  <!------------------------------- Load Esri Leaflet Geocoder from CDN ----------------------------------->
		  <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
		    integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
		    crossorigin="">
		  <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
		    integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
		    crossorigin=""></script>
             
             <!-------------------------------------------markercluster  ------------------------------------------>
	        <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.0.4/dist/MarkerCluster.Default.css">
			<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.0.4/dist/MarkerCluster.css">
			<script src="https://unpkg.com/leaflet.markercluster@1.0.4/dist/leaflet.markercluster.js"></script>
			<script src="${pageContext.request.contextPath}/resources/MovingMarker.js"></script>
			
			<!-- Load Clustered Feature Layer from CDN -->
	        <script src="https://unpkg.com/esri-leaflet-cluster@2.0.0/dist/esri-leaflet-cluster.js"></script>
	        
	        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
	        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/webmain.css">
	    
		
   <style>
     .previous {
        text-decoration: none;
        display: inline-block;
        padding: 8px 16px;
        background-color: #f1f1f1;
        cursor:pointer;
    }
   </style>
    
    <script>
    
    
	  $(document).ready(function(){

        $("#confi").addClass("active");
        $("#confi").addClass("open");
        $("#sidebar-ui").addClass("show");
        $("#ksite").addClass("active");

		 });
    
    var uid = '<%=session.getAttribute("uid")%>';
	var userN = '<%=session.getAttribute("user")%>';

	var status ='<%= session.getAttribute("isLoggedIn")%>';
	var token = '<%=session.getAttribute("token") %>';
	var jwtToken = '<%=session.getAttribute("jwtToken") %>';

	var urole = '<%=session.getAttribute("urole") %>';
	var userpri = '<%=session.getAttribute("privileges") %>';

	 
	<%
	response.setHeader("Cache-Control","no-cache");
	response.setHeader("Cache-Control","no-store");
	response.setHeader("Pragma","no-cache");
	response.setDateHeader ("Expires", 0);
	
	 if(session!=null){
		  Object obj = session.getAttribute("isLoggedIn");
		    if(obj==null){
		   response.sendRedirect("/signin"); 
	       } 
		
			}else{
			response.sendRedirect("/signin"); 
			}  
	%> 
	
	var jDataTable;
	var jData;
	var jDatanew;
	var jDataTablenew;
	var incilat=0.0; 
  	var incilon=0.0;
  	
	   /***************************************************Angular Controller**************************************************************/

       var app = angular.module("app", []);
       app.controller("GetDataController", ['$scope','$http','$window', function(scope,http,window) {
    	  
    	   
    	   
    	   /********Initial value***********/
    	   scope.mdatasite=[];
    	   scope.mPaymentdata=[];
  	       var mParkingChargesUpdate=[];
  	       var SlotUpdateArr=[];
           var aslot = 0;
		   var mslotArray=[];
		   var mParkingCharges=[];
		   scope.arrVehicleReqdata=[];
		   scope.FsiteInfo="";
		   scope.dPaymentInfo="";
		   
	     var marker;
		   var mapZoom;
		   var map;
	     

    	   document.getElementById('overlid').style.visibility = "hidden";

    	   

    	   /*******************************GET VEHICLES****************************************/
          
    	   http.get(apiURL+'getVehicles/'+jwtToken+'/'+userN+'/doGetVehicleClass').
   			then(function (response) {
   			if(response.data != null)
   				{
   				if(response.data.status==1){
   			              
   					scope.vehiclelist= response.data.myObjectList;
   							
   		         }else{
   		        
   		        	scope.vehiclelist=[];	 
   		        	 
   		         }  
   		   }else{
   		     	
   		      }      		       		
   			}, function (response) {
			});
           
        
    	    
           /********************************GET LOCATION***************************************/
           
            http.get(apiURL+'getlocation/'+jwtToken+'/'+userN+'/doGetLocation').
   			then(function (response) {
   				
   			if(response.data != null)
   				{
   				if(response.data.status==1){
   			              
   					scope.locationlist= response.data.myObjectList;
    				scope.sitelocation=scope.locationlist[0].mSiteModel
   		         }  
   		    }else{
   		          	scope.locationlist=[];
   		      }      		       		
   			}, function (response) {
			});
           
           
           
           
           
           //----------------------------------Get Site Info-----------------------//
            scope.getAllSiteData = function(){
        	   
               http.get(apiURL+'getSite/'+jwtToken+'/'+userN+'/doGetSiteInfo').
					then(function (response) {
 						if(response.data != null)
						{
 							if(response.data.status==1){
 							        Pace.stop();
 					                document.getElementById('overl').style.visibility = "hidden";   
                                    jData = [];
				             		jData= response.data.myObjectList;
    				                jDataTable.clear().rows.add(jData).draw();
				         }else{
				        	 jData = [];
				       	     jDataTable.clear().rows.add(jData).draw();
				               } 
				      }
 						else{
				        	 jData = [];
				       	     jDataTable.clear().rows.add(jData).draw();
				             } 
					}, function (response) {
					});
           };
             
           
      		//******************************SITE LIST******************************//

    	    scope.siteTable = false;	 
   			scope.siteForm = false;
    	   
    		scope.sitedata = function () { 
   			
    		scope.siteTable = true;	 
   			scope.siteForm = true;
   			
   	        http.get(apiURL+'getSite/'+jwtToken+'/'+userN+'/doGetSiteInfo').
   					then(function (response) {
   	
   						if(response.data != null)
   						{
   					  
   						if(response.data.status==1){
   							       	jData = [];
   				             		jData= response.data.myObjectList;
        				            jDataTable.clear().rows.add(jData).draw();
   				         }else{
   				        	 jData = [];
   				       	     jDataTable.clear().rows.add(jData).draw();
   				               }  
   				   }else{
   				     	 jData = [];
   				   	     jDataTable.clear().rows.add(jData).draw();
   				      }   
   						
   					  $(function() {
 				 		    Pace.on("done", function(){
 				 		   });
 				 		});
 				  	    document.getElementById('overl').style.visibility = "hidden";
 				  	   
   					}, function (response) {
					});
   			 
   	};  	
   	/*********************************SITE DATA*************************************/
   	$(document).ready(function() {
   		 jDataTable = $('#siteTable').DataTable({ 
   	 		 	data: jData,
   		         		columns: [
   		         	    {'data':'sid'},
   		                
   		             {'data':'locationDetail', 'render':function(locationDetail){
	         				
 	             		 if(locationDetail!=null)
	             			 {
 	             			if(locationDetail!=""){
  	         					//return locationDetail.locationName;
 	         					return '<i class="fi flaticon-placeholder-2 text-info" style="font-size:13px;" aria-hidden="true"></i> ' +locationDetail.locationName;

   	         				}else{
   	         					return '<p>__</p>';
   	         				}
	             			 }else{
	             				return '<p>__</p>';
	             			 }
	         			}},
   		         		{'data':'siteName', 'render':function(siteName){
 	         				
    	             		 if(siteName!=null)
	             			 {
    	             			if(siteName!=""){
     	         					return '<i class="fi flaticon-placeholder-3 text-info" style="font-size:13px;" aria-hidden="true"></i> ' +siteName;
    	             				//return siteName;
      	         				}else{
      	         					return '<p>__</p>';
      	         				}
	             			 }else{
	             				return '<p>__</p>';
	             			 }
	         			}},
	         			{'data':'description', 'render':function(description){
 	         				
    	             		 if(description!=null)
	             			 {
    	             			if(description!=""){
     	         					return description;
      	         				}else{
      	         					return '<p>__</p>';
      	         				}
	             			 }else{
	             				return '<p>__</p>';
	             			 }
	         			}},
	         			{'data':'type', 'render':function(type){
 	         				
    	             		 if(type!=null)
	             			 {
    	             			if(type!=""){
    	             				
    	             				if(type == "offstreet")
    	             					{
         	         					//return '<p style="color: green;">'+type+'</p>';
         	         					return type;

    	             					}
    	             				else
    	             					{
         	         					return type;
    	             					}
     	         					
      	         				}else{
      	         					return '<p>__</p>';
      	         				}
	             			 }else{
	             				return '<p>__</p>';
	             			 }
	         			}},
   		             	   
	         		   
	         			{'data':'availableSlots', 'render':function(availableSlots){
 	         				
   	             		 if(availableSlots!=null)
	             			 {
   	             			if(availableSlots >0){
    	         					return availableSlots;
     	         				}else{
     	         					return availableSlots;
     	         				}
	             			 }else{
	             				return '<p>__</p>';
	             			 }
	         			}},
	         			
	         			{'data':'slots', 'render':function(slots){
 	         				
   	             		 if(slots!=null)
	             			 {
   	             			if(slots >0){
    	         					return slots;
     	         				}else{
     	         					return slots;
     	         				}
	             			 }else{
	             				return '<p>__</p>';
	             			 }
	         			}},
	         			
	         			
   		               {'data': "sid", 'render':function(sid){
	         				
	         				if(sid!=null){
	         					return '<u id="edit" title="Update Site!"style="color: #1f94f4; cursor:pointer; font-size:16px;"\><span class="glyphicon glyphicon-edit"></span></u>&nbsp&nbsp&nbsp&nbsp&nbsp<u id="downloadbtn" title="Download Info!"style="color: #1f94f4; cursor:pointer; font-size:16px;"\><span class="glyphicon glyphicon-download"></span></u>';
        	    				 
	        	         		//	<u id="editSlot" title="Update Slot!" style="color: #1f94f4;padding-left: 14px; text-decoration: none;  cursor:pointer; font-size:16px;"\>Update Slot</u>';
	        	         				}
	         			}}  
   		            ],
   		           buttons: [
   		        	    'copy', 'excel', 'pdf'
   		        	  ],
   	
   		 	             "ordering": false,
   		 	             "searching": false,
   		 	             "deferRender": true,
   		 	             "autoWidth": false,
   		 	             "lengthChange": false,
   		 	             "language": {
   		     	    		"infoEmpty": "No records to show",
   		         	    	"emptyTable": "not record found",
   		     	    	    }
   		    	        });
   		  
   	  }); 
   	
    	   
    
	 //**************SELECT DESELECT TABLE ROW******************************//
	 $('#siteTable').on('click', 'tr', function () {
	 	  
		 if ( $(this).hasClass('row_selected'))
	 	  		{  
	 			   $("#siteTable tbody tr").removeClass('row_selected');
	 	  	       $(this).removeClass('row_selected');
 	 	  		   $("#updatebtn").hide();
 	 	  		   $("#updateslot").hide(); 	
	 	  		   $("#updatePayment").hide(); 	
	               $("#submitbtn").show();
	               $("#update").hide();
	               $("#allp").show();
	               scope.showPass = false;
	               scope.SiteModel = {};
	               
	               incilat=0;
	   		       incilon=0;
	   		       

	   			var latlng ={"lat":incilat,"lng":incilon};
	   			geocodeService.reverse().latlng(latlng).run(function (error, result) {
	   				 
	   			     if (marker != undefined) {
	   			    	 map.removeLayer(marker);
	   			    	 if (error) {			 	   		  	  
	   			   	       return;
	   			   	     }	        
	   			   }
	   			 
	   			     marker=  L.marker(latlng).addTo(mapZoom).bindPopup(result.address.Match_addr).openPopup(); 
	   			     map.fitBounds(mapZoom.getBounds());
	   			   });    

	   		        
	   		        for(var i=0;i<scope.vehiclelist.length;i++)
	            	   {
			    	   $("#slot"+i).val('');
	            	   }

	   	  		   scope.$apply();

	 	  	   
	 	  	    }else
	 	  	        {
	 	  	        $("#siteTable tbody tr").removeClass('row_selected');
	 	  	        $(this).addClass('row_selected');
	 	  	
	 	  	    }
	 }); 
	 
	 
	 
	  //************************* download btn*******************************//
	  	$('#siteTable').on('click','#downloadbtn', function () {
	  		
	  		var tr = $(this).closest('tr');
	  		var selectedData = jDataTable.row(tr).data();
	  		scope.arrayPaymentInf=[];
	  		
	 	    scope.dSiteInfo=[];
   		    scope.dataRequest = {
     				"siteId": selectedData.sid
   		    };
     		
			scope.requestedData = {
					"sessionTokken" : jwtToken,
					"username":userN,
					"functionName" : "doSlotsBySiteId",
		  			 "data" : JSON.stringify(scope.dataRequest)
				};
      	        
            document.getElementById('overl').style.visibility = "visible"; 

		  http({
					method :'POST',
					url :apiURL+'getSlotBySiteId',
					data :scope.requestedData
					}).then(function (response) {	
	 					if(response.data.status==1){

	 						scope.dSiteInfo= response.data.myObjectList;
	 							 						
	 						scope.requestedData = {
	 								"sessionTokken" : jwtToken,
	 								"username":userN,
	 								"functionName" : "doGetChargesbySiteId",
	 					  			 "data" : JSON.stringify(scope.dataRequest)
	 							};
	 			       	        
	 		        		http({
	 							method :'POST',
	 							url :apiURL+'getSlotBySiteId',
	 							data :scope.requestedData
	 							}).then(function (response) {	
	 			 					if(response.data.status==1){
	 			 						scope.dPaymentInfo=response.data.myObjectList;
	 			 						
	 			 						scope.FsiteInfo=scope.dSiteInfo;
	 			 						
	 			 						console.log("scope.FsiteInfo",scope.FsiteInfo);
	 			 						console.log("scope.dPaymentInfo",scope.dPaymentInfo);
	 			 						scope.arrVehicleReqdata=[];
	 			 						for(var i=0;i<scope.FsiteInfo.length;i++)
	 			 							{
	 			 							
	 			 							scope.requestdata={
	 			 									"className": scope.FsiteInfo[i].mVehicleClassModel.className,
	 		 			 							"description":scope.FsiteInfo[i].mVehicleClassModel.description,
	 		 			 							"slot": scope.FsiteInfo[i].slot,
	 		 			 							"occupiedSlot":scope.FsiteInfo[i].occupiedSlot,
	 		 			 							"availableVehicleSlot": scope.FsiteInfo[i].availableVehicleSlot,
	 		 			 							"priceHour": scope.dPaymentInfo[i].priceHour,
	 		 			 							"dayPrice": scope.dPaymentInfo[i].dayPrice,
	 		 			 							"monthPrice":scope.dPaymentInfo[i].monthPrice
	 			 							}
	 			 							scope.arrVehicleReqdata.push(scope.requestdata);
	 			 						
	 			 							}
	 			 						
	 			 						scope.DownloadRequestData={
	 			 							 "sid": scope.FsiteInfo[0].mSiteModel.sid,
	 			 							 "siteName": scope.FsiteInfo[0].mSiteModel.siteName,
	 			 							 "description": scope.FsiteInfo[0].mSiteModel.description,
	 			 							 "type": scope.FsiteInfo[0].mSiteModel.type,
	 			 							 "slots": scope.FsiteInfo[0].mSiteModel.slots,
	 			 							 "siteStatus": scope.FsiteInfo[0].mSiteModel.siteStatus,
	 			 							 "longitude": scope.FsiteInfo[0].mSiteModel.longitude,
	 			 							 "latitude": scope.FsiteInfo[0].mSiteModel.latitude,
	 			 							 "availableSlots": scope.FsiteInfo[0].mSiteModel.availableSlots,
	 			 							 "locationDetail": {
	 			 							  "id": scope.FsiteInfo[0].mSiteModel.locationDetail.id,
	 			 							  "locationName": scope.FsiteInfo[0].mSiteModel.locationDetail.locationName,
	 			 							  "status": scope.FsiteInfo[0].mSiteModel.locationDetail.status,
	 			 							  "lon": scope.FsiteInfo[0].mSiteModel.locationDetail.lon,
	 			 							  "lat": scope.FsiteInfo[0].mSiteModel.locationDetail.lat
	 			 							 },
	 			 							 "operatingCompany": scope.FsiteInfo[0].mSiteModel.operatingCompany,
	 			 							 "isSlotAvailable": scope.FsiteInfo[0].mSiteModel.isSlotAvailable,
	 			 							 "isReentryAllowed": scope.FsiteInfo[0].mSiteModel.isReentryAllowed,
	 			 							 "isMonthlyPassAllowed": scope.FsiteInfo[0].mSiteModel.isMonthlyPassAllowed,
	 			 							 "monthlyPassCost": scope.FsiteInfo[0].mSiteModel.monthlyPassCost,
	 			 							 "isOperationalinNight": scope.FsiteInfo[0].mSiteModel.isOperationalinNight,
	 			 							 
	 			 							 "mVehicleClassModel":scope.arrVehicleReqdata
	 			 						}
	 			 						
	 			 					    var link = document.createElement("a");
	 			 			        	link.download = scope.FsiteInfo[0].mSiteModel.siteName+".json";
	 			 			        	var data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(scope.DownloadRequestData, null, " "));
	 			 			        	link.href = "data:" + data;
	 			 			        	link.click();
	 			 						
	 						            document.getElementById('overl').style.visibility = "hidden"; 

	 		 						}else if(response.data.status==0)
	 								{
	 		 		 				    scope.dPaymentInfo=[];
	 									scope.message = response.data.message;
	 		 							
	 								}else{
	 									scope.message = response.data.message;
	 							    } 
	 			 				}).error(function(response) {			 
	 							});  
	 						
				            document.getElementById('overl').style.visibility = "hidden"; 

	 						
	 						
	 						
						}else if(response.data.status==0)
						{
							scope.message = response.data.message;
							
						}else{
							scope.message = response.data.message;
					    } 
	 				}).error(function(response) {			 
			});
	  		
	  		
	  	
	  		
	  		
	  		
  	        	
	  	    
		 }); 


	  
	  
	  
	  
	  
	  
	  
	   //*************************UPDATE CLICK*******************************//
	  	$('#siteTable').on('click','#edit', function () {
	   		
	  		scope.SiteModel = "";
	  		scope.$apply();

	  		var tr = $(this).closest('tr');
	  		var selectedData = jDataTable.row(tr).data();
	  			  	
	  		
	  		incilat=selectedData.latitude;
		    incilon=selectedData.longitude;    
			var latlng ={"lat":incilat,"lng":incilon};
			geocodeService.reverse().latlng(latlng).run(function (error, result) {
				 
			     if (marker != undefined) {
			    	 map.removeLayer(marker);
			    	 if (error) {			 	   		  	  
			   	       return;
			   	     }	        
			   }
			 
			     marker=  L.marker(latlng).addTo(mapZoom).bindPopup(result.address.Match_addr).openPopup(); 
			     map.fitBounds(mapZoom.getBounds());
			   }); 
             $("#updatebtn").show();
             $("#updateslot").show();
             $("#updatePayment").show();
             $("#submitbtn").hide();
             $("#update").show();
             $("#allp").hide();
             scope.showPass = false;

             scope.SiteModel =selectedData;
	   	     if(selectedData.isMonthlyPassAllowed == true)
				{   
				document.getElementById("monthlypasscheck").checked;
                scope.showPass = true;
	 		    }  
	   	      if(selectedData.isReentryAllowed == true)
			    {   
			    document.getElementById("reentry").checked;
		        }  
	   	     if(selectedData.monthlyPassCost!=""){
	   	       scope.monthlyPassCost=selectedData.monthlyPassCost;
	   	      }
	   	     
	   	     
	   	    
	   	      //scope.viewOnMap();
	  		  scope.$apply();
			}); 
	   
	   
	 
	  //*************************SLOT UPDATE CLICK*******************************//
	  	$('#siteTable').on('click','#editSlot', function () {
			   document.getElementById('overl').style.visibility = "visible"; 

	  		scope.SiteModel = "";
	  		scope.$apply();
	  		var tr = $(this).closest('tr');
	  		var selectedData = jDataTable.row(tr).data();
	  			  	
             $("#updatebtn").hide();
             $("#submitbtn").hide();
             $("#update").hide();
             $("#allp").hide();
             $("#updateslotFrom").show();
             $("#siteForm").hide();
            
 	  		 
             scope.siteTable = false;
             scope.updateslotFrom = true;
             scope.siteForm = false;

	   	     scope.$apply();

	      		scope.dataRequest = {
	      				"siteId": selectedData.sid
	    		 };
	      		
				scope.requestedData = {
						"sessionTokken" : jwtToken,
						"username":userN,
						"functionName" : "doSlotsBySiteId",
			  			 "data" : JSON.stringify(scope.dataRequest)
					};
	       	        
				http({
						method :'POST',
						url :apiURL+'getSlotBySiteId',
						data :scope.requestedData
						}).then(function (response) {	
		 					if(response.data.status==1){
		 						scope.datasite=response.data.myObjectList;
				 				scope.allslotdata=scope.datasite;
				 				scope.msite=scope.datasite[0].mSiteModel;
		     		            $('<div class="form-group row"><label for="mSiteModel" class="col-md-4 form-control-label">Site Name</label><div class="col-md-7"><input type="text" class="form-control" id="msite" name="mSiteModel" value="'+scope.msite.siteName+'" readonly="readonly" required="required" ng-model="msite"/></div></div>').appendTo('#UpdateSlotSite');   
 
				 				for(var i=0;i<scope.datasite.length;i++)
				 					{
			     		            $('<div class="form-group row"><label for="'+scope.datasite[i].mVehicleClassModel.className+'"class="col-md-4 form-control-label">'+scope.datasite[i].mVehicleClassModel.className+'</label><div class="col-md-7" ><input type="number" class="form-control" ng-model="scope.mslotdata" name="'+scope.datasite[i].mVehicleClassModel.className+'" value="'+scope.datasite[i].slot+'"  id="slot'+i+'" placeholder="Enter '+scope.datasite[i].mVehicleClassModel.className+' Slot"   required="required" /></div></div> ').appendTo('#UpdateSlotField');   
				 					}
					            document.getElementById('overl').style.visibility = "hidden"; 

	 						}else if(response.data.status==0)
							{
								scope.message = response.data.message;
	 							
							}else{
								scope.message = response.data.message;
						    } 
						}, function (response) {
						});
					 
	   	     
			}); 
	  //------------------IF CHECK ENTRY PASS ALLOWED-------//
		   
		   scope.ischeckReentry = 0;
		  scope.checkReentry = function(){
	 		if($('#reentry').is(':checked')){
				
	 			scope.ischeckReentry = 1;
	 			
			}else{
				scope.ischeckReentry= 0;
	 		}
		   }
		  
		  
	  //------------------IF CHECK MONTHLY PASS ALLOWED-------//
	 		scope.monthlyPassCost =0;
		    scope.isMonthlyPassAllowed =0;
			scope.checkMonthlyPass = function(){
				
	 		if($('#monthlypasscheck').is(':checked')){
                scope.showPass = true;
	 			 scope.isMonthlyPassAllowed = 1;
	 			
			}else{
	 			scope.showPass = false;
	 			scope.isMonthlyPassAllowed =0;
	 			scope.monthlyPassCost =0;
	 			scope.monthlyPassCost =0;
	 		}
		   }

		
		//**************ADD SITE DATA******************************//
		  scope.submitForm = function(SiteModel){
		    	scope.submitted = true;
		        	  if(typeof scope.SiteModel  !== 'undefined'){
		       		  
		       		  if(scope.SiteModel.hasOwnProperty("locationDetail")){
		   	  		   scope.locationDetail = scope.SiteModel.locationDetail;
		   	  	      }else{
		   	  		     scope.locationDetail ="";
		   	  	      }
		 		  
		       		  if(scope.SiteModel.hasOwnProperty("siteName")){
			    	  		   scope.siteName = scope.SiteModel.siteName;
			    	  	   }else{
			    	  		   scope.siteName ="";
			    	  	   }
		      		  
		      		  if(scope.SiteModel.hasOwnProperty("description")){
			    	  		   scope.description = scope.SiteModel.description;
			    	  	   }else{
			    	  		   scope.description ="";
			    	  	   }
		      		
		      		  if(scope.SiteModel.hasOwnProperty("type")){
			  		            scope.type = scope.SiteModel.type;
			  	            }else{
			  		             scope.type ="";
			  	            }
		      		  
		       		 if(scope.SiteModel.hasOwnProperty("operatingCompany")){
				            scope.operatingCompany = scope.SiteModel.operatingCompany;
			            }else{
				             scope.operatingCompany ="";
			            }
		       		 
		       		if(scope.SiteModel.hasOwnProperty("monthlyPassCost")){
			            scope.monthlyPassCost = scope.SiteModel.monthlyPassCost;
		            }else{
			             scope.monthlyPassCost ="";
		            }
		      		  
		      		
				scope.locationdata = {
			      			
						"id":SiteModel.locationDetail.id,
						"locationName":SiteModel.locationDetail.locationName,
						"lon":SiteModel.locationDetail.lon,
						"lat":SiteModel.locationDetail.lat,
			      	};	
		 		
		      		scope.dataRequest = {
		      				
		      				"siteName": scope.siteName,
		    				"description": scope.description,
		     				"type": scope.type,
		    				"latitude":incilat,
		    	      		"longitude":incilon,
		    	      		"locationDetail":scope.locationDetail,
		     	      		"operatingCompany":scope.operatingCompany,
		    	      		"isReentryAllowed":scope.ischeckReentry,
		    	      		"isSlotAvailable":1,
		    	      		"isMonthlyPassAllowed": scope.isMonthlyPassAllowed,
		    	      		"monthlyPassCost": scope.monthlyPassCost,
		    	      		"isOperationalinNight":1,
		    	      		"availableSlots":0,
		    	      	    "slots":0
		    	      		
		              };
		      		
					scope.requestedData = {
							"sessionTokken" : jwtToken,
							"username":userN,
							"functionName" : "doAddSiteInfo",
				  			 "data" : JSON.stringify(scope.dataRequest)
						};
		       	   
				//VALIDATION
				//if (scope.addUser.$valid) 
					//{
					if( scope.siteName != "" && scope.type != "" && document.getElementById('latitude').value !="" &&  document.getElementById('longitude').value !="" )
					  {
						   document.getElementById('overlid').style.visibility = "visible"; 
			        		
					        http({
							method :'POST',
							url :apiURL+'addSite',
							data :scope.requestedData
							}).then(function (response) {	
			 					if(response.data.status==1){
			 						
			 						scope.getAllSiteData(); //REFRESH DATATABLE WITH ALL SITE INFO
		  	 						scope.response_message = response.data.message;
		 	 						var x = document.getElementById("snackbar_success")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");
		   							},1000);
					 				
					 				// ---checking for latest added site-------------//
					 				
					 				if(response.data.myObjectList !=null)
					 					{
					 					scope.lastAddedSite = response.data.myObjectList;
					 					scope.mSiteData = response.data.myObjectList;
					 					}else{
					 						
					 					}
					 				
					 				scope.VehicleFrom = true;
					 			    scope.siteTable = false;	 
					 				scope.siteForm = false;
					 				scope.paymentFrom = false;
					 				 
					 				
							        document.getElementById('overlid').style.visibility = "hidden"; 
			
		 						}else if(response.data.status==0)
								{
									scope.message = response.data.message;
		 							var x = document.getElementById("snackbar_error")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");	
										 $('#paymentform').modal('hide');
									}, 1500);
					 				document.getElementById('overlid').style.visibility = "hidden";
			 						
			 						
								}else{
									scope.message = response.data.message;
									document.getElementById('overlid').style.visibility = "hidden";
							    } 
							}, function (response) {
							}); 
						 
			     	  } else  {
			   				
						  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		                
						 } 
					
						//}
					/* else  {
			   			  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		              
						 }   */
		        }// end of validate 
		     else  {
			   			  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		              
						 }  	  
		      };
		    
		      /***************************************Add Button Of Slot*************************************************/
		      
		      scope.addSlotButton=function()
		       {
		    	  mslotArray=[];

		    	  for(var i=0; i<scope.vehiclelist.length;i++)
		    		{
		    		  
		    			 var mSlot=$("#slot"+i).val();
		    		      if(mSlot!=null && mSlot!="")
		    			  {
		    				 scope.dataRequest = {
						     			"mSiteModel":scope.mSiteData,
						 				"mVehicleClassModel":scope.vehiclelist[i],
						 				"slot":mSlot,
						 				"occupiedSlot":0,
						 				"availableVehicleSlot":mSlot
						 	  };
		    				 
			    			  mslotArray.push(scope.dataRequest);
		    				}
		    			    else
		    			    {
		    				  scope.message = "Above Fields cannot be Left Blank !";
							  var x = document.getElementById("snackbar_error")
								x.className = "show";
								setTimeout(function() {
									x.className = x.className.replace("show", "");
								}, 3000);
		    				 
		    				 }
		    			
		    		 }//end of for loop
		    	  
		      	     scope.requestedData = {
						  "sessionTokken" : jwtToken,
						  "username":userN,
						  "functionName" : "doAddSlotInfo",
			  			  "data" : JSON.stringify(mslotArray)
					  };
	 			
					  if(scope.mSiteData!= null && mslotArray.length > 0 && mslotArray.length == scope.vehiclelist.length )
					      { 
			 		       document.getElementById('overlid').style.visibility = "visible";
	
					        http({
							method :'POST',
							url :apiURL+'addSlotInfo',
							data :scope.requestedData
							}).then(function (response) {	
			 					if(response.data.status==1){
			 						
			 						scope.response_message = response.data.message;
		 	 						var x = document.getElementById("snackbar_success")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 200);
		 			 				
		 			 				scope.VehicleFrom = false;
						    	    scope.siteTable = false;	 
									scope.siteForm = false;
									scope.paymentFrom = true;  
		 			 				
								}else if(response.data.status==0)
								{
									scope.message = response.data.message;
									
									var x = document.getElementById("snackbar_error")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 1000);
			 						
			 						
								}else{
									scope.message = response.data.message;
							    } 
			 					
								document.getElementById('overlid').style.visibility = "hidden";
	
							}, function (response) {
							});
						
			     	  } else  {
			   				
						  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		               
					 } 	//end of if 	    	 
		    	 		    	  
		         }//end of btn
		      
		  
	      
		     //**********************************************Add Payment Button*****************************************//
			  scope.paymentadd=function(){
			    
		         scope.formsubmitted = true;
			     mParkingCharges=[];
			     
			     for(var i=0;i<scope.vehiclelist.length;i++)
			     {
			    	 var dpriceHour=$("#priceHour"+i).val();
		    		 var ddayPrice=$("#dayPrice"+i).val();
		    		 var dmonthPrice=$("#monthPrice"+i).val();

		    		 if(dpriceHour != '' && ddayPrice != "" && dmonthPrice != "")
					 { 
						
			    	   scope.requestData={
				      	"mSiteModel":scope.mSiteData,
						"mVehicleClassModel":scope.vehiclelist[i],
		    		    "priceHour":dpriceHour,
		    		    "dayPrice":ddayPrice,
		    		    "monthPrice":dmonthPrice
			    	    }
			    	
			    	   mParkingCharges.push(scope.requestData);
			    	  
					  }
		    		  else {
			    		 
						  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		               
						 } 
			        }//end of for loop
			     

				scope.requestedData = {
						"sessionTokken" : jwtToken,
						"username":userN,
						"functionName" : "doAddPaymentInfo",
			  			 "data" : JSON.stringify(mParkingCharges)

					};
				
			   if(mParkingCharges.length > 0 && mParkingCharges.length == scope.vehiclelist.length )
				   {
	 		      
				   document.getElementById('overlid').style.visibility = "visible";

				   http({
						method :'POST',
						url :apiURL+'addPayment',
						data :scope.requestedData
						}).then(function (response) {	
		 					if(response.data.status==1){
		 						
		 						scope.response_message = response.data.message;
		 						var x = document.getElementById("snackbar_success")
				 				x.className = "show";
				 				setTimeout(function() {
									x.className = x.className.replace("show", "");					 
								}, 3000);
                          	    window.location.href = '${pageContext.servletContext.contextPath}/map';
			        	   
							}else if(response.data.status==0)
							{
								scope.message = response.data.message;
								
								var x = document.getElementById("snackbar_error")
				 				x.className = "show";
				 				setTimeout(function() {
									x.className = x.className.replace("show", "");					 
								}, 3000);
				 				
							}else{
								scope.message = response.data.message;
						    } 
		 					
							document.getElementById('overlid').style.visibility = "hidden";

						}, function (response) {
						});
				   }//end of if 
		     
		         };
      
    
	          //GO BACK TO SITE PAGE-------  
              scope.gobackto = function(){
		    	 
		    	    location.reload();
		
		    	    scope.paymentFrom = false;
		    	    scope.siteTable = true;	 
		  			scope.siteForm = true;
		  			
		   			scope.SiteModel = "";
		   		    document.getElementById('latitude').value = "";
		  			document.getElementById('longitude').value = "";  
		  			scope.submitted = false;
		     	    scope.formsubmitted = false;
  			 
               };
          
         
       /******************UPDATE SITE**************************************************/
         
    	scope.updatesite=function(SiteModel)
    	{
    		 scope.submitted = true;
    		   incilat=document.getElementById('latitude').value;
    		   incilon=document.getElementById('longitude').value;
    		 	scope.dataRequest = {
		      				"sid":SiteModel.sid,
		      				"siteName": SiteModel.siteName,
		    				"description": SiteModel.description,
		     				"type": SiteModel.type,
		    				"latitude":incilat,
		    	      		"longitude":incilon,
		    	      		"locationDetail":SiteModel.locationDetail,
		     	      		"operatingCompany":SiteModel.operatingCompany,
		    	      		"isReentryAllowed":SiteModel.isReentryAllowed,
		    	      		"isSlotAvailable":SiteModel.isSlotAvailable,
		    	      		"isMonthlyPassAllowed": SiteModel.isMonthlyPassAllowed,
		    	      		"monthlyPassCost": SiteModel.monthlyPassCost,
		    	      		"isOperationalinNight":SiteModel.isOperationalinNight,
		    	      		"availableSlots":SiteModel.availableSlots,
		    	      	    "slots":SiteModel.slots
		    	      		
		              };
		      	scope.requestedData = {
							"sessionTokken" : jwtToken,
							"username":userN,
							"functionName" : "doUpdateSiteInfo",
				  			 "data" : JSON.stringify(scope.dataRequest)
		
						};
		      	    /***********************VALIDATION**********************/
					if (scope.addUser.$valid) 
					{	
					  if(SiteModel.locationDetail != undefined && SiteModel.siteName != undefined  && SiteModel.description != undefined  &&  SiteModel.type != undefined && document.getElementById('latitude').value !=""&&  document.getElementById('longitude').value !="")
					   {    
						  document.getElementById('overlid').style.visibility = "visible";
						  			
					        http({
							method :'POST',
							url :apiURL+'updateSite',
							data :scope.requestedData
							}).then(function (response) {	
			 					if(response.data.status==1){
			 						
			 						scope.response_message = response.data.message;
			 						var x = document.getElementById("snackbar_success")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 3000);
					 				 setTimeout(function() {	
			 							 location.reload();
			  						}, 1500); 
								}else if(response.data.status==0)
								{
									scope.message = response.data.message;
									var x = document.getElementById("snackbar_error")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 3000);
					 			
									
								}else{
									scope.message = response.data.message;
									
							    } 
				 				document.getElementById('overlid').style.visibility = "hidden";

			  					      
							}, function (response) {
							});						 
			     	  } else  {
			   				
						  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		              
						 } 
					
					 
		     	  }// end of validate for angularjs
		     	  
				else  {
			   				
						  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		             
				} 
    	};//end of btn
    
	   //BACK TO TABLE
	   scope.gobacktomain = function(){
		  $('#UpdateSlotField').empty();
	      $('#UpdateSlotSite').empty();
	      $('#updateSlotbtn').empty();
	     scope.siteTable = true;	 
		 scope.siteForm = true;
         scope.updateslotFrom = false;
         $("#submitbtn").show();
         $("#allp").show();

	     };	 
	     
	     
	     /*********************************update slot************************************/
	     
	     scope.mUpdateSlotModelView=function(sensorId)
	     {
	    	
	    	 scope.mdatasite=[];
	    	 scope.dataRequest = {
	      				"siteId": sensorId
	    		 };
	      		
				scope.requestedData = {
						"sessionTokken" : jwtToken,
						"username":userN,
						"functionName" : "doSlotsBySiteId",
			  			 "data" : JSON.stringify(scope.dataRequest)
					};
	       	        
				http({
						method :'POST',
						url :apiURL+'getSlotBySiteId',
						data :scope.requestedData
						}).then(function (response) {	
		 					if(response.data.status==1){
		 					
				 					
				 					scope.mdatasite=response.data.myObjectList;	
					 				scope.msite=scope.mdatasite[0].mSiteModel;

				 					
					            document.getElementById('overl').style.visibility = "hidden"; 

	 						}else if(response.data.status==0)
							{
								scope.message = response.data.message;
	 							
							}else{
								scope.message = response.data.message;
						    } 
						}, function (response) {
						}); 
	    	 
	          $('#myModalSlot').modal('show');
	          //scope.$apply();

	     }
	     
	     
	     /**********************Update Payment Model View*******************************/
	     scope.mUpdatePaymentModelView=function(sensorId){
	    	 scope.mPaymentdata=[]; 
	   			
	    	   scope.dataRequest = {
	   					"siteId":sensorId
	   			};
	   	        	  
	   		
	   			scope.requestedData = {
						"sessionTokken" : jwtToken,
						"username":userN,
						"functionName" : "doGetChargesbySiteId",
			  		     "data" : JSON.stringify(scope.dataRequest)
				};
	   			
			         http({
						method :'POST',
						url :apiURL+'getChargesBySiteId',
						data :scope.requestedData
						}).then(function (response) {	
							
							if(response.data.status==1)
								{
								scope.mPaymentdata=response.data.myObjectList;
								scope.sitePayment=scope.mPaymentdata[0].mSiteModel;
								}
							else
								{
						    	scope.mPaymentdata=[]; 
								}
						
						}, function (response.data) {
						});  
			        
			        
	    	  /***Payment Update Model**/
	          $('#myModalPayment').modal('show');

	     }
	     
	     
	     /*******************************Payment Update*************************************/
	     
	     scope.paymentupdates=function(mSiteData,paymentData)
	     {
	    	 mParkingChargesUpdate=[];
	    	 for(var i=0;i<paymentData.length;i++)
	    		 {
	    		 var uPH=$('#updatePriceHour'+i).val();
	    		 var uDP=$('#updateDayPrice'+i).val();
	    		 var uMP=$('#updateMonthPrice'+i).val();

	    		 if(uPH !="" && uDP != "" && uMP != "")
	    			 {
	    			  scope.requestData={
	    					"paymentId":paymentData[i].paymentId,
	  				      	"mSiteModel":paymentData[i].mSiteModel,
	  						"mVehicleClassModel":paymentData[i].mVehicleClassModel,
	  		    		    "priceHour":uPH,
	  		    		    "dayPrice":uDP,
	  		    		    "monthPrice":uMP
	  			    	 }
	    			  mParkingChargesUpdate.push(scope.requestData);
	  				 }
	  		    	 else  {
	  			    		 
	  						  scope.message = "Above Fields cannot be Left Blank !";
	  						  var x = document.getElementById("snackbar_error")
	  							x.className = "show";
	  							setTimeout(function() {
	  								x.className = x.className.replace("show", "");
	  							}, 3000);
	  				 }  
	    		 }//end of for loop
	    		 

	    	      scope.requestedData = {
						"sessionTokken" : jwtToken,
						"username":userN,
						"functionName" : "doUpdatePaymentInfo",
			  			 "data" : JSON.stringify(mParkingChargesUpdate)

					};
	    	 

  	    	   if(mParkingChargesUpdate.length == paymentData.length)
  	    		   {
  	    		    document.getElementById('overlid').style.visibility = "visible";

			        http({
					method :'POST',
					url :apiURL+'updatePayment',
					data :scope.requestedData
					}).then(function (response) {	
	 					if(response.data.status==1){
	 						
	 						scope.response_message = response.data.message;
	 						var x = document.getElementById("snackbar_success")
			 				x.className = "show";
			 				setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 2000);
							
			 				setTimeout(function() {	
	 							 location.reload();
	  						}, 1500);
			 				 
						}else if(response.data.status==0)
						{
							scope.message = response.data.message;
							var x = document.getElementById("snackbar_error")
			 				x.className = "show";
			 				setTimeout(function() {
								x.className = x.className.replace("show", "");					 
							}, 3000);
			 			
							
						}else{
							scope.message = response.data.message;
							var x = document.getElementById("snackbar_error")
			 				x.className = "show";
			 				setTimeout(function() {
								x.className = x.className.replace("show", "");					 
							}, 3000);
			 			
					    } 
	 					
						document.getElementById('overlid').style.visibility = "hidden";

	  					      
					}, function (response) {
					});    
  	    		 }
  	    	   else
  	    		   {
					  scope.message = "Above Fields cannot be Left Blank !";
					  var x = document.getElementById("snackbar_error")
						x.className = "show";
						setTimeout(function() {
							x.className = x.className.replace("show", "");
						}, 3000);
	              
  	    		   }
  	    	   
	     }//end of update charges
	     
	     
	     
	     
	   
         /****************************Update Slot***********************************************/
         
	     scope.updateslotfinal = function(SiteModel){
	    	 
	    	 aslot = 0;
	    	 
	    	 for(var i=0;i<SiteModel.length;i++)
	    		 {
	    		 
	    		 var slotValue=$("#slot"+i).val();
	    		 
	 	    	 if(document.getElementById('slot'+i+'').value !="" && document.getElementById('slot'+i+'').value !="" && document.getElementById('slot'+i+'').value !="")
	    		  {
	 	    		 if(slotValue>0)
	 	    			 {
	 	    			aslot = slotValue-SiteModel[i].occupiedSlot;
	 	    			 }
 	 	    		 
	 	              scope.dataRequest = {
	 		    		    "slotId":SiteModel[i].slotId,
	 	    	      		"availableVehicleSlot":aslot,
	 	      				"mSiteModel": SiteModel[i].mSiteModel,
	 	    				"mVehicleClassModel": SiteModel[i].mVehicleClassModel,
	 	     				"occupiedSlot": SiteModel[i].occupiedSlot,
	 	    				"slot":slotValue
	 	    	      }
	 	              SlotUpdateArr.push(scope.dataRequest);
	    		  }//end of if condition
	 	    	 
	    		}//end of for loop

	    	    scope.requestedData = {
						"sessionTokken" : jwtToken,
						"username":userN,
						"functionName" : "doUpdateSlotInfo",
			  			 "data" : JSON.stringify(SlotUpdateArr)
					};
	    	    
	    	    if(SiteModel.length>0)
	    	    	{
	    	    	   document.getElementById('overlid').style.visibility = "visible";

	    	    	 http({
							method :'POST',
							url :apiURL+'updateSlotInfo',
							data :scope.requestedData
							}).then(function (response) {	
			 					if(response.data.status==1){
			 						
			 						scope.response_message = response.data.message;
			 						var x = document.getElementById("snackbar_success")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 3000);
					 				 setTimeout(function() {	
			 							 location.reload();
			  						}, 1500); 
								}else if(response.data.status==0)
								{
									scope.message = response.data.message;
									var x = document.getElementById("snackbar_error")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 3000);
					 			
								}else{
									scope.message = response.data.message;
									
							    } 
			 		    	   document.getElementById('overlid').style.visibility = "hidden";
	
			  					      
							}, function (response) {
							}); 
	    	    	}
	     }//end of update slot

	     
	     
	     
	     
	     
	     /**********************************map*********************************************/

	     
	    
		  
		    map= L.map('map').setView([21.7679,78.8718], 4);
		  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		    attribution: 'Map'
		  }).addTo(map);  
		  var smallIcon = new L.Icon({
		            iconSize: [27, 27],
		            iconAnchor: [13, 27],
		            popupAnchor: [1, -24],
		            iconUrl: 'https://cdn4.iconfinder.com/data/icons/small-n-flat/24/map-marker-512.png'
		        });

		  var arcgisOnline = L.esri.Geocoding.arcgisOnlineProvider();

		  var searchControl = L.esri.Geocoding.geosearch({
		    providers: [
		      arcgisOnline,
		      L.esri.Geocoding.mapServiceProvider({
		        label: 'States and Counties',
		        url: 'https://sampleserver6.arcgisonline.com/arcgis/rest/services/Census/MapServer',
		        layers: [2, 3],
		        searchFields: ['NAME', 'STATE_NAME']
		      })
		    ]
		  }).addTo(map);
		  
		  mapZoom = L.featureGroup().addTo(map);
		  setInterval(function (){
		         map.invalidateSize();
		       }, 100);
		 
		var geocodeService = L.esri.Geocoding.geocodeService();
		
		//var popup = L.popup();
		function onMapClick(e) {
		 //marker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);
		//popup.setLatLng(e.latlng).setContent("You clicked the map at " + e.latlng.toString()).openOn(map);
		 incilat=e.latlng.lat;
		 incilon=e.latlng.lng;  
		 document.getElementById('latitude').value = e.latlng.lat;
		 document.getElementById('longitude').value = e.latlng.lng;   
		 geocodeService.reverse().latlng(e.latlng).run(function (error, result) {	
		     if (marker != undefined) {
		         map.removeLayer(marker);
		         if (error) {
		             return;
		           } 
		   };
		     marker=  L.marker(result.latlng).addTo(map).bindPopup(result.address.Match_addr).openPopup();
		   });
		}
		map.on('click', onMapClick);

		scope.viewOnMap=function()
		{ 
			if( document.getElementById('latitude').value !=""&&  document.getElementById('longitude').value !="")
				{
				incilat=document.getElementById('latitude').value;
				   incilon=document.getElementById('longitude').value;    
					var latlng ={"lat":incilat,"lng":incilon};
					 geocodeService.reverse().latlng(latlng).run(function (error, result) {
						 
					     if (marker != undefined) {
					    	 map.removeLayer(marker);
					    	 if (error) {			 	   		  	  
					   	       return;
					   	     }	        
					   }
					     marker=  L.marker(latlng).addTo(mapZoom).bindPopup(result.address.Match_addr).openPopup(); 
					     map.fitBounds(mapZoom.getBounds());
					   }); 
				
				}else
					{
					  scope.message = "Fields cannot be Left Blank !";
					  var x = document.getElementById("snackbar_error")
						x.className = "show";
						setTimeout(function() {
							x.className = x.className.replace("show", "");
						}, 3000);
					}
		   
		} 
		
   }]);
       
 
</script> 

</head>
 <body class="" ng-controller="GetDataController"  class="ng-cloak" ng-init="sitedata()">
 
  <%@ include file="Sidebar.jsp"%>	
  
    <div class="content-wrap" >
      <!-- main page content. the place to put widgets in. usually consists of .row > .col-lg-* > .widget.  -->
      <main id="contentt" class="content" role="main" style="  padding-bottom: 70px;" class="ng-cloak">
        <!-- Page content -->
        <!-- add Site data form -->
        <div class="row" ng-show="siteForm">
         <div class="col-lg-12">
          <section class="widget">
            <div class="widget-body">
            <form class="form-horizontal"  name="addUser" novalidate>
                    <fieldset>
                        
                        <legend style="padding-bottom: 12px;font-size: 18px;" id="allp"><strong>Add Site</strong></legend>
                        <legend style="padding-bottom: 12px;font-size: 18px; display:none"id="update" ><strong>Update Site</strong></legend>
                       
                       
                       <!-------------- City ---------------->
                       <div class="form-group row">
                            <label for="mlocation" class="col-md-4 form-control-label">Location</label>
                            <div class="col-md-7">
                             <select class="select2 form-control" name ="locationDetail" ng-model="SiteModel.locationDetail"  ng-options="d as d.locationName for d in locationlist track by d.id"  id="mlocation" required>
							   <option value="" disabled selected>Select Location  </option>
 							 </select>
 						     <span style="color:red" ng-show="(addSiteModel.locationDetail.$dirty || submitted) && addSiteModel.locationDetail.$error.required"> Location is required ! </span>	 
						    </div>
                        </div>
                   
                   
                        <!------------- Site Name --------------->
                       <div class="form-group row">
                         <label for="siteName" class="col-md-4 form-control-label ">Site Name</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control  " name="siteName"	placeholder="Enter Site Name" ng-model="SiteModel.siteName"
								id="siteName" onkeyup="alphaNumericChecker()"  required="required" />
								<span style="color: red" ng-show="(addSiteModel.siteName.$dirty || submitted)&& addSiteModel.siteName.$error.required">
								Site Name  is required !</span> 
							 </div>
                        </div>
                        
                        
                        <!-----------Description ------------->
                        <div class="form-group row">
                          <label for="description" class="col-md-4 form-control-label ">Description</label>
                            <div class="col-md-7">
                                  <input type="text" class="form-control  " name="description" placeholder="Enter Description" ng-model="SiteModel.description"
								   id="description" onkeyup="alphaNumericChecker()" required="required" />
								   <span style="color: red" ng-show="(addSiteModel.description.$dirty || submitted)&& addSiteModel.description.$error.required">
									Description  is required !</span> 
						    </div>
                        </div>
                          
                        
                         <!------------ operatingCompany -------------->
                        <div class="form-group row">
                          <label for="operatingCompany" class="col-md-4 form-control-label ">Operating Company Name</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="operatingCompany" placeholder="Enter Company Name" ng-model="SiteModel.operatingCompany"
											id="operatingCompany" onkeyup="alphaNumericChecker()" required="required" />
											
									 <span style="color: red" ng-show="(addSiteModel.operatingCompany.$dirty || submitted)&& addSiteModel.operatingCompany.$error.required">
											Company Name  is required !</span> 
									 
										  </div>
                        </div>
                       
                        
                        
                        <!--Location Type  -->
                        <div class="form-group row">
                          <label for="type" class="col-md-4 form-control-label ">Site Type</label>
                            <div class="col-md-7">
                            
                            <select class="select2 form-control" name ="type" ng-model="SiteModel.type"   id="type" required>
									<option value="" disabled selected>Select Type </option>
									<option value="onstreet">onstreet</option>
									<option value="offstreet">offstreet</option>
 									</select>
 									<span style="color: red" ng-show="(addSiteModel.type.$dirty || submitted)&& addSiteModel.type.$error.required">
											Site Type  is required !</span> 
                             
									
										  </div>
                        </div>
                       
                        
                     
                          <!-- Location -->
                        <div class="form-group row">
                          <label for="latitude" class="col-md-4 form-control-label ">Site Location</label>
                            <div class="col-md-3">
                                	<input type="text" class="form-control  " name="latitude"	placeholder="Enter latitude" ng-model="SiteModel.latitude"
											id="latitude" onkeyup="alphaNumericChecker()" />
											
											<!-- <span style="color: red" ng-show="(addSiteModel.latitude.$dirty || submitted)&& addSiteModel.latitude.$error.required">
											Latitude is required !</span> --> 
									
									 <div style="margin-top: 15px;">
									 
									 <input type="text" class="form-control  " name="latitude"	placeholder="Enter longitude" ng-model="SiteModel.longitude"
											id="longitude" onkeyup="alphaNumericChecker()" />
							
							         <!--  <span style="color: red" ng-show="(addSiteModel.latitude.$dirty || submitted)&& addSiteModel.latitude.$error.required">
											Longitude  is required !</span>  -->
                             
									</div>
									
									   <div style="margin-top: 15px;"> <button class="btn btn-primary" style="background-color: #002B49;" ng-click="viewOnMap()" >Preview on Map</button></div>
					    
					    
					    		<div style="margin-top: 15px;">
					              <div class="form-check abc-checkbox abc-checkbox-success">
					               <input class="form-check-input" id="reentry" ng-model="SiteModel.isReentryAllowed" type="checkbox" ng-click="checkReentry()">
 					                <label class="form-check-label" for="reentry">Re-Entry Allowed ?
                                     </label>
                                     </div>
 					     
					            </div>
					    
					    
 					    		<div>
					              <div class="form-check abc-checkbox abc-checkbox-success">
					               <input class="form-check-input" id="monthlypasscheck" ng-model="SiteModel.isMonthlyPassAllowed" type="checkbox" ng-click="checkMonthlyPass()">
 					                <label class="form-check-label" for="monthlypasscheck">Monthly Pass Allowed ?
                                     </label>
                                     </div>
 					     
					            </div>
					            
					            <div ng-if="showPass">
					               <input type="number" class="form-control" name="isMonthlyPassAllowed" placeholder="Enter Monthly Pass Cost" ng-model="SiteModel.monthlyPassCost"
									id="isMonthlyPassAllowed" />
					            </div>
					            
					            <br>
					            <!-- update slot and update payment -->
					           	<button type="submit" ng-click="mUpdateSlotModelView(SiteModel.sid)"class="btn btn-primary btn-sm" id="updateslot" style="display:none;background-color: #1f94f4; border: #1f94f4;">Update Slot</button>
					            <button type="submit" ng-click="mUpdatePaymentModelView(SiteModel.sid)"class="btn btn-primary btn-sm" id="updatePayment" style="display:none;background-color: #1f94f4; border: #1f94f4;">Update Parking Charges</button>
					            
					      </div>
					    				   
                           <div class="col-md-4" id="mpb" style=" padding-left: 0px;">
                             <div id="map" class="mapCard"></div>
 	                       </div>	   
 	                                           
				    </div>
                     
               </fieldset>
                    <div class="form-actions bg-transparent">
                        <div class="row">
                            <div class="offset-md-4 col-md-7 col-12">
                           <button type="submit" id="submitbtn" ng-click="submitForm(SiteModel)"class="btn btn-primary" style="background-color: #002B49;" id="submitbtn">Next> </button>
                          	<button type="submit" ng-click="updatesite(SiteModel)"class="btn btn-primary" id="updatebtn"  style="display:none; background-color: #002B49;">Update </button>
    				 
                            </div>
                        </div>
                    </div>
                    </form>
                     
              </div>
           </section>
        </div>
     </div>
     
  <div id="snackbar_success">{{response_message}}</div>
  <div id="snackbar_error">{{message}}</div>
  
    <div class="row" ng-show="siteTable">
            <div class="col-lg-12">
                <section class="widget">
                    <header>
                         <h5>
                            Site Data<span class="fw-semi-bold"></span>
                        </h5>
                    </header>
                    <div class="widget-body">
                    <div class="table-responsive">
                      <table class="table table-striped dataTable  table-lg mt-lg mb-0" id="siteTable">
                      <thead class=" text-primary">
                      <tr>
                                	<th data-visible="false">ID</th>
                                	       <th style=" text-transform: revert; font-size: 15px;">Location</th>
											<th style=" text-transform: revert; font-size: 15px;">Site </th>
											<th style=" text-transform: revert; font-size: 15px;">Description</th>
											<th style=" text-transform: revert; font-size: 15px;"> Type</th>
											<th style=" text-transform: revert; font-size: 15px;"> Available Slots</th>
											<th style=" text-transform: revert; font-size: 15px;">Total Slots</th>
											<th style=" text-transform: revert; font-size: 15px;">Action</th>
											
						 </tr>
                         </thead>
                       </table>
                        </div>
                        <div class="clearfix">
                     </div>
                    </div>
                </section>
            </div>
        </div>
        
        
        
        
        

		
		<!------------------------------------Update slot model -------------------------------------------->
		
		
		 <!-- Modal -->
		  <div class="modal fade" id="myModalSlot" role="dialog">
		    <div class="modal-dialog">
		      <!-- Modal content-->
		       <div class="modal-content">
		           <div class="modal-header">
		                        <h4 class="modal-title">Update Slot</h4>
		            </div>
		            <div class="modal-body">
		                <form class="form-horizontal" name="addSlot" novalidate>
		           
		           	            <!-- siteName -->
								<div class="form-group row">
								
									<label for="msite" class="col-md-4 form-control-label">Site Name</label>
									<div class="col-md-7">
								           <input type="text" class="form-control" id="msite"
											name="msite" value="{{msite.siteName}}"
											readonly="readonly" required="required"/>
 									 </div>
 									 
								 </div>
		           		         <!-- slots -->
						         <div ng-repeat="cslot in mdatasite track by $index">
					  			
							        <div class="form-group row">
							            <label for="{{cslot.mVehicleClassModel.className}}" class="col-md-4 form-control-label">{{cslot.mVehicleClassModel.className}}</label>
							            <div class="col-md-7" >
							                     <input type="number" class="form-control"
							                      name="{{cslot.mVehicleClassModel.className}}" 
							                      id="slot{{$index}}" value="{{cslot.slot}}" step="1" min="1"  required="required" />
							             </div>
							         </div> 
							         					
						        </div>	
				    	 </form>
		               </div>
		              <div class="modal-footer">
		                 <button type="button" class="btn btn-primary"  ng-click="updateslotfinal(mdatasite)" data-dismiss="modal">Update</button>
		             </div>
		        </div>
		      
		    </div>
		    
		  </div>
		
		
		

		<!---------------------------Update payment model --------------------------->
		 <!-- Modal -->
		  <div class="modal fade" id="myModalPayment" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <h4 class="modal-title">Update Parking Charges</h4>
		        </div>
		         <div class="modal-body">
		           <form class="form-horizontal" name="addSlot" novalidate>
		           
		           
						      <div class="form-group row">
									   <label for="mSiteModel" class="col-md-12 form-control-label">Site Name
									   </label>
									   <div class="col-md-7">
								 
										<input type="text" class="form-control" id="mSitepayment"
											name="mSiteModel" value="{{sitePayment.siteName}}"
											readonly="readonly" required="required"/>
 									</div>
								</div>
 
								<div ng-repeat="cdata in mPaymentdata track by $index" ng-model= "UpdatePaymentModel">
									<div class="form-group row">
										<label for="{{cdata.className}}"
											class="col-md-12 form-control-label">{{cdata.mVehicleClassModel.className}}</label>
										<div class="col-md-4" style="padding-right: 10px;">
											<input type="number" class="form-control  " name="priceHour"
												placeholder="Hourly Price" 
											id="updatePriceHour{{$index}}" value="{{cdata.priceHour}}" required="required" />


										</div>
										<div class="col-md-4" style="padding-right: 10px;">
											<input type="number" class="form-control" name="dayPrice"
												placeholder="Daily Price" value="{{cdata.dayPrice}}"
												id="updateDayPrice{{$index}}" required="required" />



										</div>
										<div class="col-md-4">
											<input type="number" class="form-control  " name="monthprice"
												placeholder="Monthly Price" value="{{cdata.monthPrice}}"
											   id="updateMonthPrice{{$index}}" required="required" />

										</div>
										
							
										
									</div>
								</div>
					</form>
		         </div>
		         <div class="modal-footer">
		          <button type="button" class="btn btn-primary"  ng-click="paymentupdates(mSitepayment,mPaymentdata)" data-dismiss="modal">Update</button>
		        </div>
		      </div>
		      
		    </div>
		  </div>
		
		<!--------------------------- slot model --------------------------->
		
		
         <div class="row" ng-show="VehicleFrom">

			<div class="col-lg-11">
				<section class="widget">
					<div class="widget-body">
						 
						<form class="form-horizontal" name="addSlot" novalidate>
						
						<fieldset>
 							<legend style="padding-bottom:12ppx; font-size:18px;">
									<strong>Add Slot Info</strong> <span class="pull-right">
								</span> 
							</legend>
								
								<!-- siteName -->
								<div class="form-group row">
									<label for="mSiteModel" class="col-md-4 form-control-label">Site Name
									</label>
									<div class="col-md-7">
								 <input type="text" class="form-control" id="mSiteslot"
											name="mSiteModel" value="{{mSiteData.siteName}}"
											readonly="readonly" required="required"/>
 									</div>
								</div>
								
								<!-- Slots -->
								<div ng-repeat="cdata in vehiclelist track by $index" repeat-done="layoutDone()" ng-model= "PaymentModel.mVehicleClassModel" >
									<div class="form-group row">
										<label for="{{cdata.className}}"
											class="col-md-4 form-control-label">{{cdata.className}}</label>
										<div class="col-md-7" >
									<!-- 	<input type="number" class="form-control  " name="{{cdata.className}}"
												placeholder="Enter {{cdata.className}} Slot" ng-model="slotmodel"
												id="slot{{$index}}" ng-blur="saveSlotInfo(cdata, slotmodel,$event)" required="required" /> -->
												
												<input type="number" class="form-control  " name="{{cdata.className}}"
												placeholder="Enter {{cdata.className}} Slot" ng-model="slotmodel"
												id="slot{{$index}}" min="1" required="required" />
                                         </div>
									 </div>
									
								</div>
								  <div class="form-actions bg-transparent">
	                              <div class="row"   >
	                                <div class="offset-md-4 col-md-7 col-12">
	                                   <button  type="submit" ng-click="addSlotButton(vehiclelist)"  class="btn btn-primary" style="background-color: #002B49;" id="mslot{{$index}}">Next >></button>
	                               </div>
	                                </div>
	                              </div>
								
								
							</fieldset>
						</form>
					</div>
				</section>
			</div>
       </div> <!--end  -->
        
        
        
        
        
        
       <!---------------------------ADD  PAYMENT FORM AND TABLE---------------------------------->
     
     <div class="row" ng-show="paymentFrom">

			<div class="col-lg-11">
				<section class="widget">
					<div class="widget-body">
						 
						<form class="form-horizontal" name="addpayment" novalidate>
						
							<fieldset>
 								<legend style="padding-bottom:12ppx; font-size:18px;">
									<strong>Add Parking Charges</strong> <span class="pull-right">
									<!-- <a class="previous" ng-click="gobackto()">&laquo; Back</a></span> -->
								</legend>
								
								<div class="form-group row">
									<label for="mSiteModel" class="col-md-12 form-control-label">Site Name
									</label>
									<div class="col-md-4">
								 
										<input type="text" class="form-control" id="mSitepayment"
											name="mSiteModel" value="{{mSiteData.siteName}}"
											readonly="readonly" required="required"/>
 									</div>
								</div>
 
								<div ng-repeat="cdata in vehiclelist track by $index" ng-model= "PaymentModel.mVehicleClassModel">
									<div class="form-group row">
										<label for="{{cdata.className}}"
											class="col-md-12 form-control-label">{{cdata.className}}</label>
										<div class="col-md-4" style="padding-right: 10px;">
											<input type="number" class="form-control  " name="priceHour"
												placeholder="Hourly Price" ng-model="PaymentModel.priceHour"
												id="priceHour{{$index}}" required="required" />


										</div>
										<div class="col-md-4" style="padding-right: 10px;">
											<input type="number" class="form-control  " name="dayPrice"
												placeholder="Daily Price" ng-model="PaymentModel.dayPrice"
												id="dayPrice{{$index}}" required="required" />



										</div>
										<div class="col-md-4">
											<input type="number" class="form-control  " name="monthprice"
												placeholder="Monthly Price"
												ng-model="PaymentModel.monthPrice" id="monthPrice{{$index}}" required="required" />

										</div>
										
							
										
									</div>
								</div>
								
								<button type="submit" class="btn btn-primary btn-md btn-square " 
												ng-click="paymentadd(PaymentModel, cdata, $event)"
												style="background-color: #002B49;">save</button>
								
								
								<!-- <div class="col-md-2" style="padding-left: 0px;">
											<button type="submit" class="btn btn-primary btn-lg btn-square " 
												ng-click="paymentadd(PaymentModel, cdata, $event)"
												style="background-color: #002B49;">save</button>


										</div>  -->
							</fieldset>
						</form>
					</div>
				</section>
			</div>





		</div>
     
    		
		</main>
</div>
        
  <!-- spinner -->
  <div class="loader-wrap " style="background-color: rgba(0, 0, 0, 0.4);" id="overlid">
		<i class="fa fa-spinner fa-spin"></i>
   </div>
   
   
         <!-- The Loader. Is shown when pjax happens -->
      <div class="loader-wrap" id="overl">
            <i class="fa fa-circle-o-notch fa-spin-fast"></i>
        </div>
        <!-- common libraries. required for every page
         <script src="${pageContext.request.contextPath}/resources/node_modules/jquery/dist/jquery.min.js"></script> 
      -->  <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-pjax/jquery.pjax.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/popper.js/dist/umd/popper.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/js/dist/util.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/widgster/widgster.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" data-pace-options='{ "target": ".content-wrap", "ghostTime": 1000 }'></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/hammerjs/hammer.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-hammerjs/jquery.hammer.js"></script>


        <!-- common app js -->
        <script src="${pageContext.request.contextPath}/resources/js/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/app.js"></script>

        <!-- Page scripts -->
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.animator/jquery.flot.animator.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.selection.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.time.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.pie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.stack.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.crosshair.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.symbol.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.dashes/jquery.flot.dashes.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap_calendar/bootstrap_calendar/js/bootstrap_calendar.min.js"></script>
       <!-- bootstrap-daterangepicker -->	
       <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
         

     	<script>
	

	function alphaNumericChecker() {
		 
		var gsiteName = document.getElementById('siteName');
		gsiteName.value = gsiteName.value.replace(/[^a-zA-Z ]+/, '');

		var gdescription = document.getElementById('description');
		gdescription.value = gdescription.value.replace(/[^a-zA-Z0-9 ]+/, '');

		var goperatingCompany = document.getElementById('operatingCompany');
		goperatingCompany.value = goperatingCompany.value.replace(/[^a-zA-Z0-9 ]+/, '');

		var glat = document.getElementById('latitude');
		glat.value = glat.value.replace(/[^0-9. ]+/, '');

		var glon = document.getElementById('longitude');
		glon.value = glon.value.replace(/[^0-9. ]+/, '');

	}

	
</script>
</body>

</html>


<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
<meta charset="ISO-8859-1">
<title>Add Site | VPark</title>
        <link href="${pageContext.request.contextPath}/resources/css/application.min.css" rel="stylesheet">
       
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/line-awesome/dist/line-awesome/css/line-awesome.css" rel="stylesheet">
             
        <link href="${pageContext.request.contextPath}/resources/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
		
		
		<!----------------------------------------------progressbar--------------------------------------------------------->
		<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
		
  		<!----------------------------------------------Jquery------------------------------------------------------>
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script> 
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery/dist/jquery.min.js"></script> 
 
        <!-----------------------------------------dataTable------------------------------------------------------>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>
        <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        
        <!-----------------------------------------------ANGUARJS----------------------------------------------->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
	
        <!---------------------------------- API URL ------------------------------------------------------>
        <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script> 
	
        <!-- Datatables css-->
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"rel="stylesheet">
	    <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
	    rel="stylesheet">
	    <!-- Datatables js-->
        <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

        <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>

         <!---------------------------------------DATEPICKER-------------------------------------------------------->
		 <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

       
		  <!-------------------------------- Load Leaflet from CDN----------------------------------------------->
		  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
		    integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
		    crossorigin=""/>
		  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
		    integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
		    crossorigin=""></script>
		
		  <!------------------------------ Load Esri Leaflet from CDN -------------------------------------------->
		  <script src="https://unpkg.com/esri-leaflet@2.5.0/dist/esri-leaflet.js"
		    integrity="sha512-ucw7Grpc+iEQZa711gcjgMBnmd9qju1CICsRaryvX7HJklK0pGl/prxKvtHwpgm5ZHdvAil7YPxI1oWPOWK3UQ=="
		    crossorigin=""></script>
		
		  <!------------------------------- Load Esri Leaflet Geocoder from CDN ----------------------------------->
		  <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
		    integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
		    crossorigin="">
		  <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
		    integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
		    crossorigin=""></script>
             
             <!-------------------------------------------markercluster  ------------------------------------------>
	        <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.0.4/dist/MarkerCluster.Default.css">
			<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.0.4/dist/MarkerCluster.css">
			<script src="https://unpkg.com/leaflet.markercluster@1.0.4/dist/leaflet.markercluster.js"></script>
			<script src="${pageContext.request.contextPath}/resources/MovingMarker.js"></script>
			
			<!-- Load Clustered Feature Layer from CDN -->
	        <script src="https://unpkg.com/esri-leaflet-cluster@2.0.0/dist/esri-leaflet-cluster.js"></script>
	        
	        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
	        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/webmain.css">
	    
		
 <style>
   .previous {
  text-decoration: none;
  display: inline-block;
  padding: 8px 16px;
  background-color: #f1f1f1;
   cursor:pointer;
   }
 </style>
    
    <script>
    
    var uid = '<%=session.getAttribute("uid")%>';
	var userN = '<%=session.getAttribute("user")%>';

	var status ='<%= session.getAttribute("isLoggedIn")%>';
	var token = '<%=session.getAttribute("token") %>';
	var jwtToken = '<%=session.getAttribute("jwtToken") %>';

	var urole = '<%=session.getAttribute("urole") %>';
	var userpri = '<%=session.getAttribute("privileges") %>';

	 
	<%
	response.setHeader("Cache-Control","no-cache");
	response.setHeader("Cache-Control","no-store");
	response.setHeader("Pragma","no-cache");
	response.setDateHeader ("Expires", 0);
	
	 if(session!=null){
		  Object obj = session.getAttribute("isLoggedIn");
		    if(obj==null){
		   response.sendRedirect("/signin"); 
	       } 
		
			}else{
			response.sendRedirect("/signin"); 
			}  
	%> 
	
	var jDataTable;
	var jData;
	var jDatanew;
	var jDataTablenew;
	var incilat=0.0; 
  	var incilon=0.0;
       var app = angular.module("app", []);
       app.controller("GetDataController", ['$scope','$http','$window', function(scope,http,window) {
    	   document.getElementById('overl').style.visibility = "hidden";
    	   document.getElementById('overlid').style.visibility = "hidden";
    	   

    	   /*******************************GET VEHICLES****************************************/
           http.get(apiURL+'getVehicles/'+jwtToken+'/'+userN+'/doGetVehicleClass').
   			then(function (response) {
   			if(data != null)
   				{
   				if(data.status==1){
   			              
   					scope.vehiclelist= data.myObjectList;
   							
   		         }else{
   		        
   		        	scope.vehiclelist="";	 
   		        	 
   		         }  
   		   }else{
   		     	
   		      }      		       		
   			}).
   			error(function(data,status) {
   			     
   			});
           
        
    	    
           /********************************GET LOCATION***************************************/
              http.get(apiURL+'getlocation/'+jwtToken+'/'+userN+'/doGetLocation').
   			then(function (response) {
   				
   				if(data != null)
   				{
   				if(data.status==1){
   			              
   					scope.locationlist= data.myObjectList;
    				scope.sitelocation=scope.locationlist[0].mSiteModel
   		         }  
   		   }else{
   		     	
   		      }      		       		
   			}).
   			error(function(data,status) {
   			     
   			});
           
           
           //----------------FOR PAYMENT INFO-----------------//
            scope.getAllSiteData = function(){
        	   
               http.get(apiURL+'getSite/'+jwtToken+'/'+userN+'/doGetSiteInfo').
					then(function (response) {
 						if(data != null)
						{
 							if(data.status==1){
 							        Pace.stop();
 					                document.getElementById('overl').style.visibility = "hidden";   
                                    jData = [];
				             		jData= data.myObjectList;
    				                jDataTable.clear().rows.add(jData).draw();
				         }else{
				        	 jData = [];
				       	     jDataTable.clear().rows.add(jData).draw();
				               } 
				      }
 						else{
				        	 jData = [];
				       	     jDataTable.clear().rows.add(jData).draw();
				             } 
					}).
					error(function(data,status) {
					});
           };
             
      		//******************************SITE LIST******************************//

    	    scope.siteTable = false;	 
   			scope.siteForm = false;
    	   
    		scope.sitedata = function () { 
   			
    		scope.siteTable = true;	 
   			scope.siteForm = true;
   			
   	        http.get(apiURL+'getSite/'+jwtToken+'/'+userN+'/doGetSiteInfo').
   					then(function (response) {
   	
   						if(data != null)
   						{
   					    $(function() {
   				 		    Pace.on("done", function(){
   				 		   });
   				 		});
   				  	    document.getElementById('overl').style.visibility = "hidden";
   				  	   
   						if(data.status==1){
   							       	jData = [];
   				             		jData= data.myObjectList;
        				            jDataTable.clear().rows.add(jData).draw();
   				         }else{
   				        	 jData = [];
   				       	     jDataTable.clear().rows.add(jData).draw();
   				               }  
   				   }else{
   				     	 jData = [];
   				   	     jDataTable.clear().rows.add(jData).draw();
   				      }   
   						
   					}).
   					error(function(data,status) {
   					     
   					});
   			 
   	};  	
   	/*********************************SITE DATA*************************************/
   	$(document).ready(function() {
   		 jDataTable = $('#siteTable').DataTable({ 
   	 		 	data: jData,
   		         		columns: [
   		         	    {'data':'sid'},
   		                
   		             {'data':'locationDetail', 'render':function(locationDetail){
	         				
 	             		 if(locationDetail!=null)
	             			 {
 	             			if(locationDetail!=""){
  	         					return locationDetail.locationName;
   	         				}else{
   	         					return '<p>__</p>';
   	         				}
	             			 }else{
	             				return '<p>__</p>';
	             			 }
	         			}},
   		         		{'data':'siteName', 'render':function(siteName){
 	         				
    	             		 if(siteName!=null)
	             			 {
    	             			if(siteName!=""){
     	         					return siteName;
      	         				}else{
      	         					return '<p>__</p>';
      	         				}
	             			 }else{
	             				return '<p>__</p>';
	             			 }
	         			}},
	         			{'data':'description', 'render':function(description){
 	         				
    	             		 if(description!=null)
	             			 {
    	             			if(description!=""){
     	         					return description;
      	         				}else{
      	         					return '<p>__</p>';
      	         				}
	             			 }else{
	             				return '<p>__</p>';
	             			 }
	         			}},
	         			{'data':'type', 'render':function(type){
 	         				
    	             		 if(type!=null)
	             			 {
    	             			if(type!=""){
     	         					return type;
      	         				}else{
      	         					return '<p>__</p>';
      	         				}
	             			 }else{
	             				return '<p>__</p>';
	             			 }
	         			}},
   		             	   
	         			{'data':'slots', 'render':function(slots){
 	         				
   	             		 if(slots!=null)
	             			 {
   	             			if(slots >0){
    	         					return slots;
     	         				}else{
     	         					return slots;
     	         				}
	             			 }else{
	             				return '<p>__</p>';
	             			 }
	         			}},
   		               {'data': "sid", 'render':function(sid){
	         				
	         				if(sid!=null){
	         					return '<u id="edit" title="Update Site!"style="color: #1f94f4; cursor:pointer; font-size:16px;"\><span class="glyphicon glyphicon-edit"></span></u>&nbsp&nbsp<u style="padding-left: 12px;"><button type="submit"  class="btn btn-primary btn-sm" id="editSlot" style="background-color: #1f94f4; border: #1f94f4;" cursor:pointer;>Update Slot</button></u>';
        	    				 
	        	         		//	<u id="editSlot" title="Update Slot!" style="color: #1f94f4;padding-left: 14px; text-decoration: none;  cursor:pointer; font-size:16px;"\>Update Slot</u>';
	        	         				}
	         			}}  
   		            ],
   		           buttons: [
   		        	    'copy', 'excel', 'pdf'
   		        	  ],
   	
   		 	             "ordering": false,
   		 	             "searching": false,
   		 	             "deferRender": true,
   		 	             "autoWidth": false,
   		 	             "lengthChange": false,
   		 	             "language": {
   		     	    		"infoEmpty": "No records to show",
   		         	    	"emptyTable": "not record found",
   		     	    	    }
   		    	        });
   		  
   	  }); 
   	
    	   
    
	 //**************SELECT DESELECT TABLE ROW******************************//
	 $('#siteTable').on('click', 'tr', function () {
	 	  
		 if ( $(this).hasClass('row_selected'))
	 	  		{  
	 			   $("#siteTable tbody tr").removeClass('row_selected');
	 	  	       $(this).removeClass('row_selected');
 	 	  		   $("#updatebtn").hide();
	               $("#submitbtn").show();
	               $("#update").hide();
	               $("#allp").show();
	               scope.showPass = false;
	               document.getElementById("isMonthlyPassAllowed").value="";
	               scope.UserModel = "";
	   	  		   scope.$apply();

	 	  	   
	 	  	    }else
	 	  	        {
	 	  	        $("#siteTable tbody tr").removeClass('row_selected');
	 	  	        $(this).addClass('row_selected');
	 	  	
	 	  	    }
	 }); 
	 

	   //*************************UPDATE CLICK*******************************//
	  	$('#siteTable').on('click','#edit', function () {
	   		
	  		scope.UserModel = "";
	  		scope.$apply();

	  		var tr = $(this).closest('tr');
	  		var selectedData = jDataTable.row(tr).data();
	  			  	
             $("#updatebtn").show();
             $("#submitbtn").hide();
             $("#update").show();
             $("#allp").hide();
             scope.showPass = false;

             scope.UserModel =selectedData;
	   	     if(selectedData.isMonthlyPassAllowed == true)
				{   
				document.getElementById("monthlypasscheck").checked;
                scope.showPass = true;
	 		    }  
	   	      if(selectedData.isReentryAllowed == true)
			    {   
			    document.getElementById("reentry").checked;
		        }  
	   	     if(selectedData.monthlyPassCost!=""){
	   	       scope.monthlyPassCost=selectedData.monthlyPassCost;
	   	      }
	  		  scope.$apply();
			}); 
	   
	 
	  //*************************SLOT UPDATE CLICK*******************************//
	  	$('#siteTable').on('click','#editSlot', function () {
			   document.getElementById('overl').style.visibility = "visible"; 

	  		scope.UserModel = "";
	  		scope.$apply();
	  		var tr = $(this).closest('tr');
	  		var selectedData = jDataTable.row(tr).data();
	  			  	
             $("#updatebtn").hide();
             $("#submitbtn").hide();
             $("#update").hide();
             $("#allp").hide();
             $("#updateslotFrom").show();
             $("#siteForm").hide();
            
 	  		 
             scope.siteTable = false;
             scope.updateslotFrom = true;
             scope.siteForm = false;

	   	     scope.$apply();

	      		scope.dataRequest = {
	      				"siteId": selectedData.sid
	    		 };
	      		
				scope.requestedData = {
						"sessionTokken" : jwtToken,
						"username":userN,
						"functionName" : "doSlotsBySiteId",
			  			 "data" : JSON.stringify(scope.dataRequest)
					};
	       	        
				http({
						method :'POST',
						url :apiURL+'getSlotBySiteId',
						data :scope.requestedData
						}).then(function (response) {	
		 					if(response.status==1){
		 						scope.datasite=response.myObjectList;
				 				scope.allslotdata=scope.datasite;
				 				scope.msite=scope.datasite[0].mSiteModel;
		     		            $('<div class="form-group row"><label for="mSiteModel" class="col-md-4 form-control-label">Site Name</label><div class="col-md-7"><input type="text" class="form-control" id="msite" name="mSiteModel" value="'+scope.msite.siteName+'" readonly="readonly" required="required" ng-model="msite"/></div></div>').appendTo('#UpdateSlotSite');   
 
				 				for(var i=0;i<scope.datasite.length;i++)
				 					{
			     		            $('<div class="form-group row"><label for="'+scope.datasite[i].mVehicleClassModel.className+'"class="col-md-4 form-control-label">'+scope.datasite[i].mVehicleClassModel.className+'</label><div class="col-md-7" ><input type="number" class="form-control" ng-model="scope.mslotdata" name="'+scope.datasite[i].mVehicleClassModel.className+'" value="'+scope.datasite[i].slot+'"  id="slot'+i+'" placeholder="Enter '+scope.datasite[i].mVehicleClassModel.className+' Slot"   required="required" /></div></div> ').appendTo('#UpdateSlotField');   
				 					}
					            document.getElementById('overl').style.visibility = "hidden"; 

	 						}else if(response.status==0)
							{
								scope.message = response.message;
	 							
							}else{
								scope.message = response.message;
						    } 
		 				}).error(function(response) {			 
						});   
					 
	   	     
			}); 
	  //------------------IF CHECK ENTRY PASS ALLOWED-------//
		   
		   scope.ischeckReentry = 0;
		  scope.checkReentry = function(){
	 		if($('#reentry').is(':checked')){
				
	 			scope.ischeckReentry = 1;
	 			
			}else{
				scope.ischeckReentry= 0;
	 		}
		   }
		  
		  
	  //------------------IF CHECK MONTHLY PASS ALLOWED-------//
	 		scope.monthlyPassCost =0;
		    scope.isMonthlyPassAllowed =0;
			scope.checkMonthlyPass = function(){
				
	 		if($('#monthlypasscheck').is(':checked')){
                scope.showPass = true;
	 			 scope.isMonthlyPassAllowed = 1;
	 			
			}else{
	 			scope.showPass = false;
	 			scope.isMonthlyPassAllowed =0;
	 			scope.monthlyPassCost =0;
	 			scope.monthlyPassCost =0;
	 		}
		   }

		
		//**************ADD SITE DATA******************************//
		  scope.submitForm = function(UserModel){
		    	scope.submitted = true;
		        	  if(typeof scope.UserModel  !== 'undefined'){
		       		  
		       		  if(scope.UserModel.hasOwnProperty("locationDetail")){
		   	  		   scope.locationDetail = scope.UserModel.locationDetail;
		   	  	      }else{
		   	  		     scope.locationDetail ="";
		   	  	      }
		 		  
		       		  if(scope.UserModel.hasOwnProperty("siteName")){
			    	  		   scope.siteName = scope.UserModel.siteName;
			    	  	   }else{
			    	  		   scope.siteName ="";
			    	  	   }
		      		  
		      		  if(scope.UserModel.hasOwnProperty("description")){
			    	  		   scope.description = scope.UserModel.description;
			    	  	   }else{
			    	  		   scope.description ="";
			    	  	   }
		      		
		      		  if(scope.UserModel.hasOwnProperty("type")){
			  		            scope.type = scope.UserModel.type;
			  	            }else{
			  		             scope.type ="";
			  	            }
		      		  
		       		 if(scope.UserModel.hasOwnProperty("operatingCompany")){
				            scope.operatingCompany = scope.UserModel.operatingCompany;
			            }else{
				             scope.operatingCompany ="";
			            }
		       		 
		       		if(scope.UserModel.hasOwnProperty("monthlyPassCost")){
			            scope.monthlyPassCost = scope.UserModel.monthlyPassCost;
		            }else{
			             scope.monthlyPassCost ="";
		            }
		      		  
		      		
				scope.locationdata = {
			      			
						"id":UserModel.locationDetail.id,
						"locationName":UserModel.locationDetail.locationName,
						"lon":UserModel.locationDetail.lon,
						"lat":UserModel.locationDetail.lat,
			      	};	
		 		
		      		scope.dataRequest = {
		      				
		      				"siteName": scope.siteName,
		    				"description": scope.description,
		     				"type": scope.type,
		    				"latitude":incilat,
		    	      		"longitude":incilon,
		    	      		"locationDetail":scope.locationDetail,
		     	      		"operatingCompany":scope.operatingCompany,
		    	      		"isReentryAllowed":scope.ischeckReentry,
		    	      		"isSlotAvailable":1,
		    	      		"isMonthlyPassAllowed": scope.isMonthlyPassAllowed,
		    	      		"monthlyPassCost": scope.monthlyPassCost,
		    	      		"isOperationalinNight":1,
		    	      		"availableSlots":0,
		    	      	    "slots":0
		    	      		
		              };
		      		
					scope.requestedData = {
							"sessionTokken" : jwtToken,
							"username":userN,
							"functionName" : "doAddSiteInfo",
				  			 "data" : JSON.stringify(scope.dataRequest)
						};
		       	   
				//VALIDATION
				if (scope.addUser.$valid) 
					{
					 if( scope.siteName != "" && scope.type!= "" && document.getElementById('latitude').value !=""&&  document.getElementById('longitude').value !="" )
					 {
						   document.getElementById('overlid').style.visibility = "visible"; 
			        		
					        http({
							method :'POST',
							url :apiURL+'addSite',
							data :scope.requestedData
							}).then(function (response) {	
			 					if(response.status==1){
			 						
			 						scope.getAllSiteData(); //REFRESH DATATABLE WITH ALL SITE INFO
		  	 						scope.response_message = response.message;
		 	 						var x = document.getElementById("snackbar_success")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");
		   							},1000);
					 				
					 				// ---checking for latest added site-------------//
					 				
					 				if(response.myObjectList !=null)
					 					{
					 					scope.lastAddedSite = response.myObjectList;
					 					scope.mSiteData = response.myObjectList;
					 					}else{
					 						
					 					}
					 				
					 				scope.VehicleFrom = true;
					 			    scope.siteTable = false;	 
					 				scope.siteForm = false;
					 				scope.paymentFrom = false;
					 				 
					 				
							        document.getElementById('overlid').style.visibility = "hidden"; 
			
		 						}else if(response.status==0)
								{
									scope.message = response.message;
		 							var x = document.getElementById("snackbar_error")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");	
										 $('#paymentform').modal('hide');
									}, 1500);
					 				document.getElementById('overlid').style.visibility = "hidden";
			 						
			 						
								}else{
									scope.message = response.message;
									document.getElementById('overlid').style.visibility = "hidden";
							    } 
			 				}).error(function(response) {			 
							});   
						 
			     	  } else  {
			   				
						  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		                
						 } 
					
						}
					else  {
			   			  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		              
						 }  
		        }// end of validate 
		     else  {
			   			  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		              
						 }  	  
		      };
		        
		      
		      //////////////////////ADD SLOTS PAYMENT///////////////////////////////
		       scope.inputSlot=[];
		      scope.saveSlotInfo =  function(slotdata, vehicledata,event){
		    	  
		    	 scope.slotinfo=vehicledata;
		         scope.slotdata=slotdata;
		       
		         scope.dataRequest = {
		     			"mSiteModel":scope.mSiteData,
		 				"mVehicleClassModel":scope.slotdata,
		 				"slot":scope.slotinfo
		 		};
		        
		    	scope.requestedData = {
							"sessionTokken" : jwtToken,
							"username":userN,
							"functionName" : "doAddSlotInfo",
				  			 "data" : JSON.stringify(scope.dataRequest)

						};
		 			
					 if(scope.mSiteData!= null && vehicledata!=null && slotdata!=null)
					 { 
						
					        http({
							method :'POST',
							url :apiURL+'addSlotInfo',
							data :scope.requestedData
							}).then(function (response) {	
			 					if(response.status==1){
			 						
			 						scope.response_message = response.message;
		 	 						var x = document.getElementById("snackbar_success")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 200);
		 			 				
		 			 				scope.busdata=slotdata;
		 			 				
		                           event.target.disabled = true; //desabled after save
					 		       document.getElementById('overlid').style.visibility = "hidden";
								}else if(response.status==0)
								{
									scope.message = response.message;
									
									var x = document.getElementById("snackbar_error")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 1000);
					 				document.getElementById('overlid').style.visibility = "hidden";
			 						
			 						
								}else{
									scope.message = response.message;
									document.getElementById('overlid').style.visibility = "hidden";
							    } 
			 				}).error(function(response) {			 
							});  
						
			     	  } else  {
			   				
						  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		               
						 } 
		         
		      };
		      
		  
		       /************* for checking last index in ngrepeat**********************/
		        scope.slotsadd=function()
		        {
		    	  if(scope.busdata!= undefined)
		    		{
		    		 var da1=$("#slot0").val();
		    		 var da2=$("#slot1").val();
		    		 var da3=$("#slot2").val();
		    		  if(scope.busdata.className == "BUS"){
		    			
		    			  if(da1!= "" && da2!= ""){
			    				     scope.VehicleFrom = false;
						    	     scope.siteTable = false;	 
									 scope.siteForm = false;
									 scope.paymentFrom = true;  
			    			  }
			    			  else if(da2!= "" && da1!= ""){
			    				     scope.VehicleFrom = false;
						    	     scope.siteTable = false;	 
									 scope.siteForm = false;
									 scope.paymentFrom = true;  
			    			  }
			    			  
			        	 }
			        	 else  {
			        		 if(da2!= "" && da1!= "" & da3!= ""){
				    				     scope.VehicleFrom = false;
							    	     scope.siteTable = false;	 
										 scope.siteForm = false;
										 scope.paymentFrom = true;  
				    			  }
			        		 else{
			        			  scope.message = "Above Fields cannot be Left Blank !";
								  var x = document.getElementById("snackbar_error")
									x.className = "show";
									setTimeout(function() {
										x.className = x.className.replace("show", "");
									}, 3000);
			        		 }
							
			               
							 } 
						 
		    		}
		    	 else  {
		    		 
					  scope.message = "Above Fields cannot be Left Blank !";
					  var x = document.getElementById("snackbar_error")
						x.className = "show";
						setTimeout(function() {
							x.className = x.className.replace("show", "");
						}, 3000);
	               
					 } 
		    	 
				 };
				 
      
		     //////////////////////add payment///////////////////////////////
			  scope.paymentadd=function(PaymentModel, vehicleList, event){
			     scope.formsubmitted = true;
			        if(PaymentModel !== undefined){

                      scope.dataRequest = {
			      			"mSiteModel":scope.mSiteData,
							"mVehicleClassModel":vehicleList,
							"priceHour":PaymentModel.priceHour,
							"dayPrice": PaymentModel.dayPrice,
							"monthPrice":PaymentModel.monthPrice
				      		
			     	   };
			     	 
						scope.requestedData = {
								"sessionTokken" : jwtToken,
								"username":userN,
								"functionName" : "doAddPaymentInfo",
					  			 "data" : JSON.stringify(scope.dataRequest)

							};
			 			
						 if(PaymentModel.priceHour!== undefined && PaymentModel.dayPrice !== undefined && PaymentModel.monthPrice!== undefined)
						 { 
							 document.getElementById('overlid').style.visibility = "visible"; 
				        		
						       http({
								method :'POST',
								url :apiURL+'addPayment',
								data :scope.requestedData
								}).then(function (response) {	
				 					if(response.status==1){
				 						
				 						scope.response_message = response.message;
			 	 						var x = document.getElementById("snackbar_success")
						 				x.className = "show";
						 				setTimeout(function() {
											x.className = x.className.replace("show", "");					 
										}, 1000);
			 			 				
			                               event.target.disabled = true;
			                              
			                               /* checking for payment model is not blank */
			  					           scope.dataRequest = {
			  						   					"siteId":scope.mSiteData.sid
			  						   				};
			  						   	        	  
			  						   			scope.requestedData = {
			  											"sessionTokken" : jwtToken,
			  											"username":userN,
			  											"functionName" : "doGetChargesbySiteId",
			  								  		     "data" : JSON.stringify(scope.dataRequest)

			  										};
			  					   			         http({
			  											method :'POST',
			  											url :apiURL+'getChargesBySiteId',
			  											data :scope.requestedData
			  											}).success(function(data) {	
			  												if(data != null)
			  						   						{
			  						   				  	   
			  						   						 if(data.status==1){
			  						   				             		var array= data.myObjectList;
			  						   				             		var arrayone=array[0].mVehicleClassModel;
			  						   				                    var two=array[1];
				  						   			                    var three=array[2];
					  						   			                if(two != undefined)
			  						   				             	   	{
					  						   				    	    var arraytwo=two.mVehicleClassModel;
			  						   				             		}
			  						   				             	    if(three != undefined)
		  						   				             		    {
				  						   				    	        var arraythree=three.mVehicleClassModel;
		  						   				             		    }
		  						   				             	        else
		  						   				             		    {
		  						   				             	        var arraytwo="";
		  						   				             	        var arraythree="";
		  						   				             		    }
                                                                       if(arrayone.className=="BUS" && arraytwo.className=="TWO WHEELER" && arraythree.className=="CAR")
			  						   				             			{
			  				                                        	    window.location.href = '${pageContext.servletContext.contextPath}/map';
                                                                            }
			  						   				                    else if(arrayone.className=="CAR" && arraytwo.className=="TWO WHEELER" && arraythree.className=="BUS")
			  						   				             		   {
			  				                                        	    window.location.href = '${pageContext.servletContext.contextPath}/map';

			  						   				             	     	}
			  						   				                    else if(arrayone.className=="BUS" && arraytwo.className=="CAR" && arraythree.className=="TWO WHEELER")
	  						   				             		         {
	  				                                        	          window.location.href = '${pageContext.servletContext.contextPath}/map';
                                                                         }
			  						   				                    else if(arrayone.className=="CAR" && arraytwo.className=="BUS" && arraythree.className=="TWO WHEELER")
			  						   				             	       {
			  				                                        	    window.location.href = '${pageContext.servletContext.contextPath}/map';
			  						   				             		   }  
			  						   				                    else if(arrayone.className=="CAR" && arraytwo.className=="BUS" && arraythree.className=="TWO WHEELER")
		  						   				             		       {
			  				                                        	    window.location.href = '${pageContext.servletContext.contextPath}/map';
		  						   				             	     	    }
				  						   				                else if(arrayone.className=="TWO WHEELER" && arraytwo.className=="BUS" && arraythree.className=="CAR")
		  						   				             		      {
		  				                                        	      window.location.href = '${pageContext.servletContext.contextPath}/map';
		  						   				             	     	  }
			  						   				                    else if(arrayone.className=="TWO WHEELER" && arraytwo.className=="CAR" && arraythree.className=="BUS")
 						   				             		             {
 				                                        	             window.location.href = '${pageContext.servletContext.contextPath}/map';
 						   				             	     	         }
			  						   				                    else
			  						   				             			{
			  						   				             			}
			  						   				                  
			  						   				          }else{
			  						   				               }  
			  						   				    }else{
			  						   				      }   
			  											}).error(function(response) {			 
			  											});   
			  				 	          		  /* end  */
			  					        	   
						 		       document.getElementById('overlid').style.visibility = "hidden";
									}else if(response.status==0)
									{
										scope.message = response.message;
										
										var x = document.getElementById("snackbar_error")
						 				x.className = "show";
						 				setTimeout(function() {
											x.className = x.className.replace("show", "");					 
										}, 1000);
						 				
						 				document.getElementById('overlid').style.visibility = "hidden";
									}else{
										scope.message = response.message;
										document.getElementById('overlid').style.visibility = "hidden";
								    } 
				 				}).error(function(response) {			 
								}); 
							 
				     	  } else  {
				     		  
							   document.getElementById('overlid').style.visibility = "hidden";
 							   scope.message = "Above Fields cannot be Left Blank !";
							   var x = document.getElementById("snackbar_error")
								x.className = "show";
								setTimeout(function() {
									x.className = x.className.replace("show", "");
								}, 1000);
							 } 
			   		     }
			   		  else  {
			     		  
						   document.getElementById('overlid').style.visibility = "hidden";
						   scope.message = "Above Fields cannot be Left Blank !";
						   var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 1000);
						 } 
			       };
      
    
	//GO BACK TO SITE PAGE-------  
       scope.gobackto = function(){
    	 
    	    location.reload();

    	    scope.paymentFrom = false;
    	    //scope.paymentTable = false;
    	    scope.siteTable = true;	 
  			scope.siteForm = true;
  			
   			scope.UserModel = "";
   		    document.getElementById('latitude').value = "";
  			document.getElementById('longitude').value = "";  
  			scope.submitted = false;
     	    scope.formsubmitted = false;
      	    

  			 
          };
          
         
         /******************UPDATE SITE**************************************************/
    	scope.updatesite=function(UserModel)
    	{
    		 scope.submitted = true;
    		   incilat=document.getElementById('latitude').value;
    		   incilon=document.getElementById('longitude').value;
    		 	scope.dataRequest = {
		      				"sid":UserModel.sid,
		      				"siteName": UserModel.siteName,
		    				"description": UserModel.description,
		     				"type": UserModel.type,
		    				"latitude":incilat,
		    	      		"longitude":incilon,
		    	      		"locationDetail":UserModel.locationDetail,
		     	      		"operatingCompany":UserModel.operatingCompany,
		    	      		"isReentryAllowed":UserModel.isReentryAllowed,
		    	      		"isSlotAvailable":UserModel.isSlotAvailable,
		    	      		"isMonthlyPassAllowed": UserModel.isMonthlyPassAllowed,
		    	      		"monthlyPassCost": UserModel.monthlyPassCost,
		    	      		"isOperationalinNight":UserModel.isOperationalinNight,
		    	      		"availableSlots":UserModel.availableSlots,
		    	      	    "slots":UserModel.slots
		    	      		
		              };
		      	scope.requestedData = {
							"sessionTokken" : jwtToken,
							"username":userN,
							"functionName" : "doUpdateSiteInfo",
				  			 "data" : JSON.stringify(scope.dataRequest)
		
						};
		      	    /***********************VALIDATION**********************/
					if (scope.addUser.$valid) 
					{	
					  if(UserModel.locationDetail != undefined && UserModel.siteName != undefined  && UserModel.description != undefined  &&  UserModel.type != undefined && document.getElementById('latitude').value !=""&&  document.getElementById('longitude').value !="")
					   {    
						  document.getElementById('overlid').style.visibility = "visible";
						  			
					        http({
							method :'POST',
							url :apiURL+'updateSite',
							data :scope.requestedData
							}).then(function (response) {	
			 					if(response.status==1){
			 						
			 						scope.response_message = response.message;
			 						var x = document.getElementById("snackbar_success")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 3000);
					 				 setTimeout(function() {	
			 							 location.reload();
			  						}, 1500); 
								}else if(response.status==0)
								{
									scope.message = response.message;
									var x = document.getElementById("snackbar_error")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 3000);
					 			
					 				document.getElementById('overlid').style.visibility = "hidden";
									
								}else{
									scope.message = response.message;
									document.getElementById('overlid').style.visibility = "hidden";
									
							    } 
									
			  					      
			 				}).error(function(response) {			 
							});    
						 
			     	  } else  {
			   				
						  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		              
						 } 
					
					 
		     	  }// end of validate for angularjs
		     	  
					 else  {
			   				
						  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		              
						 } 
    	};
    
    	 //BACK TO TABLE
	   scope.gobacktomain = function(){
		  $('#UpdateSlotField').empty();
	      $('#UpdateSlotSite').empty();
	      $('#updateSlotbtn').empty();
	     scope.siteTable = true;	 
		 scope.siteForm = true;
         scope.updateslotFrom = false;
         $("#submitbtn").show();
         $("#allp").show();

	     };	 
	     
	     
	     /*********************************update slot************************************/
	     var firstarr=0;
	     var seconddarr=1;
	     var thirdarr=2;
	     
	     scope.updateslot = function(UserModel){
	    	 
	    	if(document.getElementById('slot'+firstarr+'').value !="" && document.getElementById('slot'+seconddarr+'').value !="" &&document.getElementById('slot'+thirdarr+'').value !="")
	    		{
	    		scope.Slotbtncall(UserModel,firstarr);
		    	scope.Slotbtncall(UserModel,seconddarr);
		    	scope.Slotbtncall(UserModel,thirdarr);
	    		}
	    	 else  {
	   				
				  scope.message = "Above Fields cannot be Left Blank !";
				  var x = document.getElementById("snackbar_error")
					x.className = "show";
					setTimeout(function() {
						x.className = x.className.replace("show", "");
					}, 3000);
             
				 } 
         };
	     
	     scope.Slotbtncall=function(UserModel,slotarr)
	     {
	    	 var UserModeldata=UserModel[slotarr]
		     var slot=document.getElementById('slot'+slotarr+'').value;
             
             scope.dataRequest = {
		    		    "slotId":UserModeldata.slotId,
	    	      		"availableVehicleSlot":UserModeldata.availableVehicleSlot,
	      				"mSiteModel": UserModeldata.mSiteModel,
	    				"mVehicleClassModel": UserModeldata.mVehicleClassModel,
	     				"occupiedSlot": UserModeldata.occupiedSlot,
	    				"slot":slot
	    	    }
                 scope.requestedData = {
						"sessionTokken" : jwtToken,
						"username":userN,
						"functionName" : "doUpdateSlotInfo",
			  			 "data" : JSON.stringify(scope.dataRequest)
					};
                 
                 http({
						method :'POST',
						url :apiURL+'updateSlotInfo',
						data :scope.requestedData
						}).then(function (response) {	
		 					if(response.status==1){
		 						
		 						scope.response_message = response.message;
		 						var x = document.getElementById("snackbar_success")
				 				x.className = "show";
				 				setTimeout(function() {
									x.className = x.className.replace("show", "");					 
								}, 3000);
				 				 setTimeout(function() {	
		 							 location.reload();
		  						}, 1500); 
							}else if(response.status==0)
							{
								scope.message = response.message;
								var x = document.getElementById("snackbar_error")
				 				x.className = "show";
				 				setTimeout(function() {
									x.className = x.className.replace("show", "");					 
								}, 3000);
				 			
				 				document.getElementById('overlid').style.visibility = "hidden";
								
							}else{
								scope.message = response.message;
								document.getElementById('overlid').style.visibility = "hidden";
								
						    } 
								
		  					      
		 				}).error(function(response) {			 
						});    
	     };
   }]);
       
 
</script> 

</head>
 <body class="" ng-controller="GetDataController"  class="ng-cloak" ng-init="sitedata()">
 
  <%@ include file="Sidebar.jsp"%>	
  
    <div class="content-wrap" >
      <!-- main page content. the place to put widgets in. usually consists of .row > .col-lg-* > .widget.  -->
      <main id="contentt" class="content" role="main" style="  padding-bottom: 70px;" class="ng-cloak">
        <!-- Page content -->
        <!-- add Site data form -->
        <div class="row" ng-show="siteForm">
         <div class="col-lg-12">
          <section class="widget">
            <div class="widget-body">
               <form class="form-horizontal"  name="addUser" novalidate>
                    <fieldset>
                        <legend style="padding-bottom: 12px;font-size: 18px;" id="allp"><strong>Add Site</strong> 
                                  </legend>
                                     <legend style="padding-bottom: 12px;font-size: 18px; display:none"id="update" ><strong>Add Site</strong> 
                                  </legend>
                        <!-- City -->
                        <div class="form-group row">
                            <label for="mlocation" class="col-md-4 form-control-label">Location</label>
                            <div class="col-md-7">
                            
                            	<select class="select2 form-control" name ="locationDetail" ng-model="UserModel.locationDetail"  ng-options="d as d.locationName for d in locationlist track by d.id"  id="mlocation" required>
									<option value="" disabled selected>Select Location  </option>
 									</select>
 									<span style="color:red" ng-show="(addUser.locationDetail.$dirty || submitted) && addUser.locationDetail.$error.required"> Location is required ! </span>	 
								 </div>
                        </div>
                   
                   
                        <!-- Site Name -->
                       <div class="form-group row">
                         <label for="siteName" class="col-md-4 form-control-label ">Site Name</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="siteName"	placeholder="Enter Site Name" ng-model="UserModel.siteName"
											id="siteName" onkeyup="alphaNumericChecker()"  required="required" />
											
									 <span style="color: red" ng-show="(addUser.siteName.$dirty || submitted)&& addUser.siteName.$error.required">
											Site Name  is required !</span> 
											
									 
										  </div>
                        </div>
                        
                        <!-- Description -->
                        <div class="form-group row">
                          <label for="description" class="col-md-4 form-control-label ">Description</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="description" placeholder="Enter Description" ng-model="UserModel.description"
											id="description" onkeyup="alphaNumericChecker()" required="required" />
											
									 <span style="color: red" ng-show="(addUser.description.$dirty || submitted)&& addUser.description.$error.required">
											Description  is required !</span> 
									 
										  </div>
                        </div>
                          
                        
                         <!-- operatingCompany -->
                        <div class="form-group row">
                          <label for="operatingCompany" class="col-md-4 form-control-label ">Operating Company Name</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="operatingCompany" placeholder="Enter Company Name" ng-model="UserModel.operatingCompany"
											id="operatingCompany" onkeyup="alphaNumericChecker()" required="required" />
											
									 <span style="color: red" ng-show="(addUser.operatingCompany.$dirty || submitted)&& addUser.operatingCompany.$error.required">
											Company Name  is required !</span> 
									 
										  </div>
                        </div>
                       
                        
                        
                        <!--Location Type  -->
                        <div class="form-group row">
                          <label for="type" class="col-md-4 form-control-label ">Site Type</label>
                            <div class="col-md-7">
                            
                            <select class="select2 form-control" name ="type" ng-model="UserModel.type"   id="type" required>
									<option value="" disabled selected>Select Type </option>
									<option value="onstreet">onstreet</option>
									<option value="offstreet">offstreet</option>
 									</select>
 									<span style="color: red" ng-show="(addUser.type.$dirty || submitted)&& addUser.type.$error.required">
											Site Type  is required !</span> 
                             
									
										  </div>
                        </div>
                       
                        
                     
                          <!-- Location -->
                        <div class="form-group row">
                          <label for="latitude" class="col-md-4 form-control-label ">Site Location</label>
                            <div class="col-md-3">
                                	<input type="text" class="form-control  " name="latitude"	placeholder="Enter latitude" ng-model="UserModel.latitude"
											id="latitude" onkeyup="alphaNumericChecker()" />
											
											<!-- <span style="color: red" ng-show="(addUser.latitude.$dirty || submitted)&& addUser.latitude.$error.required">
											Latitude is required !</span> --> 
									
									 <div style="margin-top: 15px;">
									 
									 <input type="text" class="form-control  " name="latitude"	placeholder="Enter longitude" ng-model="UserModel.longitude"
											id="longitude" onkeyup="alphaNumericChecker()" />
							
							         <!--  <span style="color: red" ng-show="(addUser.latitude.$dirty || submitted)&& addUser.latitude.$error.required">
											Longitude  is required !</span>  -->
                             
									</div>
									
									   <div style="margin-top: 15px;"> <button class="btn btn-primary" style="background-color: #002B49;" onclick="viewOnMap()" >Preview on Map</button></div>
					    
					    
					    		<div style="margin-top: 15px;">
					              <div class="form-check abc-checkbox abc-checkbox-success">
					               <input class="form-check-input" id="reentry" ng-model="UserModel.isReentryAllowed" type="checkbox" ng-click="checkReentry()">
 					                <label class="form-check-label" for="reentry">Re-Entry Allowed ?
                                     </label>
                                     </div>
 					     
					            </div>
					    
					    
 					    		<div style="margin-top: 15px;">
					              <div class="form-check abc-checkbox abc-checkbox-success">
					               <input class="form-check-input" id="monthlypasscheck" ng-model="UserModel.isMonthlyPassAllowed" type="checkbox" ng-click="checkMonthlyPass()">
 					                <label class="form-check-label" for="monthlypasscheck">Monthly Pass Allowed ?
                                     </label>
                                     </div>
 					     
					            </div>
					            
					            <div style="margin-top: 15px;" ng-if="showPass">
					               <input type="number" class="form-control" name="isMonthlyPassAllowed" placeholder="Enter Monthly Pass Cost" ng-model="UserModel.monthlyPassCost"
											id="isMonthlyPassAllowed" />
					            </div>
					      </div>
					     
					   
					     
					     
                        <div class="col-md-4" id="mpb" style=" padding-left: 0px;">
                           <div id="map" class="mapCard"></div>
 	                     </div>	                       
				         </div>
                        
                     
                    </fieldset>
                    <div class="form-actions bg-transparent">
                        <div class="row">
                            <div class="offset-md-4 col-md-7 col-12">
                           <button type="submit" id="submitbtn" ng-click="submitForm(UserModel)"class="btn btn-primary" style="background-color: #002B49;" id="submitbtn">Next> </button>
                          	<button type="submit" ng-click="updatesite(UserModel)"class="btn btn-primary" id="updatebtn"  style="display:none; background-color: #002B49;">Update </button>
    				 
                            </div>
                        </div>
                    </div>
                     
              </div>
           </section>
        </div>
     </div>
     
  <div id="snackbar_success">{{response_message}}</div>
  <div id="snackbar_error">{{message}}</div>
  
    <div class="row" ng-show="siteTable">
            <div class="col-lg-12">
                <section class="widget">
                    <header>
                         <h5>
                            Site Data<span class="fw-semi-bold"></span>
                        </h5>
                    </header>
                    <div class="widget-body">
                    <div class="table-responsive">
                      <table class="table table-striped dataTable  table-lg mt-lg mb-0" id="siteTable">
                      <thead class=" text-primary">
                      <tr>
                                	<th data-visible="false">ID</th>
                                	       <th style=" text-transform: revert; font-size: 15px;">Location</th>
											<th style=" text-transform: revert; font-size: 15px;">Site </th>
											<th style=" text-transform: revert; font-size: 15px;">Description</th>
											<th style=" text-transform: revert; font-size: 15px;"> Type</th>
											<th style=" text-transform: revert; font-size: 15px;">Total Slots</th>
											<th style=" text-transform: revert; font-size: 15px;">Action</th>
											
						 </tr>
                         </thead>
                       </table>
                        </div>
                        <div class="clearfix">
                     </div>
                    </div>
                </section>
            </div>
        </div>
        
        
        
        
        
        
    <!---------------------------VEHICLE SLOTS---------------------------------->
     
     <!-- <div class="row" ng-show="VehicleFrom">

			<div class="col-lg-12">
				<section class="widget">
					<div class="widget-body">
						 
						<form class="form-horizontal" name="addSlot" novalidate>
						
							<fieldset>
 								<legend style="padding-bottom:12ppx; font-size:18px;">
									<strong>Add Slot Info</strong> <span class="pull-right"><a class="previous" ng-click="gobacktomain()">&laquo; Back</a></span>
								</legend>
								
								<div class="form-group row">
									<label for="mSiteModel" class="col-md-3 form-control-label"><strong>Site Name</strong>
									</label>
									<div class="col-md-4">
								 
										<input type="text" class="form-control" id="mSiteModel"
											name="mSiteModel" value="{{mSiteData.siteName}}"
											readonly="readonly" required="required"/>
 									</div>
								</div>
							
								 <div ng-model= "SlotModel">
								 <div class="form-group row">
									<div class="col-md-3">
										<label for="carslots"
											class=""><strong>Car Slots</strong></label>
										<div class="" style="padding-right: 10px;">
											<input type="number" class="form-control  " name="carslots"
												placeholder="Car Slots" ng-model="PaymentModel.carslots"
												id="carslots" required="required" />
                                       </div>
									</div>
									<div class="col-md-3">
										<label for="busslots"
											class=""><strong>Bus Slots</strong></label>
										<div class="" style="padding-right: 10px;">
											<input type="number" class="form-control  " name="busslots"
												placeholder="Bus Slots" ng-model="PaymentModel.busslots"
												id="busslots" required="required" />
                                       </div>
								</div>
								<div class="col-md-3">
										<label for="bikeslots"
											class=""><strong>Bike Slots</strong></label>
										<div class="" style="padding-right: 10px;">
											<input type="number" class="form-control  " name="bikeslots"
												placeholder="Bike Slots" ng-model="PaymentModel.bikeslots"
												id="bikeslots" required="required" />
                                       </div>
								</div>
								<div class="col-md-3" style="margin-top: 2em;">
									<button type="submit" class="btn btn-primary btn-square " 
												ng-click="slotsadd(addSlot)"
												style="background-color: #002B49;">Next ></button>
								</div>
                              </div>
                                 </div>
							</fieldset>
						</form>
					</div>
				</section>
			</div>



		</div> --><!--end  -->
		
		<!---------------------------Update slot model --------------------------->
		
		 <div class="row" ng-show="updateslotFrom">

			<div class="col-lg-11">
				<section class="widget">
					<div class="widget-body">
						 
						<form class="form-horizontal" name="addSlot" novalidate>
						
						<fieldset>
 							<legend style="padding-bottom:12ppx; font-size:18px;">
									<strong>Update Slot</strong> <span class="pull-right">
								 	<a class="previous" ng-click="gobacktomain()">&laquo; Back</a>
								</span> 
							</legend>
								
								<div id="UpdateSlotSite"></div>
								<div id="UpdateSlotField"></div>
								<!-- <div id="updateSlotbtn"></div> -->
								  <div class="form-actions bg-transparent">
	                              <div class="row"   >
	                                <div class="offset-md-4 col-md-7 col-12">
	                                   <button  type="submit" ng-click="updateslot(allslotdata)" id="UpdateSlotbtn"  class="btn btn-primary" style="background-color: #002B49;">Update</button>
	                               </div>
	                                </div>
	                              </div>
								
						</fieldset>
						</form>
					</div>
				</section>
			</div>
       </div> <!--end  -->
		
		<!--------------------------- slot model --------------------------->
		
		
         <div class="row" ng-show="VehicleFrom">

			<div class="col-lg-11">
				<section class="widget">
					<div class="widget-body">
						 
						<form class="form-horizontal" name="addSlot" novalidate>
						
						<fieldset>
 							<legend style="padding-bottom:12ppx; font-size:18px;">
									<strong>Add Slot Info</strong> <span class="pull-right">
								<!-- 	<a class="previous" ng-click="gobacktomain()">&laquo; Back</a>
								--></span> 
							</legend>
								
								<!-- siteName -->
								<div class="form-group row">
									<label for="mSiteModel" class="col-md-4 form-control-label">Site Name
									</label>
									<div class="col-md-7">
								 <input type="text" class="form-control" id="mSiteslot"
											name="mSiteModel" value="{{mSiteData.siteName}}"
											readonly="readonly" required="required"/>
 									</div>
								</div>
								
								<!-- Slots -->
								<div ng-repeat="cdata in vehiclelist track by $index" repeat-done="layoutDone()" ng-model= "PaymentModel.mVehicleClassModel" >
									<div class="form-group row">
										<label for="{{cdata.className}}"
											class="col-md-4 form-control-label">{{cdata.className}}</label>
										<div class="col-md-7" >
										<input type="number" class="form-control  " name="{{cdata.className}}"
												placeholder="Enter {{cdata.className}} Slot" ng-model="slotmodel"
												id="slot{{$index}}" ng-blur="saveSlotInfo(cdata, slotmodel,$event)" required="required" />
                                         </div>
									 </div>
									 <!-- button  -->
									 <div ng-if="$last">
						       <div class="form-actions bg-transparent">
	                              <div class="row"   >
	                                <div class="offset-md-4 col-md-7 col-12">
	                                   <button  type="submit" ng-click="slotsadd()"  class="btn btn-primary" style="background-color: #002B49;" id="mslot{{$index}}">Next >></button>
	                               </div>
	                                </div>
	                              </div>
	                          </div>
								</div>
								
								
								
							</fieldset>
						</form>
					</div>
				</section>
			</div>
       </div> <!--end  -->
        
        
        
        
        
        
       <!---------------------------PAYMENT FORM AND TABLE---------------------------------->
     
     <div class="row" ng-show="paymentFrom">

			<div class="col-lg-12">
				<section class="widget">
					<div class="widget-body">
						 
						<form class="form-horizontal" name="addpayment" novalidate>
						
							<fieldset>
 								<legend style="padding-bottom:12ppx; font-size:18px;">
									<strong>Add Price Info</strong> <span class="pull-right">
									<!-- <a class="previous" ng-click="gobackto()">&laquo; Back</a></span> -->
								</legend>
								
								<div class="form-group row">
									<label for="mSiteModel" class="col-md-12 form-control-label">Site Name
									</label>
									<div class="col-md-4">
								 
										<input type="text" class="form-control" id="mSitepayment"
											name="mSiteModel" value="{{mSiteData.siteName}}"
											readonly="readonly" required="required"/>
 									</div>
								</div>
 
								<div ng-repeat="cdata in vehiclelist track by $index" ng-model= "PaymentModel.mVehicleClassModel">
									<div class="form-group row">
										<label for="{{cdata.className}}"
											class="col-md-12 form-control-label">{{cdata.className}}</label>
										<div class="col-md-3" style="padding-right: 10px;">
											<input type="number" class="form-control  " name="priceHour"
												placeholder="Hourly Price" ng-model="PaymentModel.priceHour"
												id="priceHour{{cdata.className}}{{$index}}" required="required" />


										</div>
										<div class="col-md-3" style="padding-right: 10px;">
											<input type="number" class="form-control  " name="dayPrice"
												placeholder="Daily Price" ng-model="PaymentModel.dayPrice"
												id="dayPrice{{cdata.className}}{{$index}}" required="required" />



										</div>
										<div class="col-md-3" style="padding-right: 10px;">
											<input type="number" class="form-control  " name="monthprice"
												placeholder="Monthly Price"
												ng-model="PaymentModel.monthPrice" id="monthPrice{{cdata.className}}{{$index}}" required="required" />

										</div>
										
										
										<div class="col-md-2">
											<button type="submit" class="btn btn-primary btn-square " id="{{cdata.classId}}"
												ng-click="paymentadd(PaymentModel, cdata, $event)"
												style="background-color: #002B49;">save</button>


										</div>
										
										<!-- <div class="col-md-2" ng-if="$index>0">
											<button type="button" class="btn btn-primary btn-square" id="{{cdata.classId}}"
												ng-click="paymentadd(PaymentModel, cdata, $index)" ng-disabled="disablebtn($index)"
												style="background-color: #002B49;">save</button>


										</div> -->
										
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</section>
			</div>





		</div>
     
    		
		</main>
</div>
        
  <!-- spinner -->
  <div class="loader-wrap overlay" style="background-color: rgba(0, 0, 0, 0.4);" id="overlid">
		<i class="fa fa-spinner fa-spin"></i>
   </div>
   
   
         <!-- The Loader. Is shown when pjax happens -->
      <div class="loader-wrap" id="overl">
            <i class="fa fa-circle-o-notch fa-spin-fast"></i>
        </div>
        <!-- common libraries. required for every page
         <script src="${pageContext.request.contextPath}/resources/node_modules/jquery/dist/jquery.min.js"></script> 
      -->  <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-pjax/jquery.pjax.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/popper.js/dist/umd/popper.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/dist/js/bootstrap.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/js/dist/util.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/widgster/widgster.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" data-pace-options='{ "target": ".content-wrap", "ghostTime": 1000 }'></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/hammerjs/hammer.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-hammerjs/jquery.hammer.js"></script>


        <!-- common app js -->
        <script src="${pageContext.request.contextPath}/resources/js/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/app.js"></script>

        <!-- Page scripts -->
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.animator/jquery.flot.animator.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.selection.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.time.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.pie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.stack.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.crosshair.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.symbol.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.dashes/jquery.flot.dashes.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap_calendar/bootstrap_calendar/js/bootstrap_calendar.min.js"></script>
       <!-- bootstrap-daterangepicker -->	
       <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
         

     	<script>
	  var marker;
	  var mapZoom;
	  var  map= L.map('map').setView([30.3165,78.0322], 14);
	  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	    attribution: 'Map'
	  }).addTo(map);  
	  var smallIcon = new L.Icon({
	            iconSize: [27, 27],
	            iconAnchor: [13, 27],
	            popupAnchor: [1, -24],
	            iconUrl: 'https://cdn4.iconfinder.com/data/icons/small-n-flat/24/map-marker-512.png'
	        });

	  var arcgisOnline = L.esri.Geocoding.arcgisOnlineProvider();

	  var searchControl = L.esri.Geocoding.geosearch({
	    providers: [
	      arcgisOnline,
	      L.esri.Geocoding.mapServiceProvider({
	        label: 'States and Counties',
	        url: 'https://sampleserver6.arcgisonline.com/arcgis/rest/services/Census/MapServer',
	        layers: [2, 3],
	        searchFields: ['NAME', 'STATE_NAME']
	      })
	    ]
	  }).addTo(map);
	  
	  mapZoom = L.featureGroup().addTo(map);
	  setInterval(function (){
	         map.invalidateSize();
	       }, 100);
	 
	var geocodeService = L.esri.Geocoding.geocodeService();
	
	//var popup = L.popup();
	function onMapClick(e) {
	 //marker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);
	//popup.setLatLng(e.latlng).setContent("You clicked the map at " + e.latlng.toString()).openOn(map);
	 incilat=e.latlng.lat;
	 incilon=e.latlng.lng;  
	 document.getElementById('latitude').value = e.latlng.lat;
	 document.getElementById('longitude').value = e.latlng.lng;   
	 geocodeService.reverse().latlng(e.latlng).run(function (error, result) {	
	     if (marker != undefined) {
	         map.removeLayer(marker);
	         if (error) {
	             return;
	           } 
	   };
	     marker=  L.marker(result.latlng).addTo(map).bindPopup(result.address.Match_addr).openPopup();
	   });
	}
	map.on('click', onMapClick);

	function viewOnMap()
	{ 
	   incilat=document.getElementById('latitude').value;
	   incilon=document.getElementById('longitude').value;    
		var latlng ={"lat":incilat,"lng":incilon};
		 geocodeService.reverse().latlng(latlng).run(function (error, result) {
			 
		     if (marker != undefined) {
		    	 map.removeLayer(marker);
		    	 if (error) {			 	   		  	  
		   	       return;
		   	     }	        
		   }
		     marker=  L.marker(latlng).addTo(mapZoom).bindPopup(result.address.Match_addr).openPopup(); 
		     map.fitBounds(mapZoom.getBounds());
		   }); 
	} 
	

	function alphaNumericChecker() {
		 
		var gsiteName = document.getElementById('siteName');
		gsiteName.value = gsiteName.value.replace(/[^a-zA-Z ]+/, '');

		var gdescription = document.getElementById('description');
		gdescription.value = gdescription.value.replace(/[^a-zA-Z0-9 ]+/, '');

		var goperatingCompany = document.getElementById('operatingCompany');
		goperatingCompany.value = goperatingCompany.value.replace(/[^a-zA-Z0-9 ]+/, '');

		var glat = document.getElementById('latitude');
		glat.value = glat.value.replace(/[^0-9. ]+/, '');

		var glon = document.getElementById('longitude');
		glon.value = glon.value.replace(/[^0-9. ]+/, '');

	}

	
</script>
</body>

</html>


 --%>