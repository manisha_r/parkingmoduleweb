<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
<meta charset="ISO-8859-1">
<title>Add User | VPark</title>
        <link href="${pageContext.request.contextPath}/resources/css/application.min.css" rel="stylesheet">
       
         <link rel="shortcut icon" type="image/x-icon"  href="${pageContext.request.contextPath}/resources/img/logo.png">
       
       
<%--         <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png">
 --%>        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/line-awesome/dist/line-awesome/css/line-awesome.css" rel="stylesheet">
             
        <link href="${pageContext.request.contextPath}/resources/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
		
		<!----------------------------------JQUERY/ ANGULARJS------------------------------------------------------------------- -->
<!--      	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
 -->
          <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>     
 	      <script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>
 
          <!---------------------------------- API URL ------------------------------------------------------>
         <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script>
          
          <!------------------------------ FONT --------------------------------------------------->
	     <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	
	     <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
         <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>

         <!---------------------------------------DATEPICKER-------------------------------------------------------->
		 <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/webmain.css">
	     <!---------------------------------- SweetAlert  ------------------------------------------------------>
          <script src="${pageContext.request.contextPath}/resources/sweetAlert.js"></script> 
	     
 
    <script>
    
    var uid = '<%=session.getAttribute("uid")%>';
	var userN = '<%=session.getAttribute("user")%>';


	var options = {
 		   headers: {
 		     Authorization: 'Bearer ' + '${jwtToken}' // Assuming 'jwtToken' holds the JWT
 		   }
 		 };
      
       var app = angular.module("app", []);
       app.controller("GetDataController", ['$scope','$http','$window', function(scope,http,window) {
          
    	   document.getElementById('overl').style.visibility = "hidden";
    	   document.getElementById('overlid').style.visibility = "hidden";
    	   	
    		scope.todayDate =moment().format('YYYY-MM-DD HH:mm:ss');
        	scope.formdate =moment().format('YYYY-MM-DD HH:mm:ss');
            
        	   
    	   /*******************************GETROLE****************************************/
           http.get(apiURL+'getRole/'+userN+'/doGetRole',options).
   			then(function (response) {
   			if(response.data != null)
   				{
   				if(response.data.status==1){
   			              
   					scope.getrole= response.data.myObjectList;
   					
   							
   		         }
   		         else{}  
   		   }else{
   		     	
   		      }      		       		
   			}, function (response) {
   			});
           
    	   
		  	 /////////////////////////////PASSWORD VARIFICATION///////////////////////////////////////
		  	 
						$(document).ready(function() {
							$('#user_password, #confirm_password').on('keyup', function () {
								
								 var userpass=  $('#user_password').val();
								 var confirm_pass =  $('#confirm_password').val();
								 
								if(confirm_pass!="")
									{
									 if (userpass != confirm_pass) {
										 $('#message').html('Password is not Matching').css('color', 'red');
	 									  } else {
	 										 $('#message').html('confirmed').css('color', 'green');
	 									  }
									}else{
										 $('#message').html('Password is not Matching').css('color', 'red');
									}
							});
						
    	 
           
          
           			    
      	////////////////////////ADD USER//////////////////////////////////////////////		
					
             scope.submitForm = function(){
        	 scope.submitted = true;
        	 var c_pass =  $('#confirm_password').val();
	         	     /////////////DOB///////////////
		         	 var sDateObj = $('#datetimepicker4').data('DateTimePicker').date();
			    	   if(sDateObj != null){
			    		  scope.dob = moment(sDateObj).format('YYYY-MM-DD');
		 	    	   }else
			    		   {
			    		   scope.dob = "";
			    		   }
			    	   
		         	  
         	 /////////////////MODAL VALIDATION FOR NULL VALUE///////////
        	  if(typeof scope.UserModel  !== 'undefined'){
        		 
        		  if(scope.UserModel.hasOwnProperty("username")){
	    	  		   scope.username = scope.UserModel.username;
	    	  	   }else{
	    	  		   scope.username ="";
	    	  	   }
       		  
       		  if(scope.UserModel.hasOwnProperty("firstName")){
	    	  		   scope.firstName = scope.UserModel.firstName;
	    	  	   }else{
	    	  		   scope.firstName ="";
	    	  	   }
       		  if(scope.UserModel.hasOwnProperty("middleName")){
   	  		            scope.middleName = scope.UserModel.middleName;
   	  	            }else{
   	  		             scope.middleName ="";
   	  	            }
       		  if(scope.UserModel.hasOwnProperty("lastName")){
	  		            scope.lastName = scope.UserModel.lastName;
	  	            }else{
	  		             scope.lastName ="";
	  	            }
  		  
       		  if(scope.UserModel.hasOwnProperty("emailId")){
	    	  		   scope.emailId=scope.UserModel.emailId;
	    	  	   }else{
	    	  		   scope.emailId ="";
	    	  	   }
        	
       		  if(scope.UserModel.hasOwnProperty("contactNo")){
   	  		         scope.contactNo=scope.UserModel.contactNo;
   	  	          }else{
   	  		           scope.contactNo ="";
   	  	          }
   	
       		  if(scope.UserModel.hasOwnProperty("user_password")){
   	  		   scope.user_password=scope.UserModel.user_password;
   	  	           }else{
   	  		            scope.user_password ="";
   	  	            }
       		 if(scope.UserModel.hasOwnProperty("mUserRole")){
     	  		   scope.mUserRole=scope.UserModel.mUserRole;
     	  	   }else{
     	  		   scope.mUserRole ="";
     	  	   }
   	
       		scope.dataRequest = {
					"batchId": "",
					"username": scope.username,
					"firstName": scope.firstName,
					"middleName":scope.middleName,
					"lastName": scope.lastName,
					"emailId": scope.emailId,
					"contactNo": scope.contactNo,
					"registrationDate": scope.todayDate,
					"dateOfBirth": scope.dob,
				    "password": scope.user_password,
					"userType": 1,
					"mUserRole":scope.mUserRole
				
		    };
        	  
			scope.requestedData = {
					"username":userN,
					"functionName" : "doAddUser",
		  			 "data" : JSON.stringify(scope.dataRequest)

				};
        	  
			//console.log(scope.dataRequest);
			//VALIDATION
			if (scope.addUser.$valid)
			{
			 if(scope.username != "" && scope.firstName!= "" && scope.lastName!= "" && scope.emailId!= "" && scope.contactNo!= "" && scope.dob!= "" && c_pass!= "" &&  scope.user_password == c_pass )
			 {
				 document.getElementById('overlid').style.visibility = "visible"; 
	        		
			       http({
					method :'POST',
					url :apiURL+'addUser',
					data :scope.requestedData,
				    headers: options.headers

					}).then(function (response) {
	 					if(response.data.status==1){
	 						
	 						scope.response_message = response.data.message;
	 						
	 						var x = document.getElementById("snackbar_success")
 			 				x.className = "show";
 			 				setTimeout(function() {
 								x.className = x.className.replace("show", "");	
 								window.location.href = '${pageContext.servletContext.contextPath}/user'; 
 							}, 3000);
							
						}else if(response.data.status==0)
						{
							scope.message = response.data.message;
							
							var x = document.getElementById("snackbar_error")
 			 				x.className = "show";
 			 				setTimeout(function() {
 								x.className = x.className.replace("show", "");					 
 							}, 3000);
 			 				document.getElementById('overlid').style.visibility = "hidden";
	 						
	 						
						}else if(response.data.status==21){
	  		  				 
		  		        }else{
							scope.message = response.data.message;
							document.getElementById('overlid').style.visibility = "hidden";
	 						
					    } 
							
	  					      
					}, function (response) {
					});
				 
			       
	     	  } else  {
	   				
				  scope.message = "Above Fields cannot be Left Blank !";
				  var x = document.getElementById("snackbar_error")
					x.className = "show";
					setTimeout(function() {
						x.className = x.className.replace("show", "");
					}, 3000);
                 
				 } 
			
			 }//end of if
        	  else{
        		  scope.message = "Above Fields cannot be Left Blank";
        		  var x = document.getElementById("snackbar_error")
					x.className = "show";
					setTimeout(function() {
						x.className = x.className.replace("show", "");
					}, 3000);
				
        	  }
        	  }// end of validate 
        	  
        	  else{
        		  scope.message = "Above Fields cannot be Left Blank";
        		  var x = document.getElementById("snackbar_error")
					x.className = "show";
					setTimeout(function() {
						x.className = x.className.replace("show", "");
					}, 3000);
				
        	  }
       };
   		
      });	 
   }]);
</script> 

</head>
 <body class="" ng-controller="GetDataController">
 
  <%@ include file="Sidebar.jsp"%>	
  
 
    <div class="content-wrap" >
      <!-- main page content. the place to put widgets in. usually consists of .row > .col-lg-* > .widget.  -->
      <main id="contentt" class="content" role="main" style=" padding-left: 60px;padding-right: 60px; padding-bottom: 70px;" class="ng-cloak">
        <!-- Page content -->
        <div class="row">
         <div class="col-lg-11">
          <section class="widget">
            <div class="widget-body">
              <form class="form-horizontal"  name="addUser" novalidate autocomplete="off">
                    <fieldset>
                        <legend style="padding-bottom: 17px;font-size: 19px;"><strong>Add User</strong> 
                             <a class="previous"  href="${pageContext.servletContext.contextPath}/user" style="float: right;">� Back</a>
                  </legend>
                   
                        <!-- USERNAME -->
                       <div class="form-group row">
                         <label for="username" class="col-md-4 form-control-label ">UserName</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="username"	placeholder="Enter UserName" ng-model="UserModel.username"
										 ng-minlength="4"	id="username" onkeyup="alphaNumericChecker();"  required="required" />
											
									 <span style="color: red" ng-show="(addUser.username.$dirty || submitted)&& addUser.username.$error.required">
											UserName  is required !</span> 
									<span style="color: red" ng-show="(addUser.username.$dirty || submitted) && addUser.username.$error.minlength">Minimum 4 character required!</span> 
											
									 
										  </div>
                        </div>
                        
                        <!-- FIRST NAME -->
                        <div class="form-group row">
                          <label for="firstName" class="col-md-4 form-control-label ">First Name</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="firstName"	placeholder="Enter First Name" ng-model="UserModel.firstName"
											id="firstName" onkeyup="alphaNumericChecker();" required="required" />
											
									 <span style="color: red" ng-show="(addUser.firstName.$dirty || submitted)&& addUser.firstName.$error.required">
											First Name  is required !</span> 
									 
										  </div>
                        </div>
                        
                        <!--MIDDLE NAME  -->
                        <div class="form-group row">
                          <label for="middleName" class="col-md-4 form-control-label ">Middle Name</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="middleName"	placeholder="Enter Middle Name" ng-model="UserModel.middleName"
											id="middleName" onkeyup="alphaNumericChecker();"  />
											
									
										  </div>
                        </div>
                        
                        <!-- LAST NAME -->
                        <div class="form-group row">
                          <label for="lastName" class="col-md-4 form-control-label ">Last Name</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="lastName"	placeholder="Enter Last Name" ng-model="UserModel.lastName"
											id="lastName" onkeyup="alphaNumericChecker();"  required="required" />
											
									 <span style="color: red" ng-show="(addUser.lastName.$dirty || submitted)&& addUser.lastName.$error.required">
											Last Name  is required !</span> 
									 
										  </div>
                        </div>
                        
                        <!--EMAIL ADDRESS  -->
                        <div class="form-group row"  ng-class="{true: 'error'}[submitted && addUser.emailId.$invalid]">
                          <label for="emailId" class="col-md-4 form-control-label ">Email Address</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="emailId"	placeholder="Enter Email Address" ng-model="UserModel.emailId"
										 ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"	id="emailId" onkeyup="alphaNumericChecker();"  required="required" />
											
									 <span style="color:red" ng-show="(addUser.emailId.$dirty || submitted) && addUser.emailId.$error.required"> Email   is required  !</span> 
 
													<span style="color:Red" ng-show="addUser.emailId.$dirty && addUser.emailId.$error.pattern">Please Enter Valid Email</span>

 
										  </div>
                        </div>
                        
                        <!--CONTACT NUMBER  -->
                        <div class="form-group row">
                          <label for="contactNo" class="col-md-4 form-control-label ">Contact Number</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="contactNo"	placeholder="Enter Contact Number" ng-model="UserModel.contactNo"
											ng-minlength="10" ng-maxlength="10" ng-pattern="/^[6-9][0-9]{9}$/" id="contactNo" onkeyup="alphaNumericChecker();"   required="required" />
											
									 	
 													<span style="color:red" ng-show="(addUser.contactNo.$dirty || submitted) && addUser.contactNo.$error.required">	Mobile   is required  !</span>
 													<span style="color:red" ng-show="((addUser.contactNo.$error.minlength || addUser.contactNo.$error.maxlength) && addUser	.contactNo.$dirty) ">phone number should be 10 digits</span> 
											     <span style="color:Red" ng-show="addUser.contactNo.$dirty && addUser.contactNo.$error.pattern">Please Enter Valid Mobile number!</span>
											
										  </div>
                        </div>
                    
                         <!-- DOB -->
                         <div class="form-group row">
							<label class="col-md-4 form-control-label required" for="user_dob">Date of Birth
												 </label>
											<div class="col-md-7">
  												<div class='input-group date' id='datetimepicker4'>
												<input type='text' class="form-control" name="dob"
													placeholder="Enter Date of Birth"  id="dob" ng-model="UserModel.dob" onkeyup="alphaNumericChecker();" />
												<span class="input-group-addon datetime"  > <span
													class="fa fa-calendar"></span>
												</span>
												<span style="color:red" ng-show="(addUser.dob.$dirty || submitted) && addUser.dob.$error.required">	Date Of Birth is required ! </span>
											</div>
 											</div>
										</div>
                         
                         <!-- PASSWORD -->
                         <div class="form-group row">
                            <label for="confirm_password" class="col-md-4 form-control-label ">Password</label>
                            <div class="col-md-7">
                                	<input type="password" class="form-control  " name="user_password"	placeholder="Enter Password" ng-model="UserModel.user_password"
										ng-minlength="8" ng-maxlength="20"	id="user_password"  ng-pattern="/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/"  required="required" />
											
									   <span style="color:red" ng-show="(addUser.user_password.$dirty || submitted) && addUser.user_password.$error.required">	Password   is required ! </span>
													  <span style="color: red" ng-show="(addUser.user_password.$dirty || submitted) && addUser.user_password.$error.minlength">Minimum 8 character required!</span> 
  										  											        <div ng-show="addUser.user_password.$error.pattern && addUser.user_password.$dirty" style="color: red;">Password must have a minimum length of 8 characters, consist of alphanumeric, special characters, and contain both lowercase and uppercase characters.</div>
  										
										  </div>
                        </div>
                        
                        <!--REEAT PASSWORD  -->
                        <div class="form-group row">
                            <label for="confirm_password" class="col-md-4 form-control-label ">Repeat Password</label>
                            <div class="col-md-7">
                                	<input type="password" class="form-control  " name="confirm_password"	placeholder="Enter Repeat-Password" ng-model="UserModel.confirm_password"
											id="confirm_password"  required="required" />
											
									<span id='message'></span> 
										  </div>
                        </div>
                        
                        <!-- role Class -->
                         <div class="form-group row">
                            <label for="mUserRole" class="col-md-4 form-control-label "> Role</label>
                            <div class="col-md-7">
                            
                            	<select class="select2 form-control" name ="mUserRole" ng-model="UserModel.mUserRole"ng-options="s as s.role for s in getrole track by s.roleId"  id="mGetrole" required>
									<option value="" disabled selected>Select Role  </option>
 									</select>
 									<span style="color:red" ng-show="(addUser.mUserRole.$dirty || submitted) && addUser.mUserRole.$error.required"> Select Role ! </span>	 
								 </div>
                        </div>
                    </fieldset>
                    
                    <div class="form-actions bg-transparent">
                        <div class="row">
                            <div class="offset-md-4 col-md-7 col-12">
                           <button type="submit" ng-click="submitForm()"class="btn btn-primary" style="background-color: #002B49;" id="submitbtn">Add </button>
                            </div>
                        </div>
                    </div>
                </form> 
              </div>
           </section>
        </div>
     </div>
  <div id="snackbar_success">{{response_message}}</div>
  <div id="snackbar_error">{{message}}</div>
  
  <!-- spinner -->
  <div class="loader-wrap" style="background-color: rgba(0, 0, 0, 0.4);" id="overlid">
		<i class="fa fa-spinner fa-spin"></i>
   </div>
 </main>
</div>
        <!-- The Loader. Is shown when pjax happens -->
      <div class="loader-wrap" id="overl">
            <i class="fa fa-circle-o-notch fa-spin-fast"></i>
        </div>
        <!-- common libraries. required for every page-->
          <script src="${pageContext.request.contextPath}/resources/node_modules/jquery/dist/jquery.min.js"></script> 
         <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-pjax/jquery.pjax.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/popper.js/dist/umd/popper.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/dist/js/bootstrap.js"></script>
     
 
 <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/js/dist/util.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/widgster/widgster.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" data-pace-options='{ "target": ".content-wrap", "ghostTime": 1000 }'></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/hammerjs/hammer.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-hammerjs/jquery.hammer.js"></script>


        <!-- common app js -->
        <script src="${pageContext.request.contextPath}/resources/js/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/app.js"></script>

        <!-- Page scripts -->
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.animator/jquery.flot.animator.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.selection.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.time.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.pie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.stack.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.crosshair.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.symbol.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.dashes/jquery.flot.dashes.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap_calendar/bootstrap_calendar/js/bootstrap_calendar.min.js"></script>
       <!-- bootstrap-daterangepicker -->	
       <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
         

      <script>
			$('#datetimepicker4').datetimepicker({
                format: 'YYYY-MM-DD'
	           });
			

			function alphaNumericChecker() {
				 
				var gusername = document.getElementById('username');
				gusername.value = gusername.value.replace(/[^a-zA-Z0-9@_-]+/, '');

				var gfirstName = document.getElementById('firstName');
				gfirstName.value = gfirstName.value.replace(/[^a-zA-Z ]+/, '');

				var gmiddleName = document.getElementById('middleName');
				gmiddleName.value = gmiddleName.value.replace(/[^a-zA-Z ]+/, '');

			    var glastName = document.getElementById('lastName');
			    glastName.value = glastName.value.replace(/[^a-zA-Z ]+/, '');

				var gemailId = document.getElementById('emailId');
				gemailId.value = gemailId.value.replace(/[^a-zA-Z0-9@.]+/, '');

				var gcontactNo = document.getElementById('contactNo');
				gcontactNo.value = gcontactNo.value.replace(/[^0-9 ]+/, '');
				
				var gdob = document.getElementById('dob');
				gdob.value = gdob.value.replace(/[^a-zA-Z0-9- ]+/, '');
				
				
				
				
			}

	</script>
    </body>

</html>