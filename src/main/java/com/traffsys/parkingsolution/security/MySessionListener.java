package com.traffsys.parkingsolution.security;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.annotation.WebListener;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class MySessionListener implements HttpSessionListener {

    private static final Map<String, Long> sessionLastActivityMap = new HashMap<>();

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        sessionLastActivityMap.put(session.getId(), System.currentTimeMillis());
        System.out.println("Session Created: " + session.getId());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        sessionLastActivityMap.remove(session.getId());
        System.out.println("Session Destroyed: " + session.getId());
    }

    public static void updateSessionActivity(HttpSession session) {
        sessionLastActivityMap.put(session.getId(), System.currentTimeMillis());
        System.out.println("Session Activity Updated: " + session.getId());
    }

    public static void destroyInactiveSessions(long timeoutMillis, HttpServletRequest request) {
    	System.out.println("time3");

        long currentTimeMillis = System.currentTimeMillis();
        ServletContext servletContext = request.getServletContext();
        for (Map.Entry<String, Long> entry : sessionLastActivityMap.entrySet()) {
            if (currentTimeMillis - entry.getValue() >= timeoutMillis) {
                HttpSession session = getSession(entry.getKey(), request);
                if (session != null) {
                    session.invalidate();
                    System.out.println("Session Destroyed (inactive): " + session.getId());
                }
                sessionLastActivityMap.remove(entry.getKey());
            }
        }
    }

    private static HttpSession getSession(String sessionId, HttpServletRequest request) {
        return request.getSession(false);
    }
}
