package com.traffsys.parkingsolution.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.traffsys.parkingsolution.DAO.RemoteDAO;
import com.traffsys.parkingsolution.components.AESEncryption;
import com.traffsys.parkingsolution.components.AesUtil;
import com.traffsys.parkingsolution.components.Logger;
import com.traffsys.parkingsolution.modal.RequestModel;
import com.traffsys.parkingsolution.modal.ResponseModel;
import com.traffsys.parkingsolution.modal.UserLoginModel;
import com.traffsys.parkingsolution.utils.AppConstant;
import com.traffsys.parkingsolution.utils.Util;

@RestController
public class ResetController {

	/*
	 * @RequestMapping(value = "/Reset", method = RequestMethod.POST) public
	 * ResponseModel doCheckUserSession(ModelMap model, @RequestBody RequestModel
	 * mRequestModel, HttpServletRequest mHttpServletRequest, HttpServletResponse
	 * mHttpServletResponse) throws Exception {
	 * 
	 * ResponseModel mResponseModel = null;
	 * 
	 * String stringn = mRequestModel.getData(); JSONObject mJsonObject = new
	 * JSONObject(stringn);
	 * 
	 * 
	 * if (mRequestModel.getData() != null) {
	 * 
	 * String data = mRequestModel.getData(); String functionName =
	 * mRequestModel.getFunctionName();
	 * 
	 * 
	 * String username = ""; String emailId = "";
	 * 
	 * String confirm_password = ""; if (mJsonObject.getString("confirm_password")
	 * != null) { confirm_password = mJsonObject.getString("confirm_password"); }
	 * String password = ""; if (mJsonObject.getString("password") != null) {
	 * password = mJsonObject.getString("password"); }
	 * 
	 * 
	 * if (mJsonObject.getString("emailId") != null) { emailId =
	 * mJsonObject.getString("emailId"); } if (mJsonObject.getString("username") !=
	 * null) { username = mJsonObject.getString("username"); }
	 * 
	 * 
	 * JSONObject loginData = new JSONObject(); loginData.put("username", username);
	 * loginData.put("password", password); loginData.put("emailId", emailId);
	 * loginData.put("confirm_password", confirm_password);
	 * 
	 * System.out.println("loginData: "+loginData.toString());
	 * 
	 * JSONObject masterjson = new JSONObject();
	 * 
	 * masterjson.put(AppConstant.DATA_TAG, loginData.toString());
	 * masterjson.put("functionName", functionName);
	 * 
	 * JSONObject mResponseJson = RemoteDAO.doPost(masterjson, AppConstant.API_URL +
	 * "resetUserPassword");
	 * 
	 * 
	 * 
	 * mResponseModel = Util.setResponse(mResponseJson.getInt("status"),
	 * mResponseJson.getString("message"), null); return mResponseModel;
	 * 
	 * } //return mResponseModel; return mResponseModel;
	 * 
	 * 
	 * 
	 * }
	 */

	@RequestMapping(value = "/Reset", method = RequestMethod.POST)
	public ResponseModel doCheckUserSession(ModelMap model, @RequestBody RequestModel mRequestModel, HttpServletRequest mHttpServletRequest, HttpServletResponse mHttpServletResponse) throws Exception {

	    ResponseModel mResponseModel = null;

	    String stringn = mRequestModel.getData();
	    JSONObject mJsonObject = new JSONObject(stringn);

	    if (mRequestModel.getData() != null) {

	        String data = mRequestModel.getData();
	        String functionName = mRequestModel.getFunctionName();


	        String username = "";
	        String emailId = "";
	        String confirm_password = "";
	        String password = "";

	        // Check if the data contains confirm_password and password fields
	        if (mJsonObject.has("confirm_password")) {
	            confirm_password = mJsonObject.getString("confirm_password");
	        }
	        if (mJsonObject.has("password")) {
	            password = mJsonObject.getString("password");
	        }

	        // Other fields retrieval
	        if (mJsonObject.has("emailId")) {
	            emailId = mJsonObject.getString("emailId");
	        }
	        if (mJsonObject.has("username")) {
	            username = mJsonObject.getString("username");
	        }

	        // Compare password and confirm_password
	        if (!password.equals(confirm_password)) {
	            // Password and confirm_password do not match, handle this case
	            mResponseModel = new ResponseModel(); // Instantiate ResponseModel as needed
	            mResponseModel.setStatus(400); // Set appropriate status code
	            mResponseModel.setMessage("Password and confirm password do not match");
	            return mResponseModel;
	        }

	        // Construct loginData JSONObject
	        JSONObject loginData = new JSONObject();
	        loginData.put("username", username);
	        loginData.put("password", password);
	        loginData.put("emailId", emailId);
	        loginData.put("confirm_password", confirm_password);

	        System.out.println("loginData: " + loginData.toString());

	        JSONObject masterjson = new JSONObject();

	        masterjson.put(AppConstant.DATA_TAG, loginData.toString());
	        masterjson.put("functionName", functionName);

	        JSONObject mResponseJson = RemoteDAO.doPost(masterjson, AppConstant.API_URL + "resetUserPassword");

	        mResponseModel = Util.setResponse(mResponseJson.getInt("status"), mResponseJson.getString("message"), null);
	        return mResponseModel;

	    }

	    return mResponseModel;
	}

}
