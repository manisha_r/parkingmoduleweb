package com.traffsys.parkingsolution.components;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
public class MyProperties {


	/*
	 * @Value("${blocked.gates.id}") private List<Integer> blockedGateId;
	 */
	@Value("${alloweddomains}")
	private List<String> alloweddomains;


	@Value("${whitelistip}")
	private String whitelistip;

	
	@Value("${captchaEnable}")	
	private boolean captchaEnable;
	
	
	public List<String> getAlloweddomains() {
		return alloweddomains;
	}

	public void setAlloweddomains(List<String> alloweddomains) {
		this.alloweddomains = alloweddomains;
	}

	public String getWhitelistip() {
		return whitelistip;
	}

	public void setWhitelistip(String whitelistip) {
		this.whitelistip = whitelistip;
	}


	public List<String> getAllowedDomains() {
		return alloweddomains;
	}

	public void setAllowedDomains(List<String> alloweddomains) {
		this.alloweddomains = alloweddomains;
	}

	public boolean isCaptchaEnable() {
		return captchaEnable;
	}

	public void setCaptchaEnable(boolean captchaEnable) {
		this.captchaEnable = captchaEnable;
	}

 
}
