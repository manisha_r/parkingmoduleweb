<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
<meta charset="ISO-8859-1">
<title>Add Site | VPark</title>
        <link href="${pageContext.request.contextPath}/resources/css/application.min.css" rel="stylesheet">
       
        <link rel="shortcut icon" type="image/x-icon"  href="${pageContext.request.contextPath}/resources/img/logo.png">
       
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/line-awesome/dist/line-awesome/css/line-awesome.css" rel="stylesheet">
             
        <link href="${pageContext.request.contextPath}/resources/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
		
		<!----------------------------------JQUERY/ ANGULARJS------------------------------------------------------------------- -->
     	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
     	<script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>
 
          <!---------------------------------- API URL ------------------------------------------------------>
         <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script>
          
          <!------------------------------ FONT --------------------------------------------------->
	     <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	
	     <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
         <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>

         <!---------------------------------------DATEPICKER-------------------------------------------------------->
		 <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

       
		  <!-------------------------------- Load Leaflet from CDN----------------------------------------------->
		  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
		    integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
		    crossorigin=""/>
		  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
		    integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
		    crossorigin=""></script>
		
		  <!------------------------------ Load Esri Leaflet from CDN -------------------------------------------->
		  <script src="https://unpkg.com/esri-leaflet@2.5.0/dist/esri-leaflet.js"
		    integrity="sha512-ucw7Grpc+iEQZa711gcjgMBnmd9qju1CICsRaryvX7HJklK0pGl/prxKvtHwpgm5ZHdvAil7YPxI1oWPOWK3UQ=="
		    crossorigin=""></script>
		
		  <!------------------------------- Load Esri Leaflet Geocoder from CDN ----------------------------------->
		  <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
		    integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
		    crossorigin="">
		  <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
		    integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
		    crossorigin=""></script>
             
             <!-------------------------------------------markercluster  ------------------------------------------>
	        <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.0.4/dist/MarkerCluster.Default.css">
			<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.0.4/dist/MarkerCluster.css">
			<script src="https://unpkg.com/leaflet.markercluster@1.0.4/dist/leaflet.markercluster.js"></script>
			<script src="${pageContext.request.contextPath}/resources/MovingMarker.js"></script>
			
			<!-- Load Clustered Feature Layer from CDN -->
	        <script src="https://unpkg.com/esri-leaflet-cluster@2.0.0/dist/esri-leaflet-cluster.js"></script>
     	  <!---------------------------------- SweetAlert  ------------------------------------------------------>
          <script src="${pageContext.request.contextPath}/resources/sweetAlert.js"></script> 
	     
 	

	<style type="text/css">
	
	.datetime {
    display: flex;
    align-items: center;
    padding: .375rem .75rem;
    margin-bottom: 0;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.55;
    color: #495057;
    text-align: center;
    white-space: nowrap;
    background-color: #e9ecef;
    border: 1px solid #c1ccd3;
    border-top-left-radius: 0.1rem;
    border-top-right-radius: 0.3rem;
    border-bottom-right-radius: 0.3rem;
    border-bottom-left-radius: 0.1rem;
    }
	 .previous {
     text-decoration: none;
     display: inline-block;
	 padding: 5px 14px;
     background-color: #f1f1f1;
     cursor: pointer;
     FONT-SIZE: 14PX;
     }
     
    .overlay {
	position: absolute;
	top: 0;
	left: 0;
	height: inherit;
	background-color: rgba(0, 0, 0, 0.8);
	z-index: 9999;
    }
    
    #map {
	width: 100%;
	height: 200px;
	order-style: solid;
	border-width: thin;
}

.mapCard {
	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
	transition: 0.3s;
	width: 100%;
}

.mapCard:hover {
	box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
}
	</style>
    
    <script>
    
    
	  $(document).ready(function(){

        $("#confi").addClass("active");
        $("#confi").addClass("open");
        $("#sidebar-ui").addClass("show");
        $("#kcities").addClass("active");
	});
    
    var uid = '<%=session.getAttribute("uid")%>';
	var userN = '<%=session.getAttribute("user")%>';

	var options = {
	 		   headers: {
	 		     Authorization: 'Bearer ' + '${jwtToken}' // Assuming 'jwtToken' holds the JWT
	 		   }
	 		 };
	var incilat=0.0; 
  	var incilon=0.0;
       var app = angular.module("app", []);
       app.controller("GetDataController", ['$scope','$http','$window', function(scope,http,window) {
    	   document.getElementById('overl').style.visibility = "hidden";
    	   document.getElementById('overlid').style.visibility = "hidden";
    	    
           /********************************GET LOCATION***************************************/
              http.get(apiURL+'getlocation/'+userN+'/doGetLocation',options).
              then(function (response) {
   				Pace.stop();

   			  
   				if(response.data != null)
   				{
   				if(response.data.status==1){
   			              
   					scope.locationlist= response.data.myObjectList;
   				    scope.sitelocation=scope.locationlist[0].mSiteModel
   		         }else if(response.data.status==21){
		  				 
  		         }else{
   		        	
   		               }  
   		   }else{
   		     	
   		      }      		       		
              }, function (response) {
				});
    	   
              /********************************GET SITE***************************************/
              http.(apiURL+'getSite/'+userN+'/doGetSiteInfo',options).
              then(function (response) {
   				Pace.stop();

   			  
   				if(response.data != null)
   				{
   				if(response.data.status==1){
   			              
   					scope.sitelist= response.data.myObjectList;
   				  }else if(response.data.status==21){
 		  				 
	  		      }else{
   		        	
   		               }  
   		   }else{
   		     	
   		      }      		       		
              }, function (response) {
				});
    	   
           
    	   /**********************************ADD LOCATION******************************************/
    	   
    	   scope.submitForm = function(){
          	 scope.submitted = true;
           	  
          	 
          /////////////////MODAL VALIDATION FOR NULL VALUE///////////
       	  if(typeof scope.UserModel  !== 'undefined'){
       		 
       		  if(scope.UserModel.hasOwnProperty("mlocation")){
	    	  		   scope.mlocation = scope.UserModel.mlocation.locationName;
	    	  	   }else{
	    	  		   scope.mlocation ="";
	    	  	   }
      		  
      		  if(scope.UserModel.hasOwnProperty("mSiteModel")){
	    	  		   scope.mSiteModel = scope.UserModel.mSiteModel;
	    	  	   }else{
	    	  		   scope.mSiteModel ="";
	    	  	   }
      		
      		var sitedata=[];
      		scope.dataRequest = {
      		"mSiteModel":scope.mSiteModel
      		};
      		sitedata.push(scope.dataRequest);
      		scope.dataRequest = {
					
					"locationName": scope.mlocation,
					"mSiteModel": sitedata
					
				
		    };
       	  
      		
			scope.requestedData = {
					/* "sessionTokken" : jwtToken,
					"username":userN,
					"functionName" : "doAddSiteInfo",
		  			  */
		  			  "data" : JSON.stringify(scope.dataRequest)

				};
			//VALIDATION
			if (scope.addUser.$valid)
			{
			 if(scope.siteName != "" && scope.description!= "" && scope.slots!= "" && scope.type!= "" && document.getElementById('gatlat').value !=""&&  document.getElementById('gatlon').value !="" )
			 {
				 document.getElementById('overlid').style.visibility = "visible"; 
	        		
			        http({
					method :'POST',
					url :apiURL+'',
					data :scope.requestedData,
				    headers: options.headers

					}).success(function(response) {	
	 					if(response.data.status==1){
	 						
	 						scope.response_message = response.data.message;
	 						
	 						var x = document.getElementById("snackbar_success")
			 				x.className = "show";
			 				setTimeout(function() {
								x.className = x.className.replace("show", "");					 
							}, 3000);
							//window.location.href = '${pageContext.servletContext.contextPath}/user'; 
						}else if(response.data.status==0)
						{
							scope.message = response.data.message;
							
							var x = document.getElementById("snackbar_error")
			 				x.className = "show";
			 				setTimeout(function() {
								x.className = x.className.replace("show", "");					 
							}, 3000);
			 				document.getElementById('overlid').style.visibility = "hidden";
	 						
	 						
						}else if(response.data.status==21){
	   		  				 
		  		         }else{
							scope.message = response.data.message;
							document.getElementById('overlid').style.visibility = "hidden";
	 						
					    } 
							
	  					      
					}, function (response) {
					});
	     	  } else  {
	   				
				  scope.message = "Above Fields cannot be Left Blank !";
				  var x = document.getElementById("snackbar_error")
					x.className = "show";
					setTimeout(function() {
						x.className = x.className.replace("show", "");
					}, 3000);
                
				 } 
			
			 }//end of if
       	  else{
       		  scope.message = "Above Fields cannot be Left Blank";
       		  var x = document.getElementById("snackbar_error")
					x.className = "show";
					setTimeout(function() {
						x.className = x.className.replace("show", "");
					}, 3000);
				
       	  }
       	  }// end of validate 
      };
          	 
       
    	
   }]);
</script> 

</head>
 <body class="" ng-controller="GetDataController" ">
 
  <%@ include file="Sidebar.jsp"%>	
  
 
    <div class="content-wrap" >
      <!-- main page content. the place to put widgets in. usually consists of .row > .col-lg-* > .widget.  -->
      <main id="contentt" class="content" role="main" style=" padding-left: 60px;padding-right: 60px; padding-bottom: 70px;" class="ng-cloak">
        <!-- Page content -->
        <div class="row">
         <div class="col-lg-11">
          <section class="widget">
            <div class="widget-body">
              <form class="form-horizontal"  name="addUser" novalidate autocomplete="off">
                    <fieldset>
                        <legend style="padding-bottom: 17px;font-size: 19px;"><strong>Add Site</strong> 
                             </legend>
                   
                   
                   
                      <!-- City -->
                        <div class="form-group row">
                            <label for="mlocation" class="col-md-4 form-control-label ">City</label>
                            <div class="col-md-7">
                            
                            	<select class="select2 form-control" name ="mlocation" ng-model="UserModel.mLocation" ng-change="selectedlocation(UserModel.mLocation)" ng-options="d as d.locationName for d in locationlist track by d.id"  id="mlocation" required>
									<option value="" disabled selected>Select City  </option>
 									</select>
 									<span style="color:red" ng-show="(add_vehicle.mLocation.$dirty || submitted) && add_vehicle.mLocation.$error.required"> City is required ! </span>	 
								 </div>
                        </div>
                       
                    <!--Site location  -->
                          <div class="form-group row">
                            <label for="sitemodel" class="col-md-4 form-control-label ">Location</label>
                            <div class="col-md-7">
                            
                            	<select class="select2 form-control" name ="sitemodel"  ng-model="UserModel.mSiteModel" ng-change="selectdata(UserModel.mSiteModel)" ng-options="a as a.siteName for a in sitelist track by a.sid"  id="sitemodel" required>
									<option value="" disabled selected>Select Site  </option>
 									</select>
 									 <span style="color:red" ng-show="(add_vehicle.mSiteModel.$dirty || submitted) && add_vehicle.mSiteModel.$error.required"> Location is required ! </span>	 
								 </div>
                        </div>
                   
                     
                       
                        
                 
                    </fieldset>
                    <div class="form-actions bg-transparent">
                        <div class="row">
                            <div class="offset-md-4 col-md-7 col-12">
                           <button type="submit" ng-click="submitForm()"class="btn btn-primary" id="submitbtn">Add Site </button>
                            </div>
                        </div>
                    </div>
                     
                 </form> 
              </div>
           </section>
        </div>
     </div>
  <div id="snackbar_success">{{response_message}}</div>
  <div id="snackbar_error">{{message}}</div>
  
  
 </main>
</div>
        
  <!-- spinner -->
  <div class="loader-wrap" style="background-color: rgba(0, 0, 0, 0.4);" id="overlid">
		<i class="fa fa-spinner fa-spin"></i>
   </div>
 </main>
</div>
        <!-- The Loader. Is shown when pjax happens -->
      <div class="loader-wrap" id="overl">
            <i class="fa fa-circle-o-notch fa-spin-fast"></i>
        </div>
        <!-- common libraries. required for every page-->
         <script src="${pageContext.request.contextPath}/resources/node_modules/jquery/dist/jquery.min.js"></script> 
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-pjax/jquery.pjax.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/popper.js/dist/umd/popper.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/dist/js/bootstrap.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/js/dist/util.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/widgster/widgster.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" data-pace-options='{ "target": ".content-wrap", "ghostTime": 1000 }'></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/hammerjs/hammer.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-hammerjs/jquery.hammer.js"></script>


        <!-- common app js -->
        <script src="${pageContext.request.contextPath}/resources/js/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/app.js"></script>

        <!-- Page scripts -->
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.animator/jquery.flot.animator.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.selection.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.time.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.pie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.stack.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.crosshair.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.symbol.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.dashes/jquery.flot.dashes.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap_calendar/bootstrap_calendar/js/bootstrap_calendar.min.js"></script>
       <!-- bootstrap-daterangepicker -->	
       <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
         

     <!-- 	<script>
	  var marker;
	  var mapZoom;
	  var  map= L.map('map').setView([30.3165,78.0322], 14);
	  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	    attribution: 'Map'
	  }).addTo(map);  
	  var smallIcon = new L.Icon({
	            iconSize: [27, 27],
	            iconAnchor: [13, 27],
	            popupAnchor: [1, -24],
	            iconUrl: 'https://cdn4.iconfinder.com/data/icons/small-n-flat/24/map-marker-512.png'
	        });

	  var arcgisOnline = L.esri.Geocoding.arcgisOnlineProvider();

	  var searchControl = L.esri.Geocoding.geosearch({
	    providers: [
	      arcgisOnline,
	      L.esri.Geocoding.mapServiceProvider({
	        label: 'States and Counties',
	        url: 'https://sampleserver6.arcgisonline.com/arcgis/rest/services/Census/MapServer',
	        layers: [2, 3],
	        searchFields: ['NAME', 'STATE_NAME']
	      })
	    ]
	  }).addTo(map);
	  
	  mapZoom = L.featureGroup().addTo(map);
	  setInterval(function (){
	         map.invalidateSize();
	       }, 100);
	 
	var geocodeService = L.esri.Geocoding.geocodeService();
	
	//var popup = L.popup();
	function onMapClick(e) {
	 //marker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);
	//popup.setLatLng(e.latlng).setContent("You clicked the map at " + e.latlng.toString()).openOn(map);
	 incilat=e.latlng.lat;
	 incilon=e.latlng.lng;  
	 document.getElementById('gatlat').value = e.latlng.lat;
	 document.getElementById('gatlon').value = e.latlng.lng;   
	 geocodeService.reverse().latlng(e.latlng).run(function (error, result) {	
	     if (marker != undefined) {
	         map.removeLayer(marker);
	         if (error) {
	             return;
	           } 
	   };
	     marker=  L.marker(result.latlng).addTo(map).bindPopup(result.address.Match_addr).openPopup();
	   });
	}
	map.on('click', onMapClick);

	function viewOnMap()
	{ 
	   incilat=document.getElementById('gatlat').value;
	   incilon=document.getElementById('gatlon').value ;    
		var latlng ={"lat":incilat,"lng":incilon};
		 geocodeService.reverse().latlng(latlng).run(function (error, result) {
			 
		     if (marker != undefined) {
		    	 map.removeLayer(marker);
		    	 if (error) {			 	   		  	  
		   	       return;
		   	     }	        
		   }
		     marker=  L.marker(latlng).addTo(mapZoom).bindPopup(result.address.Match_addr).openPopup(); 
		     map.fitBounds(mapZoom.getBounds());
		   }); 
	} 
</script> --><!--  -->
</body>

</html>