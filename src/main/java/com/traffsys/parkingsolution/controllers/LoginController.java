package com.traffsys.parkingsolution.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.traffsys.parkingsolution.DAO.RemoteDAO;
import com.traffsys.parkingsolution.components.AESEncryption;
import com.traffsys.parkingsolution.components.AesUtil;
import com.traffsys.parkingsolution.components.Logger;
import com.traffsys.parkingsolution.modal.RequestModel;
import com.traffsys.parkingsolution.modal.ResponseModel;
import com.traffsys.parkingsolution.modal.UserLoginModel;
import com.traffsys.parkingsolution.utils.AppConstant;
import com.traffsys.parkingsolution.utils.Util;

@RestController
public class LoginController {

	@Autowired
	HttpServletRequest request;
	HttpServletResponse response;

	public final Integer SESSION_TIMEOUT_IN_SECONDS = 60 * 30;// 30 min timeout

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public ResponseModel doLogin(ModelMap model, @RequestBody RequestModel mRequestModel) throws JSONException {

		JSONObject mParent = new JSONObject();
		mParent.put("requestmodal", mRequestModel);
		ResponseModel mResponseModel = null;
		if (mRequestModel.getData() != null) {

			String mString = mRequestModel.getData();
			String functionName = mRequestModel.getFunctionName();

			JSONObject mJsonObject = new JSONObject(mString);

			String ipAddress = "";
			ipAddress = request.getHeader("X-FORWARDED-FOR"); // proxy
			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}

			/*
			 * String username = ""; if (mJsonObject.getString("username") != null) {
			 * username = crypto(mJsonObject.getString("username")); } String password = "";
			 * if (mJsonObject.getString("password") != null) { password =
			 * crypto(mJsonObject.getString("password"));
			 * 
			 * }
			 */
			String password = "";
	        String username = "";

			if (mJsonObject.getString("username") != null && mJsonObject.getString("password") != null) {
				
				  String decryptedPassword =  new String(java.util.Base64.getDecoder().decode(mJsonObject.getString("password")));		
				  String decryptedusername =  new String(java.util.Base64.getDecoder().decode(mJsonObject.getString("username")));


			        AesUtil aesUtil = new AesUtil(128, 1000);
			        
			        if (decryptedPassword != null && decryptedPassword.split("::").length == 3) {

			            password = aesUtil.decrypt(decryptedPassword.split("::")[1], decryptedPassword.split("::")[0], "1234567891234567", decryptedPassword.split("::")[2]);

			        }
			        
			        if (decryptedusername != null && decryptedusername.split("::").length == 3) {

			        	username = aesUtil.decrypt(decryptedusername.split("::")[1], decryptedusername.split("::")[0], "1234567891234567", decryptedusername.split("::")[2]);

			        }
				
			}
			Boolean pushLogin = false;
			pushLogin = mJsonObject.getBoolean("pushLogin");

			UserLoginModel mLoginModel = new UserLoginModel();
			mLoginModel.setUsername(username);
			mLoginModel.setPassword(password);
			mLoginModel.setUserType(1);
			mLoginModel.setPushLogin(pushLogin);

			JSONObject mJsonObject2 = Util.convertPOJOToJSON(mLoginModel);

			JSONObject encryptedJSON = enCryptLoginData(mJsonObject2);
			encryptedJSON.put("sessionTokken", ipAddress);
			encryptedJSON.put("functionName", functionName);

System.out.println("data ::::::::::::::::............"+encryptedJSON);			// REMOTE DAO CONNECTION
			JSONObject mResponseJson = RemoteDAO.doPost(encryptedJSON, AppConstant.API_URL + "login");
			System.out.println(AppConstant.API_URL+ "login");
			if (mResponseJson != null) {

				if (mResponseJson.getInt("status") == 1) {

					boolean isAdmin = false;
					boolean isSuperadmin = false;
				 
					String jwtToken =mResponseJson.getString("sessionTokken");
 					
					JSONObject mObject = mResponseJson.getJSONObject("myObjectList");
					System.out.println(mResponseJson);
					String tokkenKey = mObject.getString("tokkenKey");
					int uid = mObject.getInt("userId");
					String uname = mObject.getString("username");

					JSONObject mrolejson = mObject.getJSONObject("mUserRole");
					if (mrolejson != null) {
						String urole = mrolejson.getString("role");
						if (urole != null) {
							if (!urole.isEmpty()) {
								if (urole.equalsIgnoreCase("User")) {
									isAdmin = false;
									isSuperadmin = false;

								} else if (urole.equalsIgnoreCase("superadmin")) {

									isSuperadmin = true;
									isAdmin = true;

								} else {
									isAdmin = true;
									isSuperadmin = false;

								}
							}
						}
					}

					String urole = mrolejson.getString("role");
					HttpSession session = request.getSession();

					JSONArray masterPrivileges = mrolejson.getJSONArray("masterPrivileges");

					if (masterPrivileges != null) {
						if (masterPrivileges.length() > 0) {
							for (int l = 0; l < masterPrivileges.length(); l++) {
								JSONObject mPrivilegeObject = masterPrivileges.getJSONObject(l);
								String privilegeName = mPrivilegeObject.getString("privilege");
								model.put(privilegeName, true);
								session.setAttribute(privilegeName, true);
							}

						}
					}

					String prijson = masterPrivileges.toString();

					session.setAttribute("uid", uid);
					session.setAttribute("isAdmin", isAdmin);
					session.setAttribute("isSuperadmin", isSuperadmin);
					session.setAttribute("isLoggedIn", true);
					session.setAttribute("token", tokkenKey);
					session.setAttribute("jwtToken", jwtToken);
					session.setAttribute("user", uname);
					session.setAttribute("urole", urole);
					session.setAttribute("privileges", prijson);
					session.setMaxInactiveInterval(SESSION_TIMEOUT_IN_SECONDS);

					mResponseModel = setResponse(1, mResponseJson.getString("message"), null);
					return mResponseModel;
				} else {
					mResponseModel = setResponse(0, mResponseJson.getString("message"), null);
				}

			} else {
				System.err.println("Server not responded");
				model.put("message", "Unreachable");
				mResponseModel = setResponse(0, "Error", null);

				return mResponseModel;
			}

		}
		return mResponseModel;
	}

	private String crypto(String strng) {
		String encryptedPassword = "";
		String original = "enajklwymbcdrsopqtuxvzfghi";
		String encrypted = "abcdefghijklmnopqrstuvwxyz";

		for (int i = 0; i < strng.length(); i++) {
			boolean isUpperCase = false;
			char ch = strng.charAt(i);
			if (Character.isUpperCase(ch)) {
				isUpperCase = true;
			}
			ch = Character.toLowerCase(ch);
			for (int j = 0; j < original.length(); j++) {
				if (ch == original.charAt(j)) {
					ch = encrypted.charAt(j);
					break;
				}
			}
			if (isUpperCase) {
				ch = Character.toUpperCase(ch);
			}
			encryptedPassword += ch;
		}
		return encryptedPassword;

	}

	private JSONObject enCryptLoginData(JSONObject mLoginModel) {
		JSONObject jsonObject = new JSONObject();
		final String mCipherText = AESEncryption.encrypt(mLoginModel.toString(), AppConstant.AES_KEY);
		try {
			jsonObject.put(AppConstant.DATA_TAG, mCipherText);
			jsonObject.put(AppConstant.SESSION_TOKKEN, "");
			jsonObject.put(AppConstant.USER_ID, "");
		} catch (JSONException e) {
			Logger.getPathInstance().log(e.getMessage(), LoginController.class.getSimpleName(), AppConstant.ERROR,
					e.getStackTrace()[0].getLineNumber());
		}
		return jsonObject;

	}

	private ResponseModel setResponse(final Integer status_code, final String response_message,
			final Object myObjectList) {
		ResponseModel response = new ResponseModel();
		response.setStatus(status_code);
		response.setMessage(response_message);
		response.setMyObjectList(myObjectList);
		return response;
	}

	public static JSONObject convertPOJOToJSON(final Object mModelObject) {
		ObjectMapper objectMapper = new ObjectMapper();
		JSONObject mJsonObject = null;
		String jsonObject;
		try {
			jsonObject = objectMapper.writeValueAsString(mModelObject);
			mJsonObject = new JSONObject(jsonObject);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
		return mJsonObject;

	}
}
