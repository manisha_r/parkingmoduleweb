package com.traffsys.parkingsolution.security;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class SessionCleanupScheduler {

    private static final long TIMEOUT_MILLIS = 1 * 60 * 1000; // 2 minutes

    @Autowired
    private MySessionListener sessionListener;

    private static final ThreadLocal<HttpServletRequest> threadLocalRequest = new ThreadLocal<>();
    private static final AtomicLong lastExecutionTime = new AtomicLong(0);

    public static void setThreadLocalRequest(HttpServletRequest request) {
        threadLocalRequest.set(request);
    }

    public static HttpServletRequest getThreadLocalRequest() {
        return threadLocalRequest.get();
    }

    @Scheduled(fixedDelay = TIMEOUT_MILLIS)
    public void cleanupInactiveSessions() {
    	System.out.println("time2");

        HttpServletRequest request = getThreadLocalRequest();
        long currentTimeMillis = System.currentTimeMillis();
        
        // Check if the last execution time is beyond the timeout period
        if (currentTimeMillis - lastExecutionTime.get() >= TIMEOUT_MILLIS) {
        	System.out.println("time");
            if (request != null) {
                MySessionListener.destroyInactiveSessions(TIMEOUT_MILLIS, request);
            } else {
                // Handle case when request is not available
            }
            // Update the last execution time
            lastExecutionTime.set(currentTimeMillis);
        }
    }
}
