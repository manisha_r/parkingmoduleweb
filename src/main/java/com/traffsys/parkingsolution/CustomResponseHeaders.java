package com.traffsys.parkingsolution;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;

@WebFilter("/*")
public class CustomResponseHeaders implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletResponse mHttpServletResponse = (HttpServletResponse) response;
		HttpServletRequest mHttpServletRequest = (HttpServletRequest) request;
		 
			
			 if (mHttpServletRequest.getMethod().equals("GET") || mHttpServletRequest.getMethod().equals("POST")) {
				 	
				    chain.doFilter(request, response);
				    
				    //------------response headers-----------------------//
				    
 				    mHttpServletResponse.setHeader("X-FRAME-OPTIONS", "DENY");
					mHttpServletResponse.setHeader("Strict-Transport-Security", "max-age=7776000");
					mHttpServletResponse.setHeader("Pragma", "No-cache");
					mHttpServletResponse.setHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
					mHttpServletResponse.setDateHeader("Expires", 0);
					
					mHttpServletResponse.setHeader("X-XSS-Protection", "1; mode=block");
					mHttpServletResponse.setHeader("X-Powered-By", "");
					mHttpServletResponse.setHeader("Content-Security-Policy", "default-src *; script-src 'self' 'unsafe-inline' 'unsafe-eval' *; style-src 'self' 'unsafe-inline' *; img-src 'self' blob: * data:;");
					
					mHttpServletResponse.setHeader("X-Content-Type-Options", "nosniff");
					mHttpServletResponse.setHeader("Content-Type", " text/html; charset=utf-8 ");
					 
		        } else {
		        	
		        	mHttpServletResponse.setHeader("Allow", "GET,POST");
		        	mHttpServletResponse.sendError(HttpStatus.METHOD_NOT_ALLOWED.value());
 		        }
	}

}
