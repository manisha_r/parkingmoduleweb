<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

 
<style>
	
	
	.sidebar-nav>li ul>li.active>a {
    font-weight: 600;
    color: #F2BE35;
}
	.sidebar-nav>.open>a {
    background-color: #0c1e2b;
}
	.sidebar-nav>li ul {
    padding: 0;
    font-size: 13px;
    background-color: #0c1e2b;
    list-style: none;
}
	.sidebar {
	    position: fixed;
	    z-index: 0;
	    left: 0;
	    top: 0;
	    bottom: 0;
	    width: 224px;
	    border-right: 1px solid #031d2e;
	    background-color: #1b2836;
	    color: #c2c2c2;
	}
	.sidebar-nav>li>a:hover {
	    color: #dcdbdb;
	    background-color: #0c1e2b;
	}
	.sidebar-nav>.active>a {
	    background-color: #0c1e2b;
	    font-weight: 400;
	}
	.sidebar-nav>.active>a, .sidebar-nav>.active>a:hover {
	    color: #F2BE35;
	}
	
	.sidebar-nav>.active>a .icon {
	    border-radius: 50%;
	    background-color: #F2BE35;
	}
	.logo {
	   height: 90px;
	}
	img {
	   vertical-align: middle;
	   margin-top: 10px;
	}
	</style>
        <script>
        
        var uid = '<%=session.getAttribute("uid")%>';
        var userN = '<%=session.getAttribute("user")%>';

        var status ='<%= session.getAttribute("isLoggedIn")%>';
        var token = '<%=session.getAttribute("token") %>';
        var jwtToken = '<%=session.getAttribute("jwtToken") %>';

        var urole = '<%=session.getAttribute("urole") %>';
        var userpri = '<%=session.getAttribute("privileges") %>';
        

		 $(document).ready(function(){
			
			//user
			$("#puser").append('<b>'+userN+'</b>');
			
			
			/*************** sidebar active state *****************/
			  /*  $.each($('.sidebar-nav').find('li'), function() {
			        $(this).toggleClass('active', 
			            window.location.pathname.indexOf($(this).find('a').attr('href')) > -1);
			    });    */
			    
			 
			    
			   $('#pageload').click(function() {
				    location.reload();
				});
		 });
		 
		
		 
		//-----------logout click------------//
		 app.controller('dologoutcontroller', ['$scope','$http','$window', function(scope,http,window) {
		 	
			 
			 
			 setInterval(function(){
		
		 http({
			    method: 'POST',
			    url: '${pageContext.request.contextPath}/doCheckSession',
			}).then(function (response) {
			    // Check if the response status indicates a session expiration
			    if (response.data.status == 21) {
			        // Show a warning message using SweetAlert
			        swal({
			            title:  response.data.message,
			            text: "",
			            icon: "warning",
			            buttons:  ["Cancel", "Login Again"],
			            dangerMode: true,
			        }).then((value) => {
			            // Redirect to the login page if the user chooses to login again
			            window.location.href = '${pageContext.servletContext.contextPath}/signin';
			        });
			    }
			}, function (error) {
			    // Handle errors if they occur during the POST request
			    // Add appropriate error handling logic here
			});

 
			 }, 5000);
			 scope.doLogouthere= function()
		 	  {
		 		 var reqLogout = {
		   				 method: 'POST',
		   				 url: '${pageContext.request.contextPath}/logout',
		     			}
		 		 
		 		 http(reqLogout).then(function(response){
		  			 window.location.href="${pageContext.request.contextPath}/signin";
		  		 }, function(response){
		 			 window.location.href="${pageContext.request.contextPath}/signin";	
		  		});
		 		 

		 	   }; 
		 	  
		 }]);
		 
        </script>
</head>
 <body class="">
        
        <nav id="sidebar" class="sidebar" role="navigation">
            <!-- need this .js class to initiate slimscroll -->
            <div class="js-sidebar-content" ng-controller="dologoutcontroller">
                <header class="logo d-none d-md-block">
                <img src="${pageContext.servletContext.contextPath}/resources/img/parkinglogo.png" style="width:70px; height:70px;">
                     <a><span style="color: #F2BE35;">VPark</span> app</a> 
                </header>
      
                <ul class="sidebar-nav">
                
                    <li class="" id="kmap"><a href="${pageContext.servletContext.contextPath}/map"><span class="icon"><i class="fi flaticon-map-location">  </i></span>Home</a> </li>
                     
                    <li class=""  id="khome"><a href="${pageContext.servletContext.contextPath}/Home"><span class="icon"> <i class="fi flaticon-home"></i></span>Dashboard</a></li>
                    
                    <li class=""  id="kviewSite"><a href="${pageContext.servletContext.contextPath}/viewsite"><span class="icon"><i class="fi flaticon-view-2"></i></span> Site Status</a></li>
                    
                    <li class=""  id="kDevice"><a href="${pageContext.servletContext.contextPath}/Device"><span class="icon"> <i class="fi flaticon-network"></i></span>Device Status</a></li>
                    
                    <li class=""  id="kaddvehicle"><a href="${pageContext.servletContext.contextPath}/addVehicle"><span class="icon"><i class="fi flaticon-add-2"></i></span>Register Vehicle</a></li>
                    
                    <li class="" id="kgetVehicle"><a href="${pageContext.servletContext.contextPath}/getVehicle"><span class="icon"><i class="fi flaticon-equal-1"></i></span>Vehicle Information</a></li>
                    
                    <li class="" id="kgetReserveParking"><a href="${pageContext.servletContext.contextPath}/ReserveParking"><span class="icon"><i class="fa fa-ticket" style="font: normal normal normal 14px/1 FontAwesome"></i></span>Parking Reservation </a></li>

                    <li class="" id="kgetMonthlyPass"><a href="${pageContext.servletContext.contextPath}/MonthlyPass"><span class="icon"><i class="fa fa-rupee"></i></span>Monthly Pass</a></li>
                    
                    <li class="" id="confi"><a class="collapsed" href="#sidebar-ui" data-toggle="collapse"  aria-expanded="false"><span class="icon"><i class="fi flaticon-settings-4"></i></span> Configurations<i class="toggle fa fa-angle-down"></i></a>                       
                        <ul id="sidebar-ui" class="collapse" style="">
                            <li class="" id="kallUser"><a href="${pageContext.servletContext.contextPath}/user"> All User</a></li>
                            <li class="" id="ksite"><a href="${pageContext.servletContext.contextPath}/Site">All Site</a></li>
                            <li class="" id="kcities"><a href="${pageContext.servletContext.contextPath}/City">All Cities</a></li>
                       </ul>
                    </li>
                    
                   <!--report  --> 
                    <li class="" id="sreports"><a class="collapsed" href="#report-ui" data-toggle="collapse"  aria-expanded="false"><span class="icon"> <i class="fi flaticon-controls-6"></i></span>Reports<i class="toggle fa fa-angle-down"></i></a>
                        <ul id="report-ui" class="collapse" style="">
                            <li class="" id="sSiteLogs"><a href="${pageContext.servletContext.contextPath}/SiteLogs">Site Log</a> </li>
                            <li class="" id="sOperatorWise"><a href="${pageContext.servletContext.contextPath}/OperatorWise">Operator Wise</a> </li>
                            <li class="" id="sreport"><a href="${pageContext.servletContext.contextPath}/report">Vehicle Count</a> </li>
                            <li class="" id="sallreport"><a href="${pageContext.servletContext.contextPath}/allreport">Site Report</a></li>
                            <li class="" id="spayment"> <a href="${pageContext.servletContext.contextPath}/payment">Payment Collection</a></li>
                          
                        </ul>
                    </li>
                  
                    <li class=""><%-- <a href="${pageContext.servletContext.contextPath}/logout">  --%>
                    						 <a href="" ng-click="doLogouthere()">
                    
                    <span class="icon"><i class="fi flaticon-exit"></i> </span> Logout</a></li>
                  
                </ul>
                
             
            </div>
        </nav>
            <!-- This is the white navigation bar seen on the top. A bit enhanced BS navbar. See .page-controls in _base.scss. -->
             <nav class="page-controls navbar navbar-dashboard">
                 
                <div class="container-fluid">
                    <!-- .navbar-header contains links seen on xs & sm screens -->
                    <div class="navbar-header mr-md-3">
                        <ul class="nav navbar-nav">
                            <li class="nav-item">
                                <!-- whether to automatically collapse sidebar on mouseleave. If activated acts more like usual admin templates -->
                            
                                <a class="d-none d-lg-block nav-link" id="nav-state-toggle" href="#">
                                    <i class="la la-bars"></i>
                                </a>
                                <!-- shown on xs & sm screen. collapses and expands navigation -->
            
                                <a class="d-lg-none nav-link" id="nav-collapse-toggle" href="#">
                                    <span class="square square-lg d-md-none"><i class="la la-bars"></i></span>
                                    <i class="la la-bars d-none d-md-block"></i>
                                </a>
                            </li>
                            <li class="nav-item d-none d-md-block ml-3"><a href="#" class="nav-link"><i class="la la-refresh" id="pageload"></i></a></li>
                            <li class="nav-item ml-n-xs d-none d-md-block ml-3"><a href="#" class="nav-link"><!-- <i class="la la-times"></i> --></a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right d-md-none">
                            <li class="nav-item">
                                <!-- toggles chat -->
                                <a class="nav-link" href="#" data-toggle="chat-sidebar">
                                    <span class="square square-lg"><i class="la la-globe"></i></span>
                                </a>
                            </li>
                        </ul>
                        <!-- xs & sm screen logo -->
                        <a class="navbar-brand d-md-none" href="${pageContext.servletContext.contextPath}/Home">
                            <i class="fa fa-circle text-warning"></i>
                            &nbsp;
                            VPark App
                            &nbsp;
                            <i class="fa fa-circle text-gray"></i>
                        </a>
                    </div>
            
                    <!-- this part is hidden for xs screens -->
                    <div class="navbar-header mobile-hidden">
                        <!-- search form! link it to your search server -->
                       <form class="navbar-form" role="search"> 
                            <div class="form-group">
                                <div class="input-group input-group-no-border ml-4">
                                  <!--   <input class="form-control" id="main-search" type="text" placeholder="Search">
                                    <span class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-search"></i>
                                        </span>
                                    </span> -->
                                </div>
                            </div>
                        </form>
                        <ul class="nav navbar-nav float-right">
                            <li class="dropdown nav-item">
                              
                                    <span class="thumb-sm avatar ">
                                   <%--      <img class="rounded-circle" src="${pageContext.request.contextPath}/resources/demo/img/people/a5.jpg" alt="..." >
                                 --%>   
                                 <i class="fi flaticon-user-3" style="font-size: 23px;">
                                 </i>
                                  </span>
                                    &nbsp;
                                    <strong id="puser"></strong>&nbsp;
                                   <!--  <span class="circle bg-primary fw-bold text-white">
                                        15
                                    </span> -->
                               
                        </li>
                       
                        </ul>
                    </div>
                </div>
            </nav> 

       <div class="content-wrapd">
        
        </div>
   
 
    </body>

</html>