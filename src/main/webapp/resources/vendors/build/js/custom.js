/**
 * Resize function without multiple trigger
 * 
 * Usage:
 * $(window).smartresize(function(){  
 *     // code here
 * });
 */
(function($,sr){
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
      var timeout;

        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap)
                    func.apply(obj, args); 
                timeout = null; 
            }

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100); 
        };
    };

    // smartresize 
    jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
    $BODY = $('body'),
    $MENU_TOGGLE = $('#menu_toggle'),
    $SIDEBAR_MENU = $('#sidebar-menu'),
    $SIDEBAR_FOOTER = $('.sidebar-footer'),
    $LEFT_COL = $('.left_col'),
    $RIGHT_COL = $('.right_col'),
    $NAV_MENU = $('.nav_menu'),
    $FOOTER = $('footer');

	
	
// Sidebar
function init_sidebar() {
// TODO: This is some kind of easy fix, maybe we can improve this
var setContentHeight = function () {
	// reset height
	$RIGHT_COL.css('min-height', $(window).height());

	var bodyHeight = $BODY.outerHeight(),
		footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
		leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
		contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

	// normalize content
	contentHeight -= $NAV_MENU.height() + footerHeight;

	$RIGHT_COL.css('min-height', contentHeight);
};

  $SIDEBAR_MENU.find('a').on('click', function(ev) {
	  ////console.log('clicked - sidebar_menu');
        var $li = $(this).parent();

        if ($li.is('.active')) {
            $li.removeClass('active active-sm');
            $('ul:first', $li).slideUp(function() {
                setContentHeight();
            });
        } else {
            // prevent closing menu if we are on child menu
            if (!$li.parent().is('.child_menu')) {
                $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                $SIDEBAR_MENU.find('li ul').slideUp();
            }else
            {
				if ( $BODY.is( ".nav-sm" ) )
				{
					$li.parent().find( "li" ).removeClass( "active active-sm" );
					$li.parent().find( "li ul" ).slideUp();
				}
			}
            $li.addClass('active');

            $('ul:first', $li).slideDown(function() {
                setContentHeight();
            });
        }
    });

// toggle small or large menu 
$MENU_TOGGLE.on('click', function() {
		////console.log('clicked - menu toggle');
		
		if ($BODY.hasClass('nav-md')) {
			$SIDEBAR_MENU.find('li.active ul').hide();
			$SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
		} else {
			$SIDEBAR_MENU.find('li.active-sm ul').show();
			$SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
		}

	$BODY.toggleClass('nav-md nav-sm');

	setContentHeight();

	$('.dataTable').each ( function () { $(this).dataTable().fnDraw(); });
});

	// check active menu
	$SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

	$SIDEBAR_MENU.find('a').filter(function () {
		return this.href == CURRENT_URL;
	}).parent('li').addClass('current-page').parents('ul').slideDown(function() {
		setContentHeight();
	}).parent().addClass('active');

	// recompute content when resizing
	$(window).smartresize(function(){  
		setContentHeight();
	});

	setContentHeight();

	// fixed sidebar
	if ($.fn.mCustomScrollbar) {
		$('.menu_fixed').mCustomScrollbar({
			autoHideScrollbar: true,
			theme: 'minimal',
			mouseWheel:{ preventDefault: true }
		});
	}
};
// /Sidebar

	var randNum = function() {
	  return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
	};


// Panel toolbox
$(document).ready(function() {
    $('.collapse-link').on('click', function() {
        var $BOX_PANEL = $(this).closest('.x_panel'),
            $ICON = $(this).find('i'),
            $BOX_CONTENT = $BOX_PANEL.find('.x_content');
        
        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.attr('style')) {
            $BOX_CONTENT.slideToggle(200, function(){
                $BOX_PANEL.removeAttr('style');
            });
        } else {
            $BOX_CONTENT.slideToggle(200); 
            $BOX_PANEL.css('height', 'auto');  
        }

        $ICON.toggleClass('fa-chevron-up fa-chevron-down');
    });

    $('.close-link').click(function () {
        var $BOX_PANEL = $(this).closest('.x_panel');

        $BOX_PANEL.remove();
    });
});
// /Panel toolbox

// Tooltip
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });
});
// /Tooltip

// Progressbar
if ($(".progress .progress-bar")[0]) {
    $('.progress .progress-bar').progressbar();
}
// /Progressbar

// Switchery
$(document).ready(function() {
    if ($(".js-switch")[0]) {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, {
                color: '#26B99A'
            });
        });
    }
});
// /Switchery


// iCheck
$(document).ready(function() {
    if ($("input.flat")[0]) {
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    }
});
// /iCheck

// Table
$('table input').on('ifChecked', function () {
    checkState = '';
    $(this).parent().parent().parent().addClass('selected');
    countChecked();
});
$('table input').on('ifUnchecked', function () {
    checkState = '';
    $(this).parent().parent().parent().removeClass('selected');
    countChecked();
});

var checkState = '';

$('.bulk_action input').on('ifChecked', function () {
    checkState = '';
    $(this).parent().parent().parent().addClass('selected');
    countChecked();
});
$('.bulk_action input').on('ifUnchecked', function () {
    checkState = '';
    $(this).parent().parent().parent().removeClass('selected');
    countChecked();
});
$('.bulk_action input#check-all').on('ifChecked', function () {
    checkState = 'all';
    countChecked();
});
$('.bulk_action input#check-all').on('ifUnchecked', function () {
    checkState = 'none';
    countChecked();
});

function countChecked() {
    if (checkState === 'all') {
        $(".bulk_action input[name='table_records']").iCheck('check');
    }
    if (checkState === 'none') {
        $(".bulk_action input[name='table_records']").iCheck('uncheck');
    }

    var checkCount = $(".bulk_action input[name='table_records']:checked").length;

    if (checkCount) {
        $('.column-title').hide();
        $('.bulk-actions').show();
        $('.action-cnt').html(checkCount + ' Records Selected');
    } else {
        $('.column-title').show();
        $('.bulk-actions').hide();
    }
}



// Accordion
$(document).ready(function() {
    $(".expand").on("click", function () {
        $(this).next().slideToggle(200);
        $expand = $(this).find(">:first-child");

        if ($expand.text() == "+") {
            $expand.text("-");
        } else {
            $expand.text("+");
        }
    });
});

// NProgress
if (typeof NProgress != 'undefined') {
    $(document).ready(function () {
        NProgress.start();
    });

    $(window).load(function () {
        NProgress.done();
    });
}

	
	  //hover and retain popover when on popover content
        var originalLeave = $.fn.popover.Constructor.prototype.leave;
        $.fn.popover.Constructor.prototype.leave = function(obj) {
          var self = obj instanceof this.constructor ?
            obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
          var container, timeout;

          originalLeave.call(this, obj);

          if (obj.currentTarget) {
            container = $(obj.currentTarget).siblings('.popover');
            timeout = self.timeout;
            container.one('mouseenter', function() {
              //We entered the actual popover – call off the dogs
              clearTimeout(timeout);
              //Let's monitor popover content instead
              container.one('mouseleave', function() {
                $.fn.popover.Constructor.prototype.leave.call(self, self);
              });
            });
          }
        };

        $('body').popover({
          selector: '[data-popover]',
          trigger: 'click hover',
          delay: {
            show: 50,
            hide: 400
          }
        });


	function gd(year, month, day) {
		return new Date(year, month - 1, day).getTime();
	}
	  
	
	function init_flot_chart(){
		
		if( typeof ($.plot) === 'undefined'){ return; }
		
		////console.log('init_flot_chart');
		
		
		
		var arr_data1 = [
			[gd(2012, 1, 1), 17],
			[gd(2012, 1, 2), 74],
			[gd(2012, 1, 3), 6],
			[gd(2012, 1, 4), 39],
			[gd(2012, 1, 5), 20],
			[gd(2012, 1, 6), 85],
			[gd(2012, 1, 7), 7]
		];

		var arr_data2 = [
		  [gd(2012, 1, 1), 82],
		  [gd(2012, 1, 2), 23],
		  [gd(2012, 1, 3), 66],
		  [gd(2012, 1, 4), 9],
		  [gd(2012, 1, 5), 119],
		  [gd(2012, 1, 6), 6],
		  [gd(2012, 1, 7), 9]
		];
		
		var arr_data3 = [
			[0, 1],
			[1, 9],
			[2, 6],
			[3, 10],
			[4, 5],
			[5, 17],
			[6, 6],
			[7, 10],
			[8, 7],
			[9, 11],
			[10, 35],
			[11, 9],
			[12, 12],
			[13, 5],
			[14, 3],
			[15, 4],
			[16, 9]
		];
		
		var chart_plot_02_data = [];
		
		var chart_plot_03_data = [
			[0, 1],
			[1, 9],
			[2, 6],
			[3, 10],
			[4, 5],
			[5, 17],
			[6, 6],
			[7, 10],
			[8, 7],
			[9, 11],
			[10, 35],
			[11, 9],
			[12, 12],
			[13, 5],
			[14, 3],
			[15, 4],
			[16, 9]
		];
		
		
		for (var i = 0; i < 30; i++) {
		  chart_plot_02_data.push([new Date(Date.today().add(i).days()).getTime(), randNum() + i + i + 10]);
		}
		
		
		var chart_plot_01_settings = {
          series: {
            lines: {
              show: false,
              fill: true
            },
            splines: {
              show: true,
              tension: 0.4,
              lineWidth: 1,
              fill: 0.4
            },
            points: {
              radius: 0,
              show: true
            },
            shadowSize: 2
          },
          grid: {
            verticalLines: true,
            hoverable: true,
            clickable: true,
            tickColor: "#d5d5d5",
            borderWidth: 1,
            color: '#fff'
          },
          colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
          xaxis: {
            tickColor: "rgba(51, 51, 51, 0.06)",
            mode: "time",
            tickSize: [1, "day"],
            //tickLength: 10,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10
          },
          yaxis: {
            ticks: 8,
            tickColor: "rgba(51, 51, 51, 0.06)",
          },
          tooltip: false
        }
		
		var chart_plot_02_settings = {
			grid: {
				show: true,
				aboveData: true,
				color: "#3f3f3f",
				labelMargin: 10,
				axisMargin: 0,
				borderWidth: 0,
				borderColor: null,
				minBorderMargin: 5,
				clickable: true,
				hoverable: true,
				autoHighlight: true,
				mouseActiveRadius: 100
			},
			series: {
				lines: {
					show: true,
					fill: true,
					lineWidth: 2,
					steps: false
				},
				points: {
					show: true,
					radius: 4.5,
					symbol: "circle",
					lineWidth: 3.0
				}
			},
			legend: {
				position: "ne",
				margin: [0, -25],
				noColumns: 0,
				labelBoxBorderColor: null,
				labelFormatter: function(label, series) {
					return label + '&nbsp;&nbsp;';
				},
				width: 40,
				height: 1
			},
			colors: ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'],
			shadowSize: 0,
			tooltip: true,
			tooltipOpts: {
				content: "%s: %y.0",
				xDateFormat: "%d/%m",
			shifts: {
				x: -30,
				y: -50
			},
			defaultTheme: false
			},
			yaxis: {
				min: 0
			},
			xaxis: {
				mode: "time",
				minTickSize: [1, "day"],
				timeformat: "%d/%m/%y",
				min: chart_plot_02_data[0][0],
				max: chart_plot_02_data[20][0]
			}
		};	
	
		var chart_plot_03_settings = {
			series: {
				curvedLines: {
					apply: true,
					active: true,
					monotonicFit: true
				}
			},
			colors: ["#26B99A"],
			grid: {
				borderWidth: {
					top: 0,
					right: 0,
					bottom: 1,
					left: 1
				},
				borderColor: {
					bottom: "#7F8790",
					left: "#7F8790"
				}
			}
		};
        
		
        if ($("#chart_plot_01").length){
			////console.log('Plot1');
			
			$.plot( $("#chart_plot_01"), [ arr_data1, arr_data2 ],  chart_plot_01_settings );
		}
		
		
		if ($("#chart_plot_02").length){
			////console.log('Plot2');
			
			$.plot( $("#chart_plot_02"), 
			[{ 
				label: "Email Sent", 
				data: chart_plot_02_data, 
				lines: { 
					fillColor: "rgba(150, 202, 89, 0.12)" 
				}, 
				points: { 
					fillColor: "#fff" } 
			}], chart_plot_02_settings);
			
		}
		
		if ($("#chart_plot_03").length){
			////console.log('Plot3');
			
			
			$.plot($("#chart_plot_03"), [{
				label: "Registrations",
				data: chart_plot_03_data,
				lines: {
					fillColor: "rgba(150, 202, 89, 0.12)"
				}, 
				points: {
					fillColor: "#fff"
				}
			}], chart_plot_03_settings);
			
		};
	  
	} 
	
		
	/* STARRR */
			
	function init_starrr() {
		
		if( typeof (starrr) === 'undefined'){ return; }
		////console.log('init_starrr');
		
		$(".stars").starrr();

		$('.stars-existing').starrr({
		  rating: 4
		});

		$('.stars').on('starrr:change', function (e, value) {
		  $('.stars-count').html(value);
		});

		$('.stars-existing').on('starrr:change', function (e, value) {
		  $('.stars-count-existing').html(value);
		});
		
	  };
	
	
	function init_JQVmap(){

		//////console.log('check init_JQVmap [' + typeof (VectorCanvas) + '][' + typeof (jQuery.fn.vectorMap) + ']' );	
		
		if(typeof (jQuery.fn.vectorMap) === 'undefined'){ return; }
		
		////console.log('init_JQVmap');
	     
			if ($('#world-map-gdp').length ){
		 
				$('#world-map-gdp').vectorMap({
					map: 'world_en',
					backgroundColor: null,
					color: '#ffffff',
					hoverOpacity: 0.7,
					selectedColor: '#666666',
					enableZoom: true,
					showTooltip: true,
					values: sample_data,
					scaleColors: ['#E6F2F0', '#149B7E'],
					normalizeFunction: 'polynomial'
				});
			
			}
			
			if ($('#usa_map').length ){
			
				$('#usa_map').vectorMap({
					map: 'usa_en',
					backgroundColor: null,
					color: '#ffffff',
					hoverOpacity: 0.7,
					selectedColor: '#666666',
					enableZoom: true,
					showTooltip: true,
					values: sample_data,
					scaleColors: ['#E6F2F0', '#149B7E'],
					normalizeFunction: 'polynomial'
				});
			
			}
			
	};
			
	    
	function init_skycons(){
				
			if( typeof (Skycons) === 'undefined'){ return; }
			////console.log('init_skycons');
		
			var icons = new Skycons({
				"color": "#73879C"
			  }),
			  list = [
				"clear-day", "clear-night", "partly-cloudy-day",
				"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
				"fog"
			  ],
			  i;

			for (i = list.length; i--;)
			  icons.set(list[i], list[i]);

			icons.play();
	
	}  
	   
	   
/*	function init_chart_doughnut(){
				
		if( typeof (Chart) === 'undefined'){ return; }
		
		//////console.log('init_chart_doughnut');
	 
		if ($('.canvasDoughnut1').length){
			
		var chart_doughnut_settings = {
				type: 'doughnut',
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: {
					labels: [
						"Vehicles Entering",
						"Vehicles leaving"
						
					],
					datasets: [{
						data: [38, 20],
						backgroundColor: [
							"#1ABB9C",
							"#03586A"
						],
						hoverBackgroundColor: [
							"#1ABB9C",
							"#03586A"
						]
					}]
				},
				options: { 
					legend: false, 
					responsive: false 
				}
			}
		
			$('.canvasDoughnut1').each(function(){
				
				var chart_element = $(this);
				var chart_doughnut = new Chart( chart_element, chart_doughnut_settings);
				
			});			
		
		}  
		if ($('.canvasDoughnut2').length){
			
			var chart_doughnut_settings = {
					type: 'doughnut',
					tooltipFillColor: "rgba(51, 51, 51, 0.55)",
					data: {
						labels: [
							"Normal Traffic",
							"Moderate Traffic",
							"Queue Traffic"
						],
						datasets: [{
							data: [15, 20,30],
							backgroundColor: [
								"#56d8ef",
								"#ffd323",
								"#ff3535"
							],
							hoverBackgroundColor: [
								"#56d8ef",
								"#ffd323",
								"#ff3535"
							]
						}]
					},
					options: { 
						legend: false, 
						responsive: false 
					}
				}
			
				$('.canvasDoughnut2').each(function(){
					
					var chart_element = $(this);
					var chart_doughnut = new Chart( chart_element, chart_doughnut_settings);
					
				});			
			
			}  
	}*/
	   
	function init_gauge() {
			
		if( typeof (Gauge) === 'undefined'){ return; }
		
		////console.log('init_gauge [' + $('.gauge-chart').length + ']');
		
		////console.log('init_gauge');
		

		  var chart_gauge_settings = {
		  lines: 12,
		  angle: 0,
		  lineWidth: 0.4,
		  pointer: {
			  length: 0.75,
			  strokeWidth: 0.042,
			  color: '#1D212A'
		  },
		  limitMax: 'false',
		  colorStart: '#1ABC9C',
		  colorStop: '#1ABC9C',
		  strokeColor: '#F0F3F3',
		  generateGradient: true
	  };
		
		
		if ($('#chart_gauge_01').length){ 
		
			var chart_gauge_01_elem = document.getElementById('chart_gauge_01');
			var chart_gauge_01 = new Gauge(chart_gauge_01_elem).setOptions(chart_gauge_settings);
			
		}	
		
		
		if ($('#gauge-text').length){ 
		
			chart_gauge_01.maxValue = 6000;
			chart_gauge_01.animationSpeed = 32;
			chart_gauge_01.set(3200);
			chart_gauge_01.setTextField(document.getElementById("gauge-text"));
		
		}
		
		if ($('#chart_gauge_02').length){
		
			var chart_gauge_02_elem = document.getElementById('chart_gauge_02');
			var chart_gauge_02 = new Gauge(chart_gauge_02_elem).setOptions(chart_gauge_settings);
			
		}
		
		
		if ($('#gauge-text2').length){
			
			chart_gauge_02.maxValue = 9000;
			chart_gauge_02.animationSpeed = 32;
			chart_gauge_02.set(2400);
			chart_gauge_02.setTextField(document.getElementById("gauge-text2"));
		
		}
	
	
	}   
	   	   
	/* SPARKLINES */
			
		function init_sparklines() {
			
			if(typeof (jQuery.fn.sparkline) === 'undefined'){ return; }
			////console.log('init_sparklines'); 
			
			
			$(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3, 5, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
				type: 'bar',
				height: '125',
				barWidth: 13,
				colorMap: {
					'7': '#a1a1a1'
				},
				barSpacing: 2,
				barColor: '#26B99A'
			});
			
			
			$(".sparkline_two").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
				type: 'bar',
				height: '40',
				barWidth: 9,
				colorMap: {
					'7': '#a1a1a1'	
				},
				barSpacing: 2,
				barColor: '#26B99A'
			});
			
			
			$(".sparkline_three").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
				type: 'line',
				width: '200',
				height: '40',
				lineColor: '#26B99A',
				fillColor: 'rgba(223, 223, 223, 0.57)',
				lineWidth: 2,
				spotColor: '#26B99A',
				minSpotColor: '#26B99A'
			});
			
			
			$(".sparkline11").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3], {
				type: 'bar',
				height: '40',
				barWidth: 8,
				colorMap: {
					'7': '#a1a1a1'
				},
				barSpacing: 2,
				barColor: '#26B99A'
			});
			
			
			$(".sparkline22").sparkline([2, 4, 3, 4, 7, 5, 4, 3, 5, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6], {
				type: 'line',
				height: '40',
				width: '200',
				lineColor: '#26B99A',
				fillColor: '#ffffff',
				lineWidth: 3,
				spotColor: '#34495E',
				minSpotColor: '#34495E'
			});
	
	
			$(".sparkline_bar").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3, 5], {
				type: 'bar',
				colorMap: {
					'7': '#a1a1a1'
				},
				barColor: '#26B99A'
			});
			
			
			$(".sparkline_area").sparkline([5, 6, 7, 9, 9, 5, 3, 2, 2, 4, 6, 7], {
				type: 'line',
				lineColor: '#26B99A',
				fillColor: '#26B99A',
				spotColor: '#4578a0',
				minSpotColor: '#728fb2',
				maxSpotColor: '#6d93c4',
				highlightSpotColor: '#ef5179',
				highlightLineColor: '#8ba8bf',
				spotRadius: 2.5,
				width: 85
			});
			
			
			$(".sparkline_line").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3, 5], {
				type: 'line',
				lineColor: '#26B99A',
				fillColor: '#ffffff',
				width: 85,
				spotColor: '#34495E',
				minSpotColor: '#34495E'
			});
			
			
			$(".sparkline_pie").sparkline([1, 1, 2, 1], {
				type: 'pie',
				sliceColors: ['#26B99A', '#ccc', '#75BCDD', '#D66DE2']
			});
			
			
			$(".sparkline_discreet").sparkline([4, 6, 7, 7, 4, 3, 2, 1, 4, 4, 2, 4, 3, 7, 8, 9, 7, 6, 4, 3], {
				type: 'discrete',
				barWidth: 3,
				lineColor: '#26B99A',
				width: '85',
			});

			
		};   
	   
	   
	   /* AUTOCOMPLETE */
			
		function init_autocomplete() {
			
			if( typeof ($.fn.autocomplete) === 'undefined'){ return; }
			////console.log('init_autocomplete');
			
			var countries = { AD:"Andorra",A2:"Andorra Test",AE:"United Arab Emirates",AF:"Afghanistan",AG:"Antigua and Barbuda",AI:"Anguilla",AL:"Albania",AM:"Armenia",AN:"Netherlands Antilles",AO:"Angola",AQ:"Antarctica",AR:"Argentina",AS:"American Samoa",AT:"Austria",AU:"Australia",AW:"Aruba",AX:"Åland Islands",AZ:"Azerbaijan",BA:"Bosnia and Herzegovina",BB:"Barbados",BD:"Bangladesh",BE:"Belgium",BF:"Burkina Faso",BG:"Bulgaria",BH:"Bahrain",BI:"Burundi",BJ:"Benin",BL:"Saint Barthélemy",BM:"Bermuda",BN:"Brunei",BO:"Bolivia",BQ:"British Antarctic Territory",BR:"Brazil",BS:"Bahamas",BT:"Bhutan",BV:"Bouvet Island",BW:"Botswana",BY:"Belarus",BZ:"Belize",CA:"Canada",CC:"Cocos [Keeling] Islands",CD:"Congo - Kinshasa",CF:"Central African Republic",CG:"Congo - Brazzaville",CH:"Switzerland",CI:"Côte d’Ivoire",CK:"Cook Islands",CL:"Chile",CM:"Cameroon",CN:"China",CO:"Colombia",CR:"Costa Rica",CS:"Serbia and Montenegro",CT:"Canton and Enderbury Islands",CU:"Cuba",CV:"Cape Verde",CX:"Christmas Island",CY:"Cyprus",CZ:"Czech Republic",DD:"East Germany",DE:"Germany",DJ:"Djibouti",DK:"Denmark",DM:"Dominica",DO:"Dominican Republic",DZ:"Algeria",EC:"Ecuador",EE:"Estonia",EG:"Egypt",EH:"Western Sahara",ER:"Eritrea",ES:"Spain",ET:"Ethiopia",FI:"Finland",FJ:"Fiji",FK:"Falkland Islands",FM:"Micronesia",FO:"Faroe Islands",FQ:"French Southern and Antarctic Territories",FR:"France",FX:"Metropolitan France",GA:"Gabon",GB:"United Kingdom",GD:"Grenada",GE:"Georgia",GF:"French Guiana",GG:"Guernsey",GH:"Ghana",GI:"Gibraltar",GL:"Greenland",GM:"Gambia",GN:"Guinea",GP:"Guadeloupe",GQ:"Equatorial Guinea",GR:"Greece",GS:"South Georgia and the South Sandwich Islands",GT:"Guatemala",GU:"Guam",GW:"Guinea-Bissau",GY:"Guyana",HK:"Hong Kong SAR China",HM:"Heard Island and McDonald Islands",HN:"Honduras",HR:"Croatia",HT:"Haiti",HU:"Hungary",ID:"Indonesia",IE:"Ireland",IL:"Israel",IM:"Isle of Man",IN:"India",IO:"British Indian Ocean Territory",IQ:"Iraq",IR:"Iran",IS:"Iceland",IT:"Italy",JE:"Jersey",JM:"Jamaica",JO:"Jordan",JP:"Japan",JT:"Johnston Island",KE:"Kenya",KG:"Kyrgyzstan",KH:"Cambodia",KI:"Kiribati",KM:"Comoros",KN:"Saint Kitts and Nevis",KP:"North Korea",KR:"South Korea",KW:"Kuwait",KY:"Cayman Islands",KZ:"Kazakhstan",LA:"Laos",LB:"Lebanon",LC:"Saint Lucia",LI:"Liechtenstein",LK:"Sri Lanka",LR:"Liberia",LS:"Lesotho",LT:"Lithuania",LU:"Luxembourg",LV:"Latvia",LY:"Libya",MA:"Morocco",MC:"Monaco",MD:"Moldova",ME:"Montenegro",MF:"Saint Martin",MG:"Madagascar",MH:"Marshall Islands",MI:"Midway Islands",MK:"Macedonia",ML:"Mali",MM:"Myanmar [Burma]",MN:"Mongolia",MO:"Macau SAR China",MP:"Northern Mariana Islands",MQ:"Martinique",MR:"Mauritania",MS:"Montserrat",MT:"Malta",MU:"Mauritius",MV:"Maldives",MW:"Malawi",MX:"Mexico",MY:"Malaysia",MZ:"Mozambique",NA:"Namibia",NC:"New Caledonia",NE:"Niger",NF:"Norfolk Island",NG:"Nigeria",NI:"Nicaragua",NL:"Netherlands",NO:"Norway",NP:"Nepal",NQ:"Dronning Maud Land",NR:"Nauru",NT:"Neutral Zone",NU:"Niue",NZ:"New Zealand",OM:"Oman",PA:"Panama",PC:"Pacific Islands Trust Territory",PE:"Peru",PF:"French Polynesia",PG:"Papua New Guinea",PH:"Philippines",PK:"Pakistan",PL:"Poland",PM:"Saint Pierre and Miquelon",PN:"Pitcairn Islands",PR:"Puerto Rico",PS:"Palestinian Territories",PT:"Portugal",PU:"U.S. Miscellaneous Pacific Islands",PW:"Palau",PY:"Paraguay",PZ:"Panama Canal Zone",QA:"Qatar",RE:"Réunion",RO:"Romania",RS:"Serbia",RU:"Russia",RW:"Rwanda",SA:"Saudi Arabia",SB:"Solomon Islands",SC:"Seychelles",SD:"Sudan",SE:"Sweden",SG:"Singapore",SH:"Saint Helena",SI:"Slovenia",SJ:"Svalbard and Jan Mayen",SK:"Slovakia",SL:"Sierra Leone",SM:"San Marino",SN:"Senegal",SO:"Somalia",SR:"Suriname",ST:"São Tomé and Príncipe",SU:"Union of Soviet Socialist Republics",SV:"El Salvador",SY:"Syria",SZ:"Swaziland",TC:"Turks and Caicos Islands",TD:"Chad",TF:"French Southern Territories",TG:"Togo",TH:"Thailand",TJ:"Tajikistan",TK:"Tokelau",TL:"Timor-Leste",TM:"Turkmenistan",TN:"Tunisia",TO:"Tonga",TR:"Turkey",TT:"Trinidad and Tobago",TV:"Tuvalu",TW:"Taiwan",TZ:"Tanzania",UA:"Ukraine",UG:"Uganda",UM:"U.S. Minor Outlying Islands",US:"United States",UY:"Uruguay",UZ:"Uzbekistan",VA:"Vatican City",VC:"Saint Vincent and the Grenadines",VD:"North Vietnam",VE:"Venezuela",VG:"British Virgin Islands",VI:"U.S. Virgin Islands",VN:"Vietnam",VU:"Vanuatu",WF:"Wallis and Futuna",WK:"Wake Island",WS:"Samoa",YD:"People's Democratic Republic of Yemen",YE:"Yemen",YT:"Mayotte",ZA:"South Africa",ZM:"Zambia",ZW:"Zimbabwe",ZZ:"Unknown or Invalid Region" };

			var countriesArray = $.map(countries, function(value, key) {
			  return {
				value: value,
				data: key
			  };
			});

			// initialize autocomplete with custom appendTo
			$('#autocomplete-custom-append').autocomplete({
			  lookup: countriesArray
			});
			
		};
	   
	 /* AUTOSIZE */
			
		function init_autosize() {
			
			if(typeof $.fn.autosize !== 'undefined'){
			
			autosize($('.resizable_textarea'));
			
			}
			
		};  
	   
	   /* PARSLEY */
			
		function init_parsley() {
			
			if( typeof (parsley) === 'undefined'){ return; }
			////console.log('init_parsley');
			
			$/*.listen*/('parsley:field:validate', function() {
			  validateFront();
			});
			$('#demo-form .btn').on('click', function() {
			  $('#demo-form').parsley().validate();
			  validateFront();
			});
			var validateFront = function() {
			  if (true === $('#demo-form').parsley().isValid()) {
				$('.bs-callout-info').removeClass('hidden');
				$('.bs-callout-warning').addClass('hidden');
			  } else {
				$('.bs-callout-info').addClass('hidden');
				$('.bs-callout-warning').removeClass('hidden');
			  }
			};
		  
			$/*.listen*/('parsley:field:validate', function() {
			  validateFront();
			});
			$('#demo-form2 .btn').on('click', function() {
			  $('#demo-form2').parsley().validate();
			  validateFront();
			});
			var validateFront = function() {
			  if (true === $('#demo-form2').parsley().isValid()) {
				$('.bs-callout-info').removeClass('hidden');
				$('.bs-callout-warning').addClass('hidden');
			  } else {
				$('.bs-callout-info').addClass('hidden');
				$('.bs-callout-warning').removeClass('hidden');
			  }
			};
			
			  try {
				hljs.initHighlightingOnLoad();
			  } catch (err) {}
			
		};
	   
		
		  /* INPUTS */
		  
			function onAddTag(tag) {
				alert("Added a tag: " + tag);
			  }

			  function onRemoveTag(tag) {
				alert("Removed a tag: " + tag);
			  }

			  function onChangeTag(input, tag) {
				alert("Changed a tag: " + tag);
			  }

			  //tags input
			function init_TagsInput() {
				  
				if(typeof $.fn.tagsInput !== 'undefined'){	
				 
				$('#tags_1').tagsInput({
				  width: 'auto'
				});
				
				}
				
		    };
	   
		/* SELECT2 */
	  
		function init_select2() {
			 
			if( typeof (select2) === 'undefined'){ return; }
			////console.log('init_toolbox');
			 
			$(".select2_single").select2({
			  placeholder: "Select a state",
			  allowClear: true
			});
			$(".select2_group").select2({});
			$(".select2_multiple").select2({
			  maximumSelectionLength: 4,
			  placeholder: "With Max Selection limit 4",
			  allowClear: true
			});
			
		};
	   
	   /* WYSIWYG EDITOR */

		function init_wysiwyg() {
			
		if( typeof ($.fn.wysiwyg) === 'undefined'){ return; }
		//console.log('init_wysiwyg');	
			
        function init_ToolbarBootstrapBindings() {
          var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
              'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
              'Times New Roman', 'Verdana'
            ],
            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
          $.each(fonts, function(idx, fontName) {
            fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
          });
          $('a[title]').tooltip({
            container: 'body'
          });
          $('.dropdown-menu input').click(function() {
              return false;
            })
            .change(function() {
              $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
            })
            .keydown('esc', function() {
              this.value = '';
              $(this).change();
            });

          $('[data-role=magic-overlay]').each(function() {
            var overlay = $(this),
              target = $(overlay.data('target'));
            overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
          });

          if ("onwebkitspeechchange" in document.createElement("input")) {
            var editorOffset = $('#editor').offset();

            $('.voiceBtn').css('position', 'absolute').offset({
              top: editorOffset.top,
              left: editorOffset.left + $('#editor').innerWidth() - 35
            });
          } else {
            $('.voiceBtn').hide();
          }
        }

        function showErrorAlert(reason, detail) {
          var msg = '';
          if (reason === 'unsupported-file-type') {
            msg = "Unsupported format " + detail;
          } else {
            //console.log("error uploading file", reason, detail);
          }
          $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

       $('.editor-wrapper').each(function(){
			var id = $(this).attr('id');	//editor-one
			
			$(this).wysiwyg({
				toolbarSelector: '[data-target="#' + id + '"]',
				fileUploadError: showErrorAlert
			});	
		});
 
		
        window.prettyPrint;
        prettyPrint();
	
    };
	  
	/* CROPPER */
		
		function init_cropper() {
			
			
			if( typeof ($.fn.cropper) === 'undefined'){ return; }
			//console.log('init_cropper');
			
			var $image = $('#image');
			var $download = $('#download');
			var $dataX = $('#dataX');
			var $dataY = $('#dataY');
			var $dataHeight = $('#dataHeight');
			var $dataWidth = $('#dataWidth');
			var $dataRotate = $('#dataRotate');
			var $dataScaleX = $('#dataScaleX');
			var $dataScaleY = $('#dataScaleY');
			var options = {
				  aspectRatio: 16 / 9,
				  preview: '.img-preview',
				  crop: function (e) {
					$dataX.val(Math.round(e.x));
					$dataY.val(Math.round(e.y));
					$dataHeight.val(Math.round(e.height));
					$dataWidth.val(Math.round(e.width));
					$dataRotate.val(e.rotate);
					$dataScaleX.val(e.scaleX);
					$dataScaleY.val(e.scaleY);
				  }
				};


			// Tooltip
			$('[data-toggle="tooltip"]').tooltip();


			// Cropper
			$image.on({
			  'build.cropper': function (e) {
				//console.log(e.type);
			  },
			  'built.cropper': function (e) {
				//console.log(e.type);
			  },
			  'cropstart.cropper': function (e) {
				//console.log(e.type, e.action);
			  },
			  'cropmove.cropper': function (e) {
				//console.log(e.type, e.action);
			  },
			  'cropend.cropper': function (e) {
				//console.log(e.type, e.action);
			  },
			  'crop.cropper': function (e) {
				//console.log(e.type, e.x, e.y, e.width, e.height, e.rotate, e.scaleX, e.scaleY);
			  },
			  'zoom.cropper': function (e) {
				//console.log(e.type, e.ratio);
			  }
			}).cropper(options);


			// Buttons
			if (!$.isFunction(document.createElement('canvas').getContext)) {
			  $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
			}

			if (typeof document.createElement('cropper').style.transition === 'undefined') {
			  $('button[data-method="rotate"]').prop('disabled', true);
			  $('button[data-method="scale"]').prop('disabled', true);
			}


			// Download
			if (typeof $download[0].download === 'undefined') {
			  $download.addClass('disabled');
			}


			// Options
			$('.docs-toggles').on('change', 'input', function () {
			  var $this = $(this);
			  var name = $this.attr('name');
			  var type = $this.prop('type');
			  var cropBoxData;
			  var canvasData;

			  if (!$image.data('cropper')) {
				return;
			  }

			  if (type === 'checkbox') {
				options[name] = $this.prop('checked');
				cropBoxData = $image.cropper('getCropBoxData');
				canvasData = $image.cropper('getCanvasData');

				options.built = function () {
				  $image.cropper('setCropBoxData', cropBoxData);
				  $image.cropper('setCanvasData', canvasData);
				};
			  } else if (type === 'radio') {
				options[name] = $this.val();
			  }

			  $image.cropper('destroy').cropper(options);
			});


			// Methods
			$('.docs-buttons').on('click', '[data-method]', function () {
			  var $this = $(this);
			  var data = $this.data();
			  var $target;
			  var result;

			  if ($this.prop('disabled') || $this.hasClass('disabled')) {
				return;
			  }

			  if ($image.data('cropper') && data.method) {
				data = $.extend({}, data); // Clone a new one

				if (typeof data.target !== 'undefined') {
				  $target = $(data.target);

				  if (typeof data.option === 'undefined') {
					try {
					  data.option = JSON.parse($target.val());
					} catch (e) {
					  //console.log(e.message);
					}
				  }
				}

				result = $image.cropper(data.method, data.option, data.secondOption);

				switch (data.method) {
				  case 'scaleX':
				  case 'scaleY':
					$(this).data('option', -data.option);
					break;

				  case 'getCroppedCanvas':
					if (result) {

					  // Bootstrap's Modal
					  $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

					  if (!$download.hasClass('disabled')) {
						$download.attr('href', result.toDataURL());
					  }
					}

					break;
				}

				if ($.isPlainObject(result) && $target) {
				  try {
					$target.val(JSON.stringify(result));
				  } catch (e) {
					//console.log(e.message);
				  }
				}

			  }
			});

			// Keyboard
			$(document.body).on('keydown', function (e) {
			  if (!$image.data('cropper') || this.scrollTop > 300) {
				return;
			  }

			  switch (e.which) {
				case 37:
				  e.preventDefault();
				  $image.cropper('move', -1, 0);
				  break;

				case 38:
				  e.preventDefault();
				  $image.cropper('move', 0, -1);
				  break;

				case 39:
				  e.preventDefault();
				  $image.cropper('move', 1, 0);
				  break;

				case 40:
				  e.preventDefault();
				  $image.cropper('move', 0, 1);
				  break;
			  }
			});

			// Import image
			var $inputImage = $('#inputImage');
			var URL = window.URL || window.webkitURL;
			var blobURL;

			if (URL) {
			  $inputImage.change(function () {
				var files = this.files;
				var file;

				if (!$image.data('cropper')) {
				  return;
				}

				if (files && files.length) {
				  file = files[0];

				  if (/^image\/\w+$/.test(file.type)) {
					blobURL = URL.createObjectURL(file);
					$image.one('built.cropper', function () {

					  // Revoke when load complete
					  URL.revokeObjectURL(blobURL);
					}).cropper('reset').cropper('replace', blobURL);
					$inputImage.val('');
				  } else {
					window.alert('Please choose an image file.');
				  }
				}
			  });
			} else {
			  $inputImage.prop('disabled', true).parent().addClass('disabled');
			}
			
			
		};
		
		/* CROPPER --- end */  
	  
		/* KNOB */
	  
		function init_knob() {
		
				if( typeof ($.fn.knob) === 'undefined'){ return; }
				//console.log('init_knob');
	
				$(".knob").knob({
				  change: function(value) {
					////console.log("change : " + value);
				  },
				  release: function(value) {
					////console.log(this.$.attr('value'));
					//console.log("release : " + value);
				  },
				  cancel: function() {
					//console.log("cancel : ", this);
				  },
				  /*format : function (value) {
				   return value + '%';
				   },*/
				  draw: function() {

					// "tron" case
					if (this.$.data('skin') == 'tron') {

					  this.cursorExt = 0.3;

					  var a = this.arc(this.cv) // Arc
						,
						pa // Previous arc
						, r = 1;

					  this.g.lineWidth = this.lineWidth;

					  if (this.o.displayPrevious) {
						pa = this.arc(this.v);
						this.g.beginPath();
						this.g.strokeStyle = this.pColor;
						this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
						this.g.stroke();
					  }

					  this.g.beginPath();
					  this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
					  this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
					  this.g.stroke();

					  this.g.lineWidth = 2;
					  this.g.beginPath();
					  this.g.strokeStyle = this.o.fgColor;
					  this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
					  this.g.stroke();

					  return false;
					}
				  }
				  
				});

				// Example of infinite knob, iPod click wheel
				var v, up = 0,
				  down = 0,
				  i = 0,
				  $idir = $("div.idir"),
				  $ival = $("div.ival"),
				  incr = function() {
					i++;
					$idir.show().html("+").fadeOut();
					$ival.html(i);
				  },
				  decr = function() {
					i--;
					$idir.show().html("-").fadeOut();
					$ival.html(i);
				  };
				$("input.infinite").knob({
				  min: 0,
				  max: 20,
				  stopper: false,
				  change: function() {
					if (v > this.cv) {
					  if (up) {
						decr();
						up = 0;
					  } else {
						up = 1;
						down = 0;
					  }
					} else {
					  if (v < this.cv) {
						if (down) {
						  incr();
						  down = 0;
						} else {
						  down = 1;
						  up = 0;
						}
					  }
					}
					v = this.cv;
				  }
				});
				
		};
	 
		/* INPUT MASK */
			
		function init_InputMask() {
			
			if( typeof ($.fn.inputmask) === 'undefined'){ return; }
			//console.log('init_InputMask');
			
				$(":input").inputmask();
				
		};
	  
		/* COLOR PICKER */
			 
		function init_ColorPicker() {
			
			if( typeof ($.fn.colorpicker) === 'undefined'){ return; }
			//console.log('init_ColorPicker');
			
				$('.demo1').colorpicker();
				$('.demo2').colorpicker();

				$('#demo_forceformat').colorpicker({
					format: 'rgba',
					horizontal: true
				});

				$('#demo_forceformat3').colorpicker({
					format: 'rgba',
				});

				$('.demo-auto').colorpicker();
			
		}; 
	   
	   
		/* ION RANGE SLIDER */
			
		function init_IonRangeSlider() {
			
			if( typeof ($.fn.ionRangeSlider) === 'undefined'){ return; }
			//console.log('init_IonRangeSlider');
			
			$("#range_27").ionRangeSlider({
			  type: "double",
			  min: 1000000,
			  max: 2000000,
			  grid: true,
			  force_edges: true
			});
			$("#range").ionRangeSlider({
			  hide_min_max: true,
			  keyboard: true,
			  min: 0,
			  max: 5000,
			  from: 1000,
			  to: 4000,
			  type: 'double',
			  step: 1,
			  prefix: "$",
			  grid: true
			});
			$("#range_25").ionRangeSlider({
			  type: "double",
			  min: 1000000,
			  max: 2000000,
			  grid: true
			});
			$("#range_26").ionRangeSlider({
			  type: "double",
			  min: 0,
			  max: 10000,
			  step: 500,
			  grid: true,
			  grid_snap: true
			});
			$("#range_31").ionRangeSlider({
			  type: "double",
			  min: 0,
			  max: 100,
			  from: 30,
			  to: 70,
			  from_fixed: true
			});
			$(".range_min_max").ionRangeSlider({
			  type: "double",
			  min: 0,
			  max: 100,
			  from: 30,
			  to: 70,
			  max_interval: 50
			});
			$(".range_time24").ionRangeSlider({
			  min: +moment().subtract(12, "hours").format("X"),
			  max: +moment().format("X"),
			  from: +moment().subtract(6, "hours").format("X"),
			  grid: true,
			  force_edges: true,
			  prettify: function(num) {
				var m = moment(num, "X");
				return m.format("Do MMMM, HH:mm");
			  }
			});
			
		};
	   
	   
	   /* DATERANGEPICKER */
	   
		function init_daterangepicker() {

			if( typeof ($.fn.daterangepicker) === 'undefined'){ return; }
			//console.log('init_daterangepicker');
		
			var cb = function(start, end, label) {
			  //console.log(start.toISOString(), end.toISOString(), label);
			  $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			};

			var optionSet1 = {
			  startDate: moment().subtract(29, 'days'),
			  endDate: moment(),
			  minDate: '01/01/2012',
			  maxDate: '12/31/2015',
			  dateLimit: {
				days: 60
			  },
			  showDropdowns: true,
			  showWeekNumbers: true,
			  timePicker: false,
			  timePickerIncrement: 1,
			  timePicker12Hour: true,
			  ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			  },
			  opens: 'left',
			  buttonClasses: ['btn btn-default'],
			  applyClass: 'btn-small btn-primary',
			  cancelClass: 'btn-small',
			  format: 'MM/DD/YYYY',
			  separator: ' to ',
			  locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Clear',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			  }
			};
			
			$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
			$('#reportrange').daterangepicker(optionSet1, cb);
			$('#reportrange').on('show.daterangepicker', function() {
			  //console.log("show event fired");
			});
			$('#reportrange').on('hide.daterangepicker', function() {
			  //console.log("hide event fired");
			});
			$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
			  //console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
			});
			$('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
			  //console.log("cancel event fired");
			});
			$('#options1').click(function() {
			  $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
			});
			$('#options2').click(function() {
			  $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
			});
			$('#destroy').click(function() {
			  $('#reportrange').data('daterangepicker').remove();
			});
   
		}
   	   
	   function init_daterangepicker_right() {
	      
				if( typeof ($.fn.daterangepicker) === 'undefined'){ return; }
				//console.log('init_daterangepicker_right');
		  
				var cb = function(start, end, label) {
				  //console.log(start.toISOString(), end.toISOString(), label);
				  $('#reportrange_right span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
				};

				var optionSet1 = {
				  startDate: moment().subtract(29, 'days'),
				  endDate: moment(),
				  minDate: '01/01/2012',
				  maxDate: '12/31/2020',
				  dateLimit: {
					days: 60
				  },
				  showDropdowns: true,
				  showWeekNumbers: true,
				  timePicker: false,
				  timePickerIncrement: 1,
				  timePicker12Hour: true,
				  ranges: {
					'Today': [moment(), moment()],
					'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					'This Month': [moment().startOf('month'), moment().endOf('month')],
					'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				  },
				  opens: 'right',
				  buttonClasses: ['btn btn-default'],
				  applyClass: 'btn-small btn-primary',
				  cancelClass: 'btn-small',
				  format: 'MM/DD/YYYY',
				  separator: ' to ',
				  locale: {
					applyLabel: 'Submit',
					cancelLabel: 'Clear',
					fromLabel: 'From',
					toLabel: 'To',
					customRangeLabel: 'Custom',
					daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
					monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
					firstDay: 1
				  }
				};

				$('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

				$('#reportrange_right').daterangepicker(optionSet1, cb);

				$('#reportrange_right').on('show.daterangepicker', function() {
				  //console.log("show event fired");
				});
				$('#reportrange_right').on('hide.daterangepicker', function() {
				  //console.log("hide event fired");
				});
				$('#reportrange_right').on('apply.daterangepicker', function(ev, picker) {
				  //console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
				});
				$('#reportrange_right').on('cancel.daterangepicker', function(ev, picker) {
				  //console.log("cancel event fired");
				});

				$('#options1').click(function() {
				  $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
				});

				$('#options2').click(function() {
				  $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
				});

				$('#destroy').click(function() {
				  $('#reportrange_right').data('daterangepicker').remove();
				});

	   }
	   
	    function init_daterangepicker_single_call() {
	      
			if( typeof ($.fn.daterangepicker) === 'undefined'){ return; }
			//console.log('init_daterangepicker_single_call');
		   
			$('#single_cal1').daterangepicker({
			  singleDatePicker: true,
			  singleClasses: "picker_1"
			}, function(start, end, label) {
			  //console.log(start.toISOString(), end.toISOString(), label);
			});
			$('#single_cal2').daterangepicker({
			  singleDatePicker: true,
			  singleClasses: "picker_2"
			}, function(start, end, label) {
			  //console.log(start.toISOString(), end.toISOString(), label);
			});
			$('#single_cal3').daterangepicker({
			  singleDatePicker: true,
			  singleClasses: "picker_3"
			}, function(start, end, label) {
			  //console.log(start.toISOString(), end.toISOString(), label);
			});
			$('#single_cal4').daterangepicker({
			  singleDatePicker: true,
			  singleClasses: "picker_4"
			}, function(start, end, label) {
			  //console.log(start.toISOString(), end.toISOString(), label);
			});
  
  
		}
		
		 
		function init_daterangepicker_reservation() {
	      
			if( typeof ($.fn.daterangepicker) === 'undefined'){ return; }
			//console.log('init_daterangepicker_reservation');
		 
			$('#reservation').daterangepicker(null, function(start, end, label) {
			  //console.log(start.toISOString(), end.toISOString(), label);
			});

			$('#reservation-time').daterangepicker({
			  timePicker: true,
			  timePickerIncrement: 30,
			  locale: {
				format: 'MM/DD/YYYY h:mm A'
			  }
			});
	
		}
	   
	   /* SMART WIZARD */
		
		function init_SmartWizard() {
			
			if( typeof ($.fn.smartWizard) === 'undefined'){ return; }
			//console.log('init_SmartWizard');
			
			$('#wizard').smartWizard();

			$('#wizard_verticle').smartWizard({
			  transitionEffect: 'slide'
			});

			$('.buttonNext').addClass('btn btn-success');
			$('.buttonPrevious').addClass('btn btn-primary');
			$('.buttonFinish').addClass('btn btn-default');
			
		};
	   
	   
	  /* VALIDATOR */

	  function init_validator () {
		 
		if( typeof (validator) === 'undefined'){ return; }
		//console.log('init_validator'); 
	  
	  // initialize the validator function
      validator.message.date = 'not a real date';

      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

      $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }

        if (submit)
          this.submit();

        return false;
		});
	  
	  };
	   
	  	/* PNotify */
			
		function init_PNotify() {
			
			if( typeof (PNotify) === 'undefined'){ return; }
			//console.log('init_PNotify');
		}; 
	   
	   
	   /* CUSTOM NOTIFICATION */
			
		function init_CustomNotification() {
			
			//console.log('run_customtabs');
			
			if( typeof (CustomTabs) === 'undefined'){ return; }
			//console.log('init_CustomTabs');
			
			var cnt = 10;

			TabbedNotification = function(options) {
			  var message = "<div id='ntf" + cnt + "' class='text alert-" + options.type + "' style='display:none'><h2><i class='fa fa-bell'></i> " + options.title +
				"</h2><div class='close'><a href='javascript:;' class='notification_close'><i class='fa fa-close'></i></a></div><p>" + options.text + "</p></div>";

			  if (!document.getElementById('custom_notifications')) {
				alert('doesnt exists');
			  } else {
				$('#custom_notifications ul.notifications').append("<li><a id='ntlink" + cnt + "' class='alert-" + options.type + "' href='#ntf" + cnt + "'><i class='fa fa-bell animated shake'></i></a></li>");
				$('#custom_notifications #notif-group').append(message);
				cnt++;
				CustomTabs(options);
			  }
			};

			CustomTabs = function(options) {
			  $('.tabbed_notifications > div').hide();
			  $('.tabbed_notifications > div:first-of-type').show();
			  $('#custom_notifications').removeClass('dsp_none');
			  $('.notifications a').click(function(e) {
				e.preventDefault();
				var $this = $(this),
				  tabbed_notifications = '#' + $this.parents('.notifications').data('tabbed_notifications'),
				  others = $this.closest('li').siblings().children('a'),
				  target = $this.attr('href');
				others.removeClass('active');
				$this.addClass('active');
				$(tabbed_notifications).children('div').hide();
				$(target).show();
			  });
			};

			CustomTabs();

			var tabid = idname = '';

			$(document).on('click', '.notification_close', function(e) {
			  idname = $(this).parent().parent().attr("id");
			  tabid = idname.substr(-2);
			  $('#ntf' + tabid).remove();
			  $('#ntlink' + tabid).parent().remove();
			  $('.notifications a').first().addClass('active');
			  $('#notif-group div').first().css('display', 'block');
			});
			
		};
		
			/* EASYPIECHART */
			
			function init_EasyPieChart() {
				
				if( typeof ($.fn.easyPieChart) === 'undefined'){ return; }
				//console.log('init_EasyPieChart');
				
				$('.chart').easyPieChart({
				  easing: 'easeOutElastic',
				  delay: 3000,
				  barColor: '#26B99A',
				  trackColor: '#fff',
				  scaleColor: false,
				  lineWidth: 20,
				  trackWidth: 16,
				  lineCap: 'butt',
				  onStep: function(from, to, percent) {
					$(this.el).find('.percent').text(Math.round(percent));
				  }
				});
				var chart = window.chart = $('.chart').data('easyPieChart');
				$('.js_update').on('click', function() {
				  chart.update(Math.random() * 200 - 100);
				});

				//hover and retain popover when on popover content
				var originalLeave = $.fn.popover.Constructor.prototype.leave;
				$.fn.popover.Constructor.prototype.leave = function(obj) {
				  var self = obj instanceof this.constructor ?
					obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
				  var container, timeout;

				  originalLeave.call(this, obj);

				  if (obj.currentTarget) {
					container = $(obj.currentTarget).siblings('.popover');
					timeout = self.timeout;
					container.one('mouseenter', function() {
					  //We entered the actual popover – call off the dogs
					  clearTimeout(timeout);
					  //Let's monitor popover content instead
					  container.one('mouseleave', function() {
						$.fn.popover.Constructor.prototype.leave.call(self, self);
					  });
					});
				  }
				};

				$('body').popover({
				  selector: '[data-popover]',
				  trigger: 'click hover',
				  delay: {
					show: 50,
					hide: 400
				  }
				});
				
			};
	   
		
		function init_charts() {
			
				//console.log('run_charts  typeof [' + typeof (Chart) + ']');
			
				if( typeof (Chart) === 'undefined'){ return; }
				
				//console.log('init_charts');
			
				
				Chart.defaults.global.legend = {
					enabled: false
				};
				
				

			if ($('#canvas_line').length ){
				
				var canvas_line_00 = new Chart(document.getElementById("canvas_line"), {
				  type: 'line',
				  data: {
					labels: ["January", "February", "March", "April", "May", "June", "July"],
					datasets: [{
					  label: "My First dataset",
					  backgroundColor: "rgba(38, 185, 154, 0.31)",
					  borderColor: "rgba(38, 185, 154, 0.7)",
					  pointBorderColor: "rgba(38, 185, 154, 0.7)",
					  pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
					  pointHoverBackgroundColor: "#fff",
					  pointHoverBorderColor: "rgba(220,220,220,1)",
					  pointBorderWidth: 1,
					  data: [31, 74, 6, 39, 20, 85, 7]
					}, {
					  label: "My Second dataset",
					  backgroundColor: "rgba(3, 88, 106, 0.3)",
					  borderColor: "rgba(3, 88, 106, 0.70)",
					  pointBorderColor: "rgba(3, 88, 106, 0.70)",
					  pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
					  pointHoverBackgroundColor: "#fff",
					  pointHoverBorderColor: "rgba(151,187,205,1)",
					  pointBorderWidth: 1,
					  data: [82, 23, 66, 9, 99, 4, 2]
					}]
				  },
				});
				
			}

			
			if ($('#canvas_line1').length ){
			
				var canvas_line_01 = new Chart(document.getElementById("canvas_line1"), {
				  type: 'line',
				  data: {
					labels: ["January", "February", "March", "April", "May", "June", "July"],
					datasets: [{
					  label: "My First dataset",
					  backgroundColor: "rgba(38, 185, 154, 0.31)",
					  borderColor: "rgba(38, 185, 154, 0.7)",
					  pointBorderColor: "rgba(38, 185, 154, 0.7)",
					  pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
					  pointHoverBackgroundColor: "#fff",
					  pointHoverBorderColor: "rgba(220,220,220,1)",
					  pointBorderWidth: 1,
					  data: [31, 74, 6, 39, 20, 85, 7]
					}, {
					  label: "My Second dataset",
					  backgroundColor: "rgba(3, 88, 106, 0.3)",
					  borderColor: "rgba(3, 88, 106, 0.70)",
					  pointBorderColor: "rgba(3, 88, 106, 0.70)",
					  pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
					  pointHoverBackgroundColor: "#fff",
					  pointHoverBorderColor: "rgba(151,187,205,1)",
					  pointBorderWidth: 1,
					  data: [82, 23, 66, 9, 99, 4, 2]
					}]
				  },
				});
			
			}
				
				
			if ($('#canvas_line2').length ){		
			
				var canvas_line_02 = new Chart(document.getElementById("canvas_line2"), {
				  type: 'line',
				  data: {
					labels: ["January", "February", "March", "April", "May", "June", "July"],
					datasets: [{
					  label: "My First dataset",
					  backgroundColor: "rgba(38, 185, 154, 0.31)",
					  borderColor: "rgba(38, 185, 154, 0.7)",
					  pointBorderColor: "rgba(38, 185, 154, 0.7)",
					  pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
					  pointHoverBackgroundColor: "#fff",
					  pointHoverBorderColor: "rgba(220,220,220,1)",
					  pointBorderWidth: 1,
					  data: [31, 74, 6, 39, 20, 85, 7]
					}, {
					  label: "My Second dataset",
					  backgroundColor: "rgba(3, 88, 106, 0.3)",
					  borderColor: "rgba(3, 88, 106, 0.70)",
					  pointBorderColor: "rgba(3, 88, 106, 0.70)",
					  pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
					  pointHoverBackgroundColor: "#fff",
					  pointHoverBorderColor: "rgba(151,187,205,1)",
					  pointBorderWidth: 1,
					  data: [82, 23, 66, 9, 99, 4, 2]
					}]
				  },
				});

			}	
			
			
			if ($('#canvas_line3').length ){
			
				var canvas_line_03 = new Chart(document.getElementById("canvas_line3"), {
				  type: 'line',
				  data: {
					labels: ["January", "February", "March", "April", "May", "June", "July"],
					datasets: [{
					  label: "My First dataset",
					  backgroundColor: "rgba(38, 185, 154, 0.31)",
					  borderColor: "rgba(38, 185, 154, 0.7)",
					  pointBorderColor: "rgba(38, 185, 154, 0.7)",
					  pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
					  pointHoverBackgroundColor: "#fff",
					  pointHoverBorderColor: "rgba(220,220,220,1)",
					  pointBorderWidth: 1,
					  data: [31, 74, 6, 39, 20, 85, 7]
					}, {
					  label: "My Second dataset",
					  backgroundColor: "rgba(3, 88, 106, 0.3)",
					  borderColor: "rgba(3, 88, 106, 0.70)",
					  pointBorderColor: "rgba(3, 88, 106, 0.70)",
					  pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
					  pointHoverBackgroundColor: "#fff",
					  pointHoverBorderColor: "rgba(151,187,205,1)",
					  pointBorderWidth: 1,
					  data: [82, 23, 66, 9, 99, 4, 2]
					}]
				  },
				});

			}	
			
			
			if ($('#canvas_line4').length ){
				
				var canvas_line_04 = new Chart(document.getElementById("canvas_line4"), {
				  type: 'line',
				  data: {
					labels: ["January", "February", "March", "April", "May", "June", "July"],
					datasets: [{
					  label: "My First dataset",
					  backgroundColor: "rgba(38, 185, 154, 0.31)",
					  borderColor: "rgba(38, 185, 154, 0.7)",
					  pointBorderColor: "rgba(38, 185, 154, 0.7)",
					  pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
					  pointHoverBackgroundColor: "#fff",
					  pointHoverBorderColor: "rgba(220,220,220,1)",
					  pointBorderWidth: 1,
					  data: [31, 74, 6, 39, 20, 85, 7]
					}, {
					  label: "My Second dataset",
					  backgroundColor: "rgba(3, 88, 106, 0.3)",
					  borderColor: "rgba(3, 88, 106, 0.70)",
					  pointBorderColor: "rgba(3, 88, 106, 0.70)",
					  pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
					  pointHoverBackgroundColor: "#fff",
					  pointHoverBorderColor: "rgba(151,187,205,1)",
					  pointBorderWidth: 1,
					  data: [82, 23, 66, 9, 99, 4, 2]
					}]
				  },
				});		
				
			}
			
				
			  // Line chart
			 
			if ($('#lineChart').length ){	
			
			  var ctx = document.getElementById("lineChart");
			  var lineChart = new Chart(ctx, {
				type: 'line',
				data: {
				  labels: ["January", "February", "March", "April", "May", "June", "July"],
				  datasets: [{
					label: "My First dataset",
					backgroundColor: "rgba(38, 185, 154, 0.31)",
					borderColor: "rgba(38, 185, 154, 0.7)",
					pointBorderColor: "rgba(38, 185, 154, 0.7)",
					pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
					pointHoverBackgroundColor: "#fff",
					pointHoverBorderColor: "rgba(220,220,220,1)",
					pointBorderWidth: 1,
					data: [31, 74, 6, 39, 20, 85, 7]
				  }, {
					label: "My Second dataset",
					backgroundColor: "rgba(3, 88, 106, 0.3)",
					borderColor: "rgba(3, 88, 106, 0.70)",
					pointBorderColor: "rgba(3, 88, 106, 0.70)",
					pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
					pointHoverBackgroundColor: "#fff",
					pointHoverBorderColor: "rgba(151,187,205,1)",
					pointBorderWidth: 1,
					data: [82, 23, 66, 9, 99, 4, 2]
				  }]
				},
			  });
			
			}
				
			
			
			  
		
			  

			  // Doughnut chart
			  
			if ($('#canvasDoughnut').length ){ 
			  
			  var ctx = document.getElementById("canvasDoughnut");
			  var data = {
				labels: [
				  "Dark Grey",
				  "Purple Color",
				  "Gray Color",
				  "Green Color",
				  "Blue Color"
				],
				datasets: [{
				  data: [120, 50, 140, 180, 100],
				  backgroundColor: [
					"#455C73",
					"#9B59B6",
					"#BDC3C7",
					"#26B99A",
					"#3498DB"
				  ],
				  hoverBackgroundColor: [
					"#34495E",
					"#B370CF",
					"#CFD4D8",
					"#36CAAB",
					"#49A9EA"
				  ]

				}]
			  };

			  var canvasDoughnut = new Chart(ctx, {
				type: 'doughnut',
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: data
			  });
			 
			} 

			  // Radar chart
			  
			if ($('#canvasRadar').length ){ 
			  
			  var ctx = document.getElementById("canvasRadar");
			  var data = {
				labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
				datasets: [{
				  label: "My First dataset",
				  backgroundColor: "rgba(3, 88, 106, 0.2)",
				  borderColor: "rgba(3, 88, 106, 0.80)",
				  pointBorderColor: "rgba(3, 88, 106, 0.80)",
				  pointBackgroundColor: "rgba(3, 88, 106, 0.80)",
				  pointHoverBackgroundColor: "#fff",
				  pointHoverBorderColor: "rgba(220,220,220,1)",
				  data: [65, 59, 90, 81, 56, 55, 40]
				}, {
				  label: "My Second dataset",
				  backgroundColor: "rgba(38, 185, 154, 0.2)",
				  borderColor: "rgba(38, 185, 154, 0.85)",
				  pointColor: "rgba(38, 185, 154, 0.85)",
				  pointStrokeColor: "#fff",
				  pointHighlightFill: "#fff",
				  pointHighlightStroke: "rgba(151,187,205,1)",
				  data: [28, 48, 40, 19, 96, 27, 100]
				}]
			  };

			  var canvasRadar = new Chart(ctx, {
				type: 'radar',
				data: data,
			  });
			
			}
			
			
			  // Pie chart
			  if ($('#pieChart').length ){
				  
				  var ctx = document.getElementById("pieChart");
				  var data = {
					datasets: [{
					  data: [120, 50, 140, 180, 100],
					  backgroundColor: [
						"#455C73",
						"#9B59B6",
						"#BDC3C7",
						"#26B99A",
						"#3498DB"
					  ],
					  label: 'My dataset' // for legend
					}],
					labels: [
					  "Dark Gray",
					  "Purple",
					  "Gray",
					  "Green",
					  "Blue"
					]
				  };

				  var pieChart = new Chart(ctx, {
					data: data,
					type: 'pie',
					otpions: {
					  legend: false
					}
				  });
				  
			  }
			
			  
			  // PolarArea chart

			if ($('#polarArea').length ){

				var ctx = document.getElementById("polarArea");
				var data = {
				datasets: [{
				  data: [120, 50, 140, 180, 100],
				  backgroundColor: [
					"#455C73",
					"#9B59B6",
					"#BDC3C7",
					"#26B99A",
					"#3498DB"
				  ],
				  label: 'My dataset'
				}],
				labels: [
				  "Dark Gray",
				  "Purple",
				  "Gray",
				  "Green",
				  "Blue"
				]
				};

				var polarArea = new Chart(ctx, {
				data: data,
				type: 'polarArea',
				options: {
				  scale: {
					ticks: {
					  beginAtZero: true
					}
				  }
				}
				});
			
			}
		}

		/* COMPOSE */
		
		function init_compose() {
		
			if( typeof ($.fn.slideToggle) === 'undefined'){ return; }
			//console.log('init_compose');
		
			$('#compose, .compose-close').click(function(){
				$('.compose').slideToggle();
			});
		
		};
	   
	   	/* CALENDAR */
		  
		    function  init_calendar() {
					
				if( typeof ($.fn.fullCalendar) === 'undefined'){ return; }
				//console.log('init_calendar');
					
				var date = new Date(),
					d = date.getDate(),
					m = date.getMonth(),
					y = date.getFullYear(),
					started,
					categoryClass;

				var calendar = $('#calendar').fullCalendar({
				  header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay,listMonth'
				  },
				  selectable: true,
				  selectHelper: true,
				  select: function(start, end, allDay) {
					$('#fc_create').click();

					started = start;
					ended = end;

					$(".antosubmit").on("click", function() {
					  var title = $("#title").val();
					  if (end) {
						ended = end;
					  }

					  categoryClass = $("#event_type").val();

					  if (title) {
						calendar.fullCalendar('renderEvent', {
							title: title,
							start: started,
							end: end,
							allDay: allDay
						  },
						  true // make the event "stick"
						);
					  }

					  $('#title').val('');

					  calendar.fullCalendar('unselect');

					  $('.antoclose').click();

					  return false;
					});
				  },
				  eventClick: function(calEvent, jsEvent, view) {
					$('#fc_edit').click();
					$('#title2').val(calEvent.title);

					categoryClass = $("#event_type").val();

					$(".antosubmit2").on("click", function() {
					  calEvent.title = $("#title2").val();

					  calendar.fullCalendar('updateEvent', calEvent);
					  $('.antoclose2').click();
					});

					calendar.fullCalendar('unselect');
				  },
				  editable: true,
				  events: [{
					title: 'All Day Event',
					start: new Date(y, m, 1)
				  }, {
					title: 'Long Event',
					start: new Date(y, m, d - 5),
					end: new Date(y, m, d - 2)
				  }, {
					title: 'Meeting',
					start: new Date(y, m, d, 10, 30),
					allDay: false
				  }, {
					title: 'Lunch',
					start: new Date(y, m, d + 14, 12, 0),
					end: new Date(y, m, d, 14, 0),
					allDay: false
				  }, {
					title: 'Birthday Party',
					start: new Date(y, m, d + 1, 19, 0),
					end: new Date(y, m, d + 1, 22, 30),
					allDay: false
				  }, {
					title: 'Click for Google',
					start: new Date(y, m, 28),
					end: new Date(y, m, 29),
					url: 'http://google.com/'
				  }]
				});
				
			};
	   
		/* DATA TABLES */
			
			function init_DataTables() {
				
				//console.log('run_datatables');
				
				if( typeof ($.fn.DataTable) === 'undefined'){ return; }
				//console.log('init_DataTables');
				
				var handleDataTableButtons = function() {
				  if ($("#datatable-buttons").length) {
					$("#datatable-buttons").DataTable({
					  dom: "Blfrtip",
					  buttons: [
						{
						  extend: "copy",
						  className: "btn-sm"
						},
						{
						  extend: "csv",
						  className: "btn-sm"
						},
						{
						  extend: "excel",
						  className: "btn-sm"
						},
						{
						  extend: "pdfHtml5",
						  className: "btn-sm"
						},
						{
						  extend: "print",
						  className: "btn-sm"
						},
					  ],
					  responsive: true
					});
				  }
				};

				TableManageButtons = function() {
				  "use strict";
				  return {
					init: function() {
					  handleDataTableButtons();
					}
				  };
				}();

				$('#datatable').dataTable();

				$('#datatable-keytable').DataTable({
				  keys: true
				});

				$('#datatable-responsive').DataTable();

				$('#datatable-scroller').DataTable({
				  ajax: "js/datatables/json/scroller-demo.json",
				  deferRender: true,
				  scrollY: 380,
				  scrollCollapse: true,
				  scroller: true
				});

				$('#datatable-fixed-header').DataTable({
				  fixedHeader: true
				});

				var $datatable = $('#datatable-checkbox');

				$datatable.dataTable({
				  'order': [[ 1, 'asc' ]],
				  'columnDefs': [
					{ orderable: false, targets: [0] }
				  ]
				});
				$datatable.on('draw.dt', function() {
				  $('checkbox input').iCheck({
					checkboxClass: 'icheckbox_flat-green'
				  });
				});

				TableManageButtons.init();
				
			};
	   
			/* CHART - MORRIS  */
		var VehicleCountBarGraph=null;
		
		function init_morris_charts(vehicleCountData) {
			if(VehicleCountBarGraph)
			$("#graph_bar").empty();
			
			if ($('#graph_bar').length ){ 
				
	    var ConvertedData = [];
	for (var i = 0; i < vehicleCountData.length; i++) {
		var DatetimeValue = vehicleCountData[i].Hour;

		var DatetimeValueSplit = DatetimeValue.split(" ");
		var timeValueSplit = DatetimeValueSplit[1].split(":");
		var hourValue = parseInt(timeValueSplit[0]);

		var tempObj = {
			"Hour" : hourValue,
			"Count" : vehicleCountData[i].Count
		};

		ConvertedData.push(tempObj);

	}
				  
				    var count00=0;
					var count01=0;
					var count02=0;
					var count03=0;
					var count04=0;
					var count05=0;
					var count06=0;
					var count07=0;
					var count08=0;
					var count09=0;
					var count10=0;
					var count11=0;
					var count12=0;
					var count13=0;
					var count14=0;
					var count15=0;
					var count16=0;
					var count17=0;
					var count18=0;
					var count19=0;
					var count20=0;
					var count21=0;
					var count22=0;
					var count23=0;
					 
				  for(var j=0;j<ConvertedData.length;j++)
					  {
					  if(ConvertedData[j].Hour=="00")
					  {
						  count00 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="01")
					  {
						  count01 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="02")
					  {
						  count02 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="03")
					  {
						  count03 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="04")
					  {
						  count04 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="05")
					  {
						  count05 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="06")
					  {
						  count06 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="07")
					  {
						  count07 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="08")
					  {
						  count08 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="09")
					  {
						  count09 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="10")
					  {
						  count10 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="11")
					  {
						  count11 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="12")
					  {
						  count12 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="13")
					  {
						  count13 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="14")
					  {
						  count14 +=ConvertedData[j].Count;
					  }else  if(ConvertedData[j].Hour=="15")
					  {
						  count15 +=ConvertedData[j].Count;
					  }else   if(ConvertedData[j].Hour=="16")
					  {
						  count16 +=ConvertedData[j].Count;
					  }else   if(ConvertedData[j].Hour=="17")
					  {
						  count17 +=ConvertedData[j].Count;
					  }else   if(ConvertedData[j].Hour=="18")
					  {
						  count18 +=ConvertedData[j].Count;
					  }else   if(ConvertedData[j].Hour=="19")
					  {
						  count19 +=ConvertedData[j].Count;
					  }else   if(ConvertedData[j].Hour=="20")
					  {
						  count20 +=ConvertedData[j].Count;
					  }else   if(ConvertedData[j].Hour=="21")
					  {
						  count21 +=ConvertedData[j].Count;
					  }else   if(ConvertedData[j].Hour=="22")
					  {count22 +=ConvertedData[j].Count;
						  
					  }else   if(ConvertedData[j].Hour=="23")
					  {
						  count23 +=ConvertedData[j].Count;
					  } 
					  }
				  
			 
			if( typeof (Morris) === 'undefined'){ return; }
			 
	 var dataArr= [{"Hour":"00:00-01:00","Count":count00},{"Hour":"01:00-02:00","Count":count01},{"Hour":"02:00-03:00","Count":count02},{"Hour":"03:00-04:00","Count":count03},{"Hour":"04:00-05:00","Count":count04},{"Hour":"05:00-06:00","Count":count05},{"Hour":"06:00-07:00","Count":count06},{"Hour":"07:00-08:00","Count":count07},{"Hour":"08:00-09:00","Count":count08},{"Hour":"9:00-10:00","Count":count09},{"Hour":"10:00-11:00","Count":count10},{"Hour":"11:00-12:00","Count":count11},{"Hour":"12:00-13:00","Count":count12},{"Hour":"13:00-14:00","Count":count13},{"Hour":"14:00-15:00","Count":count14},{"Hour":"15:00-16:00","Count":count15},{"Hour":"16:00-17:00","Count":count16},{"Hour":"17:00-18:00","Count":count17},{"Hour":"18:00-19:00","Count":count18},{"Hour":"19:00-20:00","Count":count19},{"Hour":"20:00-21:00","Count":count20},{"Hour":"21:00-22:00","Count":count21},{"Hour":"22:00-23:00","Count":count22},{"Hour":"23:00-24:00","Count":count23}];

			if ($('#graph_bar').length){ 
			
			  VehicleCountBarGraph=	Morris.Bar({
				  element: 'graph_bar', 
 				  data: dataArr,
				  xkey: 'Hour',
				  ykeys: ['Count'],
				  labels: ['Count'],
				  barRatio: 0.4,
				  xLabelAngle: 35,
				  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
 				  hideHover: 'auto',
				  resize: true
				});

			}	
			
				}
			if ($('#graph_bar_group').length ){
			
				Morris.Bar({
				  element: 'graph_bar_group',
				  data: [
					{"period": "2016-10-01", "licensed": 807, "sorned": 660},
					{"period": "2016-09-30", "licensed": 1251, "sorned": 729},
					{"period": "2016-09-29", "licensed": 1769, "sorned": 1018},
					{"period": "2016-09-20", "licensed": 2246, "sorned": 1461},
					{"period": "2016-09-19", "licensed": 2657, "sorned": 1967},
					{"period": "2016-09-18", "licensed": 3148, "sorned": 2627},
					{"period": "2016-09-17", "licensed": 3471, "sorned": 3740},
					{"period": "2016-09-16", "licensed": 2871, "sorned": 2216},
					{"period": "2016-09-15", "licensed": 2401, "sorned": 1656},
					{"period": "2016-09-10", "licensed": 2115, "sorned": 1022}
				  ],
				  xkey: 'period',
				  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
				  ykeys: ['licensed', 'sorned'],
				  labels: ['Licensed', 'SORN'],
				  hideHover: 'auto',
				  xLabelAngle: 60,
				  resize: true
				});

			}
			
			if ($('#graphx').length ){
			
				Morris.Bar({
				  element: 'graphx',
				  data: [
					{x: '2015 Q1', y: 2, z: 3, a: 4},
					{x: '2015 Q2', y: 3, z: 5, a: 6},
					{x: '2015 Q3', y: 4, z: 3, a: 2},
					{x: '2015 Q4', y: 2, z: 4, a: 5}
				  ],
				  xkey: 'x',
				  ykeys: ['y', 'z', 'a'],
				  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
				  hideHover: 'auto',
				  labels: ['Y', 'Z', 'A'],
				  resize: true
				}).on('click', function (i, row) {
					//console.log(i, row);
				});

			}
			
			if ($('#graph_area').length ){
			
				Morris.Area({
				  element: 'graph_area',
				  data: [
					{period: '2014 Q1', iphone: 2666, ipad: null, itouch: 2647},
					{period: '2014 Q2', iphone: 2778, ipad: 2294, itouch: 2441},
					{period: '2014 Q3', iphone: 4912, ipad: 1969, itouch: 2501},
					{period: '2014 Q4', iphone: 3767, ipad: 3597, itouch: 5689},
					{period: '2015 Q1', iphone: 6810, ipad: 1914, itouch: 2293},
					{period: '2015 Q2', iphone: 5670, ipad: 4293, itouch: 1881},
					{period: '2015 Q3', iphone: 4820, ipad: 3795, itouch: 1588},
					{period: '2015 Q4', iphone: 15073, ipad: 5967, itouch: 5175},
					{period: '2016 Q1', iphone: 10687, ipad: 4460, itouch: 2028},
					{period: '2016 Q2', iphone: 8432, ipad: 5713, itouch: 1791}
				  ],
				  xkey: 'period',
				  ykeys: ['iphone', 'ipad', 'itouch'],
				  labels: ['iPhone', 'iPad', 'iPod Touch'],
				  pointSize: 2,
				  hideHover: 'auto',
				  resize: true
				});

			}
			
			if ($('#graph_donut').length ){
			
				Morris.Donut({
				  element: 'graph_donut',
				  data: [
					{label: 'Jam', value: 25},
					{label: 'Frosted', value: 40},
					{label: 'Custard', value: 25},
					{label: 'Sugar', value: 10}
				  ],
				  colors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
				  formatter: function (y) {
					return y + "%";
				  },
				  resize: true
				});

			}
			
			if ($('#graph_line').length ){
			
				Morris.Line({
				  element: 'graph_line',
				  xkey: 'year',
				  ykeys: ['value'],
				  labels: ['Value'],
				  hideHover: 'auto',
				  lineColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
				  data: [
					{year: '2012', value: 20},
					{year: '2013', value: 10},
					{year: '2014', value: 5},
					{year: '2015', value: 5},
					{year: '2016', value: 20}
				  ],
				  resize: true
				});

				$MENU_TOGGLE.on('click', function() {
				  $(window).resize();
				});
			
			}
			
		};
	   
		
		
		/* ECHRTS */
	
		
		function init_echarts(fd) {
		
				if( typeof (echarts) === 'undefined')
				{
					return;
				
				}
				//console.log('init_echarts');
			
		
				  var theme = {
				  color: [
					  '#26B99A', '#34495E', '#BDC3C7', '#3498DB',
					  '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
				  ],

				  title: {
					  itemGap: 8,
					  textStyle: {
						  fontWeight: 'normal',
						  color: '#408829'
					  }
				  },

				  dataRange: {
					  color: ['#1f610a', '#97b58d']
				  },

				  toolbox: {
					  color: ['#408829', '#408829', '#408829', '#408829']
				  },

				  tooltip: {
					  backgroundColor: 'rgba(0,0,0,0.5)',
					  axisPointer: {
						  type: 'line',
						  lineStyle: {
							  color: '#408829',
							  type: 'dashed'
						  },
						  crossStyle: {
							  color: '#408829'
						  },
						  shadowStyle: {
							  color: 'rgba(200,200,200,0.3)'
						  }
					  }
				  },

				  dataZoom: {
					  dataBackgroundColor: '#eee',
					  fillerColor: 'rgba(64,136,41,0.2)',
					  handleColor: '#408829'
				  },
				  grid: {
					  borderWidth: 0
				  },

				  categoryAxis: {
					  axisLine: {
						  lineStyle: {
							  color: '#408829'
						  }
					  },
					  splitLine: {
						  lineStyle: {
							  color: ['#eee']
						  }
					  }
				  },

				  valueAxis: {
					  axisLine: {
						  lineStyle: {
							  color: '#408829'
						  }
					  },
					  splitArea: {
						  show: true,
						  areaStyle: {
							  color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
						  }
					  },
					  splitLine: {
						  lineStyle: {
							  color: ['#eee']
						  }
					  }
				  },
				  timeline: {
					  lineStyle: {
						  color: '#408829'
					  },
					  controlStyle: {
						  normal: {color: '#408829'},
						  emphasis: {color: '#408829'}
					  }
				  },

				  k: {
					  itemStyle: {
						  normal: {
							  color: '#68a54a',
							  color0: '#a9cba2',
							  lineStyle: {
								  width: 1,
								  color: '#408829',
								  color0: '#86b379'
							  }
						  }
					  }
				  },
				  map: {
					  itemStyle: {
						  normal: {
							  areaStyle: {
								  color: '#ddd'
							  },
							  label: {
								  textStyle: {
									  color: '#c12e34'
								  }
							  }
						  },
						  emphasis: {
							  areaStyle: {
								  color: '#99d2dd'
							  },
							  label: {
								  textStyle: {
									  color: '#c12e34'
								  }
							  }
						  }
					  }
				  },
				  force: {
					  itemStyle: {
						  normal: {
							  linkStyle: {
								  strokeColor: '#408829'
							  }
						  }
					  }
				  },
				  chord: {
					  padding: 4,
					  itemStyle: {
						  normal: {
							  lineStyle: {
								  width: 1,
								  color: 'rgba(128, 128, 128, 0.5)'
							  },
							  chordStyle: {
								  lineStyle: {
									  width: 1,
									  color: 'rgba(128, 128, 128, 0.5)'
								  }
							  }
						  },
						  emphasis: {
							  lineStyle: {
								  width: 1,
								  color: 'rgba(128, 128, 128, 0.5)'
							  },
							  chordStyle: {
								  lineStyle: {
									  width: 1,
									  color: 'rgba(128, 128, 128, 0.5)'
								  }
							  }
						  }
					  }
				  },
				  gauge: {
					  startAngle: 225,
					  endAngle: -45,
					  axisLine: {
						  show: true,
						  lineStyle: {
							  color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
							  width: 8
						  }
					  },
					  axisTick: {
						  splitNumber: 10,
						  length: 12,
						  lineStyle: {
							  color: 'auto'
						  }
					  },
					  axisLabel: {
						  textStyle: {
							  color: 'auto'
						  }
					  },
					  splitLine: {
						  length: 18,
						  lineStyle: {
							  color: 'auto'
						  }
					  },
					  pointer: {
						  length: '90%',
						  color: 'auto'
					  },
					  title: {
						  textStyle: {
							  color: '#333'
						  }
					  },
					  detail: {
						  textStyle: {
							  color: 'auto'
						  }
					  }
				  },
				  textStyle: {
					  fontFamily: 'Arial, Verdana, sans-serif'
				  }
			  };

			  
			  //echart Bar
			  
			if ($('#mainb').length ){
			  
				  var echartBar = echarts.init(document.getElementById('mainb'), theme);

				  echartBar.setOption({
					title: {
					  text: 'Graph title',
					  subtext: 'Graph Sub-text'
					},
					tooltip: {
					  trigger: 'axis'
					},
					legend: {
					  data: ['sales', 'purchases']
					},
					toolbox: {
					  show: false
					},
					calculable: false,
					xAxis: [{
					  type: 'category',
					  data: ['1?', '2?', '3?', '4?', '5?', '6?', '7?', '8?', '9?', '10?', '11?', '12?']
					}],
					yAxis: [{
					  type: 'value'
					}],
					series: [{
					  name: 'sales',
					  type: 'bar',
					  data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
					  markPoint: {
						data: [{
						  type: 'max',
						  name: '???'
						}, {
						  type: 'min',
						  name: '???'
						}]
					  },
					  markLine: {
						data: [{
						  type: 'average',
						  name: '???'
						}]
					  }
					}, {
					  name: 'purchases',
					  type: 'bar',
					  data: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3],
					  markPoint: {
						data: [{
						  name: 'sales',
						  value: 182.2,
						  xAxis: 7,
						  yAxis: 183,
						}, {
						  name: 'purchases',
						  value: 2.3,
						  xAxis: 11,
						  yAxis: 3
						}]
					  },
					  markLine: {
						data: [{
						  type: 'average',
						  name: '???'
						}]
					  }
					}]
				  });

			}
			  
			  
			  
			  
			   //echart Radar
			  
			if ($('#echart_sonar').length ){ 
			  
			  var echartRadar = echarts.init(document.getElementById('echart_sonar'), theme);

			  echartRadar.setOption({
				title: {
				  text: 'Budget vs spending',
				  subtext: 'Subtitle'
				},
				 tooltip: {
					trigger: 'item'
				},
				legend: {
				  orient: 'vertical',
				  x: 'right',
				  y: 'bottom',
				  data: ['Allocated Budget', 'Actual Spending']
				},
				toolbox: {
				  show: true,
				  feature: {
					restore: {
					  show: true,
					  title: "Restore"
					},
					saveAsImage: {
					  show: true,
					  title: "Save Image"
					}
				  }
				},
				polar: [{
				  indicator: [{
					text: 'Sales',
					max: 6000
				  }, {
					text: 'Administration',
					max: 16000
				  }, {
					text: 'Information Techology',
					max: 30000
				  }, {
					text: 'Customer Support',
					max: 38000
				  }, {
					text: 'Development',
					max: 52000
				  }, {
					text: 'Marketing',
					max: 25000
				  }]
				}],
				calculable: true,
				series: [{
				  name: 'Budget vs spending',
				  type: 'radar',
				  data: [{
					value: [4300, 10000, 28000, 35000, 50000, 19000],
					name: 'Allocated Budget'
				  }, {
					value: [5000, 14000, 28000, 31000, 42000, 21000],
					name: 'Actual Spending'
				  }]
				}]
			  });

			} 
			  
			   //echart Funnel
			  
			if ($('#echart_pyramid').length ){ 
			  
			  var echartFunnel = echarts.init(document.getElementById('echart_pyramid'), theme);

			  echartFunnel.setOption({
				title: {
				  text: 'Echart Pyramid Graph',
				  subtext: 'Subtitle'
				},
				tooltip: {
				  trigger: 'item',
				  formatter: "{a} <br/>{b} : {c}%"
				},
				toolbox: {
				  show: true,
				  feature: {
					restore: {
					  show: true,
					  title: "Restore"
					},
					saveAsImage: {
					  show: true,
					  title: "Save Image"
					}
				  }
				},
				legend: {
				  data: ['Something #1', 'Something #2', 'Something #3', 'Something #4', 'Something #5'],
				  orient: 'vertical',
				  x: 'left',
				  y: 'bottom'
				},
				calculable: true,
				series: [{
				  name: '漏斗图',
				  type: 'funnel',
				  width: '40%',
				  data: [{
					value: 60,
					name: 'Something #1'
				  }, {
					value: 40,
					name: 'Something #2'
				  }, {
					value: 20,
					name: 'Something #3'
				  }, {
					value: 80,
					name: 'Something #4'
				  }, {
					value: 100,
					name: 'Something #5'
				  }]
				}]
			  });

			} 
			  
			   //echart Gauge
			  
			if ($('#echart_gauge').length ){ 
			  
			  var echartGauge = echarts.init(document.getElementById('echart_gauge'), theme);

			  echartGauge.setOption({
				tooltip: {
				  formatter: "{a} <br/>{b} : {c}%"
				},
				toolbox: {
				  show: true,
				  feature: {
					restore: {
					  show: true,
					  title: "Restore"
					},
					saveAsImage: {
					  show: true,
					  title: "Save Image"
					}
				  }
				},
				series: [{
				  name: 'Performance',
				  type: 'gauge',
				  center: ['50%', '50%'],
				  startAngle: 140,
				  endAngle: -140,
				  min: 0,
				  max: 100,
				  precision: 0,
				  splitNumber: 10,
				  axisLine: {
					show: true,
					lineStyle: {
					  color: [
						[0.2, 'lightgreen'],
						[0.4, 'orange'],
						[0.8, 'skyblue'],
						[1, '#ff4500']
					  ],
					  width: 30
					}
				  },
				  axisTick: {
					show: true,
					splitNumber: 5,
					length: 8,
					lineStyle: {
					  color: '#eee',
					  width: 1,
					  type: 'solid'
					}
				  },
				  axisLabel: {
					show: true,
					formatter: function(v) {
					  switch (v + '') {
						case '10':
						  return 'a';
						case '30':
						  return 'b';
						case '60':
						  return 'c';
						case '90':
						  return 'd';
						default:
						  return '';
					  }
					},
					textStyle: {
					  color: '#333'
					}
				  },
				  splitLine: {
					show: true,
					length: 30,
					lineStyle: {
					  color: '#eee',
					  width: 2,
					  type: 'solid'
					}
				  },
				  pointer: {
					length: '80%',
					width: 8,
					color: 'auto'
				  },
				  title: {
					show: true,
					offsetCenter: ['-65%', -10],
					textStyle: {
					  color: '#333',
					  fontSize: 15
					}
				  },
				  detail: {
					show: true,
					backgroundColor: 'rgba(0,0,0,0)',
					borderWidth: 0,
					borderColor: '#ccc',
					width: 100,
					height: 40,
					offsetCenter: ['-60%', 10],
					formatter: '{value}%',
					textStyle: {
					  color: 'auto',
					  fontSize: 30
					}
				  },
				  data: [{
					value: 50,
					name: 'Performance'
				  }]
				}]
			  });

			} 
			  
			   //echart Line
			  
			if ($('#echart_line').length ){ 
			  
			  var echartLine = echarts.init(document.getElementById('echart_line'), theme);

			  echartLine.setOption({
				title: {
				  text: 'Line Graph',
				  subtext: 'Subtitle'
				},
				tooltip: {
				  trigger: 'axis'
				},
				legend: {
				  x: 220,
				  y: 40,
				  data: ['Intent', 'Pre-order', 'Deal']
				},
				toolbox: {
				  show: true,
				  feature: {
					magicType: {
					  show: true,
					  title: {
						line: 'Line',
						bar: 'Bar',
						stack: 'Stack',
						tiled: 'Tiled'
					  },
					  type: ['line', 'bar', 'stack', 'tiled']
					},
					restore: {
					  show: true,
					  title: "Restore"
					},
					saveAsImage: {
					  show: true,
					  title: "Save Image"
					}
				  }
				},
				calculable: true,
				xAxis: [{
				  type: 'category',
				  boundaryGap: false,
				  data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
				}],
				yAxis: [{
				  type: 'value'
				}],
				series: [{
				  name: 'Deal',
				  type: 'line',
				  smooth: true,
				  itemStyle: {
					normal: {
					  areaStyle: {
						type: 'default'
					  }
					}
				  },
				  data: [10, 12, 21, 54, 260, 830, 710]
				}, {
				  name: 'Pre-order',
				  type: 'line',
				  smooth: true,
				  itemStyle: {
					normal: {
					  areaStyle: {
						type: 'default'
					  }
					}
				  },
				  data: [30, 182, 434, 791, 390, 30, 10]
				}, {
				  name: 'Intent',
				  type: 'line',
				  smooth: true,
				  itemStyle: {
					normal: {
					  areaStyle: {
						type: 'default'
					  }
					}
				  },
				  data: [1320, 1132, 601, 234, 120, 90, 20]
				}]
			  });

			} 
			//ECHART SCATTER
		/*	if ($('#echart_scatter1').length ){ 
		        var echartScatter = echarts.init(document.getElementById('echart_scatter1'), theme);
		        //var fd =[{"Status":"vehicle count","Hour":"2019-12-25 00:00:00","Count":68},{"Status":"vehicle count","Hour":"2019-12-25 00:05:24","Count":3},{"Status":"vehicle count","Hour":"2019-12-25 00:05:25","Count":59},{"Status":"vehicle count","Hour":"2019-12-25 00:10:11","Count":34},{"Status":"vehicle count","Hour":"2019-12-25 00:10:12","Count":25},{"Status":"vehicle count","Hour":"2019-12-25 00:15:09","Count":16},{"Status":"vehicle count","Hour":"2019-12-25 00:15:10","Count":10},{"Status":"vehicle count","Hour":"2019-12-25 00:20:07","Count":24},{"Status":"vehicle count","Hour":"2019-12-25 00:25:01","Count":23},{"Status":"vehicle count","Hour":"2019-12-25 00:30:00","Count":64},{"Status":"vehicle count","Hour":"2019-12-25 00:35:00","Count":48},{"Status":"vehicle count","Hour":"2019-12-25 00:40:01","Count":22},{"Status":"vehicle count","Hour":"2019-12-25 00:45:00","Count":25},{"Status":"vehicle count","Hour":"2019-12-25 00:45:01","Count":9},{"Status":"vehicle count","Hour":"2019-12-25 00:50:00","Count":24},{"Status":"vehicle count","Hour":"2019-12-25 00:55:00","Count":46},{"Status":"vehicle count","Hour":"2019-12-25 01:00:00","Count":46},{"Status":"vehicle count","Hour":"2019-12-25 01:00:01","Count":14},{"Status":"vehicle count","Hour":"2019-12-25 01:05:01","Count":42},{"Status":"vehicle count","Hour":"2019-12-25 01:10:02","Count":34},{"Status":"vehicle count","Hour":"2019-12-25 01:15:02","Count":11},{"Status":"vehicle count","Hour":"2019-12-25 01:20:01","Count":11},{"Status":"vehicle count","Hour":"2019-12-25 01:20:02","Count":8},{"Status":"vehicle count","Hour":"2019-12-25 01:25:01","Count":17},{"Status":"vehicle count","Hour":"2019-12-25 01:30:02","Count":16},{"Status":"vehicle count","Hour":"2019-12-25 01:35:02","Count":24},{"Status":"vehicle count","Hour":"2019-12-25 01:40:01","Count":14},{"Status":"vehicle count","Hour":"2019-12-25 01:45:01","Count":2},{"Status":"vehicle count","Hour":"2019-12-25 01:45:02","Count":15},{"Status":"vehicle count","Hour":"2019-12-25 01:50:03","Count":16},{"Status":"vehicle count","Hour":"2019-12-25 01:55:01","Count":31},{"Status":"vehicle count","Hour":"2019-12-25 01:55:02","Count":8},{"Status":"vehicle count","Hour":"2019-12-25 02:00:02","Count":15},{"Status":"vehicle count","Hour":"2019-12-25 02:05:01","Count":14},{"Status":"vehicle count","Hour":"2019-12-25 02:10:01","Count":1},{"Status":"vehicle count","Hour":"2019-12-25 02:10:02","Count":17},{"Status":"vehicle count","Hour":"2019-12-25 02:15:02","Count":17},{"Status":"vehicle count","Hour":"2019-12-25 02:20:03","Count":25},{"Status":"vehicle count","Hour":"2019-12-25 02:25:01","Count":25},{"Status":"vehicle count","Hour":"2019-12-25 02:30:01","Count":11},{"Status":"vehicle count","Hour":"2019-12-25 02:35:00","Count":20},{"Status":"vehicle count","Hour":"2019-12-25 02:40:02","Count":37},{"Status":"vehicle count","Hour":"2019-12-25 02:45:03","Count":7},{"Status":"vehicle count","Hour":"2019-12-25 02:45:04","Count":19},{"Status":"vehicle count","Hour":"2019-12-25 02:50:00","Count":12},{"Status":"vehicle count","Hour":"2019-12-25 02:50:01","Count":10},{"Status":"vehicle count","Hour":"2019-12-25 02:55:02","Count":18},{"Status":"vehicle count","Hour":"2019-12-25 03:00:03","Count":16},{"Status":"vehicle count","Hour":"2019-12-25 03:00:04","Count":1},{"Status":"vehicle count","Hour":"2019-12-25 03:05:02","Count":20},{"Status":"vehicle count","Hour":"2019-12-25 03:10:02","Count":24},{"Status":"vehicle count","Hour":"2019-12-25 03:15:01","Count":27},{"Status":"vehicle count","Hour":"2019-12-25 03:20:02","Count":26},{"Status":"vehicle count","Hour":"2019-12-25 03:25:01","Count":28},{"Status":"vehicle count","Hour":"2019-12-25 03:30:01","Count":15},{"Status":"vehicle count","Hour":"2019-12-25 03:35:01","Count":26},{"Status":"vehicle count","Hour":"2019-12-25 03:40:01","Count":21},{"Status":"vehicle count","Hour":"2019-12-25 03:45:00","Count":13},{"Status":"vehicle count","Hour":"2019-12-25 03:45:01","Count":1},{"Status":"vehicle count","Hour":"2019-12-25 03:50:01","Count":20},{"Status":"vehicle count","Hour":"2019-12-25 03:55:01","Count":28},{"Status":"vehicle count","Hour":"2019-12-25 04:00:01","Count":21},{"Status":"vehicle count","Hour":"2019-12-25 04:05:22","Count":6},{"Status":"vehicle count","Hour":"2019-12-25 04:05:23","Count":18},{"Status":"vehicle count","Hour":"2019-12-25 04:10:18","Count":31},{"Status":"vehicle count","Hour":"2019-12-25 04:15:10","Count":1},{"Status":"vehicle count","Hour":"2019-12-25 04:15:16","Count":2},{"Status":"vehicle count","Hour":"2019-12-25 04:15:17","Count":18},{"Status":"vehicle count","Hour":"2019-12-25 04:20:01","Count":20},{"Status":"vehicle count","Hour":"2019-12-25 04:25:00","Count":32},{"Status":"vehicle count","Hour":"2019-12-25 04:30:00","Count":22},{"Status":"vehicle count","Hour":"2019-12-25 04:35:01","Count":28},{"Status":"vehicle count","Hour":"2019-12-25 04:40:00","Count":19},{"Status":"vehicle count","Hour":"2019-12-25 04:45:00","Count":36},{"Status":"vehicle count","Hour":"2019-12-25 04:50:01","Count":41},{"Status":"vehicle count","Hour":"2019-12-25 04:55:01","Count":32},{"Status":"vehicle count","Hour":"2019-12-25 05:00:00","Count":11},{"Status":"vehicle count","Hour":"2019-12-25 05:00:01","Count":6},{"Status":"vehicle count","Hour":"2019-12-25 05:05:01","Count":25},{"Status":"vehicle count","Hour":"2019-12-25 05:10:00","Count":55},{"Status":"vehicle count","Hour":"2019-12-25 05:15:00","Count":17},{"Status":"vehicle count","Hour":"2019-12-25 05:20:00","Count":44},{"Status":"vehicle count","Hour":"2019-12-25 05:25:00","Count":41},{"Status":"vehicle count","Hour":"2019-12-25 05:30:00","Count":14},{"Status":"vehicle count","Hour":"2019-12-25 05:30:01","Count":10},{"Status":"vehicle count","Hour":"2019-12-25 05:35:00","Count":14},{"Status":"vehicle count","Hour":"2019-12-25 05:35:01","Count":13},{"Status":"vehicle count","Hour":"2019-12-25 05:40:00","Count":38},{"Status":"vehicle count","Hour":"2019-12-25 05:45:00","Count":48},{"Status":"vehicle count","Hour":"2019-12-25 05:50:01","Count":27},{"Status":"vehicle count","Hour":"2019-12-25 05:55:01","Count":72},{"Status":"vehicle count","Hour":"2019-12-25 06:00:01","Count":23},{"Status":"vehicle count","Hour":"2019-12-25 06:00:02","Count":3},{"Status":"vehicle count","Hour":"2019-12-25 06:05:09","Count":24},{"Status":"vehicle count","Hour":"2019-12-25 06:10:11","Count":59},{"Status":"vehicle count","Hour":"2019-12-25 06:15:01","Count":33},{"Status":"vehicle count","Hour":"2019-12-25 06:20:01","Count":54},{"Status":"vehicle count","Hour":"2019-12-25 06:25:00","Count":68},{"Status":"vehicle count","Hour":"2019-12-25 06:30:01","Count":14},{"Status":"vehicle count","Hour":"2019-12-25 06:30:02","Count":16},{"Status":"vehicle count","Hour":"2019-12-25 06:35:02","Count":62},{"Status":"vehicle count","Hour":"2019-12-25 06:40:00","Count":34},{"Status":"vehicle count","Hour":"2019-12-25 06:45:00","Count":88},{"Status":"vehicle count","Hour":"2019-12-25 06:50:00","Count":68},{"Status":"vehicle count","Hour":"2019-12-25 06:55:00","Count":43},{"Status":"vehicle count","Hour":"2019-12-25 07:00:00","Count":86},{"Status":"vehicle count","Hour":"2019-12-25 07:05:01","Count":36},{"Status":"vehicle count","Hour":"2019-12-25 07:10:00","Count":64},{"Status":"vehicle count","Hour":"2019-12-25 07:15:00","Count":43},{"Status":"vehicle count","Hour":"2019-12-25 07:20:01","Count":66},{"Status":"vehicle count","Hour":"2019-12-25 07:25:01","Count":91},{"Status":"vehicle count","Hour":"2019-12-25 07:30:00","Count":58},{"Status":"vehicle count","Hour":"2019-12-25 07:35:01","Count":92},{"Status":"vehicle count","Hour":"2019-12-25 07:40:00","Count":93},{"Status":"vehicle count","Hour":"2019-12-25 07:40:01","Count":10},{"Status":"vehicle count","Hour":"2019-12-25 07:45:00","Count":56},{"Status":"vehicle count","Hour":"2019-12-25 07:45:01","Count":9},{"Status":"vehicle count","Hour":"2019-12-25 07:50:00","Count":40},{"Status":"vehicle count","Hour":"2019-12-25 07:50:01","Count":3},{"Status":"vehicle count","Hour":"2019-12-25 07:55:00","Count":79},{"Status":"vehicle count","Hour":"2019-12-25 07:55:01","Count":16},{"Status":"vehicle count","Hour":"2019-12-25 08:00:00","Count":18},{"Status":"vehicle count","Hour":"2019-12-25 08:00:01","Count":37},{"Status":"vehicle count","Hour":"2019-12-25 08:05:01","Count":48},{"Status":"vehicle count","Hour":"2019-12-25 08:10:00","Count":90},{"Status":"vehicle count","Hour":"2019-12-25 08:10:01","Count":5},{"Status":"vehicle count","Hour":"2019-12-25 08:15:00","Count":65},{"Status":"vehicle count","Hour":"2019-12-25 08:20:00","Count":59},{"Status":"vehicle count","Hour":"2019-12-25 08:25:01","Count":70},{"Status":"vehicle count","Hour":"2019-12-25 08:30:00","Count":31},{"Status":"vehicle count","Hour":"2019-12-25 08:30:01","Count":75},{"Status":"vehicle count","Hour":"2019-12-25 08:35:00","Count":62},{"Status":"vehicle count","Hour":"2019-12-25 08:35:01","Count":5},{"Status":"vehicle count","Hour":"2019-12-25 08:40:00","Count":76},{"Status":"vehicle count","Hour":"2019-12-25 08:45:00","Count":89},{"Status":"vehicle count","Hour":"2019-12-25 08:50:01","Count":53},{"Status":"vehicle count","Hour":"2019-12-25 08:55:00","Count":83},{"Status":"vehicle count","Hour":"2019-12-25 09:00:00","Count":85},{"Status":"vehicle count","Hour":"2019-12-25 09:05:00","Count":71},{"Status":"vehicle count","Hour":"2019-12-25 09:10:00","Count":115},{"Status":"vehicle count","Hour":"2019-12-25 09:15:00","Count":113},{"Status":"vehicle count","Hour":"2019-12-25 09:20:00","Count":86},{"Status":"vehicle count","Hour":"2019-12-25 09:25:00","Count":94},{"Status":"vehicle count","Hour":"2019-12-25 09:30:01","Count":100},{"Status":"vehicle count","Hour":"2019-12-25 09:35:00","Count":104},{"Status":"vehicle count","Hour":"2019-12-25 09:40:00","Count":106},{"Status":"vehicle count","Hour":"2019-12-25 09:45:00","Count":77},{"Status":"vehicle count","Hour":"2019-12-25 09:50:02","Count":183},{"Status":"vehicle count","Hour":"2019-12-25 09:55:01","Count":111},{"Status":"vehicle count","Hour":"2019-12-25 09:55:02","Count":3},{"Status":"vehicle count","Hour":"2019-12-25 10:00:00","Count":116},{"Status":"vehicle count","Hour":"2019-12-25 10:05:00","Count":99},{"Status":"vehicle count","Hour":"2019-12-25 10:10:01","Count":96},{"Status":"vehicle count","Hour":"2019-12-25 10:15:00","Count":116},{"Status":"vehicle count","Hour":"2019-12-25 10:20:01","Count":152},{"Status":"vehicle count","Hour":"2019-12-25 10:25:00","Count":77},{"Status":"vehicle count","Hour":"2019-12-25 10:30:00","Count":87},{"Status":"vehicle count","Hour":"2019-12-25 10:35:00","Count":118},{"Status":"vehicle count","Hour":"2019-12-25 10:40:00","Count":121},{"Status":"vehicle count","Hour":"2019-12-25 10:45:00","Count":111},{"Status":"vehicle count","Hour":"2019-12-25 10:50:00","Count":127},{"Status":"vehicle count","Hour":"2019-12-25 10:55:00","Count":92},{"Status":"vehicle count","Hour":"2019-12-25 11:00:01","Count":124},{"Status":"vehicle count","Hour":"2019-12-25 11:05:00","Count":125},{"Status":"vehicle count","Hour":"2019-12-25 11:10:00","Count":165},{"Status":"vehicle count","Hour":"2019-12-25 11:15:01","Count":97},{"Status":"vehicle count","Hour":"2019-12-25 11:20:01","Count":140},{"Status":"vehicle count","Hour":"2019-12-25 11:25:01","Count":93},{"Status":"vehicle count","Hour":"2019-12-25 11:30:00","Count":127},{"Status":"vehicle count","Hour":"2019-12-25 11:35:01","Count":117},{"Status":"vehicle count","Hour":"2019-12-25 11:40:01","Count":113},{"Status":"vehicle count","Hour":"2019-12-25 11:45:00","Count":116},{"Status":"vehicle count","Hour":"2019-12-25 11:45:01","Count":8},{"Status":"vehicle count","Hour":"2019-12-25 11:50:00","Count":81},{"Status":"vehicle count","Hour":"2019-12-25 11:50:01","Count":105},{"Status":"vehicle count","Hour":"2019-12-25 11:55:02","Count":94},{"Status":"vehicle count","Hour":"2019-12-25 12:00:00","Count":156},{"Status":"vehicle count","Hour":"2019-12-25 12:05:04","Count":32},{"Status":"vehicle count","Hour":"2019-12-25 12:05:06","Count":122},{"Status":"vehicle count","Hour":"2019-12-25 12:10:18","Count":139},{"Status":"vehicle count","Hour":"2019-12-25 12:10:19","Count":18},{"Status":"vehicle count","Hour":"2019-12-25 12:15:05","Count":106},{"Status":"vehicle count","Hour":"2019-12-25 12:20:00","Count":316},{"Status":"vehicle count","Hour":"2019-12-25 12:25:01","Count":270},{"Status":"vehicle count","Hour":"2019-12-25 12:30:00","Count":294},{"Status":"vehicle count","Hour":"2019-12-25 12:35:00","Count":269},{"Status":"vehicle count","Hour":"2019-12-25 12:40:01","Count":253},{"Status":"vehicle count","Hour":"2019-12-25 12:45:00","Count":137},{"Status":"vehicle count","Hour":"2019-12-25 12:50:00","Count":129},{"Status":"vehicle count","Hour":"2019-12-25 12:55:00","Count":124},{"Status":"vehicle count","Hour":"2019-12-25 13:00:00","Count":148},{"Status":"vehicle count","Hour":"2019-12-25 13:05:00","Count":134},{"Status":"vehicle count","Hour":"2019-12-25 13:10:01","Count":120},{"Status":"vehicle count","Hour":"2019-12-25 13:15:00","Count":149},{"Status":"vehicle count","Hour":"2019-12-25 13:20:01","Count":132},{"Status":"vehicle count","Hour":"2019-12-25 13:25:00","Count":129},{"Status":"vehicle count","Hour":"2019-12-25 13:30:00","Count":118},{"Status":"vehicle count","Hour":"2019-12-25 13:35:00","Count":87},{"Status":"vehicle count","Hour":"2019-12-25 13:40:00","Count":47},{"Status":"vehicle count","Hour":"2019-12-25 13:45:00","Count":127},{"Status":"vehicle count","Hour":"2019-12-25 13:50:00","Count":120},{"Status":"vehicle count","Hour":"2019-12-25 13:55:00","Count":339},{"Status":"vehicle count","Hour":"2019-12-25 14:00:00","Count":304},{"Status":"vehicle count","Hour":"2019-12-25 14:05:00","Count":403},{"Status":"vehicle count","Hour":"2019-12-25 14:10:00","Count":327},{"Status":"vehicle count","Hour":"2019-12-25 14:15:00","Count":369},{"Status":"vehicle count","Hour":"2019-12-25 14:20:00","Count":466},{"Status":"vehicle count","Hour":"2019-12-25 14:25:00","Count":323},{"Status":"vehicle count","Hour":"2019-12-25 14:30:00","Count":239},{"Status":"vehicle count","Hour":"2019-12-25 14:30:01","Count":99},{"Status":"vehicle count","Hour":"2019-12-25 14:35:00","Count":327},{"Status":"vehicle count","Hour":"2019-12-25 14:40:00","Count":410},{"Status":"vehicle count","Hour":"2019-12-25 14:45:00","Count":324},{"Status":"vehicle count","Hour":"2019-12-25 14:50:00","Count":323},{"Status":"vehicle count","Hour":"2019-12-25 14:55:00","Count":221},{"Status":"vehicle count","Hour":"2019-12-25 14:55:01","Count":86},{"Status":"vehicle count","Hour":"2019-12-25 15:00:00","Count":353},{"Status":"vehicle count","Hour":"2019-12-25 15:05:00","Count":341},{"Status":"vehicle count","Hour":"2019-12-25 15:10:00","Count":390},{"Status":"vehicle count","Hour":"2019-12-25 15:15:00","Count":358},{"Status":"vehicle count","Hour":"2019-12-25 15:20:00","Count":342},{"Status":"vehicle count","Hour":"2019-12-25 15:25:01","Count":340},{"Status":"vehicle count","Hour":"2019-12-25 15:25:02","Count":3},{"Status":"vehicle count","Hour":"2019-12-25 15:30:00","Count":371},{"Status":"vehicle count","Hour":"2019-12-25 15:35:00","Count":332},{"Status":"vehicle count","Hour":"2019-12-25 15:40:00","Count":313},{"Status":"vehicle count","Hour":"2019-12-25 15:45:00","Count":347},{"Status":"vehicle count","Hour":"2019-12-25 15:50:00","Count":359},{"Status":"vehicle count","Hour":"2019-12-25 15:55:00","Count":335},{"Status":"vehicle count","Hour":"2019-12-25 16:00:00","Count":132},{"Status":"vehicle count","Hour":"2019-12-25 16:05:04","Count":106},{"Status":"vehicle count","Hour":"2019-12-25 16:05:06","Count":9},{"Status":"vehicle count","Hour":"2019-12-25 16:05:08","Count":1},{"Status":"vehicle count","Hour":"2019-12-25 16:10:00","Count":148},{"Status":"vehicle count","Hour":"2019-12-25 16:15:00","Count":138},{"Status":"vehicle count","Hour":"2019-12-25 16:20:00","Count":132},{"Status":"vehicle count","Hour":"2019-12-25 16:25:00","Count":104},{"Status":"vehicle count","Hour":"2019-12-25 16:30:01","Count":170},{"Status":"vehicle count","Hour":"2019-12-25 16:35:00","Count":149},{"Status":"vehicle count","Hour":"2019-12-25 16:40:00","Count":101},{"Status":"vehicle count","Hour":"2019-12-25 16:45:01","Count":155},{"Status":"vehicle count","Hour":"2019-12-25 16:50:00","Count":163},{"Status":"vehicle count","Hour":"2019-12-25 16:55:00","Count":150},{"Status":"vehicle count","Hour":"2019-12-25 17:00:00","Count":145},{"Status":"vehicle count","Hour":"2019-12-25 17:05:01","Count":133},{"Status":"vehicle count","Hour":"2019-12-25 17:10:00","Count":133},{"Status":"vehicle count","Hour":"2019-12-25 17:15:00","Count":148},{"Status":"vehicle count","Hour":"2019-12-25 17:20:00","Count":160},{"Status":"vehicle count","Hour":"2019-12-25 17:25:00","Count":144},{"Status":"vehicle count","Hour":"2019-12-25 17:30:00","Count":137},{"Status":"vehicle count","Hour":"2019-12-25 17:35:00","Count":122},{"Status":"vehicle count","Hour":"2019-12-25 17:40:00","Count":155},{"Status":"vehicle count","Hour":"2019-12-25 17:45:00","Count":144},{"Status":"vehicle count","Hour":"2019-12-25 17:50:00","Count":167},{"Status":"vehicle count","Hour":"2019-12-25 17:55:00","Count":82},{"Status":"vehicle count","Hour":"2019-12-25 18:00:00","Count":243},{"Status":"vehicle count","Hour":"2019-12-25 18:05:03","Count":145},{"Status":"vehicle count","Hour":"2019-12-25 18:05:04","Count":3},{"Status":"vehicle count","Hour":"2019-12-25 18:05:05","Count":4},{"Status":"vehicle count","Hour":"2019-12-25 18:10:00","Count":133},{"Status":"vehicle count","Hour":"2019-12-25 18:15:00","Count":137},{"Status":"vehicle count","Hour":"2019-12-25 18:20:00","Count":80},{"Status":"vehicle count","Hour":"2019-12-25 18:25:00","Count":191},{"Status":"vehicle count","Hour":"2019-12-25 18:30:00","Count":198},{"Status":"vehicle count","Hour":"2019-12-25 18:35:00","Count":153},{"Status":"vehicle count","Hour":"2019-12-25 18:35:01","Count":4},{"Status":"vehicle count","Hour":"2019-12-25 18:40:00","Count":158},{"Status":"vehicle count","Hour":"2019-12-25 18:45:00","Count":112},{"Status":"vehicle count","Hour":"2019-12-25 18:50:00","Count":80},{"Status":"vehicle count","Hour":"2019-12-25 18:55:00","Count":152},{"Status":"vehicle count","Hour":"2019-12-25 19:00:00","Count":185},{"Status":"vehicle count","Hour":"2019-12-25 19:05:00","Count":118},{"Status":"vehicle count","Hour":"2019-12-25 19:10:01","Count":137},{"Status":"vehicle count","Hour":"2019-12-25 19:15:01","Count":155},{"Status":"vehicle count","Hour":"2019-12-25 19:20:00","Count":140},{"Status":"vehicle count","Hour":"2019-12-25 19:25:00","Count":154},{"Status":"vehicle count","Hour":"2019-12-25 19:30:00","Count":141},{"Status":"vehicle count","Hour":"2019-12-25 19:30:01","Count":34},{"Status":"vehicle count","Hour":"2019-12-25 19:35:00","Count":158},{"Status":"vehicle count","Hour":"2019-12-25 19:40:00","Count":160},{"Status":"vehicle count","Hour":"2019-12-25 19:45:01","Count":80},{"Status":"vehicle count","Hour":"2019-12-25 19:45:02","Count":78},{"Status":"vehicle count","Hour":"2019-12-25 19:50:01","Count":132},{"Status":"vehicle count","Hour":"2019-12-25 19:55:00","Count":157},{"Status":"vehicle count","Hour":"2019-12-25 20:00:00","Count":142},{"Status":"vehicle count","Hour":"2019-12-25 20:05:00","Count":139},{"Status":"vehicle count","Hour":"2019-12-25 20:10:00","Count":109},{"Status":"vehicle count","Hour":"2019-12-25 20:15:00","Count":130},{"Status":"vehicle count","Hour":"2019-12-25 20:20:00","Count":147},{"Status":"vehicle count","Hour":"2019-12-25 20:25:00","Count":151},{"Status":"vehicle count","Hour":"2019-12-25 20:30:00","Count":173},{"Status":"vehicle count","Hour":"2019-12-25 20:35:00","Count":137},{"Status":"vehicle count","Hour":"2019-12-25 20:40:00","Count":138},{"Status":"vehicle count","Hour":"2019-12-25 20:45:00","Count":126},{"Status":"vehicle count","Hour":"2019-12-25 20:50:00","Count":103},{"Status":"vehicle count","Hour":"2019-12-25 20:55:00","Count":125},{"Status":"vehicle count","Hour":"2019-12-25 21:00:00","Count":113},{"Status":"vehicle count","Hour":"2019-12-25 21:05:01","Count":135},{"Status":"vehicle count","Hour":"2019-12-25 21:10:00","Count":101},{"Status":"vehicle count","Hour":"2019-12-25 21:15:00","Count":47},{"Status":"vehicle count","Hour":"2019-12-25 21:15:01","Count":55},{"Status":"vehicle count","Hour":"2019-12-25 21:20:01","Count":111},{"Status":"vehicle count","Hour":"2019-12-25 21:25:01","Count":124},{"Status":"vehicle count","Hour":"2019-12-25 21:30:00","Count":48},{"Status":"vehicle count","Hour":"2019-12-25 21:30:01","Count":57},{"Status":"vehicle count","Hour":"2019-12-25 21:35:01","Count":92},{"Status":"vehicle count","Hour":"2019-12-25 21:40:01","Count":39},{"Status":"vehicle count","Hour":"2019-12-25 21:40:02","Count":26},{"Status":"vehicle count","Hour":"2019-12-25 21:45:02","Count":76},{"Status":"vehicle count","Hour":"2019-12-25 21:45:03","Count":2},{"Status":"vehicle count","Hour":"2019-12-25 21:50:02","Count":57},{"Status":"vehicle count","Hour":"2019-12-25 21:50:03","Count":1},{"Status":"vehicle count","Hour":"2019-12-25 21:55:01","Count":61},{"Status":"vehicle count","Hour":"2019-12-25 21:55:02","Count":25},{"Status":"vehicle count","Hour":"2019-12-25 22:00:02","Count":88},{"Status":"vehicle count","Hour":"2019-12-25 22:05:00","Count":39},{"Status":"vehicle count","Hour":"2019-12-25 22:05:01","Count":22},{"Status":"vehicle count","Hour":"2019-12-25 22:10:01","Count":56},{"Status":"vehicle count","Hour":"2019-12-25 22:15:00","Count":53},{"Status":"vehicle count","Hour":"2019-12-25 22:15:01","Count":26},{"Status":"vehicle count","Hour":"2019-12-25 22:20:01","Count":54},{"Status":"vehicle count","Hour":"2019-12-25 22:25:01","Count":69},{"Status":"vehicle count","Hour":"2019-12-25 22:30:01","Count":51},{"Status":"vehicle count","Hour":"2019-12-25 22:35:01","Count":65},{"Status":"vehicle count","Hour":"2019-12-25 22:40:01","Count":59},{"Status":"vehicle count","Hour":"2019-12-25 22:45:00","Count":43},{"Status":"vehicle count","Hour":"2019-12-25 22:50:01","Count":85},{"Status":"vehicle count","Hour":"2019-12-25 22:55:01","Count":45},{"Status":"vehicle count","Hour":"2019-12-25 23:00:01","Count":36},{"Status":"vehicle count","Hour":"2019-12-25 23:05:00","Count":45},{"Status":"vehicle count","Hour":"2019-12-25 23:10:01","Count":85},{"Status":"vehicle count","Hour":"2019-12-25 23:15:01","Count":17},{"Status":"vehicle count","Hour":"2019-12-25 23:15:02","Count":3},{"Status":"vehicle count","Hour":"2019-12-25 23:20:02","Count":47},{"Status":"vehicle count","Hour":"2019-12-25 23:25:01","Count":60},{"Status":"vehicle count","Hour":"2019-12-25 23:30:00","Count":10},{"Status":"vehicle count","Hour":"2019-12-25 23:30:01","Count":43},{"Status":"vehicle count","Hour":"2019-12-25 23:35:01","Count":34},{"Status":"vehicle count","Hour":"2019-12-25 23:35:02","Count":3},{"Status":"vehicle count","Hour":"2019-12-25 23:40:00","Count":47},{"Status":"vehicle count","Hour":"2019-12-25 23:40:01","Count":6},{"Status":"vehicle count","Hour":"2019-12-25 23:45:00","Count":24},{"Status":"vehicle count","Hour":"2019-12-25 23:50:01","Count":27},{"Status":"vehicle count","Hour":"2019-12-25 23:55:01","Count":25}]
					  var ConvertedData=[];
					  
					  for(var i=0;i<fd.length;i++)
					  {
						  var tm =fd[i].Hour;
						  
						   var res = tm.split(" ");
						   var res1 =res[1].split(":");
						   var t1= parseInt(res1[0]);  
						   var t2=parseInt(res1[1])*0.0167; 
						  var intVal=(t1+t2).toFixed(2);
						  
						  tempArr=[intVal,fd[i].Count];
						  
						  
						  ConvertedData.push(tempArr);
						  
					  }
		            //console.log(ConvertedData);
					  echartScatter.setOption({
						title: {
						  text: 'Wrong Direction' 
						},
						tooltip: {
						  trigger: 'axis',
						  showDelay: 0,
						  axisPointer: {
							type: 'cross',
							lineStyle: {
							  type: 'dashed',
							  width: 1
							}
						  }
						},
						 
						toolbox: {
						  show: true,
						  feature: {
							saveAsImage: {
							  show: true,
							  title: "Save Image"
							}
						  }
						},
						xAxis: [{
						  type: 'value',
						  scale: true,
						   min: 0,
		                   max: 24,
						   stepSize: 2,
						  axisLabel: {
							formatter: '{value}:00'
						  }
						}],
						yAxis: [{
						  type: 'value',
						  scale: true,
						  axisLabel: {
							formatter: '{value}'
						  }
						}],
						series: [{
						  
						  type: 'scatter',
						  tooltip: {
							trigger: 'item',
							formatter: function(params) {
								
							 var res = params.value[0].split(".");
							 
						     var t1= parseInt(res[0]);
		                     if(t1<10)
		                     {
		                    t1="0"+t1;
		                     }	
				
						     var t2=parseInt(res[1])*0.6; 
							 var t3 =Math.round(t2);
			                 if(t3<10)
				                  {
		                      t3="0"+t3;
				                   }
							   return  'Hour : ' + t1+':'+t3+'<br>' + 'Count : ' +params.value[1] ;
							 
							}
						  },
						  data: ConvertedData,
						  markPoint: {
							data: [{
							  type: 'max',
							  name: 'Max'
							}, {
							  type: 'min',
							  name: 'Min'
							}]
						  },
						  markLine: {
							data: [{
							  type: 'average',
							  name: 'Mean'
							}]
						  }
						} ] 
					  });

					} */
			if ($('#echart_scatter').length ){ 
	        var echartScatter = echarts.init(document.getElementById('echart_scatter'), theme);
	        //var fd =[{"Hour":"2019-12-25 12:12:00","Count":68},{"Hour":"2019-12-25 00:05:24","Count":3},{"Hour":"2019-12-25 00:05:25","Count":59},{"Hour":"2019-12-25 00:10:11","Count":34},{"Hour":"2019-12-25 00:10:12","Count":25},{"Hour":"2019-12-25 00:15:09","Count":16},{"Hour":"2019-12-25 00:15:10","Count":10},{"Hour":"2019-12-25 00:20:07","Count":24},{"Hour":"2019-12-25 00:25:01","Count":23},{"Hour":"2019-12-25 00:30:00","Count":64},{"Hour":"2019-12-25 00:35:00","Count":48},{"Hour":"2019-12-25 00:40:01","Count":22},{"Hour":"2019-12-25 00:45:00","Count":25},{"Hour":"2019-12-25 00:45:01","Count":9},{"Hour":"2019-12-25 00:50:00","Count":24},{"Hour":"2019-12-25 00:55:00","Count":46},{"Hour":"2019-12-25 01:00:00","Count":46},{"Hour":"2019-12-25 01:00:01","Count":14},{"Hour":"2019-12-25 01:05:01","Count":42},{"Hour":"2019-12-25 01:10:02","Count":34},{"Hour":"2019-12-25 01:15:02","Count":11},{"Hour":"2019-12-25 01:20:01","Count":11},{"Hour":"2019-12-25 01:20:02","Count":8},{"Hour":"2019-12-25 01:25:01","Count":17},{"Hour":"2019-12-25 01:30:02","Count":16},{"Hour":"2019-12-25 01:35:02","Count":24},{"Hour":"2019-12-25 01:40:01","Count":14},{"Hour":"2019-12-25 01:45:01","Count":2},{"Hour":"2019-12-25 01:45:02","Count":15},{"Hour":"2019-12-25 01:50:03","Count":16},{"Hour":"2019-12-25 01:55:01","Count":31},{"Hour":"2019-12-25 01:55:02","Count":8},{"Hour":"2019-12-25 02:00:02","Count":15},{"Hour":"2019-12-25 02:05:01","Count":14},{"Hour":"2019-12-25 02:10:01","Count":1},{"Hour":"2019-12-25 02:10:02","Count":17},{"Hour":"2019-12-25 02:15:02","Count":17},{"Hour":"2019-12-25 02:20:03","Count":25},{"Hour":"2019-12-25 02:25:01","Count":25},{"Hour":"2019-12-25 02:30:01","Count":11},{"Hour":"2019-12-25 02:35:00","Count":20},{"Hour":"2019-12-25 02:40:02","Count":37},{"Hour":"2019-12-25 02:45:03","Count":7},{"Hour":"2019-12-25 02:45:04","Count":19},{"Hour":"2019-12-25 02:50:00","Count":12},{"Hour":"2019-12-25 02:50:01","Count":10},{"Hour":"2019-12-25 02:55:02","Count":18},{"Hour":"2019-12-25 03:00:03","Count":16},{"Hour":"2019-12-25 03:00:04","Count":1},{"Hour":"2019-12-25 03:05:02","Count":20},{"Hour":"2019-12-25 03:10:02","Count":24},{"Hour":"2019-12-25 03:15:01","Count":27},{"Hour":"2019-12-25 03:20:02","Count":26},{"Hour":"2019-12-25 03:25:01","Count":28},{"Hour":"2019-12-25 03:30:01","Count":15},{"Hour":"2019-12-25 03:35:01","Count":26},{"Hour":"2019-12-25 03:40:01","Count":21},{"Hour":"2019-12-25 03:45:00","Count":13},{"Hour":"2019-12-25 03:45:01","Count":1},{"Hour":"2019-12-25 03:50:01","Count":20},{"Hour":"2019-12-25 03:55:01","Count":28},{"Hour":"2019-12-25 04:00:01","Count":21},{"Hour":"2019-12-25 04:05:22","Count":6},{"Hour":"2019-12-25 04:05:23","Count":18},{"Hour":"2019-12-25 04:10:18","Count":31},{"Hour":"2019-12-25 04:15:10","Count":1},{"Hour":"2019-12-25 04:15:16","Count":2},{"Hour":"2019-12-25 04:15:17","Count":18},{"Hour":"2019-12-25 04:20:01","Count":20},{"Hour":"2019-12-25 04:25:00","Count":32},{"Hour":"2019-12-25 04:30:00","Count":22},{"Hour":"2019-12-25 04:35:01","Count":28},{"Hour":"2019-12-25 04:40:00","Count":19},{"Hour":"2019-12-25 04:45:00","Count":36},{"Hour":"2019-12-25 04:50:01","Count":41},{"Hour":"2019-12-25 04:55:01","Count":32},{"Hour":"2019-12-25 05:00:00","Count":11},{"Hour":"2019-12-25 05:00:01","Count":6},{"Hour":"2019-12-25 05:05:01","Count":25},{"Hour":"2019-12-25 05:10:00","Count":55},{"Hour":"2019-12-25 05:15:00","Count":17},{"Hour":"2019-12-25 05:20:00","Count":44},{"Hour":"2019-12-25 05:25:00","Count":41},{"Hour":"2019-12-25 05:30:00","Count":14},{"Hour":"2019-12-25 05:30:01","Count":10},{"Hour":"2019-12-25 05:35:00","Count":14},{"Hour":"2019-12-25 05:35:01","Count":13},{"Hour":"2019-12-25 05:40:00","Count":38},{"Hour":"2019-12-25 05:45:00","Count":48},{"Hour":"2019-12-25 05:50:01","Count":27},{"Hour":"2019-12-25 05:55:01","Count":72},{"Hour":"2019-12-25 06:00:01","Count":23},{"Hour":"2019-12-25 06:00:02","Count":3},{"Hour":"2019-12-25 06:05:09","Count":24},{"Hour":"2019-12-25 06:10:11","Count":59},{"Hour":"2019-12-25 06:15:01","Count":33},{"Hour":"2019-12-25 06:20:01","Count":54},{"Hour":"2019-12-25 06:25:00","Count":68},{"Hour":"2019-12-25 06:30:01","Count":14},{"Hour":"2019-12-25 06:30:02","Count":16},{"Hour":"2019-12-25 06:35:02","Count":62},{"Hour":"2019-12-25 06:40:00","Count":34},{"Hour":"2019-12-25 06:45:00","Count":88},{"Hour":"2019-12-25 06:50:00","Count":68},{"Hour":"2019-12-25 06:55:00","Count":43},{"Hour":"2019-12-25 07:00:00","Count":86},{"Hour":"2019-12-25 07:05:01","Count":36},{"Hour":"2019-12-25 07:10:00","Count":64},{"Hour":"2019-12-25 07:15:00","Count":43},{"Hour":"2019-12-25 07:20:01","Count":66},{"Hour":"2019-12-25 07:25:01","Count":91},{"Hour":"2019-12-25 07:30:00","Count":58},{"Hour":"2019-12-25 07:35:01","Count":92},{"Hour":"2019-12-25 07:40:00","Count":93},{"Hour":"2019-12-25 07:40:01","Count":10},{"Hour":"2019-12-25 07:45:00","Count":56},{"Hour":"2019-12-25 07:45:01","Count":9},{"Hour":"2019-12-25 07:50:00","Count":40},{"Hour":"2019-12-25 07:50:01","Count":3},{"Hour":"2019-12-25 07:55:00","Count":79},{"Hour":"2019-12-25 07:55:01","Count":16},{"Hour":"2019-12-25 08:00:00","Count":18},{"Hour":"2019-12-25 08:00:01","Count":37},{"Hour":"2019-12-25 08:05:01","Count":48},{"Hour":"2019-12-25 08:10:00","Count":90},{"Hour":"2019-12-25 08:10:01","Count":5},{"Hour":"2019-12-25 08:15:00","Count":65},{"Hour":"2019-12-25 08:20:00","Count":59},{"Hour":"2019-12-25 08:25:01","Count":70},{"Hour":"2019-12-25 08:30:00","Count":31},{"Hour":"2019-12-25 08:30:01","Count":75},{"Hour":"2019-12-25 08:35:00","Count":62},{"Hour":"2019-12-25 08:35:01","Count":5},{"Hour":"2019-12-25 08:40:00","Count":76},{"Hour":"2019-12-25 08:45:00","Count":89},{"Hour":"2019-12-25 08:50:01","Count":53},{"Hour":"2019-12-25 08:55:00","Count":83},{"Hour":"2019-12-25 09:00:00","Count":85},{"Hour":"2019-12-25 09:05:00","Count":71},{"Hour":"2019-12-25 09:10:00","Count":115},{"Hour":"2019-12-25 09:15:00","Count":113},{"Hour":"2019-12-25 09:20:00","Count":86},{"Hour":"2019-12-25 09:25:00","Count":94},{"Hour":"2019-12-25 09:30:01","Count":100},{"Hour":"2019-12-25 09:35:00","Count":104},{"Hour":"2019-12-25 09:40:00","Count":106},{"Hour":"2019-12-25 09:45:00","Count":77},{"Hour":"2019-12-25 09:50:02","Count":183},{"Hour":"2019-12-25 09:55:01","Count":111},{"Hour":"2019-12-25 09:55:02","Count":3},{"Hour":"2019-12-25 10:00:00","Count":116},{"Hour":"2019-12-25 10:05:00","Count":99},{"Hour":"2019-12-25 10:10:01","Count":96},{"Hour":"2019-12-25 10:15:00","Count":116},{"Hour":"2019-12-25 10:20:01","Count":152},{"Hour":"2019-12-25 10:25:00","Count":77},{"Hour":"2019-12-25 10:30:00","Count":87},{"Hour":"2019-12-25 10:35:00","Count":118},{"Hour":"2019-12-25 10:40:00","Count":121},{"Hour":"2019-12-25 10:45:00","Count":111},{"Hour":"2019-12-25 10:50:00","Count":127},{"Hour":"2019-12-25 10:55:00","Count":92},{"Hour":"2019-12-25 11:00:01","Count":124},{"Hour":"2019-12-25 11:05:00","Count":125},{"Hour":"2019-12-25 11:10:00","Count":165},{"Hour":"2019-12-25 11:15:01","Count":97},{"Hour":"2019-12-25 11:20:01","Count":140},{"Hour":"2019-12-25 11:25:01","Count":93},{"Hour":"2019-12-25 11:30:00","Count":127},{"Hour":"2019-12-25 11:35:01","Count":117},{"Hour":"2019-12-25 11:40:01","Count":113},{"Hour":"2019-12-25 11:45:00","Count":116},{"Hour":"2019-12-25 11:45:01","Count":8},{"Hour":"2019-12-25 11:50:00","Count":81},{"Hour":"2019-12-25 11:50:01","Count":105},{"Hour":"2019-12-25 11:55:02","Count":94},{"Hour":"2019-12-25 12:00:00","Count":156},{"Hour":"2019-12-25 12:05:04","Count":32},{"Hour":"2019-12-25 12:05:06","Count":122},{"Hour":"2019-12-25 12:10:18","Count":139},{"Hour":"2019-12-25 12:10:19","Count":18},{"Hour":"2019-12-25 12:15:05","Count":106},{"Hour":"2019-12-25 12:20:00","Count":316},{"Hour":"2019-12-25 12:25:01","Count":270},{"Hour":"2019-12-25 12:30:00","Count":294},{"Hour":"2019-12-25 12:35:00","Count":269},{"Hour":"2019-12-25 12:40:01","Count":253},{"Hour":"2019-12-25 12:45:00","Count":137},{"Hour":"2019-12-25 12:50:00","Count":129},{"Hour":"2019-12-25 12:55:00","Count":124},{"Hour":"2019-12-25 13:00:00","Count":148},{"Hour":"2019-12-25 13:05:00","Count":134},{"Hour":"2019-12-25 13:10:01","Count":120},{"Hour":"2019-12-25 13:15:00","Count":149},{"Hour":"2019-12-25 13:20:01","Count":132},{"Hour":"2019-12-25 13:25:00","Count":129},{"Hour":"2019-12-25 13:30:00","Count":118},{"Hour":"2019-12-25 13:35:00","Count":87},{"Hour":"2019-12-25 13:40:00","Count":47},{"Hour":"2019-12-25 13:45:00","Count":127},{"Hour":"2019-12-25 13:50:00","Count":120},{"Hour":"2019-12-25 13:55:00","Count":339},{"Hour":"2019-12-25 14:00:00","Count":304},{"Hour":"2019-12-25 14:05:00","Count":403},{"Hour":"2019-12-25 14:10:00","Count":327},{"Hour":"2019-12-25 14:15:00","Count":369},{"Hour":"2019-12-25 14:20:00","Count":466},{"Hour":"2019-12-25 14:25:00","Count":323},{"Hour":"2019-12-25 14:30:00","Count":239},{"Hour":"2019-12-25 14:30:01","Count":99},{"Hour":"2019-12-25 14:35:00","Count":327},{"Hour":"2019-12-25 14:40:00","Count":410},{"Hour":"2019-12-25 14:45:00","Count":324},{"Hour":"2019-12-25 14:50:00","Count":323},{"Hour":"2019-12-25 14:55:00","Count":221},{"Hour":"2019-12-25 14:55:01","Count":86},{"Hour":"2019-12-25 15:00:00","Count":353},{"Hour":"2019-12-25 15:05:00","Count":341},{"Hour":"2019-12-25 15:10:00","Count":390},{"Hour":"2019-12-25 15:15:00","Count":358},{"Hour":"2019-12-25 15:20:00","Count":342},{"Hour":"2019-12-25 15:25:01","Count":340},{"Hour":"2019-12-25 15:25:02","Count":3},{"Hour":"2019-12-25 15:30:00","Count":371},{"Hour":"2019-12-25 15:35:00","Count":332},{"Hour":"2019-12-25 15:40:00","Count":313},{"Hour":"2019-12-25 15:45:00","Count":347},{"Hour":"2019-12-25 15:50:00","Count":359},{"Hour":"2019-12-25 15:55:00","Count":335},{"Hour":"2019-12-25 16:00:00","Count":132},{"Hour":"2019-12-25 16:05:04","Count":106},{"Hour":"2019-12-25 16:05:06","Count":9},{"Hour":"2019-12-25 16:05:08","Count":1},{"Hour":"2019-12-25 16:10:00","Count":148},{"Hour":"2019-12-25 16:15:00","Count":138},{"Hour":"2019-12-25 16:20:00","Count":132},{"Hour":"2019-12-25 16:25:00","Count":104},{"Hour":"2019-12-25 16:30:01","Count":170},{"Hour":"2019-12-25 16:35:00","Count":149},{"Hour":"2019-12-25 16:40:00","Count":101},{"Hour":"2019-12-25 16:45:01","Count":155},{"Hour":"2019-12-25 16:50:00","Count":163},{"Hour":"2019-12-25 16:55:00","Count":150},{"Hour":"2019-12-25 17:00:00","Count":145},{"Hour":"2019-12-25 17:05:01","Count":133},{"Hour":"2019-12-25 17:10:00","Count":133},{"Hour":"2019-12-25 17:15:00","Count":148},{"Hour":"2019-12-25 17:20:00","Count":160},{"Hour":"2019-12-25 17:25:00","Count":144},{"Hour":"2019-12-25 17:30:00","Count":137},{"Hour":"2019-12-25 17:35:00","Count":122},{"Hour":"2019-12-25 17:40:00","Count":155},{"Hour":"2019-12-25 17:45:00","Count":144},{"Hour":"2019-12-25 17:50:00","Count":167},{"Hour":"2019-12-25 17:55:00","Count":82},{"Hour":"2019-12-25 18:00:00","Count":243},{"Hour":"2019-12-25 18:05:03","Count":145},{"Hour":"2019-12-25 18:05:04","Count":3},{"Hour":"2019-12-25 18:05:05","Count":4},{"Hour":"2019-12-25 18:10:00","Count":133},{"Hour":"2019-12-25 18:15:00","Count":137},{"Hour":"2019-12-25 18:20:00","Count":80},{"Hour":"2019-12-25 18:25:00","Count":191},{"Hour":"2019-12-25 18:30:00","Count":198},{"Hour":"2019-12-25 18:35:00","Count":153},{"Hour":"2019-12-25 18:35:01","Count":4},{"Hour":"2019-12-25 18:40:00","Count":158},{"Hour":"2019-12-25 18:45:00","Count":112},{"Hour":"2019-12-25 18:50:00","Count":80},{"Hour":"2019-12-25 18:55:00","Count":152},{"Hour":"2019-12-25 19:00:00","Count":185},{"Hour":"2019-12-25 19:05:00","Count":118},{"Hour":"2019-12-25 19:10:01","Count":137},{"Hour":"2019-12-25 19:15:01","Count":155},{"Hour":"2019-12-25 19:20:00","Count":140},{"Hour":"2019-12-25 19:25:00","Count":154},{"Hour":"2019-12-25 19:30:00","Count":141},{"Hour":"2019-12-25 19:30:01","Count":34},{"Hour":"2019-12-25 19:35:00","Count":158},{"Hour":"2019-12-25 19:40:00","Count":160},{"Hour":"2019-12-25 19:45:01","Count":80},{"Hour":"2019-12-25 19:45:02","Count":78},{"Hour":"2019-12-25 19:50:01","Count":132},{"Hour":"2019-12-25 19:55:00","Count":157},{"Hour":"2019-12-25 20:00:00","Count":142},{"Hour":"2019-12-25 20:05:00","Count":139},{"Hour":"2019-12-25 20:10:00","Count":109},{"Hour":"2019-12-25 20:15:00","Count":130},{"Hour":"2019-12-25 20:20:00","Count":147},{"Hour":"2019-12-25 20:25:00","Count":151},{"Hour":"2019-12-25 20:30:00","Count":173},{"Hour":"2019-12-25 20:35:00","Count":137},{"Hour":"2019-12-25 20:40:00","Count":138},{"Hour":"2019-12-25 20:45:00","Count":126},{"Hour":"2019-12-25 20:50:00","Count":103},{"Hour":"2019-12-25 20:55:00","Count":125},{"Hour":"2019-12-25 21:00:00","Count":113},{"Hour":"2019-12-25 21:05:01","Count":135},{"Hour":"2019-12-25 21:10:00","Count":101},{"Hour":"2019-12-25 21:15:00","Count":47},{"Hour":"2019-12-25 21:15:01","Count":55},{"Hour":"2019-12-25 21:20:01","Count":111},{"Hour":"2019-12-25 21:25:01","Count":124},{"Hour":"2019-12-25 21:30:00","Count":48},{"Hour":"2019-12-25 21:30:01","Count":57},{"Hour":"2019-12-25 21:35:01","Count":92},{"Hour":"2019-12-25 21:40:01","Count":39},{"Hour":"2019-12-25 21:40:02","Count":26},{"Hour":"2019-12-25 21:45:02","Count":76},{"Hour":"2019-12-25 21:45:03","Count":2},{"Hour":"2019-12-25 21:50:02","Count":57},{"Hour":"2019-12-25 21:50:03","Count":1},{"Hour":"2019-12-25 21:55:01","Count":61},{"Hour":"2019-12-25 21:55:02","Count":25},{"Hour":"2019-12-25 22:00:02","Count":88},{"Hour":"2019-12-25 22:05:00","Count":39},{"Hour":"2019-12-25 22:05:01","Count":22},{"Hour":"2019-12-25 22:10:01","Count":56},{"Hour":"2019-12-25 22:15:00","Count":53},{"Hour":"2019-12-25 22:15:01","Count":26},{"Hour":"2019-12-25 22:20:01","Count":54},{"Hour":"2019-12-25 22:25:01","Count":69},{"Hour":"2019-12-25 22:30:01","Count":51},{"Hour":"2019-12-25 22:35:01","Count":65},{"Hour":"2019-12-25 22:40:01","Count":59},{"Hour":"2019-12-25 22:45:00","Count":43},{"Hour":"2019-12-25 22:50:01","Count":85},{"Hour":"2019-12-25 22:55:01","Count":45},{"Hour":"2019-12-25 23:00:01","Count":36},{"Hour":"2019-12-25 23:05:00","Count":45},{"Hour":"2019-12-25 23:10:01","Count":85},{"Hour":"2019-12-25 23:15:01","Count":17},{"Hour":"2019-12-25 23:15:02","Count":3},{"Hour":"2019-12-25 23:20:02","Count":47},{"Hour":"2019-12-25 23:25:01","Count":60},{"Hour":"2019-12-25 23:30:00","Count":10},{"Hour":"2019-12-25 23:30:01","Count":43},{"Hour":"2019-12-25 23:35:01","Count":34},{"Hour":"2019-12-25 23:35:02","Count":3},{"Hour":"2019-12-25 23:40:00","Count":47},{"Hour":"2019-12-25 23:40:01","Count":6},{"Hour":"2019-12-25 23:45:00","Count":24},{"Hour":"2019-12-25 23:50:01","Count":27},{"Hour":"2019-12-25 23:55:01","Count":25}];
				  var ConvertedData=[];
				  
				  for(var i=0;i<fd.length;i++)
				  {
					  var tm =fd[i].Hour;
					  
					   var res = tm.split(" ");
					   var res1 =res[1].split(":");
					   var t1= parseInt(res1[0]);  
					   var t2=parseInt(res1[1])*0.0167; 
					  var intVal=(t1+t2).toFixed(2);
					  
					  tempArr=[intVal,fd[i].Count];
					  
					  
					  ConvertedData.push(tempArr);
					  
				  }
	            ////console.log(ConvertedData);
				  echartScatter.setOption({
    				  tooltip: {
					  trigger: 'axis',
					  showDelay: 0,
					  axisPointer: {
						type: 'cross',
						lineStyle: {
						  type: 'dashed',
						  width: 1
						}
					  }
					},
					 
					toolbox: {
					  show: true,
					  feature: {
						saveAsImage: {
						  show: true,
						  title: "Save Image"
						}
					  }
					},
					xAxis: [{
					  type: 'value',
					  scale: true,
					   min: 0,
	                   max: 24,
					   stepSize: 2,
					  axisLabel: {
						formatter: '{value}:00'
					  }
					}],
					yAxis: [{
					  type: 'value',
					  scale: true,
					  axisLabel: {
						formatter: '{value}'
					  }
					}],
					series: [{
					  
					  type: 'scatter',
					  tooltip: {
						trigger: 'item',
						formatter: function(params) {
							
						 var res = params.value[0].split(".");
						 
					     var t1= parseInt(res[0]);
	                     if(t1<10)
	                     {
	                    t1="0"+t1;
	                     }	
			
					     var t2=parseInt(res[1])*0.6; 
						 var t3 =Math.round(t2);
		                 if(t3<10)
			                  {
	                      t3="0"+t3;
			                   }
						   return  'Hour : ' + t1+':'+t3+' hr <br>' + 'Count : ' +params.value[1] ;
						 
						}
					  },
					  data: ConvertedData,
					  markPoint: {
						data: [{
						  type: 'max',
						  name: 'Max'
						}, {
						  type: 'min',
						  name: 'Min'
						}]
					  },
					  markLine: {
						data: [{
						  type: 'average',
						  name: 'Mean'
						}]
					  }
					} ] 
				  });

				}   
 
			  
			   //echart Bar Horizontal
			  
			if ($('#echart_bar_horizontal').length ){ 
			  
			  var echartBar = echarts.init(document.getElementById('echart_bar_horizontal'), theme);

			  echartBar.setOption({
   				title: {
				  text: 'Bar Graph',
				  subtext: 'Graph subtitle'
				},
				tooltip: {
				  trigger: 'axis'
				},
				legend: {
				  x: 100,
				  data: ['2015', '2016']
				},
				toolbox: {
				  show: true,
				  feature: {
					saveAsImage: {
					  show: true,
					  title: "Save Image"
					}
				  }
				},
				calculable: true,
				xAxis: [{
				  type: 'value',
				  boundaryGap: [0, 0.01]
				}],
				yAxis: [{
				  type: 'category',
				  data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun']
				}],
				series: [{
				  name: '2015',
				  type: 'bar',
				  data: [18203, 23489, 29034, 104970, 131744, 630230]
				}, {
				  name: '2016',
				  type: 'bar',
				  data: [19325, 23438, 31000, 121594, 134141, 681807]
				}]
			  });

			} 
			  
			   //echart Pie Collapse
			  
			if ($('#echart_pie2').length ){ 
			  
			  var echartPieCollapse = echarts.init(document.getElementById('echart_pie2'), theme);
			  
			  echartPieCollapse.setOption({
				tooltip: {
				  trigger: 'item',
				  formatter: "{a} <br/>{b} : {c} ({d}%)"
				},
				legend: {
				  x: 'center',
				  y: 'bottom',
				  data: ['rose1', 'rose2', 'rose3', 'rose4', 'rose5', 'rose6']
				},
				toolbox: {
				  show: true,
				  feature: {
					magicType: {
					  show: true,
					  type: ['pie', 'funnel']
					},
					restore: {
					  show: true,
					  title: "Restore"
					},
					saveAsImage: {
					  show: true,
					  title: "Save Image"
					}
				  }
				},
				calculable: true,
				series: [{
				  name: 'Area Mode',
				  type: 'pie',
				  radius: [25, 90],
				  center: ['50%', 170],
				  roseType: 'area',
				  x: '50%',
				  max: 40,
				  sort: 'ascending',
				  data: [{
					value: 10,
					name: 'rose1'
				  }, {
					value: 5,
					name: 'rose2'
				  }, {
					value: 15,
					name: 'rose3'
				  }, {
					value: 25,
					name: 'rose4'
				  }, {
					value: 20,
					name: 'rose5'
				  }, {
					value: 35,
					name: 'rose6'
				  }]
				}]
			  });

			} 
			  
			   //echart Donut
			  
			if ($('#echart_donut').length ){  
			  
			  var echartDonut = echarts.init(document.getElementById('echart_donut'), theme);
			  
			  echartDonut.setOption({
				tooltip: {
				  trigger: 'item',
				  formatter: "{a} <br/>{b} : {c} ({d}%)"
				},
				calculable: true,
				legend: {
				  x: 'center',
				  y: 'bottom',
				  data: ['Direct Access', 'E-mail Marketing', 'Union Ad', 'Video Ads', 'Search Engine']
				},
				toolbox: {
				  show: true,
				  feature: {
					magicType: {
					  show: true,
					  type: ['pie', 'funnel'],
					  option: {
						funnel: {
						  x: '25%',
						  width: '50%',
						  funnelAlign: 'center',
						  max: 1548
						}
					  }
					},
					restore: {
					  show: true,
					  title: "Restore"
					},
					saveAsImage: {
					  show: true,
					  title: "Save Image"
					}
				  }
				},
				series: [{
				  name: 'Access to the resource',
				  type: 'pie',
				  radius: ['35%', '55%'],
				  itemStyle: {
					normal: {
					  label: {
						show: true
					  },
					  labelLine: {
						show: true
					  }
					},
					emphasis: {
					  label: {
						show: true,
						position: 'center',
						textStyle: {
						  fontSize: '14',
						  fontWeight: 'normal'
						}
					  }
					}
				  },
				  data: [{
					value: 335,
					name: 'Direct Access'
				  }, {
					value: 310,
					name: 'E-mail Marketing'
				  }, {
					value: 234,
					name: 'Union Ad'
				  }, {
					value: 135,
					name: 'Video Ads'
				  }, {
					value: 1548,
					name: 'Search Engine'
				  }]
				}]
			  });

			} 
			  
			   //echart Pie
			  
			if ($('#echart_pie').length ){  
			  
			  var echartPie = echarts.init(document.getElementById('echart_pie'), theme);

			  echartPie.setOption({
				tooltip: {
				  trigger: 'item',
				  formatter: "{a} <br/>{b} : {c} ({d}%)"
				},
				legend: {
				  x: 'center',
				  y: 'bottom',
				  data: ['Direct Access', 'E-mail Marketing', 'Union Ad', 'Video Ads', 'Search Engine']
				},
				toolbox: {
				  show: true,
				  feature: {
					magicType: {
					  show: true,
					  type: ['pie', 'funnel'],
					  option: {
						funnel: {
						  x: '25%',
						  width: '50%',
						  funnelAlign: 'left',
						  max: 1548
						}
					  }
					},
					restore: {
					  show: true,
					  title: "Restore"
					},
					saveAsImage: {
					  show: true,
					  title: "Save Image"
					}
				  }
				},
				calculable: true,
				series: [{
				  name: '访问来源',
				  type: 'pie',
				  radius: '55%',
				  center: ['50%', '48%'],
				  data: [{
					value: 335,
					name: 'Direct Access'
				  }, {
					value: 310,
					name: 'E-mail Marketing'
				  }, {
					value: 234,
					name: 'Union Ad'
				  }, {
					value: 135,
					name: 'Video Ads'
				  }, {
					value: 1548,
					name: 'Search Engine'
				  }]
				}]
			  });

			  var dataStyle = {
				normal: {
				  label: {
					show: false
				  },
				  labelLine: {
					show: false
				  }
				}
			  };

			  var placeHolderStyle = {
				normal: {
				  color: 'rgba(0,0,0,0)',
				  label: {
					show: false
				  },
				  labelLine: {
					show: false
				  }
				},
				emphasis: {
				  color: 'rgba(0,0,0,0)'
				}
			  };

			} 
			  
			   //echart Mini Pie
			  
			if ($('#echart_mini_pie').length ){ 
			  
			  var echartMiniPie = echarts.init(document.getElementById('echart_mini_pie'), theme);

			  echartMiniPie .setOption({
				title: {
				  text: 'Chart #2',
				  subtext: 'From ExcelHome',
				  sublink: 'http://e.weibo.com/1341556070/AhQXtjbqh',
				  x: 'center',
				  y: 'center',
				  itemGap: 20,
				  textStyle: {
					color: 'rgba(30,144,255,0.8)',
					fontFamily: '微软雅黑',
					fontSize: 35,
					fontWeight: 'bolder'
				  }
				},
				tooltip: {
				  show: true,
				  formatter: "{a} <br/>{b} : {c} ({d}%)"
				},
				legend: {
				  orient: 'vertical',
				  x: 170,
				  y: 45,
				  itemGap: 12,
				  data: ['68%Something #1', '29%Something #2', '3%Something #3'],
				},
				toolbox: {
				  show: true,
				  feature: {
					mark: {
					  show: true
					},
					dataView: {
					  show: true,
					  title: "Text View",
					  lang: [
						"Text View",
						"Close",
						"Refresh",
					  ],
					  readOnly: false
					},
					restore: {
					  show: true,
					  title: "Restore"
					},
					saveAsImage: {
					  show: true,
					  title: "Save Image"
					}
				  }
				},
				series: [{
				  name: '1',
				  type: 'pie',
				  clockWise: false,
				  radius: [105, 130],
				  itemStyle: dataStyle,
				  data: [{
					value: 68,
					name: '68%Something #1'
				  }, {
					value: 32,
					name: 'invisible',
					itemStyle: placeHolderStyle
				  }]
				}, {
				  name: '2',
				  type: 'pie',
				  clockWise: false,
				  radius: [80, 105],
				  itemStyle: dataStyle,
				  data: [{
					value: 29,
					name: '29%Something #2'
				  }, {
					value: 71,
					name: 'invisible',
					itemStyle: placeHolderStyle
				  }]
				}, {
				  name: '3',
				  type: 'pie',
				  clockWise: false,
				  radius: [25, 80],
				  itemStyle: dataStyle,
				  data: [{
					value: 3,
					name: '3%Something #3'
				  }, {
					value: 97,
					name: 'invisible',
					itemStyle: placeHolderStyle
				  }]
				}]
			  });

			} 
			  
			   //echart Map
			  
			if ($('#echart_world_map').length ){ 
			  
				  var echartMap = echarts.init(document.getElementById('echart_world_map'), theme);
				  
				   
				  echartMap.setOption({
					title: {
					  text: 'World Population (2010)',
					  subtext: 'from United Nations, Total population, both sexes combined, as of 1 July (thousands)',
					  x: 'center',
					  y: 'top'
					},
					tooltip: {
					  trigger: 'item',
					  formatter: function(params) {
						var value = (params.value + '').split('.');
						value = value[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,') + '.' + value[1];
						return params.seriesName + '<br/>' + params.name + ' : ' + value;
					  }
					},
					toolbox: {
					  show: true,
					  orient: 'vertical',
					  x: 'right',
					  y: 'center',
					  feature: {
						mark: {
						  show: true
						},
						dataView: {
						  show: true,
						  title: "Text View",
						  lang: [
							"Text View",
							"Close",
							"Refresh",
						  ],
						  readOnly: false
						},
						restore: {
						  show: true,
						  title: "Restore"
						},
						saveAsImage: {
						  show: true,
						  title: "Save Image"
						}
					  }
					},
					dataRange: {
					  min: 0,
					  max: 1000000,
					  text: ['High', 'Low'],
					  realtime: false,
					  calculable: true,
					  color: ['#087E65', '#26B99A', '#CBEAE3']
					},
					series: [{
					  name: 'World Population (2010)',
					  type: 'map',
					  mapType: 'world',
					  roam: false,
					  mapLocation: {
						y: 60
					  },
					  itemStyle: {
						emphasis: {
						  label: {
							show: true
						  }
						}
					  },
					  data: [{
						name: 'Afghanistan',
						value: 28397.812
					  }, {
						name: 'Angola',
						value: 19549.124
					  }, {
						name: 'Albania',
						value: 3150.143
					  }, {
						name: 'United Arab Emirates',
						value: 8441.537
					  }, {
						name: 'Argentina',
						value: 40374.224
					  }, {
						name: 'Armenia',
						value: 2963.496
					  }, {
						name: 'French Southern and Antarctic Lands',
						value: 268.065
					  }, {
						name: 'Australia',
						value: 22404.488
					  }, {
						name: 'Austria',
						value: 8401.924
					  }, {
						name: 'Azerbaijan',
						value: 9094.718
					  }, {
						name: 'Burundi',
						value: 9232.753
					  }, {
						name: 'Belgium',
						value: 10941.288
					  }, {
						name: 'Benin',
						value: 9509.798
					  }, {
						name: 'Burkina Faso',
						value: 15540.284
					  }, {
						name: 'Bangladesh',
						value: 151125.475
					  }, {
						name: 'Bulgaria',
						value: 7389.175
					  }, {
						name: 'The Bahamas',
						value: 66402.316
					  }, {
						name: 'Bosnia and Herzegovina',
						value: 3845.929
					  }, {
						name: 'Belarus',
						value: 9491.07
					  }, {
						name: 'Belize',
						value: 308.595
					  }, {
						name: 'Bermuda',
						value: 64.951
					  }, {
						name: 'Bolivia',
						value: 716.939
					  }, {
						name: 'Brazil',
						value: 195210.154
					  }, {
						name: 'Brunei',
						value: 27.223
					  }, {
						name: 'Bhutan',
						value: 716.939
					  }, {
						name: 'Botswana',
						value: 1969.341
					  }, {
						name: 'Central African Republic',
						value: 4349.921
					  }, {
						name: 'Canada',
						value: 34126.24
					  }, {
						name: 'Switzerland',
						value: 7830.534
					  }, {
						name: 'Chile',
						value: 17150.76
					  }, {
						name: 'China',
						value: 1359821.465
					  }, {
						name: 'Ivory Coast',
						value: 60508.978
					  }, {
						name: 'Cameroon',
						value: 20624.343
					  }, {
						name: 'Democratic Republic of the Congo',
						value: 62191.161
					  }, {
						name: 'Republic of the Congo',
						value: 3573.024
					  }, {
						name: 'Colombia',
						value: 46444.798
					  }, {
						name: 'Costa Rica',
						value: 4669.685
					  }, {
						name: 'Cuba',
						value: 11281.768
					  }, {
						name: 'Northern Cyprus',
						value: 1.468
					  }, {
						name: 'Cyprus',
						value: 1103.685
					  }, {
						name: 'Czech Republic',
						value: 10553.701
					  }, {
						name: 'Germany',
						value: 83017.404
					  }, {
						name: 'Djibouti',
						value: 834.036
					  }, {
						name: 'Denmark',
						value: 5550.959
					  }, {
						name: 'Dominican Republic',
						value: 10016.797
					  }, {
						name: 'Algeria',
						value: 37062.82
					  }, {
						name: 'Ecuador',
						value: 15001.072
					  }, {
						name: 'Egypt',
						value: 78075.705
					  }, {
						name: 'Eritrea',
						value: 5741.159
					  }, {
						name: 'Spain',
						value: 46182.038
					  }, {
						name: 'Estonia',
						value: 1298.533
					  }, {
						name: 'Ethiopia',
						value: 87095.281
					  }, {
						name: 'Finland',
						value: 5367.693
					  }, {
						name: 'Fiji',
						value: 860.559
					  }, {
						name: 'Falkland Islands',
						value: 49.581
					  }, {
						name: 'France',
						value: 63230.866
					  }, {
						name: 'Gabon',
						value: 1556.222
					  }, {
						name: 'United Kingdom',
						value: 62066.35
					  }, {
						name: 'Georgia',
						value: 4388.674
					  }, {
						name: 'Ghana',
						value: 24262.901
					  }, {
						name: 'Guinea',
						value: 10876.033
					  }, {
						name: 'Gambia',
						value: 1680.64
					  }, {
						name: 'Guinea Bissau',
						value: 10876.033
					  }, {
						name: 'Equatorial Guinea',
						value: 696.167
					  }, {
						name: 'Greece',
						value: 11109.999
					  }, {
						name: 'Greenland',
						value: 56.546
					  }, {
						name: 'Guatemala',
						value: 14341.576
					  }, {
						name: 'French Guiana',
						value: 231.169
					  }, {
						name: 'Guyana',
						value: 786.126
					  }, {
						name: 'Honduras',
						value: 7621.204
					  }, {
						name: 'Croatia',
						value: 4338.027
					  }, {
						name: 'Haiti',
						value: 9896.4
					  }, {
						name: 'Hungary',
						value: 10014.633
					  }, {
						name: 'Indonesia',
						value: 240676.485
					  }, {
						name: 'India',
						value: 1205624.648
					  }, {
						name: 'Ireland',
						value: 4467.561
					  }, {
						name: 'Iran',
						value: 240676.485
					  }, {
						name: 'Iraq',
						value: 30962.38
					  }, {
						name: 'Iceland',
						value: 318.042
					  }, {
						name: 'Israel',
						value: 7420.368
					  }, {
						name: 'Italy',
						value: 60508.978
					  }, {
						name: 'Jamaica',
						value: 2741.485
					  }, {
						name: 'Jordan',
						value: 6454.554
					  }, {
						name: 'Japan',
						value: 127352.833
					  }, {
						name: 'Kazakhstan',
						value: 15921.127
					  }, {
						name: 'Kenya',
						value: 40909.194
					  }, {
						name: 'Kyrgyzstan',
						value: 5334.223
					  }, {
						name: 'Cambodia',
						value: 14364.931
					  }, {
						name: 'South Korea',
						value: 51452.352
					  }, {
						name: 'Kosovo',
						value: 97.743
					  }, {
						name: 'Kuwait',
						value: 2991.58
					  }, {
						name: 'Laos',
						value: 6395.713
					  }, {
						name: 'Lebanon',
						value: 4341.092
					  }, {
						name: 'Liberia',
						value: 3957.99
					  }, {
						name: 'Libya',
						value: 6040.612
					  }, {
						name: 'Sri Lanka',
						value: 20758.779
					  }, {
						name: 'Lesotho',
						value: 2008.921
					  }, {
						name: 'Lithuania',
						value: 3068.457
					  }, {
						name: 'Luxembourg',
						value: 507.885
					  }, {
						name: 'Latvia',
						value: 2090.519
					  }, {
						name: 'Morocco',
						value: 31642.36
					  }, {
						name: 'Moldova',
						value: 103.619
					  }, {
						name: 'Madagascar',
						value: 21079.532
					  }, {
						name: 'Mexico',
						value: 117886.404
					  }, {
						name: 'Macedonia',
						value: 507.885
					  }, {
						name: 'Mali',
						value: 13985.961
					  }, {
						name: 'Myanmar',
						value: 51931.231
					  }, {
						name: 'Montenegro',
						value: 620.078
					  }, {
						name: 'Mongolia',
						value: 2712.738
					  }, {
						name: 'Mozambique',
						value: 23967.265
					  }, {
						name: 'Mauritania',
						value: 3609.42
					  }, {
						name: 'Malawi',
						value: 15013.694
					  }, {
						name: 'Malaysia',
						value: 28275.835
					  }, {
						name: 'Namibia',
						value: 2178.967
					  }, {
						name: 'New Caledonia',
						value: 246.379
					  }, {
						name: 'Niger',
						value: 15893.746
					  }, {
						name: 'Nigeria',
						value: 159707.78
					  }, {
						name: 'Nicaragua',
						value: 5822.209
					  }, {
						name: 'Netherlands',
						value: 16615.243
					  }, {
						name: 'Norway',
						value: 4891.251
					  }, {
						name: 'Nepal',
						value: 26846.016
					  }, {
						name: 'New Zealand',
						value: 4368.136
					  }, {
						name: 'Oman',
						value: 2802.768
					  }, {
						name: 'Pakistan',
						value: 173149.306
					  }, {
						name: 'Panama',
						value: 3678.128
					  }, {
						name: 'Peru',
						value: 29262.83
					  }, {
						name: 'Philippines',
						value: 93444.322
					  }, {
						name: 'Papua New Guinea',
						value: 6858.945
					  }, {
						name: 'Poland',
						value: 38198.754
					  }, {
						name: 'Puerto Rico',
						value: 3709.671
					  }, {
						name: 'North Korea',
						value: 1.468
					  }, {
						name: 'Portugal',
						value: 10589.792
					  }, {
						name: 'Paraguay',
						value: 6459.721
					  }, {
						name: 'Qatar',
						value: 1749.713
					  }, {
						name: 'Romania',
						value: 21861.476
					  }, {
						name: 'Russia',
						value: 21861.476
					  }, {
						name: 'Rwanda',
						value: 10836.732
					  }, {
						name: 'Western Sahara',
						value: 514.648
					  }, {
						name: 'Saudi Arabia',
						value: 27258.387
					  }, {
						name: 'Sudan',
						value: 35652.002
					  }, {
						name: 'South Sudan',
						value: 9940.929
					  }, {
						name: 'Senegal',
						value: 12950.564
					  }, {
						name: 'Solomon Islands',
						value: 526.447
					  }, {
						name: 'Sierra Leone',
						value: 5751.976
					  }, {
						name: 'El Salvador',
						value: 6218.195
					  }, {
						name: 'Somaliland',
						value: 9636.173
					  }, {
						name: 'Somalia',
						value: 9636.173
					  }, {
						name: 'Republic of Serbia',
						value: 3573.024
					  }, {
						name: 'Suriname',
						value: 524.96
					  }, {
						name: 'Slovakia',
						value: 5433.437
					  }, {
						name: 'Slovenia',
						value: 2054.232
					  }, {
						name: 'Sweden',
						value: 9382.297
					  }, {
						name: 'Swaziland',
						value: 1193.148
					  }, {
						name: 'Syria',
						value: 7830.534
					  }, {
						name: 'Chad',
						value: 11720.781
					  }, {
						name: 'Togo',
						value: 6306.014
					  }, {
						name: 'Thailand',
						value: 66402.316
					  }, {
						name: 'Tajikistan',
						value: 7627.326
					  }, {
						name: 'Turkmenistan',
						value: 5041.995
					  }, {
						name: 'East Timor',
						value: 10016.797
					  }, {
						name: 'Trinidad and Tobago',
						value: 1328.095
					  }, {
						name: 'Tunisia',
						value: 10631.83
					  }, {
						name: 'Turkey',
						value: 72137.546
					  }, {
						name: 'United Republic of Tanzania',
						value: 44973.33
					  }, {
						name: 'Uganda',
						value: 33987.213
					  }, {
						name: 'Ukraine',
						value: 46050.22
					  }, {
						name: 'Uruguay',
						value: 3371.982
					  }, {
						name: 'United States of America',
						value: 312247.116
					  }, {
						name: 'Uzbekistan',
						value: 27769.27
					  }, {
						name: 'Venezuela',
						value: 236.299
					  }, {
						name: 'Vietnam',
						value: 89047.397
					  }, {
						name: 'Vanuatu',
						value: 236.299
					  }, {
						name: 'West Bank',
						value: 13.565
					  }, {
						name: 'Yemen',
						value: 22763.008
					  }, {
						name: 'South Africa',
						value: 51452.352
					  }, {
						name: 'Zambia',
						value: 13216.985
					  }, {
						name: 'Zimbabwe',
						value: 13076.978
					  }]
					}]
				  });
	   
			}
	   
		}  
	   
	   
	$(document).ready(function() {
				
		init_sparklines();
		init_flot_chart();
		init_sidebar();
		init_wysiwyg();
		init_InputMask();
		init_JQVmap();
		init_cropper();
		init_knob();
		init_IonRangeSlider();
		init_ColorPicker();
		init_TagsInput();
		init_parsley();
		init_daterangepicker();
		init_daterangepicker_right();
		init_daterangepicker_single_call();
		init_daterangepicker_reservation();
		init_SmartWizard();
		init_EasyPieChart();
		init_charts();
		//init_echarts();
		//init_morris_charts();
		init_skycons();
		init_select2();
		init_validator();
		init_DataTables();
		//init_chart_doughnut();
		init_gauge();
		init_PNotify();
		init_starrr();
		init_calendar();
		init_compose();
		init_CustomNotification();
		init_autosize();
		init_autocomplete();
				
	});	
	

