


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="com.traffsys.parkingsolution.utils.*"%>
   
<!DOCTYPE html>
<html ng-app="app">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="${pageContext.request.contextPath}/resources/logindemo/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/css/util.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/css/main.css">
<!--===============================================================================================-->

 
          <!---------------------------------- API URL ------------------------------------------------------>
         <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script>
         
	<script src="${pageContext.request.contextPath}/resources/aes.js"></script>
	   	<script src="${pageContext.request.contextPath}/resources/secure/secureLogin.js"></script>
	
   	<script src="${pageContext.request.contextPath}/resources/secureLogin.js"></script>
   	   	<script src="${pageContext.request.contextPath}/resources/pbkdf2.js"></script>
  
 <link rel="stylesheet" type="text/css"
	href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
	

 <script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>
 
 <style>
  .overlay {
	position: absolute;
	top: 0;
	left: 0;
	height: inherit;
	width: 100%;
	background-color: rgba(0, 0, 0, 0.8);
	z-index: 10;
}

#ico {
	font-size: 56px;
	color: white;
	position: fixed;
	top: 50%;
	left: 50%;
	width: 300px;
	line-height: 200px;
	height: 200px;
	margin-left: -150px;
	margin-top: -100px;
	background-color: rgba(0, 0, 0, 0.5);
	text-align: center;
	z-index: 10;
	outline: 9999px solid rgba(0, 0, 0, 0.5);
} 
.errormsg
{
    /* font-family: Raleway-Black; */
    font-family: inherit;
    font-size: 16px;
    color: red;
    line-height: 1.2;
    padding-bottom: 5px;
    /* text-transform: uppercase; */
    text-align: center;
    width: 100%;
    display: block;
    }

</style>
 
<script>
var captchaEnable='${captchaEnable}';

	var app = angular.module("app", []);
	app.controller("postDataController", [ '$scope', '$http', '$window',
			function(scope, http, window) {
		
		 document.getElementById('overl').style.visibility = "hidden";
		 
		 var pushLogin=false;

		 if(captchaEnable=='true' )
 		{
 		 
 		 document.getElementById('captchaBox').style.display = 'block';
 		 
 		}else
 			{
 	 
 			 document.getElementById('captchaBox').style.display = 'none';
 		 
 	    }

		  	 /////////////////////////////PASSWORD VARIFICATION///////////////////////////////////////
	  	 
					$(document).ready(function() {
							$('#user_password, #confirm_password').on('keyup', function () {
							
								 var userpass=  $('#user_password').val();
							 var confirm_pass =  $('#confirm_password').val();
							 
							if(confirm_pass!="")
								{
								 if (userpass != confirm_pass) {
									 $('#message').html('Password is not Matching').css('color', 'red');
 									  } else {
 										  
 										  console.log("confirm_pass",confirm_pass);
 										 $('#message').html('Password match').css('color', '#e6bc22');
 									  }
									}else{
									 $('#message').html('Password is not Matching').css('color', 'red');
								}
							});
							
		 
				scope.submitForm = function() {
					scope.submitted = true;
					 var cpass =  $('#confirm_password').val();
						 
					   
	  /////////////////MODAL VALIDATION FOR NULL VALUE/////////// 
				 
	  			if(typeof scope.resetmodel  !== 'undefined'){
	  				
	  		
	  			
	  			if(scope.resetmodel.hasOwnProperty("user_password")){
	    	  		   scope.user_password = scope.resetmodel.user_password;
	    	  	   }else{
	    	  		   scope.user_password ="";
	    	  	   }
	  			
	  			if(scope.resetmodel.hasOwnProperty("email_address")){
	    	  		   scope.email_address = scope.resetmodel.email_address;
	    	  	   }else{
	    	  		   scope.email_address ="";
	    	  	   }
	  			
	  			if(scope.resetmodel.hasOwnProperty("username")){
	    	  		   scope.username = scope.resetmodel.username;
	    	  	   }else{
	    	  		   scope.username ="";
	    	  	   }
	  			
	  			

	 			/*Aes En*/
				/*  var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
		          var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);

		          var aesUtil = new AesUtil(128, 1000);
		          var ciphertext = aesUtil.encrypt(salt, iv, _key, scope.user_password);
		          var usern = aesUtil.encrypt(salt, iv, _key, email_address);

		          var aesPassword = (iv + "::" + salt + "::" + ciphertext);
		          var userName = (iv + "::" + salt + "::" + usern);

		          var password = btoa(aesPassword);
		          var userName = btoa(userName);
	 			 */
	  			
	  			/* scope.userData = {
		         			
		               		 "password":password,
		               		 "emailId":userName 
		               		
		                		
		         	  }; */
					

	  			/*  scope.requestedData = {
	  							"functionName" : "doResetPassword",
	  				  			 "data" : JSON.stringify({"username": scope.username,"password" : scope.user_password, "emailId":scope.email_address,"confirm_password":scope.confirm_password})
	  					}; */
	  					
		         	 scope.requestedData = {
	  							"functionName" : "doResetPassword",
	  				  			 "data" : JSON.stringify({"username": scope.username,"password" : crypto(scope.user_password), "emailId":scope.email_address,"confirm_password":crypto(cpass)})

	  					};
		         	 
	  				 
					
					 console.log("resetData:",scope.requestedData);

					
					if(scope.user_password!= "" && scope.email_address!= "" &&  scope.user_password == cpass && cpass!= "") {
					
						if(captchaEnable=='true')
				 		{	
				 		if(ValidCaptcha())
			   			{
				 			
				 			document.getElementById('overl').style.visibility = "visible";
				     		  
				    		   http({
								method :'POST',
								url :"${pageContext.servletContext.contextPath}/Reset",
								data :scope.requestedData
								}).then(function (response) {
									if(response.data.status==1){
										scope.response=response;
										//scope.response_message= response.data.message;

										 var x = document.getElementById("snackbar_success")
						    			    x.className = "show";
						    			    setTimeout(function() {
						    				 x.className = x.className.replace("show", "");
						    			    }, 3000);
					    			   setTimeout(loginredirect, 3500);
	 								}else if(response.data.status==0)
									{
										scope.errormsg = response.data.message;
				 						document.getElementById('overl').style.visibility = "hidden";
				 				 
									}else{
										
										scope.errormsg = response.data.message;
										document.getElementById('overl').style.visibility = "hidden";
									}
										
				  					      
								}, function (response) {
								});
			   			}
				 		else
				 			{
				 			
				 			scope.message="Enter Valid Captcha";
							  
							var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
					   		DrawCaptcha();
				 			}
				 		}
						else
							{
							document.getElementById('overl').style.visibility = "visible";
				     		  
				    		   http({
								method :'POST',
								url :"${pageContext.servletContext.contextPath}/Reset",
								data :scope.requestedData
								}).then(function (response) {
									if(response.data.status==1){
										scope.response=response;

										 var x = document.getElementById("snackbar_success")
						    			    x.className = "show";
						    			    setTimeout(function() {
						    				 x.className = x.className.replace("show", "");
						    			    }, 3000);
					    			   setTimeout(loginredirect, 3500);	
					    			  }else if(response.data.status==0)
									{
										scope.errormsg = response.data.message;
				 						document.getElementById('overl').style.visibility = "hidden";
				 				 
									}else{
										
										scope.errormsg = response.data.message;
										document.getElementById('overl').style.visibility = "hidden";
									}
										
				  					      
								}, function (response) {
								});
							}
			     		  
					 } else  {
		    				
	    				  scope.message = "Above Fields cannot be Left Blank";
	    					var x = document.getElementById("snackbar_error")
	    					x.className = "show";
	    					setTimeout(function() {
	    						x.className = x.className.replace("show", "");
	    					}, 3000);
	    					DrawCaptcha();
	    				 }	
						
						
		  			}//end modal validation
		  			else{
		  			 scope.message = "Above Fields cannot be Left Blank";
   					var x = document.getElementById("snackbar_error")
   					x.className = "show";
   					setTimeout(function() {
   						x.className = x.className.replace("show", "");
   					}, 3000);
   					DrawCaptcha();
		  			}
						  
					
					  
					 

				};

	});
					function loginredirect()
					   {
					 	   window.location.href = '${pageContext.servletContext.contextPath}/signin';
					   }
			}]);
</script>

</head>
<body ng-controller="postDataController" onload=" DrawCaptcha();">
	<div class="limiter">
	
		<div class="container-login100" >
		  <!-- spinner div -->
		 <div class="overlay" id="overl">
			<div id="ico">
				<i class="fa fa-spinner fa-spin"></i>
			</div>
		</div>
		
		
			<div class="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-77">
				<form class="login100-form validate-form" name="add_user_form" novalidate autocomplete="off">
					<span class="login100-form-title p-b-55">
						Reset Password 
					</span>
					  <span  class="errormsg">{{errormsg}}</span>


                    <div class="wrap-input100 validate-input m-b-16" ng-class="{true: 'error'}[submitted && add_user_form.username.$invalid]" >
						<input class="input100" type="text" id="username" 
													placeholder="Enter Username " name="username"  
													ng-model="resetmodel.username" required="required" >
						<span class="focus-input100"></span>
						
						 <span style="color:red" ng-show="(add_user_form.email_address.$dirty || submitted) && add_user_form.email_address.$error.required"> User Name   is required  !</span> 
						
						<!-- <span class="symbol-input100">
							<span class="lnr lnr-envelope"></span>
						</span> -->
					</div>
					
					
					
					<div class="wrap-input100 validate-input m-b-16" ng-class="{true: 'error'}[submitted && add_user_form.email_address.$invalid]" >
						<input class="input100" type="text" id="email_address"  ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"
													placeholder="Enter Email Address" name="email_address"  
													ng-model="resetmodel.email_address" required="required" >
						<span class="focus-input100"></span>
						
						 <span style="color:red" ng-show="(add_user_form.email_address.$dirty || submitted) && add_user_form.email_address.$error.required"> Email   is required  !</span> 
                         <span style="color:Red" ng-show="add_user_form.email_address.$dirty && add_user_form.email_address.$error.pattern">Please Enter Valid Email</span>
						
						<!-- <span class="symbol-input100">
							<span class="lnr lnr-envelope"></span>
						</span> -->
					</div>

					<div class="wrap-input100 validate-input m-b-16" >
						<input class="input100" type="password" 	id="user_password" name="user_password" required="required" placeholder="Enter New Password"  ng-minlength="8" ng-maxlength="20"
													ng-model="resetmodel.user_password" ng-pattern="/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/">
					   <span class="focus-input100"></span>
						 <span style="color:red" ng-show="(add_user_form.user_password.$dirty || submitted) && add_user_form.user_password.$error.required">	Password   is required ! </span>
						<span style="color: red" ng-show="(add_user_form.user_password.$dirty || submitted) && add_user_form.user_password.$error.minlength">Minimum 8 character required!</span> 
  						<div ng-show="add_user_form.user_password.$error.pattern && add_user_form.user_password.$dirty" style="color: red;">Password must have a minimum length of 8 characters, consist of alphanumeric, special characters, and contain both lowercase and uppercase characters.</div>
  								
						<!-- <span class="symbol-input100">
							<span class="lnr lnr-lock"></span>
						</span> -->
					</div>
					
					<div class="wrap-input100 validate-input m-b-12" >
						<input class="input100" type="password"id="confirm_password" name="confirm_password" placeholder="Confirm Password" required="required"
													ng-model="resetmodel.confirm_password">
						<span class="focus-input100"></span>
						<span id='message'></span>
						<!-- <span class="symbol-input100">
							<span class="lnr lnr-lock"></span>
						</span> -->
					</div>

                     
                     <div id="captchaBox">
						<div >
                         <div class="wrap-input100 validate-input m-b-10 p-t-5" >	
                         					 <input type="text" id="txtCaptcha"  style=" height: 35px;border-radius: 3px;padding: 0 30px 0 20px; text-align:center; border:none; background-color:#363746; color:#fff; font-size:large; font-weight:bold; font-family:cursive;" disabled/>
					 
                       </div>						
                       </div>
						<div class="row m-b-25">
							  <div class="col-sm-1 col-sm-1 col-md-1 col-xs-1">
							  </div>
							  <div class="col-sm-8 col-sm-8 col-md-8 col-xs-8">
							  <input type="text" class="form-control" placeholder="Enter above captcha" style=" text-align:center;  font-family:sans-serif; border-radius:5px;" id="txtInput"/> 
							  
							  </div>
							  <div class="col-sm-2 col-sm-2 col-md-2 col-xs-2">
							  <button  class="btn btn-primary" id="btnrefresh" onclick="DrawCaptcha();"><i class="fa fa-refresh"></i></button>
							  </div>
							  <div class="col-sm-1 col-sm-1 col-md-1 col-xs-1">
							  </div>
						
						</div>
						</div>
						 <br>
                     
					<div class="container-login100-form-btn">
						<button id="resetbtn" class="login100-form-btn" ng-click="submitForm()">
							Reset
						</button>
					</div>

		
				</form>
			</div>
		</div>
	</div>
		<div id="snackbar_success">{{response.data.message}}</div>
	 <div id="snackbar_error">{{message}}</div> 
 
<!--===============================================================================================-->	
	<script src="${pageContext.request.contextPath}/resources/logindemo/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="${pageContext.request.contextPath}/resources/logindemo/vendor/bootstrap/js/popper.js"></script>
	<script src="${pageContext.request.contextPath}/resources/logindemo/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="${pageContext.request.contextPath}/resources/logindemo/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="${pageContext.request.contextPath}/resources/logindemo/js/main.js"></script>
	
	<script type="text/javascript">
	
    function DrawCaptcha()
    {
        var a = Math.ceil(Math.random() * 10)+ '';
        var b = Math.ceil(Math.random() * 10)+ '';       
        var c = Math.ceil(Math.random() * 10)+ '';  
        var d = Math.ceil(Math.random() * 10)+ '';  
        var e = Math.ceil(Math.random() * 10)+ '';  
        var f = Math.ceil(Math.random() * 10)+ '';  
        var g = Math.ceil(Math.random() * 10)+ '';  
        var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' '+ f + ' ' + g;
        document.getElementById("txtCaptcha").value = code
    }

    // Validate the Entered input aganist the generated security code function   
    function ValidCaptcha(){
        var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
        var str2 = removeSpaces(document.getElementById('txtInput').value);
        if (str1 == str2) 
    	{
    	return true;
    	}else
    		{
    		document.getElementById('txtInput').value="";
    		 return false;
    		}
        
    }
    // Remove the spaces from the entered and generated code
    function removeSpaces(string)
    {
        return string.split(' ').join('');
    }
	</script>
</body>
</html>



