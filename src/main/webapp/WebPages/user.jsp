<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
<meta charset="ISO-8859-1">
<title>User Information | VPark</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="Sing App - Bootstrap 4 Admin Dashboard Template">
        <meta name="keywords" content="bootstrap admin template,admin template,admin dashboard,admin dashboard template,admin,dashboard,bootstrap,template">
        <meta name="author" content="Flatlogic LLC">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
     
	    <!-----------------------------------------MAIN CSS----------------------------------------------------->
        <link href="${pageContext.request.contextPath}/resources/css/application.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png">
        <link rel="shortcut icon" type="image/x-icon"  href="${pageContext.request.contextPath}/resources/img/logo.png">
        
        <!-----------------------------------------------FONT STYLE--------------------------------------------->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/line-awesome/dist/line-awesome/css/line-awesome.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
		
		<!----------------------------------------------PROGRESSBAR--------------------------------------------------------->
		<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
		
		<!----------------------------------------------JQUERY------------------------------------------------------>
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script> 
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery/dist/jquery.min.js"></script> 
 
        <!-----------------------------------------DATATABLE------------------------------------------------------>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>
        <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        
        <!-----------------------------------------------ANGUARJS----------------------------------------------->
        <script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>
	
        <!---------------------------------- API URL ------------------------------------------------------>
        <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script> 
	
        <!-- Datatables css-->
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"rel="stylesheet">
	    <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
	    rel="stylesheet">
	    
	    <!-- Datatables js-->
        <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	    
	    <!-- date time picker -->
	    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
	    
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/webmain.css">
     	  <!---------------------------------- SweetAlert  ------------------------------------------------------>
          <script src="${pageContext.request.contextPath}/resources/sweetAlert.js"></script> 
	     
 	   
	 
	  <script>
	  
	  $(document).ready(function(){

          $("#confi").addClass("active");
          $("#confi").addClass("open");
          $("#sidebar-ui").addClass("show");
          $("#kallUser").addClass("active");

  		 });
     
		
		/**********************************SESSION************************************************* */
		var uid = '<%=session.getAttribute("uid")%>';
		var userN = '<%=session.getAttribute("user")%>';
		
		  var options = {
	    		   headers: {
	    		     Authorization: 'Bearer ' + '${jwtToken}' // Assuming 'jwtToken' holds the JWT
	    		   }
	    		 };
	      		
		
		var jDataTable;
		var jData;
		var app = angular.module("app", []);
		app.controller("GetDataController", ['$scope','$http','$window', function(scope,http,window) {
			
			document.getElementById('overlid').style.visibility = "hidden";
				
			
			scope.requestData =
		    {
				"data" : "",
				"username" : uid,
				"basicAuth" : ""
		     };
			
		
			 
			   /*******************************GET ROLE****************************************/
		           http.get(apiURL+'getRole/'+userN+'/doGetRole',options).
		   			then(function (response) {
		   			if(response.data != null)
		   				{
		   				if(response.data.status==1){
		   			              
		   					scope.getrole= response.data.myObjectList;
		   				 }else if(response.data.status==21){
		   		  				 
			  		        }else{
		   					scope.getrole= "";
		   				 }  
		   		   }else{
		   		     	
		   		      }      		       		
		   			}, function (response) {
		   			});
		           
			 
			//******************************USER DATA*****************************//
			   scope.GetUser = function () { 
					  
				   
					  http.get(apiURL+'getUser/'+userN+'/doGetUser',options).
						then(function (response) {
							if(response.data != null)
							{
							if(response.data.status==1){
								
								        Pace.stop();
								        document.getElementById('overl').style.visibility = "hidden";
								    	jData = [];
					             		jData= response.data.myObjectList;
					             		jDataTable.clear().rows.add(jData).draw();
					             		
					         }else if(response.data.status==21){
			   		  				 
				  		        }else{
					        	 jData = [];
					       	 jDataTable.clear().rows.add(jData).draw();
					               }  
					   }else{
					     	 jData = [];
					   	 jDataTable.clear().rows.add(jData).draw();
					      }      
							//document.getElementById('overl').style.visibility = "hidden";
						}, function (response) {
						});
	     	};  	
		
		
		
	     	/****************************USER DATATABLE*************************************/
		   $(document).ready(function() {
		   	 jDataTable = $('#users').DataTable({ 
		     		 	data: jData,
			         		columns: [
			         			{'data':'userId'},
			             	    {'data':'username', 'render': function(username){
		 	             		
		 	             		if(username!=""){
		 	             			
		 	             			return username;
		 	             		}else{
		 	             			
		 	             			return '</p>__</p>';
		 	             		}
		 	             	}},
		 	             	{'data':'firstName', 'render': function(firstName){
		 	             		
		 	             		if(firstName!=""){
		 	             			
		 	             			return firstName;
		 	             		}else{
		 	             			
		 	             			return '</p>__</p>';
		 	             		}
		 	             	}},
		 	             	{'data':'middleName', 'render': function(middleName){
		 	             		
		 	             		if(middleName!= null)
		 	             		{
		 	             		if(middleName!=""){
		 	             			
		 	             			return middleName;
		 	             		}else{
		 	             			
		 	             			return '<small>__</small>';
		 	             		}
		 	             		}else{
		 	             			return '<small>__</small>';
		 	             		}
		 	             	}},
		 	             	{'data':'lastName', 'render': function(lastName){
		 	             		
		 	             		if(lastName!= null)
		 	             		{
		 	             		if(lastName!=""){
		 	             			
		 	             			return lastName;
		 	             		}else{
		 	             			
		 	             			return '<small>__</small>';
		 	             		}
		 	             		}else{
		 	             			return '<small>__</small>';
		 	             		}
		 	             	}},
		 	             	{'data':'emailId', 'render': function(emailId){
		 	             		
		 	             		if(emailId!= null)
		 	             		{
		 	             		if(emailId!=""){
		 	             			
		 	             			return emailId;
		 	             		}else{
		 	             			
		 	             			return '<small>__</small>';
		 	             		}
		 	             		}else{
		 	             			return '<small>__</small>';
		 	             		}
		 	             	}},
		 	             	{'data':'contactNo', 'render': function(contactNo){
		 	             		
		 	             		if(contactNo!= null)
		 	             		{
		 	             		if(contactNo!=""){
		 	             			
		 	             			return contactNo;
		 	             		}else{
		 	             			
		 	             			return '<small>__</small>';
		 	             		}
		 	             		}else{
		 	             			return '<small>__</small>';
		 	             		}
		 	             	}},
		 	             	{'data':'dateOfBirth', 'render': function(dateOfBirth){
		 	             		
		 	             		if(dateOfBirth!= null)
		 	             		{
		 	             		if(dateOfBirth!=""){
		 	             			
		 	             			return dateOfBirth;
		 	             		}else{
		 	             			
		 	             			
		 	             			return '<small>__</small>';
		 	             		}
		 	             		}else{
		 	             			return '<small>__</small>';
		 	             		}
		 	             	}},
		 	             	{'data':'mUserRole', 'render': function(mUserRole){
		 	             		
		 	             		if(mUserRole!= null)
		 	             		{
		 	             		if(mUserRole!=""){
		 	             			
		 	             			return mUserRole.role;
		 	             		}else{
		 	             			
		 	             			return '<small>__</small>';
		 	             		}
		 	             		}else{
		 	             			return '<small>__</small>';
		 	             		}
		 	             		 
		 	             	}},
		 	             	 {'data': "userId", 'render':function(userId){
			 	         				
			 	         				if(userId!=null){
			 	         						return '<u id="edit" style="color: #1f94f4;padding-left: 14px; cursor:pointer; font-size:18px;"\><span class="glyphicon glyphicon-edit"></span></u>';
			 	         				   
			 	         				}
			 	         			}}  
			       	               
								 
		 	       
			            ],
			            buttons: [
		 	        	    'copy', 'excel', 'pdf'
		 	        	  ],
		 	        	
		 	            
		 	             "ordering": false,
		 	             "searching": false,
		 	             "deferRender": true,
		 	             "autoWidth": false,
		 	             "lengthChange": false,
		 	             "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		       	    	 "language": {
		     	    		"infoEmpty": "No records to show",
		         	    	"emptyTable": "not record found",
		     	    	    }
		    	          }); 
			      });
		   
		   
		   
		 //**************SELECT DESELECT TABLE ROW******************************//
		 $('#users').on('click', 'tr', function () {
		 	  
		 	
		 		 if ( $(this).hasClass('row_selected'))
		 	  		{  
		 			   $("#users tbody tr").removeClass('row_selected');
		 	  	       $(this).removeClass('row_selected');
		 	  	   
		 	  	    }else
		 	  	        {
		 	  	        $("#users tbody tr").removeClass('row_selected');
		 	  	        $(this).addClass('row_selected');
		 	  	
		 	  	    }
		 }); 
		 
		
		   //*************************UPDATE CLICK*******************************//
		  	$('#users').on('click','#edit', function () {
		   		
		  		var tr = $(this).closest('tr');
		  		var selectedData = jDataTable.row(tr).data();
		  		$("#edit_tab").show();
		  		$("#user_tab").hide();
		   		scope.UserModel =selectedData;
		  		scope.$apply();
				}); 
		
		    //BACK TO TABLE
		    scope.gobackto = function(){
		   	 $("#edit_tab").hide();
		   	 $("#user_tab").show();
		     };
		     
			    
		   	////////////////////////UPDATE USER//////////////////////////////////////////////		
							
		        scope.submitForm = function(UserModel){
		   		 scope.submitted = true;
		      	  
			         	     /////////////dateOfBirth///////////////
				         	
					    	   
					    	   var sDateObj = $('#datetimepicker4').data('DateTimePicker').date();
					    	   if(sDateObj != null){
					    		 
					    		   UserModel.dateOfBirth = moment(sDateObj).format('YYYY-MM-DD');
						    	   }else
					    		   {
						    		   UserModel.dateOfBirth = UserModel.dateOfBirth;
					    		   }
					    	   
				         	  
		      	 /////////////////MODAL VALIDATION FOR NULL VALUE///////////
		  
			
		    
					scope.requestedData = {
							"username":userN,
							"functionName" : "doUpdateUser",
				  			 "data" : JSON.stringify(UserModel)
		
						};
		     	  
		      	    /***********************VALIDATION**********************/
					if (scope.addUser.$valid) 
					{	
					  if(UserModel.username !==undefined && UserModel.firstName !==undefined && UserModel.lastName !==undefined && UserModel.emailId !==undefined && UserModel.contactNo !==undefined )
					   {    
						  document.getElementById('overlid').style.visibility = "visible";
							
					        http({
							method :'POST',
							url :apiURL+'updateUser',
							data :scope.requestedData,
						    headers: options.headers

							}).then(function (response) {
			 					if(response.data.status==1){
			 						
			 						scope.response_message = response.data.message;
			 						var x = document.getElementById("snackbar_success")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 3000);
									window.location.href = '${pageContext.servletContext.contextPath}/user'; 
								}else if(response.data.status==0)
								{
									scope.message = response.data.message;
									var x = document.getElementById("snackbar_error")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 3000);
					 			
					 				document.getElementById('overlid').style.visibility = "hidden";
									
								}else if(response.data.status==21){
			   		  				 
				  		        }else{
									scope.message = response.data.message;
									document.getElementById('overlid').style.visibility = "hidden";
									
							    } 
									
			  					      
							}, function (response) {
							});   
						 
			     	  } else  {
			   				
						  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		              
						 } 
					
					 
		     	  }// end of validate 
		     	};
		      
		}]);
       </script>
</head>
<body class="" ng-controller="GetDataController" ng-init="GetUser()">
<%@ include file="Sidebar.jsp"%>	
  
 <!--  	<div  id="user_tab">     -->
    <div class="content-wrap">
     <!-- main page content. the place to put widgets in. usually consists of .row > .col-lg-* > .widget.  -->
      <main id="contentt" class="content" role="main" class="ng-cloak">
                <!-- Page content -->
          <div  id="user_tab"> 
          <div class="row">
            <div class="col-lg-12">
              <section class="widget">
                    <header>
                         <h5>User Data<span class="fw-semi-bold"></span><a style="float: right;" href="${pageContext.servletContext.contextPath}/addUser" id="adduserlink"><i style="color:#1f94f4; font-size: small;"><i class="fa fa-plus"></i>&nbsp;<u>Add User</u></i></a>							
			              </h5>
                     </header>
                <div class="widget-body">
                   <div class="table-responsive">
                     <table class="table table-striped  table-lg mt-lg mb-0" id="users">
                       <thead class=" text-primary">
                          <tr>
                              		<th data-visible="false">ID</th>
									<th style=" text-transform: revert; font-size: 15px;">Username</th>
									<th style=" text-transform: revert; font-size: 15px;">First Name</th>
									<th style=" text-transform: revert; font-size: 15px;">Middle Name</th>
									<th style=" text-transform: revert; font-size: 15px;">Last Name</th>
									<th style=" text-transform: revert; font-size: 15px;">Email</th>
									<th style=" text-transform: revert; font-size: 15px;">Contact</th>
									<th style=" text-transform: revert; font-size: 15px;">DOB</th>
									<th style=" text-transform: revert; font-size: 15px;">Role</th>
								    <th style=" text-transform: revert;font-size: 15px;">Actions</th>
						  </tr>
                        </thead>
                       </table>
                      </div>
                    <div class="clearfix">
                    </div>
                  </div>
                </section>
            </div>
          </div>
         </div>
          <!-- end user_tab-->
        <!--------------------------------UPDATE USER-------------------------------------->
      <div id="edit_tab" style="display:none;">
        <!-- start update_tab-->
         <div class="row">
         <div class="col-lg-11">
           <section class="widget">
        
            <div class="widget-body">
            <form class="form-horizontal"  name="addUser" novalidate autocomplete="off">
              <fieldset>
             
                        <legend style="padding-bottom: 17px;font-size: 19px;"><strong>Update User</strong>
                       <a class="previous" ng-click="gobackto()" style="float: right;">� Back</a>
                        </legend>
                   
                      <!--   USER NAME -->
                        <div class="form-group row">
                            <label for="username" class="col-md-4 form-control-label ">UserName</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="username"	placeholder="Enter UserName" ng-model="UserModel.username"
										 ng-minlength="4"	id="username" onkeyup="alphaNumericChecker()" required="required" />
											
									 <span style="color: red" ng-show="(addUser.username.$dirty || submitted)&& addUser.username.$error.required">
											UserName  is required !</span> 
									<span style="color: red" ng-show="(addUser.username.$dirty || submitted) && addUser.username.$error.minlength">Minimum 4 character required!</span> 
											
									 
										  </div>
                           </div>
                           
                          <!--  FIRST NAME -->
                           <div class="form-group row">
                           <label for="firstName" class="col-md-4 form-control-label ">First Name</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="firstName"	placeholder="Enter First Name" ng-model="UserModel.firstName"
											id="firstName" onkeyup="alphaNumericChecker()"  required="required" />
											
									 <span style="color: red" ng-show="(addUser.firstName.$dirty || submitted)&& addUser.firstName.$error.required">
											First Name  is required !</span> 
									 
										  </div>
                            </div>
                            
                         <!--    MIDDLE NAME -->
                            <div class="form-group row">
                            <label for="middleName" class="col-md-4 form-control-label ">Middle Name</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="middleName"	placeholder="Enter Middle Name" ng-model="UserModel.middleName"
											id="middleName" onkeyup="alphaNumericChecker()"  />
											
									
										  </div>
                            </div>
                            
                        <!--     LAST NAME -->
                            <div class="form-group row">
                            <label for="lastName" class="col-md-4 form-control-label ">Last Name</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="lastName"	placeholder="Enter Last Name" ng-model="UserModel.lastName"
											id="lastName" onkeyup="alphaNumericChecker()"  required="required" />
											
									 <span style="color: red" ng-show="(addUser.lastName.$dirty || submitted)&& addUser.lastName.$error.required">
											Last Name  is required !</span> 
									 
										  </div>
                           </div>
                           
                       <!--     EMAIL ADDRESS  -->
                            <div class="form-group row"  ng-class="{true: 'error'}[submitted && addUser.emailId.$invalid]">
                            <label for="emailId" class="col-md-4 form-control-label ">Email Address</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="emailId"	placeholder="Enter Email Address" ng-model="UserModel.emailId"
										 ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"	id="emailId" onkeyup="alphaNumericChecker()"  required="required" />
											
									 <span style="color:red" ng-show="(addUser.emailId.$dirty || submitted) && addUser.emailId.$error.required"> Email is required  !</span> 
 
													<span style="color:Red" ng-show="addUser.emailId.$dirty && addUser.emailId.$error.pattern">Please Enter Valid Email</span>

 
										  </div>
                            </div>
                            
                        <!--     CONTACT NUMBER -->
                            <div class="form-group row">
                            <label for="contactNo" class="col-md-4 form-control-label ">Contact Number</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="contactNo"	placeholder="Enter Contact Number" ng-model="UserModel.contactNo"
											ng-minlength="10" ng-maxlength="10" ng-pattern="/^[6-9][0-9]{9}$/" id="contactNo" onkeyup="alphaNumericChecker()" required="required" />
											
									 	
 													<span style="color:red" ng-show="(addUser.contactNo.$dirty || submitted) && addUser.contactNo.$error.required">	Mobile   is required  !</span>
 													<span style="color:red" ng-show="((addUser.contactNo.$error.minlength || addUser.contactNo.$error.maxlength) && addUser	.contactNo.$dirty) ">phone number should be 10 digits</span> 
											     <span style="color:Red" ng-show="addUser.contactNo.$dirty && addUser.contactNo.$error.pattern">Please Enter Valid Mobile number!</span>
											
										  </div>
                           </div>
                           
                        <!--   DOB -->
                           <div class="form-group row">
											<label class="col-md-4 form-control-label required" for="user_dateOfBirth">Date of Birth
												 </label>
											<div class="col-md-7">
  												<div class='input-group date' id='datetimepicker4'>
												<input type='text' class="form-control" name="dateOfBirth"
													placeholder="Enter Date of Birth" ng-model="UserModel.dateOfBirth" id="dob" onkeyup="alphaNumericChecker()"  />
												<span class="input-group-addon datetime"  > <span
													class="fa fa-calendar"></span>
												</span>
												<span style="color:red" ng-show="(addUser.dateOfBirth.$dirty || submitted) && addUser.dateOfBirth.$error.required">	Date Of Birth is required ! </span>
											</div>
 											</div>
										</div>
                         
                        
                       <!--    ROLE -->
                           <div class="form-group row">
                            <label for="mUserRole" class="col-md-4 form-control-label "> Role</label>
                            <div class="col-md-7">
                            
                            	<select class="select2 form-control" name ="mUserRole" ng-model="UserModel.mUserRole"ng-options="s as s.role for s in getrole track by s.roleId"  id="mGetrole" required>
									<option value="" disabled selected>Select Role  </option>
 									</select>
 									<span style="color:red" ng-show="(addUser.mUserRole.$dirty || submitted) && addUser.mUserRole.$error.required"> Select Role ! </span>	 
								 </div>
                          </div>
                        
                      
                    </fieldset>
               <!--      
                    BUTTON -->
                    <div class="form-actions bg-transparent">
                        <div class="row">
                            <div class="offset-md-4 col-md-7 col-12">
                           <button type="submit" ng-click="submitForm(UserModel)"class="btn btn-primary" style="background-color: #002B49;" id="submitbtn">Update </button>
                            </div>
                        </div>
                    </div>
                </form> 
              </div>
           </section>
        </div>
      </div>
      </div>
          
          <!-- end update_tab-->
          </div>
         </main>
        </div>
       <!-- </div> --><!-- end user_tab-->
       
     
        
        
	<div id="snackbar_success">{{response_message}}</div>
	<div id="snackbar_error">{{message}}</div>

    <div class="loader-wrap" style="background-color: rgba(0, 0, 0, 0.4);" id="overlid">
		<i class="fa fa-spinner fa-spin"></i>
    </div>
           
          <div class="loader-wrap"id="overl">
            <i class="fa fa-circle-o-notch fa-spin-fast"></i>
        </div> 
      
       
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-pjax/jquery.pjax.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/popper.js/dist/umd/popper.js"></script>
         <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/dist/js/bootstrap.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/js/dist/util.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/widgster/widgster.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" data-pace-options='{ "target": ".content-wrap", "ghostTime": 1000 }'></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/hammerjs/hammer.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-hammerjs/jquery.hammer.js"></script>


        <!-- common app js -->
        <script src="${pageContext.request.contextPath}/resources/js/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/app.js"></script>

        <!-- Page scripts -->
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.animator/jquery.flot.animator.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.selection.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.time.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.pie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.stack.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.crosshair.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.symbol.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.dashes/jquery.flot.dashes.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap_calendar/bootstrap_calendar/js/bootstrap_calendar.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
         
        <script>
			$('#datetimepicker4').datetimepicker({
                format: 'YYYY-MM-DD'
	           });
			
			function alphaNumericChecker() {
				 
				var gusername = document.getElementById('username');
				gusername.value = gusername.value.replace(/[^a-zA-Z0-9@_-]+/, '');

				var gfirstName = document.getElementById('firstName');
				gfirstName.value = gfirstName.value.replace(/[^a-zA-Z ]+/, '');

				var gmiddleName = document.getElementById('middleName');
				gmiddleName.value = gmiddleName.value.replace(/[^a-zA-Z ]+/, '');

			    var glastName = document.getElementById('lastName');
			    glastName.value = glastName.value.replace(/[^a-zA-Z ]+/, '');

				var gemailId = document.getElementById('emailId');
				gemailId.value = gemailId.value.replace(/[^a-zA-Z0-9@.]+/, '');

				var gcontactNo = document.getElementById('contactNo');
				gcontactNo.value = gcontactNo.value.replace(/[^0-9 ]+/, '');
				
				var gdob = document.getElementById('dob');
				gdob.value = gdob.value.replace(/[^a-zA-Z0-9- ]+/, '');
				
				
			}

		</script>
    </body>

</html>

