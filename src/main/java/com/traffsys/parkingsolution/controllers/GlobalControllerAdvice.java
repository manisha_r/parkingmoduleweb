package com.traffsys.parkingsolution.controllers;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.traffsys.parkingsolution.components.MyProperties;

@ControllerAdvice
public class GlobalControllerAdvice {

	private final MyProperties myProperties;

	public GlobalControllerAdvice(MyProperties myProperties) {
		this.myProperties = myProperties;
	}

	@ModelAttribute("captchaEnable")
	public boolean isCaptchaEnable() {
		return myProperties.isCaptchaEnable();
	}

}
