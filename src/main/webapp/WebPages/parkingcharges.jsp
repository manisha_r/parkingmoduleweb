<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
<meta charset="ISO-8859-1">
<title>Site Charges Info | VPark</title>

	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="Sing App - Bootstrap 4 Admin Dashboard Template">
        <meta name="keywords" content="bootstrap admin template,admin template,admin dashboard,admin dashboard template,admin,dashboard,bootstrap,template">
        <meta name="author" content="Flatlogic LLC">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
     
	    <!-----------------------------------------MAIN CSS----------------------------------------------------->
        <link href="${pageContext.request.contextPath}/resources/css/application.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png">
        <link rel="shortcut icon" type="image/x-icon"  href="${pageContext.request.contextPath}/resources/img/logo.png">
        
        <!-----------------------------------------------FONT STYLE--------------------------------------------->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/line-awesome/dist/line-awesome/css/line-awesome.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
		
		<!----------------------------------------------PROGRESSBAR--------------------------------------------------------->
		<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
		
		<!----------------------------------------------JQUERY------------------------------------------------------>
            <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>   

        <!-----------------------------------------DATATABLE------------------------------------------------------>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>
        <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        
        <!-----------------------------------------------ANGUARJS----------------------------------------------->
        <script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>
	
        <!---------------------------------- API URL ------------------------------------------------------>
        <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script> 
	
        <!-- Datatables css-->
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"rel="stylesheet">
	    <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
	    rel="stylesheet">
	    
	    <!-- Datatables js-->
        <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	    
	    <!-- date time picker -->
	    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
	    
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/webmain.css">
	
    	  <!---------------------------------- SweetAlert  ------------------------------------------------------>
          <script src="${pageContext.request.contextPath}/resources/sweetAlert.js"></script> 
	     
 
	
	
	 
	  <script>

		
		/**********************************SESSION************************************************* */
		var uid = '<%=session.getAttribute("uid")%>';
		var userN = '<%=session.getAttribute("user")%>';
		
		var status ='<%= session.getAttribute("isLoggedIn")%>';
		var token = '<%=session.getAttribute("token") %>';
		var options = {
		 		   headers: {
		 		     Authorization: 'Bearer ' + '${jwtToken}' // Assuming 'jwtToken' holds the JWT
		 		   }
		 		 };		
		


		var jDataTable;
		var jData;
		var locationarray=[];
		var app = angular.module("app", []);
		app.controller("GetDataController", ['$scope','$http','$window', function(scope,http,window) {
			
			document.getElementById('overlid').style.visibility = "hidden";
			scope.mSiteModel=1;	
			
			scope.requestData =
		    {
				"data" : "",
				"username" : uid,
				"basicAuth" : ""
		     };
			
			
			 $(function() {
				    Pace.on("done", function(){
				      /*   $('contentt').fadeIn(1000); */
				    	 
				    });
				});
			 scope.GetPayment = function (){
			    
	           /********************************GET SITE***************************************/
	              http.get(apiURL+'getSite/'+userN+'/doGetSiteInfo',options).
	   			  then(function (response) {
	   				  
	   				  
	   				//Pace.stop();

	   			   // document.getElementById('overl').style.visibility = "visible";
	   				if(response.data != null)
	   				{
	   				if(response.data.status==1){
	   				
	   			        scope.sitedata= response.data.myObjectList;
	   			        
	   			        scope.mSiteDrop = scope.sitedata[0];
 	   			      
	   			    //-----------------------HTTP-------------------------------//
	   			        	
		   			scope.dataRequest = {
		   					"siteId":scope.mSiteDrop.sid
		   					
		   				};
		   	        	  
		   		
		   			scope.requestedData = {
							"username":userN,
							"functionName" : "doGetChargesbySiteId",
				  		     "data" : JSON.stringify(scope.dataRequest)

						};
	   			         http({
							method :'POST',
							url :apiURL+'getChargesBySiteId',
							data :scope.requestedData,
						    headers: options.headers

							}).then(function (response) {	
								if(response.data != null)
		   						{

		   					    $(function() {
		   				 		    Pace.on("done", function(){
		   				 		   });
		   				 		});
		   				  	      document.getElementById('overl').style.visibility = "hidden";
		   				  	   
		   						 if(response.data.status==1){
		   					              	jData = [];
		   				             		jData= response.data.myObjectList;
		   				             	    jDataTable.clear().rows.add(jData).draw();
		   				          }else if(response.data.status==21){
				   		  				 
					  		        }else{
		   				        	 jData = [];
		   				       	  jDataTable.clear().rows.add(jData).draw();
		   				               }  
		   				    }else{
		   				     	 jData = [];
		   				   	 jDataTable.clear().rows.add(jData).draw();
		   				      }   
							}, function (response) {
							});
	   			        
	   			        
	   			        
	   			        //---------------------------HTTP END------------------------//
	   			        
  	   			       
	   				}else if(response.data.status==21){
   		  				 
	  		        }else
	   				{
	   					scope.sitedata= "";
	   		        }  
	   		   }else{
	   		     	
	   		      }      		       		
	   			}, function (response) {
	   			});
	           
	           
	           //----------------first call-------------------------------//

			 };
	        
	      
	          	scope.changeSite = function(siteId){
	        	   
	          	   document.getElementById('overlid').style.visibility = "visible";
				  	   
	        	   
	          		scope.dataRequest = {
		   					"siteId":siteId
		   				};
		   	        	  
		   			scope.requestedData = {
							"username":userN,
							"functionName" : "doGetChargesbySiteId",
				  		     "data" : JSON.stringify(scope.dataRequest)

						};
	   			         http({
							method :'POST',
							url :apiURL+'getChargesBySiteId',
							data :scope.requestedData
							}).then(function (response) {	
								if(response.data != null)
		   						{
                             document.getElementById('overlid').style.visibility = "hidden";
		   				  	   
		   						 if(response.data.status==1){
		   					              	jData = [];
		   				             		jData= response.data.myObjectList;
		   				             	    jDataTable.clear().rows.add(jData).draw();
		   				          }else if(response.data.status==21){
				   		  				 
					  		      }else{
		   				        	 jData = [];
		   				       	  jDataTable.clear().rows.add(jData).draw();
		   				               }  
		   				    }else{
		   				     	 jData = [];
		   				   	 jDataTable.clear().rows.add(jData).draw();
		   				      }   
							}, function (response) {
							});  
 	          		
			   	}
			   	
		   		//******************************PAYMENT LIST******************************//
		   		
		   		/* scope.GetPayment = function (siteId) { 
		   			
		   			alert("sdf:", siteId);
		   			
		   			console.log(siteId);
		   			
		   			scope.dataRequest = {
		   					"siteId":siteId
		   					
		   				};
		   	        	  
		   			console.log(scope.dataRequest);
		   			scope.requestedData = {
							"sessionTokken" : jwtToken,
							"username":userN,
							"functionName" : "doGetChargesbySiteId",
				  		     "data" : JSON.stringify(scope.dataRequest)

						};
					   http({
							method :'POST',
							url :apiURL+'getChargesBySiteId',
							data :scope.requestedData
							}).then(function (response) {	
								if(data != null)
		   						{

		   					    $(function() {
		   				 		    Pace.on("done", function(){
		   				 		   });
		   				 		});
		   				  	      document.getElementById('overl').style.visibility = "hidden";
		   				  	   
		   						 if(data.status==1){
		   					              	jData = [];
		   				             		jData= data.myObjectList;
		   				             	    jDataTable.clear().rows.add(jData).draw();
		   				          }else{
		   				        	 jData = [];
		   				       	  jDataTable.clear().rows.add(jData).draw();
		   				               }  
		   				    }else{
		   				     	 jData = [];
		   				   	 jDataTable.clear().rows.add(jData).draw();
		   				      }   
							}, function (response) {
		}); 
		   		        	
		   	};  	 */
		  
		   	/*********************************PARKING DATA*************************************/
		   	$(document).ready(function() {
		   		 jDataTable = $('#users').DataTable({ 
		   	 		 	data: jData,
		   		         		columns: [
		   		         		
		   		   		         		{'data':'mVehicleClassModel', 'render':function(mVehicleClassModel){
		   		 	         				
		   		    	             		 if(mVehicleClassModel!=null)
		   			             			 {
		   		    	             			if(mVehicleClassModel!=""){
		   		     	         					return mVehicleClassModel.className;
		   		      	         				}else{
		   		      	         					return '<p>__</p>';
		   		      	         				}
		   			             			 }else{
		   			             				return '<p>__</p>';
		   			             			 }
		   			         			}},
		   			         			{'data':'priceHour', 'render':function(priceHour){
		   		 	         				
		   		   	             		 if(priceHour!=null)
		   			             			 {
		   		   	             			if(priceHour!=""){
		   		    	         					return priceHour;
		   		     	         				}else{
		   		     	         					return '<p>__</p>';
		   		     	         				}
		   			             			 }else{
		   			             				return '<p>__</p>';
		   			             			 }
		   			         			}},
		   			         			{'data':'dayPrice', 'render':function(dayPrice){
		   		 	         				
		   		    	             		 if(dayPrice!=null)
		   			             			 {
		   		    	             			if(dayPrice!=""){
		   		     	         					return dayPrice;
		   		      	         				}else{
		   		      	         					return '<p>__</p>';
		   		      	         				}
		   			             			 }else{
		   			             				return '<p>__</p>';
		   			             			 }
		   			         			}},
		   			         			{'data':'monthPrice', 'render':function(monthPrice){
		   		 	         				
		   		    	             		 if(monthPrice!=null)
		   			             			 {
		   		    	             			if(monthPrice!=""){
		   		     	         					return monthPrice;
		   		      	         				}else{
		   		      	         					return '<p>__</p>';
		   		      	         				}
		   			             			 }else{
		   			             				return '<p>__</p>';
		   			             			 }
		   			         			}},
		   		             	
		   		             	  {'data': "paymentId", 'render':function(paymentId){
		   		         				
		   		         				if(paymentId!=null){
		   		         					
		   		         				  return '<u id="edit" style="color: #1f94f4;padding-left: 14px; cursor:pointer; font-size:18px;"\><span class="glyphicon glyphicon-edit"></span></u>';
			 	         				   		
		   		         				}else{
			 	         				   	
		   		         				     return '<p>__</p>';
		   		         				     
			 	         				   	}
		   		         				
		   		         			}}  
		   	    	               
		   		       
		   		            ],
		   		           buttons: [
		   		        	    'copy', 'excel', 'pdf'
		   		        	  ],
		   	
		   		 	             "ordering": false,
		   		 	             "searching": false,
		   		 	             "deferRender": true,
		   		 	             "autoWidth": false,
		   		 	            "lengthChange": false,
		   		 	             "language": {
		   		     	    		"infoEmpty": "No records to show",
		   		         	    	"emptyTable": "no record found",
		   		     	    	    }
		   		    	        }); 
		   	  }); 
		   	
		   	
		   
		 //**************SELECT DESELECT TABLE ROW******************************//
		 $('#users').on('click', 'tr', function () {
		 	  
		 	
		 		 if ( $(this).hasClass('row_selected'))
		 	  		{  
		 			   $("#users tbody tr").removeClass('row_selected');
		 	  	       $(this).removeClass('row_selected');
		 	  	   
		 	  	    }else
		 	  	        {
		 	  	        $("#users tbody tr").removeClass('row_selected');
		 	  	        $(this).addClass('row_selected');
		 	  	
		 	  	    }
		 }); 
	
			
		   //*************************UPDATE CLICK*******************************//
		  	$('#users').on('click','#edit', function () {
		  	
		  	 	var tr = $(this).closest('tr');
		  		var selectedData = jDataTable.row(tr).data();
		   		$("#edit_tab").show();
		  		$("#user_tab").hide();
		   		scope.PaymentModel =selectedData;
		  		scope.$apply(); 
				}); 
		   
		    //BACK TO TABLE
		    scope.gobackto = function(){
		   	 $("#edit_tab").hide();
		   	 $("#user_tab").show();
		     };
		     
			    
		   	////////////////////////UPDATE PAYENT//////////////////////////////////////////////		
							
		        scope.submitForm = function(UserModel){
		   		 scope.submitted = true;
		      	scope.requestedData = {
							"username":userN,
							"functionName" : "doUpdatePaymentInfo",
				  			 "data" : JSON.stringify(UserModel)
		
						};
		     	  
		      	    /***********************VALIDATION**********************/
					if (scope.addpayment.$valid) 
					{	
					  if(UserModel.monthPrice != undefined && UserModel.dayPrice != undefined  && UserModel.mSiteModel != undefined  && UserModel.priceHour != undefined  )
					   {    
						  document.getElementById('overlid').style.visibility = "visible";
							
					        http({
							method :'POST',
							url :apiURL+'updatePayment',
							data :scope.requestedData,
						    headers: options.headers

							}).then(function (response) {
			 					if(response.data.status==1){
			 						
			 						scope.response_message = response.data.message;
			 						var x = document.getElementById("snackbar_success")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");
						 				window.location.href = '${pageContext.servletContext.contextPath}/parkingcharges'; 

									}, 1000);
									

								}else if(response.data.status==0)
								{
									scope.message = response.data.message;
									var x = document.getElementById("snackbar_error")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 3000);
					 			
					 				document.getElementById('overlid').style.visibility = "hidden";
									
								}else if(response.data.status==21){
			   		  				 
				  		        }else{
									scope.message = response.data.message;
									document.getElementById('overlid').style.visibility = "hidden";
									
							    } 
									
			  					      
							}, function (response) {
							}); 
						 
			     	  } else  {
			   				
						  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		              
						 } 
					
					 
		     	  }// end of validate 
					else  {
		   				
						  scope.message = "Above Fields cannot be Left Blank !";
						  var x = document.getElementById("snackbar_error")
							x.className = "show";
							setTimeout(function() {
								x.className = x.className.replace("show", "");
							}, 3000);
		              
						 } 
					
					 
		     	};
		      
		}]);
       </script>
</head>
<body class="" ng-controller="GetDataController" ng-init="GetPayment()">
<%@ include file="Sidebar.jsp"%>	
  
 <!--  	<div  id="user_tab">     -->
    <div class="content-wrap">
     <!-- main page content. the place to put widgets in. usually consists of .row > .col-lg-* > .widget.  -->
      <main id="contentt" class="content" role="main" class="ng-cloak">
                <!-- Page content -->
          <div  id="user_tab"> 
          <div class="row">
            <div class="col-lg-12">
              <section class="widget">
                    <header>
                         <h5><strong>Site Charges Details<span class="fw-semi-bold"></span></strong> 
                  <%--        <a style="float: right;" href="${pageContext.servletContext.contextPath}/addUser" id="adduserlink"><i style="color:#1f94f4; font-size: small;"><i class="fa fa-plus"></i>&nbsp;<u>Add User</u></i></a>							
			            --%>
			              </h5> 
                     </header>
                <div class="widget-body">
              
               
                 <div class="form-group row">
                             <div class="col-md-3">
                            
                            	<select class="select2 form-control" name ="sitemodel"  ng-model="mSiteDrop"   ng-options="a as a.siteName for a in sitedata track by a.sid" ng-change="changeSite(mSiteDrop.sid)" id="mSiteDrop" required>
									<option value="" disabled selected>Select Site  </option>
 									</select>
 								</div>

                        </div>
               
                   <div class="table-responsive">
                     <table class="table table-striped  table-lg mt-lg mb-0" id="users">
                       <thead class=" text-primary">
                          <tr>
                                           <th style=" text-transform: revert; font-size: 15px;">Vehicle Class</th>
										   <th style=" text-transform: revert; font-size: 15px;">Hourly Price </th>
										   <th style=" text-transform: revert; font-size: 15px;">Daily Price</th>
										   <th style=" text-transform: revert; font-size: 15px;">Monthly Price</th>
 										   <th style=" text-transform: revert; font-size: 15px;">Action</th>
						  </tr>
                        </thead>
                       </table>
                      </div>
                    <div class="clearfix">
                    </div>
                  </div>
                </section>
            </div>
          </div>
         </div>
          <!-- end user_tab-->
        <!--------------------------------UPDATE Parking-------------------------------------->
      <div id="edit_tab" style="display:none;">
        <!-- start update_tab-->
         <div class="row">
         <div class="col-lg-11">
           <section class="widget">
        
            <div class="widget-body">
          	<form class="form-horizontal" name="addpayment" novalidate autocomplete="off">
              <fieldset>
             
                        <legend style="padding-bottom: 17px;font-size: 19px;"><strong>Update Site Charges</strong>
                       <a class="previous" ng-click="gobackto()" style="float: right;">� Back</a>
                        </legend>
                   
                    	<!-- <div class="form-group row">
                    	
									<label for="mSiteModel"  class="col-md-4 form-control-label">Site Name
									</label>
									<div class="col-md-7">
								 
										<input type="text" class="form-control" id="mSiteModel" readonly="readonly"
											name="siteName"ng-model="PaymentModel.mSiteModel.siteName"
											 placeholder="Site Name" required="required"/>
											  <span style="color: red" ng-show="(addpayment.mSiteModel.$dirty || submitted)&& addpayment.mSiteModel.$error.required">
											Site Name is required !</span> 
 									</div>
								</div> -->
								
								<div class="form-group row">
                    	
									<label for="mSiteModel"  class="col-md-4 form-control-label">Vehicle Type
									</label>
									<div class="col-md-7">
								 
										<input type="text" class="form-control" id="mSiteModel" readonly="readonly"
											ng-model="PaymentModel.mVehicleClassModel.className" placeholder="Vehicle Type" required="required"/>
  									</div>
								</div>
                           
                           <div class="form-group row">
									<label for="priceHour" class="col-md-4 form-control-label">Hourly Price
									</label>
									<div class="col-md-7">
								 
										<input type="number" class="form-control" id="priceHour" placeholder="Hourly Price"
											name="priceHour" ng-model="PaymentModel.priceHour"
											 required="required"/>
											  <span style="color: red" ng-show="(addpayment.priceHour.$dirty || submitted)&& addpayment.priceHour.$error.required">
											Hourly Price is required !</span> 
 									</div>
								</div>
                           
                           <div class="form-group row">
									<label for="dayPrice" class="col-md-4 form-control-label">Daily Price
									</label>
									<div class="col-md-7">
								 
										<input type="number" class="form-control" id="dayPrice" placeholder="Daily Price"
											name="dayPrice" ng-model="PaymentModel.dayPrice"
											required="required"/>
											
											 <span style="color: red" ng-show="(addpayment.dayPrice.$dirty || submitted)&& addpayment.dayPrice.$error.required">
											Daily Price is required !</span> 
 									</div>
								</div>
                           
                           <div class="form-group row">
									<label for="monthPrice" class="col-md-4 form-control-label">Monthly Price
									</label>
									<div class="col-md-7">
								 
										<input type="number" class="form-control" id="monthPrice" placeholder="monthPrice"
											name="monthPrice" ng-model="PaymentModel.monthPrice"
											 required="required"/>
											 <span style="color: red" ng-show="(addpayment.monthPrice.$dirty || submitted)&& addpayment.monthPrice.$error.required">
											Monthly Price is required !</span> 
 									</div>
								</div>
                           
                    </fieldset>
               <!--      
                    BUTTON -->
                    <div class="form-actions bg-transparent">
                        <div class="row">
                            <div class="offset-md-4 col-md-7 col-12">
                           <button type="submit" ng-click="submitForm(PaymentModel)"class="btn btn-primary"style="background-color: #002B49;" id="submitbtn">Update</button>
                            </div>
                        </div>
                    </div>
                </form> 
              </div>
           </section>
        </div>
      </div>
      </div>
          
          <!-- end update_tab-->
         
         </main>
        </div>
     
        
	<div id="snackbar_success">{{response_message}}</div>
	<div id="snackbar_error">{{message}}</div>

    <div class="loader-wrap" style="background-color: rgba(0, 0, 0, 0.4);" id="overlid">
		<i class="fa fa-spinner fa-spin"></i>
    </div>
           
          <div class="loader-wrap"id="overl">
            <i class="fa fa-circle-o-notch fa-spin-fast"></i>
        </div> 
      
       
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-pjax/jquery.pjax.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/popper.js/dist/umd/popper.js"></script>
 <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/dist/js/bootstrap.js"></script>        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/js/dist/util.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/widgster/widgster.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" data-pace-options='{ "target": ".content-wrap", "ghostTime": 1000 }'></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/hammerjs/hammer.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-hammerjs/jquery.hammer.js"></script>


        <!-- common app js -->
        <script src="${pageContext.request.contextPath}/resources/js/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/app.js"></script>

        <!-- Page scripts -->
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.animator/jquery.flot.animator.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.selection.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.time.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.pie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.stack.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.crosshair.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.symbol.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.dashes/jquery.flot.dashes.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap_calendar/bootstrap_calendar/js/bootstrap_calendar.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
         
        <script>
			$('#datetimepicker4').datetimepicker({
                format: 'YYYY-MM-DD'
	           });
		</script>
    </body>

</html>

