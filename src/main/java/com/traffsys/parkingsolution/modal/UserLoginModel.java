package com.traffsys.parkingsolution.modal;

import java.io.Serializable;

public class UserLoginModel  implements Serializable{

	private static final long serialVersionUID = 1L;

	private String username;
	private String password;
	private Integer userType;
	private Boolean pushLogin;
	
	public Boolean getPushLogin() {
		return pushLogin;
	}

	public void setPushLogin(Boolean pushLogin) {
		this.pushLogin = pushLogin;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public UserLoginModel() {
		super();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
