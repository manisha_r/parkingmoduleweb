<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
<meta charset="ISO-8859-1">
<title>OperatorWise Report | VPark</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="Sing App - Bootstrap 4 Admin Dashboard Template">
        <meta name="keywords" content="bootstrap admin template,admin template,admin dashboard,admin dashboard template,admin,dashboard,bootstrap,template">
        <meta name="author" content="Flatlogic LLC">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <!-----------------------------------------main css----------------------------------------------------->
        <link href="${pageContext.request.contextPath}/resources/css/application.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png">
        <link rel="shortcut icon" type="image/x-icon"  href="${pageContext.request.contextPath}/resources/img/logo.png">
        
        <!-----------------------------------------------font style--------------------------------------------->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/line-awesome/dist/line-awesome/css/line-awesome.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
		
		<!----------------------------------------------progressbar--------------------------------------------------------->
		<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
		
		<!----------------------------------------------Jquery------------------------------------------------------>
           <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>   

        <!-----------------------------------------dataTable------------------------------------------------------>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>
        <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        
        <!-----------------------------------------------ANGUARJS----------------------------------------------->
        <script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>
	
        <!---------------------------------- API URL ------------------------------------------------------>
        <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script> 
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/webmain.css">
	    
        <!-- Datatables css-->
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"rel="stylesheet">
	    <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
	    rel="stylesheet">
	    <!-- Datatables js-->
        <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
      <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.14.2.js"></script>
 
 
   <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
         <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>
 
  <!---------------------------------------DATEPICKER-------------------------------------------------------->
		 <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/webmain.css">
     	  <!---------------------------------- SweetAlert  ------------------------------------------------------>
          <script src="${pageContext.request.contextPath}/resources/sweetAlert.js"></script> 
	     
 
 <style>
 
 
 .btn-primary{
  background-color: #002B49 !important; 
 }
 
 </style>
 	
<script>
$(document).ready(function(){

	  $("#sreports").addClass("active");
    $("#sreports").addClass("open");
    $("#report-ui").addClass("show");


    $("#sOperatorWise").addClass("active");

	 });


	/**********************************SESSION ********************************************************** */
	var uid = '<%=session.getAttribute("uid")%>';
	var userN = '<%=session.getAttribute("user")%>';
	
	var status ='<%= session.getAttribute("isLoggedIn")%>';
	var token = '<%=session.getAttribute("token") %>';
	var options = {
	 		   headers: {
	 		     Authorization: 'Bearer ' + '${jwtToken}' // Assuming 'jwtToken' holds the JWT
	 		   }
	 		 };	
	
	
	var jDataTable;
	var jData;
	var app = angular.module("app", []);
	app.controller("GetDataController", ['$scope','$http','$window', function(scope,http,window) {
	  
		  scope.requestData =
	       {
			"data" : "",
			"username" : uid,
			"basicAuth" : ""
	       };
		
		    document.getElementById('overlid').style.visibility = "hidden";
     	  //Pace.stop();
		  //document.getElementById('overl').style.visibility = "hidden";

		  
		  //-----------------------CURRENT TODAY -------------------------//
		  
		  scope.todayDate =moment().format('YYYY-MM-DD');
		  scope.EndDate =moment().format('YYYY-MM-DD');
		  scope.formdate =moment().format('YYYY-MM-DD HH:mm:ss');

		  
		  
		  //---------------------------SITE DATA-----------------------------------//
					  
      		 http.get(apiURL+'getSite/'+userN+'/doGetSiteInfo',options).
      				then(function (response) {
      					 
      				if(response.data != null)
      					{
      					if(response.data.status==1){
      						
      					     scope.siteData = response.data.myObjectList;
      					     
      	                 }else if(response.data.status==21){
		   		  				 
			  		        } else{
      	                    scope.siteData =[];
      	                 }  
      			       } 		       		
      				}, function (response) {
      				});
		
		
		
		//******************************REPORT LIST******************************//
		   scope.GetReporttoday = function () { 
			
			
			    scope.dataRequest= {
    			      "startdate": scope.todayDate,
  			          "enddate": null,
  			          "siteId":0
  	             }
      
			    
  	             scope.requestedData = {
					   "username":userN,
					   "functionName" : "doGetReportByOperator",
		  		       "data" : JSON.stringify(scope.dataRequest)
				 };
			    
			 
			     http({
			   		 method : 'POST',
							 url : apiURL+'getVehicleCountReport',
							 data:scope.requestedData,
							    headers: options.headers

					         })
					         .then(function (response) {
				 		  	   	if(response.data != null)
					    		{
					    		if(response.data.status==1){
					    			 
							              	jData = [];
						              		jData= response.data.myObjectList;
						              		jDataTable.clear().rows.add(jData).draw();
						              		Pace.stop();
									        document.getElementById('overl').style.visibility = "hidden";	
 					              }else if(response.data.status==21){
 				   		  				 
 					  		        }else{
					             	 jData = [];
					            	 jDataTable.clear().rows.add(jData).draw();
					            	 Pace.stop();
								     document.getElementById('overl').style.visibility = "hidden";
 					                    }  
					           }else{
					          	 jData = [];
					        	 jDataTable.clear().rows.add(jData).draw();
					        	 Pace.stop();
							     document.getElementById('overl').style.visibility = "hidden";
 					           } 
					         }, function (response) {
					 		});		 
	                 };  	
	
	
	
	
	   /*************************************FILTER BUTTON  ************************************************/
	   scope.fromdate =null;
	   scope.todate =null;
 	   scope.siteId = "";
       scope.btnfilter = function(ReportModel){

		//**************************DATE FEILD***********************//
		 var sDateObj = $('#myDatepicker1').data('DateTimePicker').date();
		 if(sDateObj != null){
  		  scope.fromdate = moment(sDateObj).format('YYYY-MM-DD');
	    	   }
		 else
  		   {
  		   scope.fromdate = null;
  		   }
		 
		 var endDateObj = $('#myDatepicker2').data('DateTimePicker').date();
		 if(endDateObj != null){
  		  scope.todate = moment(endDateObj).format('YYYY-MM-DD');
	    	   }else
  		   {
  		   scope.todate = null;
  		   }
	   
		
  	      //model validation//
 	      if(typeof scope.ReportModel  !== 'undefined'){
				
  	    	if(scope.ReportModel.hasOwnProperty("mSiteModel")){
				
  	  		   scope.siteId = scope.ReportModel.mSiteModel;
  	  		   
  	  		 }else{
  	  			scope.siteId = 0;
  	  	     }
 	       } 
  	      
    	   scope.dataRequest= {
      			      "startdate": scope.fromdate,
    			       "enddate": scope.todate,
    			       "siteId":scope.siteId
    	   }
        
    	   scope.requestedData = {
					"username":userN,
					"functionName" : "doGetReportByOperator",
		  		     "data" : JSON.stringify(scope.dataRequest)
			};
			
    	   
		 	 if(scope.fromdate !== null && scope.siteId !== "")
		 	 {
		 	   document.getElementById('overlid').style.visibility = "visible"; 
		
	    	   http({
	   		   method : 'POST',
					 url : apiURL+'getVehicleCountReport',
					 data:scope.requestedData,
					    headers: options.headers

			         })
			         .then(function (response) {
		 		  	   	if(response.data != null)
			    		{
			    		if(response.data.status==1){
					              	jData = [];
				              		jData= response.data.myObjectList;
				              		jDataTable.clear().rows.add(jData).draw();
				              			
									document.getElementById('overlid').style.visibility = "hidden";
			              }else if(response.data.status==21){
		   		  				 
			  		        }else{
			             	 jData = [];
			            	 jDataTable.clear().rows.add(jData).draw();
			            	 
			            	 document.getElementById('overlid').style.visibility = "hidden";
			                   }  
			           }else{
			          	 jData = [];
			        	 jDataTable.clear().rows.add(jData).draw();
			        	 
			        	 document.getElementById('overlid').style.visibility = "hidden";
			           } 
		 		  	   	
			         }, function (response) {
			 		});
		 	       }
		           else{  
		  			 
		  	           scope.siteId = "";
		  	           scope.fromdate =null;
		  	 	       scope.todate =null;
		  	 	       scope.message = "Fields cannot be Left Blank !";
					    var x = document.getElementById("snackbar_error")
						x.className = "show";
						setTimeout(function() {
							x.className = x.className.replace("show", "");
						}, 2000);
	
		  	 	        document.getElementById('overl').style.visibility = "hidden";  
		  	            
		        }              
      };
      
      

	//*****************************RESET BUTTON*********************//
	
	scope.btnreset= function(){
		   
		   scope.fromdate = null; 
	       scope.todate= null;
	       scope.ReportModel={};
	       scope.siteId = "";

	      $('#myDatepicker1').data("DateTimePicker").clear();
	   	  $('#myDatepicker2').data("DateTimePicker").clear();
	   	  scope.GetReporttoday();
		
	};
	
	
	
	/*********************************Operator Wise Datatable*************************************/
	$(document).ready(function() {
		 jDataTable = $('#reports').DataTable({ 
			
			           data: jData,
	 		 	
		         		columns: [
		         			
		         			 {'data':'selecteddate', 'render': function(selecteddate){
				             		
				             		if(selecteddate!=""){
				             			
				             			return selecteddate;
				             		}else{
				             			
				             			return '<p>__</p>';
				             		}
				             }},
		         		
			            	{'data':'sitename', 'render': function(sitename){
			             		
			             		if(sitename != ""){
			             			
			             			return sitename;
			             		}else{
			             			
			             			return '<p>__</p>';
			             		}
			             	}},
		             	  
                             {'data':'type', 'render': function(type){
			             		
			             		if(type != ""){
			             			
			             			return type;
			             		}else{
			             			
			             			return '<p>__</p>';
			             		}
			                }}, 
                            
			                {'data':'total_vehicle', 'render': function(total_vehicle){
			             		
			             		if(total_vehicle != null){
			             			
			             			return total_vehicle;
			             		}else{
			             			
			             			return '<p>__</p>';
			             		}
			                }}, 
			                
	                        {'data':'EntryCount', 'render': function(EntryCount){
		             		
		             		if(EntryCount != null){
		             			
		             			return EntryCount;
		             		}else{
		             			
		             			return '<p>__</p>';
		             		}
		             	   }},
		             	  
		             	   {'data':'ExitCount', 'render': function(ExitCount){
		             		
		             		if(ExitCount != null){
		             			
		             			return ExitCount;
		             		}else{
		             			
		             			return '<p>__</p>';
		             		}
		             	  }},	
			             
		             	 {'data':'userId', 'render': function(userId){
			             		
			             		if(userId != null){
			             			
			             			return userId;
			             		}else{
			             			
			             			return '<p>__</p>';
			             		}
			             	}},
				            {'data':'totalamount', 'render': function(totalamount){
			             		
			             		if(totalamount != null){
			             			
			             			return totalamount+'<span style="padding-left: 5px;">Rs.</span>';
			             		}else{
			             			
			             			return '<p>__</p>';
			             		}
			             	}}
		            ],
		           buttons: [
		        	    'copy', 'excel', 'pdf'
		        	  ],
	
		 	             "ordering": false,
		 	             "searching": false,
		 	             "deferRender": true,
		 	             "autoWidth": false,
		 	            "lengthChange": false,
		 	             "language": {
		     	    		"infoEmpty": "No records to show",
		         	    	"emptyTable": "No record found",
		     	    	    }
		    	        }); 
	           });  //end of datatable 	 
	}]);
	
</script>
</head>


<body class="" ng-controller="GetDataController" ng-init="GetReporttoday()">
 
  <%@ include file="Sidebar.jsp"%>	
   
  <div class="content-wrap">
    
    <main id="contentt" class="content" role="main" class="ng-cloak">
      <!-- Page content -->
        <div class="row">
            <div class="col-lg-12">
                <section class="widget" style="margin-bottom: 15px;">
                   <header>
                         <h5>Operator Wise <span class="fw-semi-bold"></span></h5>
                    </header>
                    
                   <div class="widget-body">
                    
                     <div id="filterdiv">
 						<form id="alert_form" autocomplete="off">
							<div class="row">
								
								<!------------------Start-Date------------------->
								<div class="col-sm-3 col-md-3  col-xs-12">
									<b>Start-Date</b>
									<div class="row">
										<div class="col-sm-12 col-md-12 col-xs-12">
											<div class="form-group">
											 												
												<div class='input-group date' id='myDatepicker1'>
												<input type='text' class="form-control" name="in_time"
													placeholder="Start-Date" ng-model="ReportModel.in_time" />
												<span class="input-group-addon datetime"  > <span
													class="fa fa-calendar"></span>
												</span>
 											</div>
												
											</div>
										</div>
									</div>
								</div>
								
					           <!------------------End-Date------------------->
								<div class="col-sm-3 col-md-3 col-xs-12">
									 <b>End-Date</b>
									<div class="row">
										<div class="col-sm-12 col-md-12 col-xs-12">
											<div class="form-group">
												<div class='input-group date' id='myDatepicker2'>
												<input type='text' class="form-control" name="out_time"
													placeholder="End-Date" ng-model="ReportModel.out_time" />
												<span class="input-group-addon datetime"  > <span
													class="fa fa-calendar"></span>
												</span>
 											</div>
											</div>
										</div>
									</div>
								</div>
								
								<!------------------Site------------------->
								<div class="col-sm-3 col-md-3 col-xs-12">
 									<b>Site</b>
									<div class="row">
										<div class="col-sm-12 col-md-12 col-xs-12">
											<div class="form-group">
												<select class="form-control mb-3" id="mSiteData" ng-model="ReportModel.mSiteModel">
													<option value="" disabled selected>Select Site</option>
												 	<option value="0"> ALL</option>
 													<option  ng-repeat="mSiteModel in siteData" value="{{mSiteModel.sid}}">{{mSiteModel.siteName}}</option>
 												</select> 	
											</div>
										</div>
									</div>
								</div>
								
							 <!------------------Button------------------->
					          <div class="col-sm-3 col-md-3 col-xs-12">
 						           <b>&nbsp;</b>
 						           <div class="row">
   							          <div class="col-sm-12 col-md-12 col-xs-12">
   							             <button class="btn btn-primary" ng-click="btnfilter(ReportModel)"id="serach">Search</button>
 							              <button type="submit" id="btnreset" class="btn btn-danger" ng-click="btnreset()">Reset</button>
   							          </div>
   							       </div>
    					      </div>
					 
					      </div><!--end of row  -->
 				        </form><!-- end of form -->
    			      </div>
                    </div><!--end widget-body  -->
                 </section>
                    
                    
                    
                 <!---------------------------------------------------- TABLE SECTION -------------------------------------------------------->
                 
                  <section class="widget">
                     <div class="table-responsive">
                      <table class="table table-striped dataTable  table-lg mt-lg mb-0" id="reports">
                       <thead class=" text-primary">
                       <tr>
                       		<th style=" text-transform: revert; font-size: 15px;">Date-Time</th>
                      		<th style=" text-transform: revert; font-size: 15px;">Site</th>
  							<th style=" text-transform: revert; font-size: 15px;">Type</th>
  							<th style=" text-transform: revert; font-size: 15px;">Total Vehicle</th>
  							
  							<th style=" text-transform: revert; font-size: 15px;">Entry Count</th>
  			                <th style=" text-transform: revert; font-size: 15px;">Exit Count</th>
							<th style=" text-transform: revert; font-size: 15px;">UserId</th>
						    <th style=" text-transform: revert; font-size: 15px;">Total Amount </th>
 					   </tr>
 					    
                      </thead>
                     </table>
                    </div>
                 </section>
                        
                <div class="clearfix"> </div>
                   
              </div>
               
            </div><!-- end of row -->
          </main><!-- end of main -->
        </div>
        
        <div id="snackbar_success">{{response_message}} </div>
		<div id="snackbar_error">{{message}} </div>
		
		<!-- spinner -->
        <div class="loader-wrap " style="background-color: rgba(0, 0, 0, 0.4);" id="overlid">
		<i class="fa fa-spinner fa-spin"></i>
        </div>
        
        <div class="loader-wrap"id="overl">
            <i class="fa fa-circle-o-notch fa-spin-fast"></i>
        </div> 
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-pjax/jquery.pjax.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/popper.js/dist/umd/popper.js"></script>
  <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/dist/js/bootstrap.js"></script>        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/js/dist/util.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/widgster/widgster.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" data-pace-options='{ "target": "body", "ghostTime": 1000 }'></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/hammerjs/hammer.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-hammerjs/jquery.hammer.js"></script>


        <!-- common app js -->
        <script src="${pageContext.request.contextPath}/resources/js/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/app.js"></script>

        <!-- Page scripts -->
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.animator/jquery.flot.animator.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.selection.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.time.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.pie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.stack.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.crosshair.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.symbol.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.dashes/jquery.flot.dashes.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap_calendar/bootstrap_calendar/js/bootstrap_calendar.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>
       
        <!-- bootstrap-daterangepicker -->	
       <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
         
       
       <script>
       
       $('#myDatepicker1').datetimepicker({
   		
           format: 'YYYY-MM-DD' 
           
         });
       
	   $('#myDatepicker2').datetimepicker({
	  		
	       format: 'YYYY-MM-DD'
	          
	    });
	  	 
       
       </script>
       
    </body>

</html>










