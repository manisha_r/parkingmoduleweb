package com.traffsys.parkingsolution.security;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.firewall.RequestRejectedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(RequestRejectedException.class)
    public ResponseEntity<String> handleRequestRejectedException(RequestRejectedException ex) {
        // Customize the response for a rejected TRACE request
		 HttpHeaders headers = createCustomHeaders();
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).headers(headers)
                             .body("TRACE method is not allowed");
    }

	 @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	    public ResponseEntity<String> handleMethodNotSupportedException(HttpRequestMethodNotSupportedException ex) {
	        String errorMessage = "HTTP method '" + ex.getMethod() + "' is not supported for this endpoint.";
	        HttpHeaders headers = createCustomHeaders();
	        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).headers(headers).body(errorMessage);
	    }
	
	
	 @ExceptionHandler(Exception.class)
	    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	    public ResponseEntity<String> handleGenericException(Exception ex) {
	        String errorMessage = "An unexpected error occurred.";
	        HttpHeaders headers = createCustomHeaders();
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).headers(headers).body(errorMessage);
	    }
	  
	
	 private HttpHeaders createCustomHeaders() {
	        HttpHeaders headers = new HttpHeaders();
	        headers.add("X-FRAME-OPTIONS", "DENY");
	        headers.add("Strict-Transport-Security", "max-age=7776000");
	        headers.add("Pragma", "No-cache");
	        headers.add("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
	        headers.add("X-XSS-Protection", "1; mode=block");
	        headers.add("X-Powered-By", "");
	        headers.add("Content-Security-Policy", "default-src *; script-src 'self' 'unsafe-inline' 'unsafe-eval' *; style-src 'self' 'unsafe-inline' *; img-src 'self' blob: *;");
	        headers.add("X-Content-Type-Options", "nosniff");
	        headers.add("Allow", "GET, POST"); // Specify allowed HTTP methods
	        headers.add("Referrer-Policy", "strict-origin-when-cross-origin");
	        return headers;
	    }

}
