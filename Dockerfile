# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
ARG JAR_FILE=target/*.war
COPY ${JAR_FILE} /parkingweb.war
ENTRYPOINT ["java","-jar","/parkingweb.war"]


# run application with this command line
#To Build Docker Image
#docker build -f DockerFile -t parkingweb .
#To Run docker Image
#docker run --name -p 8087:8087 parkingweb parkingweb
#docker run -d --network="host" --restart always --name apigateway <ImageID>
#docker run -p 8888:8082 --name logger nikhoney27/loggermod
#docker exec -it eTmcs bash