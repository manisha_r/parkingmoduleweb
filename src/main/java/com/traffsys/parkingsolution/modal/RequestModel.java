package com.traffsys.parkingsolution.modal;

public class RequestModel {
	private String data;
	private String username;
	private String sessionTokken;
	private String functionName;

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSessionTokken() {
		return sessionTokken;
	}

	public void setSessionTokken(String sessionTokken) {
		this.sessionTokken = sessionTokken;
	}

	@Override
	public String toString() {
		return "RequestModel [data=" + data + ", username=" + username + ", sessionTokken=" + sessionTokken
				+ ", functionName=" + functionName + "]";
	}

	
}
