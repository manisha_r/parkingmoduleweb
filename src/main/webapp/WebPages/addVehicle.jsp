<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
<meta charset="ISO-8859-1">
<title>Add parking | VPark</title>

        <!-- application -->
        <link href="${pageContext.request.contextPath}/resources/css/application.min.css" rel="stylesheet">
       
        <link rel="shortcut icon" type="image/x-icon"  href="${pageContext.request.contextPath}/resources/img/logo.png">
       
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/line-awesome/dist/line-awesome/css/line-awesome.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
		 
	    <!----------------------------------JQUERY/ANGUARJS------------------------------------------------------------------- -->
          <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>     
     	<script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>
 
         <!---------------------------------- API URL ------------------------------------------------------>
         <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script>
          
         <!-- FONT -->
	     <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	     
	     <!--SNACKBAR  -->
	     <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
         <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/webmain.css">
     	  
     	  <!---------------------------------- SweetAlert  ------------------------------------------------------>
          <script src="${pageContext.request.contextPath}/resources/sweetAlert.js"></script> 
	     
 	

   
    <script>
       
    $(document).ready(function(){

        $("#kaddvehicle").addClass("active");

     });
   

	var uid = '<%=session.getAttribute("uid")%>';
	var userN = '<%=session.getAttribute("user")%>';

	var options = {
	 		   headers: {
	 		     Authorization: 'Bearer ' + '${jwtToken}' // Assuming 'jwtToken' holds the JWT
	 		   }
	 		 };
	
     var locationdata=[];
     var app = angular.module("app", []);
     app.controller("GetDataController", ['$scope','$http','$window', function(scope,http,window) {
      
    
  	  document.getElementById('overl').style.visibility = "hidden";
  	  document.getElementById('overlid').style.visibility = "hidden";
	    
    		scope.todayDate =moment().format('YYYY-MM-DD HH:mm:ss');
        	
        	
    	   /*******************************GET VEHICLES****************************************/
           http.get(apiURL+'getVehicles/'+userN+'/doGetVehicleClass',options).
   			then(function (response) {
   			if(response.data != null)
   				{
   				if(response.data.status==1){
   			              
   					scope.vehiclelist= response.data.myObjectList;
   							
   		         }else if(response.data.status==21){
		         }
   		         else{
   		        	scope.vehiclelist="";	 
   		         }  
   		   }else{
   		     	
   		      }      		       		
   			}, function (response) {
   			});
           
           
           /********************************GET SITE***************************************/
              http.get(apiURL+'getSite/'+userN+'/doGetSiteInfo',options).
   			then(function (response) {
   				Pace.stop();

   			  
   				if(response.data != null)
   				{
   				if(response.data.status==1){
   			        scope.sitedata= response.data.myObjectList;
   			        scope.locationdata=[];
   			        var locationarray=[];
   			        for(i=0; i<scope.sitedata.length;i++)
   			        	{
   			         scope.locationdata=scope.sitedata[i].locationDetail;
   			         locationarray.push(scope.locationdata);
   			         }
   			       scope.locationdata=locationarray;
   			   
   				}else if(response.data.status==21){
		        }else
   				{
   					scope.sitedata= "";
   		        }  
   		   }else{
   		     	
   		      }      		       		
   			}, function (response) {
   			});
      
           
           
           //****************SELECT LOCATION ACCORDING TO CITY WISE**************//
          // scope.masterlocation = [];
           scope.selectedlocation= function(VehicleModel){
        	  
        	
               scope.VehicleModel.masterlocation= VehicleModel.locationDetail.locationName;
   	       }   
           
           
           scope.addVehicle = function(responsedata){
        	 scope.submitted = true;
           	 /////////////////MODAL VALIDATION FOR NULL VALUE///////////
        	  if(typeof scope.VehicleModel  !== 'undefined'){
        		 
        		  if(scope.VehicleModel.hasOwnProperty("vehicleNumber")){
	    	  		   scope.vehicleNumber = scope.VehicleModel.vehicleNumber;
	    	  	   }else{
	    	  		   scope.vehicleNumber ="";
	    	  	   }
       		  
       		  if(scope.VehicleModel.hasOwnProperty("mVehicleClass")){
	    	  		   scope.mVehicleClass = scope.VehicleModel.mVehicleClass;
	    	  	   }else{
	    	  		   scope.mVehicleClass ="";
	    	  	   }
       		  if(scope.VehicleModel.hasOwnProperty("masterlocation")){
   	  		            scope.masterlocation = scope.VehicleModel.masterlocation;
   	  	            }else{
   	  		             scope.masterlocation ="";
   	  	            }
  		  
       		  if(scope.VehicleModel.hasOwnProperty("mSiteModel")){
	    	  		   scope.sitemodel=scope.VehicleModel.mSiteModel;
	    	  	   }else{
	    	  		   scope.sitemodel ="";
	    	  	   }
        	
       	
       		
			scope.dataRequest = {
				"vehicleNumber" :scope.vehicleNumber,
				"mVehicleClass" : scope.mVehicleClass,
				"inTime" : "",
				"outTime" : "",
				"mSiteModel" : scope.sitemodel,
				"userType":10,
				"userId":uid
				
			};
        	  
			scope.requestedData = {
					
					"username":userN,
					"functionName" : "doAddParkingInfo",
		  		     "data" : JSON.stringify(scope.dataRequest)

				};
        	  
			
			if (scope.add_vehicle.$valid) 
			{	
			  if(scope.vehicleNumber != "" && scope.mVehicleClass!= ""&& scope.sitemodel!= ""){
				document.getElementById('overlid').style.visibility = "visible"; 
        		  
	    	 	   http({
					method :'POST',
					url :apiURL+'addParking',
					data :scope.requestedData,
				    headers: options.headers

					}).then(function (response) {
	 					if(response.data.status==1){
	 						
	 						scope.response_message = response.data.message;
	 						var x = document.getElementById("snackbar_success")
 			 				x.className = "show";
 			 				setTimeout(function() {
 								x.className = x.className.replace("show", "");	
 								window.location.href = '${pageContext.servletContext.contextPath}/getVehicle'; 
 							}, 2000);
							
						}else if(response.data.status==0)
						{
							scope.message = response.data.message;
							var x = document.getElementById("snackbar_error")
 			 				x.className = "show";
 			 				setTimeout(function() {
 								x.className = x.className.replace("show", "");					 
 							}, 3000);
 			 			    document.getElementById('overlid').style.visibility = "hidden";
	 						
						}else if(response.data.status==21){
		  		        }
						
						else{
							scope.message = response.data.message;
							document.getElementById('overlid').style.visibility = "hidden";
					    } 
							
	  					      
					}, function (response) {
					}); 
				 
	     	  } else  {
	   				
				  scope.message = "Above Fields cannot be Left Blank !";
				  var x = document.getElementById("snackbar_error")
					x.className = "show";
					setTimeout(function() {
						x.className = x.className.replace("show", "");
					}, 2000);
                 
				 } 
			
			 }//end of if
        	  else{
        		  scope.message = "Above Fields cannot be Left Blank";
        		  var x = document.getElementById("snackbar_error")
					x.className = "show";
					setTimeout(function() {
						x.className = x.className.replace("show", "");
					}, 2000);
				
        	  }
           }//END VALIDATE
           else{
     		  scope.message = "Above Fields cannot be Left Blank";
     		  var x = document.getElementById("snackbar_error")
					x.className = "show";
					setTimeout(function() {
						x.className = x.className.replace("show", "");
					}, 2000);
				
     	  }
   		};
   		

       }]);
</script> 

</head>
 <body class="ng-cloak" ng-controller="GetDataController" ">
 
  <%@ include file="Sidebar.jsp"%>	
  
 
    <div class="content-wrap" >
 
     <!-- main page content. the place to put widgets in. usually consists of .row > .col-lg-* > .widget.  -->
      <main id="contentt" class="content" role="main" style=" padding-left: 60px;padding-right: 60px; padding-bottom: 70px;" class="ng-cloak">
        <!-- Page content -->
        <div class="row">
         <div class="col-lg-11">
           <section class="widget">
             <div class="widget-body">
               <form class="form-horizontal"  name="add_vehicle" novalidate autocomplete="off">
                    <fieldset>
                      <legend style="padding-bottom: 17px;font-size: 19px;"><strong>Register Vehicle</strong> </legend>
                        
                        <!-- Vehicle Number -->
                        <div class="form-group row">
                            <label for="vehicleNumber" class="col-md-4 form-control-label ">Vehicle Number</label>
                            <div class="col-md-7">
                                	<input type="text" class="form-control  " name="vehicleNumber"	placeholder="Enter Vehicle Number" ng-model="VehicleModel.vehicleNumber"
											id="vehicleNumber" onkeyup="alphaNumericChecker()"  required="required" />
											
									 <span style="color: red" ng-show="(add_vehicle.vehicleNumber.$dirty || submitted)&& add_vehicle.vehicleNumber.$error.required">
											Vehicle Number  is required !</span> 
									 
										  </div>
                        </div>
                        
                        <!-- Vehicle Class -->
                         <div class="form-group row">
                            <label for="mVehicleClass" class="col-md-4 form-control-label ">Vehicle Class</label>
                            <div class="col-md-7">
                            
                            	<select class="select2 form-control" name ="mVehicleClass" ng-model="VehicleModel.mVehicleClass"ng-options="s as s.className for s in vehiclelist track by s.classId"  id="mVehicleClass" required>
									<option value="" disabled selected>Select Vehicle Class  </option>
 									</select>
 									<span style="color:red" ng-show="(add_vehicle.mVehicleClass.$dirty || submitted) && add_vehicle.mVehicleClass.$error.required"> Select Vehicle Class  ! </span>	 
								 </div>
                        </div>
                        
                            <!--Location  -->
                          <div class="form-group row">
                            <label for="sitemodel" class="col-md-4 form-control-label ">Site Name</label>
                            <div class="col-md-7">
                            
                            	<select class="select2 form-control" name ="sitemodel"  ng-model="VehicleModel.mSiteModel"  ng-change="selectedlocation(VehicleModel.mSiteModel)"  ng-options="a as a.siteName for a in sitedata track by a.sid"  id="sitemodel" required>
									<option value="" disabled selected>Select Site  </option>
 									</select>
 									 <span style="color:red" ng-show="(add_vehicle.sitemodel.$dirty || submitted) && add_vehicle.sitemodel.$error.required"> Site Name is required ! </span>	 
								  </div>
                        </div>
                        
                        <!-- City -->
                        <div class="form-group row">
                            <label for="mlocation" class="col-md-4 form-control-label ">Location</label>
                            <div class="col-md-7">
                           
                            <input type="text" class="form-control  " name="masterlocation"	placeholder="Enter Location Name" ng-model="VehicleModel.masterlocation"
											id="masterlocation"  required="required" readonly="readonly" />
									
                            	<!-- <select class="select2 form-control" name ="masterlocation" ng-model="VehicleModel.masterlocation" id="masterlocation" required readonly="readonly">
									<option value="" disabled selected>Select Location  </option>
 									</select>
 									<span style="color:red" ng-show="(add_vehicle.mLocation.$dirty || submitted) && add_vehicle.mLocation.$error.required"> Location is required ! </span>	 
								 --> </div>
                        </div>
                       
                    
                    </fieldset>
                    <div class="form-actions bg-transparent">
                        <div class="row">
                            <div class="offset-md-4 col-md-7 col-12">
                           <button type="submit" ng-click="addVehicle(VehicleModel)"class="btn btn-primary" style="background-color: #002B49;" id="submitbtn">Register</button>
                            </div>
                        </div>
                    </div>
                </form> 
            </div>
        </section>
    </div>
    
  </div>
 	<div id="snackbar_success">{{response_message}}</div>
	<div id="snackbar_error">{{message}}</div>
	<!-- spinner -->
    <div class="loader-wrap " style="background-color: rgba(0, 0, 0, 0.4);" id="overlid">
		<i class="fa fa-spinner fa-spin"></i>
    </div>
	
 </main>
</div>
        <!-- The Loader. Is shown when pjax happens -->
      <div class="loader-wrap" id="overl">
            <i class="fa fa-circle-o-notch fa-spin-fast"></i>
        </div>
         <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
         <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
        
        <!-- common libraries. required for every page-->
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-pjax/jquery.pjax.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/popper.js/dist/umd/popper.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/dist/js/bootstrap.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/js/dist/util.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/widgster/widgster.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" data-pace-options='{ "target": ".content-wrap", "ghostTime": 1000 }'></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/hammerjs/hammer.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-hammerjs/jquery.hammer.js"></script>


        <!-- common app js -->
        <script src="${pageContext.request.contextPath}/resources/js/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/app.js"></script>

        <!-- Page scripts -->
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.animator/jquery.flot.animator.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.selection.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.time.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.pie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.stack.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.crosshair.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.symbol.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.dashes/jquery.flot.dashes.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap_calendar/bootstrap_calendar/js/bootstrap_calendar.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>
   <script>
   //TO CHECK INPUT FIELDS ONLY CONTAING ALPHA_NUMERIC
	  	 function alphaNumericChecker() {
			 
         var fvehicleNumber = document.getElementById('vehicleNumber');
         fvehicleNumber.value = fvehicleNumber.value.replace(/[^A-Z0-9 ]+/, '');
         }
   </script>  
    </body>

</html>