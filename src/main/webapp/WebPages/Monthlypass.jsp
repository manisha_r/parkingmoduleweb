<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
<meta charset="ISO-8859-1">
<title>Monthly Pass | VPark</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="Sing App - Bootstrap 4 Admin Dashboard Template">
        <meta name="keywords" content="bootstrap admin template,admin template,admin dashboard,admin dashboard template,admin,dashboard,bootstrap,template">
        <meta name="author" content="Flatlogic LLC">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <!-----------------------------------------main css----------------------------------------------------->
        <link href="${pageContext.request.contextPath}/resources/css/application.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png">
        <link rel="shortcut icon" type="image/x-icon"  href="${pageContext.request.contextPath}/resources/img/logo.png">
        
        
        <!-----------------------------------------------font style--------------------------------------------->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/line-awesome/dist/line-awesome/css/line-awesome.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
		
		<!----------------------------------------------progressbar--------------------------------------------------------->
		<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
		
		<!----------------------------------------------Jquery------------------------------------------------------>
        <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>   
 
        <!-----------------------------------------dataTable------------------------------------------------------>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>
        <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        
        <!-----------------------------------------------ANGUARJS----------------------------------------------->
        <script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>
	
        <!---------------------------------- API URL ------------------------------------------------------>
        <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script> 
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/webmain.css">
	    
        <!-- Datatables css-->
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"rel="stylesheet">
	    <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
	    rel="stylesheet">
	    <!-- Datatables js-->
        <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.14.2.js"></script>
 	
 	
 	    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
        <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>
 
        <!---------------------------------------DATEPICKER-------------------------------------------------------->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/webmain.css">
        
        <!---------------------------------- SweetAlert  ------------------------------------------------------>
        <script src="${pageContext.request.contextPath}/resources/sweetAlert.js"></script> 
	     
 
        <style>
        
        
        #users_length, #users_info {
	      display: none;
          }
       
       .dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
         cursor: not-allowed;
         border: 1px solid #fff!important;
         background-color: #ffff!important;
         border: 1px solid transparent;
         background: transparent;
         box-shadow: none;
         color: #333 !important;
         background: linear-gradient(to bottom, #fff 0%, #fff 100%)!important;
        } 
        
	  .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
	    
	    color: #333 !important; 
	    cursor: none;
	    background-color: #411616 !important;
	    background: -webkit-linear-gradient(top, #fff 0%, #dcdcdc 100%) !important;
	    background: -moz-linear-gradient(top, #fff 0%, #dcdcdc 100%) !important;
	    background: -ms-linear-gradient(top, #fff 0%, #dcdcdc 100%) !important;
	    background: -o-linear-gradient(top, #fff 0%, #dcdcdc 100%)!important;
	    background: linear-gradient(to bottom, #fff 0%, #dcdcdc 100%) !important;
	 }    
	 .previous {
	     text-decoration: none;
	     display: inline-block;
		 padding: 5px 14px;
	     background-color: #ffff !important;
	     cursor: pointer;
	     FONT-SIZE: 14PX;
	     }
        </style>
        
        
       <script>

	      $(document).ready(function(){
	
	       $("#kgetMonthlyPass").addClass("active");
	
		   });

	
		/**********************************SESSION ********************************************************** */
		var uid = '<%=session.getAttribute("uid")%>';
		var userN = '<%=session.getAttribute("user")%>';
		
		var options = {
		 		   headers: {
		 		     Authorization: 'Bearer ' + '${jwtToken}' // Assuming 'jwtToken' holds the JWT
		 		   }
		 		 };
	
		var jDataTable;
		var jData;
		var app = angular.module("app", []);
		app.controller("GetDataController", ['$scope','$http','$window', function(scope,http,window) {
		  
			scope.requestData =
		    {
				"data" : "",
				"username" : uid,
				"basicAuth" : ""
		     };
		 
			
			document.getElementById('overlid').style.visibility = "hidden";
			
		
		   
			//******************************MonthlyPass LIST******************************//
			
			   
			  scope.getMonthlyPass = function () { 
			     
					  http.get(apiURL+'getMonthlyPass/'+userN+'/doGetMonthlyPass',options).
						then(function (response) {
						  console.log(response.data);
							if(response.data != null)
		  		    		{
			  	 		  	   	if(response.data.status==1){
									jData = [];
				              		jData= response.data.myObjectList;
				              		jDataTable.clear().rows.add(jData).draw();
				              		scope.totalRecord= response.data.totalRecord;
						            scope.totalPages=response.data.totalPages;
										 
								}else if(response.data.status==21){
			   		  				 
				  		        }else
								{
									 jData = [];
					            	 jDataTable.clear().rows.add(jData).draw();
									
								} 
			  	 		  	   	
		  		    	    }else{
		  		    	    	
		   		          	 jData = [];
		   		        	 jDataTable.clear().rows.add(jData).draw();
		   		        	 Pace.stop();
		   		        	 
		   		             } 
							Pace.stop();

					       document.getElementById('overl').style.visibility = "hidden";

						}, function (response) {
						});
					
						 
		       }; 
				
			
		/*********************************MonthlyPass DATA*************************************/
		$(document).ready(function() {
			 jDataTable = $('#MonthlyPass').DataTable({ 
		 		 	data: jData,
			         		columns: [
			         			
				             	{'data':'mSiteModel.siteName'},

			         			
			         			{'data':'mPublicRegisterVehicleDetails', 'render': function(mPublicRegisterVehicleDetails){
				             		
				             		if(mPublicRegisterVehicleDetails!=""){
				             			
				             			return mPublicRegisterVehicleDetails.vehicleNumber;
				             			
				             		}else{
				             			
				             			return '<p>__</p>';
				             		}
				            }},
			         			
				             	{'data':'mPublicRegisterVehicleDetails', 'render': function(mPublicRegisterVehicleDetails){
				             		
				             		if(mPublicRegisterVehicleDetails !=null){
				             			
		 		             			if(mPublicRegisterVehicleDetails.mVehicleClassModel.className =='CAR'){
				             				return '<p><i class="fa fa-car text-primary" style="font-size:15px;"></i></p>';
				             				
				             			}else if(mPublicRegisterVehicleDetails.mVehicleClassModel.className =='TWO WHEELER'){
				             				
				             				return '<p><i class="fa fa-motorcycle text-primary" style="font-size:15px;"></i></p>';
				             				
				             			}else if(mPublicRegisterVehicleDetails.mVehicleClassModel.className =='BUS'){
				             				
				             				return '<p><i class="fa fa-bus text-primary" style="font-size:15px;"></i></p>';
				             				
				             			}else{
				             				return mPublicRegisterVehicleDetails.mVehicleClassModel.className;
				             			}
				             			
				             		}else{
				             			
				             			return '<p>__</p>';
				             		}
				             	}},
				             	
				           
					            
			             	{'data':'inTime', 'render': function(inTime){
			             		
			             		if(inTime!= null)
			             		{
			             		if(inTime!=""){
			             			
			             			return  moment(inTime).format('YYYY-MM-DD, h:mm a');
			             		}else{
			             			
			             			return '<p>__</p>';
			             		}
			             		}else{
			             			return '<p>__</p>';
			             		}	
			             	}},
			             	{'data':'outTime', 'render': function(outTime){
			             		
			             		if(outTime!= null)
			             		{
			             		if(outTime!=""){
			             			
			             			return  moment(outTime).format('YYYY-MM-DD, h:mm a');
			             		}else{
			             			
	 		             			return '<p>__</p>';
		 	             		}
		 	             		}else{
	 		             			
		 	             			return '<p>__</p>';
		 	             		}
			             	}},

                            {'data':'durationinDay', 'render': function(durationinDay){
			             		
			             		if(durationinDay!= null)
			             		{
			             		if(durationinDay!=""){
			             			
			             			return  durationinDay + " Days";
			             		}else{
			             			
	 		             			return '<p>__</p>';
		 	             		}
		 	             		}else{
	 		             			
		 	             			return '<p>__</p>';
		 	             		}
			             	}},
			             	{'data':'mPublicRegisterVehicleDetails', 'render': function(mPublicRegisterVehicleDetails){
			             		
			             		if(mPublicRegisterVehicleDetails!=""){
			             			
			             			return mPublicRegisterVehicleDetails.mPublicUserModel.mobileNumber;
			             			
			             		}else{
			             			
			             			return '<p>__</p>';
			             		}
			                   }},

			             	{'data':'passCost', 'render': function(passCost){
			             		
			             		if(passCost!=null){
			             			
			             			return passCost +" Rs.";
			             			
			             		}else{
			             			
			             			return '<p>__</p>';
			             		}
			            }},
			        	{'data':'passPaymentStatus', 'render': function(passPaymentStatus){
		             		
		             		if(passPaymentStatus!=null){
		             			
		             			
		             			if(passPaymentStatus==1)
		             				{
	 			 	         		return '<p class="text-success"><strong>Paid</strong></p>';
		             				}
		             			else
		             				{
	 			 	         		return '<p class="text-danger"><strong>Not Paid</strong></p>';
		             				}
		             			
		             		}else{
		             			
		             			return '<p>__</p>';
		             		}
		            }}

			         			         
			             	
			       
			            ],
			           buttons: [
			        	    'copy', 'excel', 'pdf'
			        	  ],
		
			 	             "ordering": false,
			 	             "searching": false,
			 	             "deferRender": true,
			 	             "autoWidth": false,
			 	             "lengthChange": false,
			 	             "language": {
			     	    		"infoEmpty": "No records to show",
			         	    	"emptyTable": "no record found",
			     	    	  },
			                   "columnDefs": [
				                  { "width": "7%", "targets": 7 }
				                  ]			    	        }); 
		  }); 
		
		
		   
		   
		//**************SELECT DESELECT TABLE ROW******************************//
		$('#MonthlyPass').on('click', 'tr', function () {
			
				 if ( $(this).hasClass('row_selected'))
			  		{  
					   $("#MonthlyPass tbody tr").removeClass('row_selected');
			  	       $(this).removeClass('row_selected');
			      }else
			  	        {
			  	        $("#MonthlyPass tbody tr").removeClass('row_selected');
			  	        $(this).addClass('row_selected');
			  	 
			  	    }
			}); 
		
	     	/* date */
			scope.formatDate = function(date){
				  return new Date(date)
			}
			
			 scope.AddPayment=function (pid,amount)
			 {
				  scope.dataRequest = {
				   			"pid":pid,
				   			"amountPaid":amount,
				   			"paymentMode":1, 
				   			"receiptNumber":"",
				   			"transactionId":"",
				   			"bankCode":""
				   		};
				       scope.requestedData = {
							"username":userN,
							"functionName" : "doUpdatePaymentInfoTable", //
				  			 "data" : JSON.stringify(scope.dataRequest)
		               };
					   http({
							method :'POST',
							url :apiURL+'updatePaymentInfo', 
							data :scope.requestedData,
						    headers: options.headers

							}).then(function (response) {
								if(response.data.status==1){
								      // scope.getMonthlyPass();
									    document.getElementById('overlid').style.visibility = "hidden";
										scope.parkingcharges=response.data.myObjectList;
										$('#exampleModal').modal('show');
	
								}else if(response.data.status==21){
			   		  				 
				  		        }else
								{
		 							scope.message = response.data.message;
									var x = document.getElementById("snackbar_error")
					 				x.className = "show";
					 				setTimeout(function() {
										x.className = x.className.replace("show", "");					 
									}, 3000);
					 			    document.getElementById('overlid').style.visibility = "hidden";
					 			    $('#exampleModal').modal('hide');
									
								} 
							}, function (response) {
							});
			 }
		
			 
			 //*************************UPDATE CLICK*******************************//
		  	$('#MonthlyPass').on('click','#edit', function () {
			 	scope.intime="";
		  	   	scope.vehiclenumber="";
		  	   	scope.amount="";
		  	   	scope.outime="";
		  		var tr = $(this).closest('tr');
		  		var selectedData = jDataTable.row(tr).data();
		   	    scope.UserModel =selectedData;
		   		scope.rowdata =selectedData;
		   	    document.getElementById('overlid').style.visibility = "visible";
		   	    
		    	scope.minutes = 0;
			    scope.hours = 0;
			    scope.days = 0;
		   	
		   	   /*******************************GET PAYMENT****************************************/
		          scope.dataRequest = {
			   			"pid":selectedData.pid,
			   			"siteId":selectedData.mSiteModel.sid,
			   			"vehicleId":selectedData.mVehicleClass.classId,
			   			"inTime":selectedData.inTime
			   		};
			       scope.requestedData = {
						"username":userN,
						"functionName" : "doGetParkingCharges",
			  			 "data" : JSON.stringify(scope.dataRequest)
	               };
				   http({
						method :'POST',
						url :apiURL+'getParkingcharges',
						data :scope.requestedData,
					    headers: options.headers

						}).then(function (response) {
							if(response.data.status==1){
							      //scope.getMonthlyPass();
								    document.getElementById('overlid').style.visibility = "hidden";
									scope.parkingcharges=response.data.myObjectList;
									var pid=scope.parkingcharges.pid;
									scope.intime=scope.parkingcharges.inTime;
									scope.vehiclenumber=scope.parkingcharges.vehicleNumber;
									scope.outime=scope.parkingcharges.outTime;
									scope.amount=scope.parkingcharges.amount;
									var famount=scope.parkingcharges.amount;
								    scope.sitename=scope.parkingcharges.mSiteModel.siteName;
								    scope.AddPayment(pid,famount);
								  
								    var timestamp1 = new Date(scope.intime).getTime();
								    var timestamp2 = new Date(scope.outime).getTime();
	
								    var milliseconds = timestamp2 - timestamp1
								    
	 							    var seconds = milliseconds / 1000;
								    var minutes = Math.floor(seconds / 60);
								    var hours = Math.floor(minutes / 60) ;
								    var days = Math.floor(hours / 24) ;
								    
								    scope.minutes = minutes% 60;
								    scope.hours = hours% 24;
								    scope.days = days% 365;
								    
							   		$('#exampleModal').modal('show');
							   		scope.getData();	
	
							}else if(response.data.status==21){
		   		  				 
			  		        }else
							{
	 							scope.message = response.data.message;
								var x = document.getElementById("snackbar_error")
				 				x.className = "show";
				 				setTimeout(function() {
									x.className = x.className.replace("show", "");					 
								}, 3000);
				 			    document.getElementById('overlid').style.visibility = "hidden";
				 			    $('#exampleModal').modal('hide');
								
							} 
						}, function (response) {
						});  
	            }); 
		   
		   
		   
	       //*****************************RESET BUTTON*********************//
			
			scope.btnreset= function(){
				  scope.filterModel= "";
				  $('#myDatepicker1').data("DateTimePicker").clear();
			   	  $('#myDatepicker2').data("DateTimePicker").clear();
				  scope.fromdate = null; 
			      scope.todate= null;
			      scope.pageNumber =0;
		  		  scope.lowerlimit = 0;
		  		  scope.upperlimit=10;
		  		  scope.pageSize= 10;
		  		  scope.totalrecord=0;
		  		  scope.totalPages =0;
			      scope.getMonthlyPass();
			};
			 
		
		}]);
       
		</script>
</head>
<body class="" ng-controller="GetDataController" ng-init="getMonthlyPass()">
 
  <%@ include file="Sidebar.jsp"%>	
    
  
  
  <div class="content-wrap">
    
    <!-- main page content. the place to put widgets in. usually consists of .row > .col-lg-* > .widget.  -->
    <main id="contentt" class="content" role="main" class="ng-cloak">
      <!-- Page content -->
      
       <!--Heading  -->
     
      
                
      <!-- Table Content -->
        <div class="row">
            <div class="col-lg-12">
                <section class="widget">
                  <header>
                         <h5>All Monthly Pass<span class="fw-semi-bold"></span>
                        </h5>
                    </header>
                    <div class="widget-body">
                    <div class="table-responsive">
                      <table class="table table-striped dataTable  table-lg mt-lg mb-0" id="MonthlyPass">
                      <thead class=" text-primary">
                      <tr>
                                	        <th style=" text-transform: revert; font-size: 15px;">Site</th>
                                 			<th style=" text-transform: revert; font-size: 15px;">Vehicle Number</th>
                                			<th style=" text-transform: revert; font-size: 15px;">Type</th>
											<th style=" text-transform: revert; font-size: 15px;">Start Date</th>
											<th style=" text-transform: revert; font-size: 15px;">End Date</th>
											<th style=" text-transform: revert; font-size: 15px;">Duration</th>
											<th style=" text-transform: revert; font-size: 15px;">Contact Number</th>
											<th style=" text-transform: revert; font-size: 15px;">Cost</th>
											<th style=" text-transform: revert; font-size: 15px;">Payment Status</th>
											
											
										
											
						 </tr>
                         </thead>
                       </table>
                       
                       
                        <div class="clearfix">
                     </div>
                    </div>
                    </div>
                </section>
            </div>
        

            
        </div>
        <div id="snackbar_success">{{response_message}} </div>
		<div id="snackbar_error">{{message}} </div>
		
		<!-- spinner -->
        <div class="loader-wrap" style="background-color: rgba(0, 0, 0, 0.4);" id="overlid">
		<i class="fa fa-spinner fa-spin"></i>
        </div>
        
        </main>
        </div>
              
        <div class="loader-wrap"id="overl">
            <i class="fa fa-circle-o-notch fa-spin-fast"></i>
        </div> 
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-pjax/jquery.pjax.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/popper.js/dist/umd/popper.js"></script>
         <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/dist/js/bootstrap.js"></script>
       <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/js/dist/util.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/widgster/widgster.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" data-pace-options='{ "target": "body", "ghostTime": 1000 }'></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/hammerjs/hammer.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-hammerjs/jquery.hammer.js"></script>


        <!-- common app js -->
        <script src="${pageContext.request.contextPath}/resources/js/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/app.js"></script>

        <!-- Page scripts -->
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.animator/jquery.flot.animator.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.selection.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.time.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.pie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.stack.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.crosshair.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.symbol.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.dashes/jquery.flot.dashes.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap_calendar/bootstrap_calendar/js/bootstrap_calendar.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>
       
        <!-- bootstrap-daterangepicker -->	
       <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
         
      
         <script>
       
       $('#myDatepicker1').datetimepicker({
   		
           format: 'YYYY-MM-DD' 
  		//format: 'YYYY-MM-DD  h:mm:ss a'
        
         });
	  	$('#myDatepicker2').datetimepicker({
	          format: 'YYYY-MM-DD'
	  	//	format: 'YYYY-MM-DD  h:mm:ss a'
	         });
	  	 
	     //TO CHECK INPUT FIELDS ONLY CONTAING ALPHA_NUMERIC
	  	  	 function alphaNumericChecker() {
	  			 
	           var fvehicleNumber = document.getElementById('vehicleNumber');
	           fvehicleNumber.value = fvehicleNumber.value.replace(/[^a-zA-Z0-9 ]+/, '');
	           }
       </script>
    </body>

</html>










