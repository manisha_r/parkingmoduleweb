<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="app">
<head>
<meta charset="ISO-8859-1">
<title>Parking Information | VPark</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="Sing App - Bootstrap 4 Admin Dashboard Template">
        <meta name="keywords" content="bootstrap admin template,admin template,admin dashboard,admin dashboard template,admin,dashboard,bootstrap,template">
        <meta name="author" content="Flatlogic LLC">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <!-----------------------------------------main css----------------------------------------------------->
        <link href="${pageContext.request.contextPath}/resources/css/application.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png">
        <link rel="shortcut icon" type="image/x-icon"  href="${pageContext.request.contextPath}/resources/img/logo.png">
        
        
        <!-----------------------------------------------font style--------------------------------------------->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/line-awesome/dist/line-awesome/css/line-awesome.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
		
		<!----------------------------------------------progressbar--------------------------------------------------------->
		<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
		
		<!----------------------------------------------Jquery------------------------------------------------------>
        <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>   
 
        <!-----------------------------------------dataTable------------------------------------------------------>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>
        <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        
        <!-----------------------------------------------ANGUARJS----------------------------------------------->
        <script src="${pageContext.request.contextPath}/resources/angularjs/angularv1.8.3.min2.js"></script>
        <!---------------------------------- API URL ------------------------------------------------------>
        <script src="${pageContext.request.contextPath}/resources/API_URL.js"></script> 
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/webmain.css">
	    
        <!-- Datatables css-->
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"rel="stylesheet">
	    <link href="${pageContext.request.contextPath}/resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
	    rel="stylesheet">
	    <!-- Datatables js-->
        <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
	    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/logindemo/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.14.2.js"></script>
 	
 	
 	    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/snackbar.css">
        <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>
 
        <!---------------------------------------DATEPICKER-------------------------------------------------------->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/webmain.css">
    
    	  <!---------------------------------- SweetAlert  ------------------------------------------------------>
          <script src="${pageContext.request.contextPath}/resources/sweetAlert.js"></script> 
    
    
        <style>
        
        
         #users_length, #users_info {
	      display: none;
     }
       .dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
    cursor: not-allowed;
    border: 1px solid #fff!important;
    background-color: #ffff!important;
    /* color: #361313 !important; */
    border: 1px solid transparent;
    background: transparent;
    box-shadow: none;
    color: #333 !important;
    background: linear-gradient(to bottom, #fff 0%, #fff 100%)!important;
} 
 .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
    
    color: #333 !important; 
       cursor: none;
    /* border: 1px solid #979797; */
    background-color: #411616 !important;
    /* background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #fff), color-stop(100%, #dcdcdc)); */
    background: -webkit-linear-gradient(top, #fff 0%, #dcdcdc 100%) !important;
    background: -moz-linear-gradient(top, #fff 0%, #dcdcdc 100%) !important;
    background: -ms-linear-gradient(top, #fff 0%, #dcdcdc 100%) !important;
    background: -o-linear-gradient(top, #fff 0%, #dcdcdc 100%)!important;
    background: linear-gradient(to bottom, #fff 0%, #dcdcdc 100%) !important;
}    
 .previous {
     text-decoration: none;
     display: inline-block;
	 padding: 5px 14px;
     background-color: #ffff !important;
     cursor: pointer;
     FONT-SIZE: 14PX;
     }
        </style>
<script>

      $(document).ready(function(){

       $("#kgetVehicle").addClass("active");

	   });


	/**********************************SESSION ********************************************************** */
	var uid = '<%=session.getAttribute("uid")%>';
	var userN = '<%=session.getAttribute("user")%>';
	
	var options = {
	 		   headers: {
	 		     Authorization: 'Bearer ' + '${jwtToken}' // Assuming 'jwtToken' holds the JWT
	 		   }
	 		 };	
	
	var jDataTable;
	var jData;
	var app = angular.module("app", []);
	app.controller("GetDataController", ['$scope','$http','$window', function(scope,http,window) {
	  
		scope.requestData =
	    {
			"data" : "",
			"username" : uid,
			"basicAuth" : ""
	     };
	 
		
		document.getElementById('overlid').style.visibility = "hidden";
		scope.formdate =moment().format('YYYY-MM-DD HH:mm:ss');
		scope.currentdate =moment().format('YYYY-MM-DD');
		
	   /*******************************GET VEHICLES****************************************/
	    http.get(apiURL+'getVehicles/'+userN+'/doGetVehicleClass',options).
			then(function (response) {
			if(response.data != null)
				{
				if(response.data.status==1){
					scope.vehiclelist= response.data.myObjectList;
		         }else if(response.data.status==21){
		  				 
		         }else{
		        	scope.vehiclelist="";	 
		         }  
		   }else{
			   scope.vehiclelist="";	
		      }      		       		
			}, function (response) {
			});
		 /********************************GET SITE***************************************/
	    http.get(apiURL+'getSite/'+userN+'/doGetSiteInfo',options).
			then(function (response) {
				Pace.stop();
			  
				if(response.data != null)
				{
				if(response.data.status==1){
			        scope.sitedata= response.data.myObjectList;
			     }else if(response.data.status==21){
		  				 
		         }else
				 {
					scope.sitedata= "";
		        }  
		   }else{
			   scope.sitedata= "";
		      }      		       		
			}, function (response) {
			});
		 
		
		 
	    scope.fromdate =null;
	    scope.todate =null;
	    scope.vehicleNumber =null;
	    scope.type =null;
	    scope.siteId =null;
	    scope.userType =null;
	   
		//******************************PARKING LIST******************************//
		
		      scope.pageNumber =0;
   		       scope.lowerlimit = 0;
   		       scope.upperlimit=10;
   		       scope.pageSize = 10;
   		       scope.totalRecord=0;
   		       scope.totalPages =0;
   		       
		  scope.GetParking = function () { 
		       
 					scope.dataRequest = {
 			    			
 							"startdate":scope.currentdate
 			    	};	
					
			   scope.requestedData = {
						"username":userN,
						"functionName" : "doGetFilterData",
						"pageNo":  scope.pageNumber,
		  				"pageSize": 10,
			  			"data" : JSON.stringify(scope.dataRequest)
	            };
	    	
	 			   http({
						method :'POST',
						url :apiURL+'getFilterData',
						data :scope.requestedData,
					    headers: options.headers

						}).then(function (response) {
						if(response.data != null)
		  		    		{
		  	 		  	   	if(response.data.status==1){
								jData = [];
			              		jData= response.data.myObjectList;
			              		jDataTable.clear().rows.add(jData).draw();
			              		scope.totalRecord= response.data.totalRecord;
					            scope.totalPages=response.data.totalPages;
 			              		Pace.stop();
						        document.getElementById('overl').style.visibility = "hidden";
									 
							}else if(response.data.status==21){
		  		  				 
			  		        } else
							{
								 jData = [];
				            	 jDataTable.clear().rows.add(jData).draw();
								 Pace.stop();
							     document.getElementById('overl').style.visibility = "hidden";
								
							} 
		  		    	  }else{
		   		          	 jData = [];
		   		        	 jDataTable.clear().rows.add(jData).draw();
		   		        	 Pace.stop();
					         document.getElementById('overl').style.visibility = "hidden";
		   		           } 
		   	 		  	   	
						}, function (response) {
						});
	       };  	
	
	
	       scope.pageNumber =0;
	       scope.lowerlimit = 0;
	       scope.upperlimit=10;
	       scope.pageSize= 10;
	       scope.totalRecord=0; 
   
    //*************************FILTER BUTTON *********************************************//
    scope.btnfilter = function(filterModel){
    	  
    	       scope.pageNumber =0;
		       scope.lowerlimit = 0;
		       scope.upperlimit=10;
		       scope.pageSize= 10;
		       scope.totalRecord=0;
		     
		   document.getElementById('overlid').style.visibility = "visible"; 
		  		 
    	   
		//************DATE FEILDS***************//
		 var sDateObj = $('#myDatepicker1').data('DateTimePicker').date();
		 if(sDateObj != null){
    		  scope.fromdate = moment(sDateObj).format('YYYY-MM-DD HH:mm:ss');
	    	   }else
    		   {
    		   scope.fromdate = null;
    		   }
		 
		 var endDateObj = $('#myDatepicker2').data('DateTimePicker').date();
		 if(endDateObj != null){
    		  scope.todate = moment(endDateObj).format('YYYY-MM-DD HH:mm:ss');
	    	   }else
    		   {
    		   scope.todate = null;
    		   }
	   
    	
		//model validation//
		   if(typeof scope.filterModel  !== 'undefined'){
				
			   
	  			if(scope.filterModel.hasOwnProperty("vehicleNumber")){
	    	  		   scope.vehicleNumber = scope.filterModel.vehicleNumber;
	    	  	   }else{
	    	  		   scope.vehicleNumber =null;
	    	  	   }
	  			
	  			if(scope.filterModel.hasOwnProperty("mVehicleClass")){
	    	  		   scope.type = scope.filterModel.mVehicleClass.classId;
	    	  	   }else{
	    	  		   scope.type =null;
	    	  	   }
	  			
	  			if(scope.filterModel.hasOwnProperty("mSiteModel")){
	    	  		   scope.siteId = scope.filterModel.mSiteModel.sid;
	    	  	   }else{
	    	  		   scope.siteId =null;
	    	  	   }
	  			
	  			
	  			if(scope.filterModel.hasOwnProperty("userType")){
	    	  		   scope.userType = scope.filterModel.userType;
	    	  	   }else{
	    	  		   scope.userType =null;
	    	  	   }
		   }
	   
		if(scope.fromdate != null  || scope.vehicleNumber != null){
			
	
    	scope.dataRequest = {
    			"startdate":scope.fromdate,
    			"enddate": scope.todate,
    			"vehicleNumber":scope.vehicleNumber,
    			"vehicle_type": scope.type,
    			"userType":scope.userType,
    			"siteId":scope.siteId
    	};  
    	
    	scope.requestedData = {
					"username":userN,
					"functionName" : "doGetFilterData",
					"pageNo":  scope.pageNumber,
	  				"pageSize": 10,
		  			"data" : JSON.stringify(scope.dataRequest)
            };
    	
    	
 			   http({
					method :'POST',
					url :apiURL+'getFilterData',
					data :scope.requestedData,
				    headers: options.headers

					}).then(function (response) {	
 					if(response.data != null)
	  		    		{
						if(response.data.status==1){
							jData = [];
		              		jData= response.data.myObjectList;
 		              		scope.totalRecord= response.data.totalRecord;
		              		jDataTable.clear().rows.add(jData).draw();
 		              	    document.getElementById('overlid').style.visibility = "hidden";
 		              	    $("#users_previous").addClass("disabled");
              		   
              		   
 		              	  //  $("#users_previous").removeClass("disabled");
						}else if(response.data.status==21){
	  		  				 
		  		        } else
						{
							 jData = [];
			            	 jDataTable.clear().rows.add(jData).draw();
			 			     document.getElementById('overlid').style.visibility = "hidden";
						} 
	  		    	  }else{
	   		          	 jData = [];
	   		        	 jDataTable.clear().rows.add(jData).draw();
	   		             document.getElementById('overlid').style.visibility = "hidden";
	   		           } 
	   	 		  	   	
					}, function (response) {
					});
		}
		else{  //end modal validation
 			 
			    scope.vehicleNumber =null;
	            scope.type =null;
	            scope.userType =null;
	            scope.siteId =null;
	            scope.message = "Select at least one field to search !";
			     var x = document.getElementById("snackbar_error")
				 x.className = "show";
			     
				setTimeout(function() {
					x.className = x.className.replace("show", "");					 
				}, 3000);
				
			   document.getElementById('overlid').style.visibility = "hidden";
	           
	           
			}                
			
			
    	
    };
	 
	 
		 
	 //***************NEXT PREVIOUS CALL*********************//
	  		scope.getData = function(){
	  				        	 
	  			// scope.fromdate = null;
		         if(scope.fromdate!=null)
		        	 {
		        	
		        	  scope.fromdate=scope.fromdate;
		        	  
		        	 }else{
		        		
		        		 scope.fromdate = scope.currentdate;
		        		
		        	 }
		             scope.dataRequest = {
		        			
		        			"startdate":scope.fromdate,
		        			"enddate": scope.todate,
		        			"vehicleNumber":scope.vehicleNumber,
		        			"vehicle_type": scope.type,
		        			"userType":scope.userType,
		        			"siteId":scope.siteId
		        			
		        	};  
		        	
		        	scope.requestedData = {
		    					"username":userN,
		    					"functionName" : "doGetFilterData",
		    					"pageNo":  scope.pageNumber,
		    	  				"pageSize": 10,
		    		  			"data" : JSON.stringify(scope.dataRequest)
		                };
		        	
		        	
		     			   http({
		    					method :'POST',
		    					url :apiURL+'getFilterData',
		    					data :scope.requestedData,
		    				    headers: options.headers

		    					}).then(function (response) {	
		     					if(response.data != null)
		    	  		    	{
		    						if(response.data.status==1)
		    						{
				              	    jData = [];
				              	    jDataupdate =[];
				              		jData= response.data.myObjectList;
				              	  	scope.totalRecord= response.data.totalRecord;
					                scope.totalPages=response.data.totalPages;
				              		jDataTable.clear().rows.add(jData).draw();
								    scope.todayDate=null;
					                document.getElementById('overlid').style.visibility = "hidden";
						        
						       }else if(response.data.status==21){
			  		  				 
			  		           }else{
			             	       jData = [];
			            	       jDataTable.clear().rows.add(jData).draw();
			            	       document.getElementById('overlid').style.visibility = "hidden";
			            	  }  
		            }else{
		              	 jData = [];
		             	 jDataTable.clear().rows.add(jData).draw();
     		        	 document.getElementById('overlid').style.visibility = "hidden";
		           } 
	 		  	   	
		    					}, function (response) {
								});

		 };
	 
	 
	 
	/*********************************PARKING DATA*************************************/
	$(document).ready(function() {
		 jDataTable = $('#users').DataTable({ 
	 		 	data: jData,
		         		columns: [
		         			{'data':'pid'},
		             	    {'data':'vehicleNumber', 'render': function(vehicleNumber){
		             		
		             		if(vehicleNumber!=""){
		             			
		             			return vehicleNumber;
		             			
		             		}else{
		             			
		             			return '<p>__</p>';
		             		}
		             	}},
		             	{'data':'mVehicleClass', 'render': function(mVehicleClass){
		             		
		             		if(mVehicleClass !=null){
		             			
 		             			if(mVehicleClass.className =='CAR'){
		             				return '<p><i class="fa fa-car text-primary" style="font-size:15px;"></i></p>';
		             				
		             			}else if(mVehicleClass.className =='TWO WHEELER'){
		             				
		             				return '<p><i class="fa fa-motorcycle text-primary" style="font-size:15px;"></i></p>';
		             				
		             			}else if(mVehicleClass.className =='BUS'){
		             				
		             				return '<p><i class="fa fa-bus text-primary" style="font-size:15px;"></i></p>';
		             				
		             			}else{
		             				return mVehicleClass.className;
		             			}
		             			
		             		}else{
		             			
		             			return '<p>__</p>';
		             		}
		             	}},
		             	{'data':'inTime', 'render': function(inTime){
		             		
		             		if(inTime!= null)
		             		{
		             		if(inTime!=""){
		             			
		             			return  moment(inTime).format('YYYY-MM-DD, h:mm a');
		             		}else{
		             			
		             			return '<p>__</p>';
		             		}
		             		}else{
		             			return '<p>__</p>';
		             		}	
		             	}},
		             	{'data':'outTime', 'render': function(outTime){
		             		
		             		if(outTime!= null)
		             		{
		             		if(outTime!=""){
		             			
		             			return  moment(outTime).format('YYYY-MM-DD, h:mm a');
		             		}else{
		             			
 		             			return '<p>__</p>';
	 	             		}
	 	             		}else{
 		             			
	 	             			return '<p>__</p>';
	 	             		}
		             	}},
		             	 
		             	{'data':'mSiteModel.siteName'},
		             	
						{'data': "status", 'render':function(status, data, row){
	         				
	         			  if(status==0 && row.userType ==11 || status==0 && row.userType==10){
	         					return '<p class="text-danger"><strong>Exited <i class="fa fa-arrow-right"></i></strong></p>';
		 	         		} 
	         				 
	         				 
	         				 if(status==1 && row.userType==10)
	         			    {

  		             			return '<p><span class="badge badge-success">Parked</span></p>'
  		             			
 			 	         	} 
	         				 
	         			  if(status==1 && row.userType==11){
 			 	         		return '<p class="text-success"><strong>Booked By User</strong></p>';
 			 	         		
 			 	         	} else{
 			 	         		return '<p class="text-success">__</p>'
 			 	         	} 
	         				
 	         			}} ,
		             	
		             	
		             	{'data': "userType", 'render':function(userType){
	         				
		             		if(userType !=null){
		             			
		             			if(userType==10){
		         					return '<p class="text-info"><strong>Operator</strong></p>';
			 	         			}
		         				else if(userType==11)
		         			    {
		         					return '<p class="text-info"><strong>User</strong></p>';
		         					
	 			 	         	}else{
	 			 	         		return '<p>__</p>';
	 			 	         	}
		             		}
	         				
	         			}} ,
		         
		             	  {'data': "userType", 'render':function(userType, data, row){
		             		  
		             		  if(userType != null){
		             			  
		             			 if(userType==11){
			         					//return '<p><span class="badge badge-info">Booked</span></p>';
			         					return '<p class="text-success"><strong>Booked</strong></p>';
	 		 	         			}
			         				else if(userType==10 && row.status==1)
			         			    {
			         					return '<button id="edit"class="btn btn-primary btn-sm"  style="background-color: #002B49; border-color: #002B49; "role="button"><span class="circle bg-white" style="width: 1.15em;height: 1.27em;"> <i class="glyphicon glyphicon-arrow-right  text-gray" style="top:1.5px"></i></span> Exit</button>'
	  			 	         		
			         			    }else{
			         			    	
	  			 	         		return '<p>__</p>';
		  			 	         		
	  			 	         		}
		             		  }
		         				
		         				
		         			}}  
	    	               
		       
		            ],
		           buttons: [
		        	    'copy', 'excel', 'pdf'
		        	  ],
	
		 	             "ordering": false,
		 	             "searching": false,
		 	             "deferRender": true,
		 	             "autoWidth": false,
		 	             "lengthChange": false,
		 	             "language": {
		     	    		"infoEmpty": "No records to show",
		         	    	"emptyTable": "no record found",
		     	    	  },
		     	    	   drawCallback: function(){
		                          if(scope.pageNumber>0)
		                          {
		                           $("#users_previous").removeClass("disabled");
		                          }    
		                            else
		                           {
		                             $("#users_previous").addClass("disabled");
		                            }
	                              $("#users_next").removeClass("disabled");
	                               
	                              $("#users_next").on("click",function(){
	                                 	   
	                                	   if((scope.pageNumber+1)*10< scope.totalRecord)
	                                           {
	    	                            	   document.getElementById('overlid').style.visibility = "visible";    
	    	                            	
	    	                                   $("#users_next").removeClass("disabled");
	      	                                    scope.pageNumber=scope.pageNumber+1;
	      	                                    scope.upperlimit=0;
	      	                                    scope.getData();	     	                                   
	                                            }else
	                                                 {
	                                                 $("#users_next").addClass("disabled");
	                                                 }
	                              });
	                           
	                           $("#users_previous").on("click",function(){ 
	                        	  if(scope.totalPages>1)
	                           			{
	                                	    document.getElementById('overlid').style.visibility = "visible";        
	                                         if(scope.pageNumber > 0)
	 			                        	  {  
	                                        	   scope.pageNumber=scope.pageNumber-1;  
	 			                        	       scope.getData();	 
	 			                        	     
	 			                        	  }
	                                           else
	                                        	   {
	                                        	  
	                                        	   document.getElementById('overlid').style.visibility = "hidden";
	                                        	   $("#users_previous").addClass("disabled");
	 	 			                        	
	                                        	   }
 	                                                              		
 	                                          } 
	                                   
	                               
	                            });
	                           
	                        //if record is less then 10// for datatableinfo
	                            if(scope.totalRecord<10){
	                            	$("#users_next").addClass("disabled");
	                            }  
	                            else
	              	      		{
	                            scope.upperlimit=(scope.pageNumber+1)*10;
 	                           
	              	     	    }
	                        
	                        if((scope.pageNumber+1)*10>scope.totalRecord)
	                        	{
 	                           	 scope.upperlimit=scope.totalRecord;
	                        	}
	                            
	                   
	                        if( scope.totalRecord == scope.upperlimit)
	                        	{
	                        	$("#users_next").addClass("disabled");
	                        	}
	                          } //end of drawcallback function
		    	        }); 
	  }); 
	
	
	   
	   
	//**************SELECT DESELECT TABLE ROW******************************//
	$('#users').on('click', 'tr', function () {
		
			 if ( $(this).hasClass('row_selected'))
		  		{  
				   $("#users tbody tr").removeClass('row_selected');
		  	       $(this).removeClass('row_selected');
		      }else
		  	        {
		  	        $("#users tbody tr").removeClass('row_selected');
		  	        $(this).addClass('row_selected');
		  	 
		  	    }
		}); 
	
     	/* date */
		scope.formatDate = function(date){
			  return new Date(date)
		}
		
		 scope.AddPayment=function (pid,amount)
		 {
			  scope.dataRequest = {
			   			"pid":pid,
			   			"amountPaid":amount,
			   			"paymentMode":1, 
			   			"receiptNumber":"",
			   			"transactionId":"",
			   			"bankCode":""
			   		};
			       scope.requestedData = {
						"username":userN,
						"functionName" : "doUpdatePaymentInfoTable", //
			  			 "data" : JSON.stringify(scope.dataRequest)
	               };
				   http({
						method :'POST',
						url :apiURL+'updatePaymentInfo', 
						data :scope.requestedData,
					    headers: options.headers

						}).then(function (response) {
							if(response.data.status==1){
							      // scope.GetParking();
								    document.getElementById('overlid').style.visibility = "hidden";
									scope.parkingcharges=response.data.myObjectList;
									$('#exampleModal').modal('show');

							}else if(response.data.status==21){
		  		  				 
			  		        } else
							{
	 							scope.message = response.data.message;
								var x = document.getElementById("snackbar_error")
				 				x.className = "show";
				 				setTimeout(function() {
									x.className = x.className.replace("show", "");					 
								}, 3000);
				 			    document.getElementById('overlid').style.visibility = "hidden";
				 			    $('#exampleModal').modal('hide');
								
							} 
						}, function (response) {
						});
		 }
	
		 
		 //*************************UPDATE CLICK*******************************//
	  	$('#users').on('click','#edit', function () {
		 	scope.intime="";
	  	   	scope.vehiclenumber="";
	  	   	scope.amount="";
	  	   	scope.outime="";
	  		var tr = $(this).closest('tr');
	  		var selectedData = jDataTable.row(tr).data();
	   	    scope.UserModel =selectedData;
	   		scope.rowdata =selectedData;
	   	    document.getElementById('overlid').style.visibility = "visible";
	   	    
	    	scope.minutes = 0;
		    scope.hours = 0;
		    scope.days = 0;
	   	
	   	   /*******************************GET PAYMENT****************************************/
	          scope.dataRequest = {
		   			"pid":selectedData.pid,
		   			"siteId":selectedData.mSiteModel.sid,
		   			"vehicleId":selectedData.mVehicleClass.classId,
		   			"inTime":selectedData.inTime
		   		};
		       scope.requestedData = {
					"username":userN,
					"functionName" : "doGetParkingCharges",
		  			 "data" : JSON.stringify(scope.dataRequest)
               };
			   http({
					method :'POST',
					url :apiURL+'getParkingcharges',
					data :scope.requestedData,
				    headers: options.headers

					}).then(function (response) {
						if(response.data.status==1){
						      //scope.GetParking();
							    document.getElementById('overlid').style.visibility = "hidden";
								scope.parkingcharges=response.data.myObjectList;
								var pid=scope.parkingcharges.pid;
								scope.intime=scope.parkingcharges.inTime;
								scope.vehiclenumber=scope.parkingcharges.vehicleNumber;
								scope.outime=scope.parkingcharges.outTime;
								scope.amount=scope.parkingcharges.amount;
								var famount=scope.parkingcharges.amount;
							    scope.sitename=scope.parkingcharges.mSiteModel.siteName;
							    scope.AddPayment(pid,famount);
							  
							    var timestamp1 = new Date(scope.intime).getTime();
							    var timestamp2 = new Date(scope.outime).getTime();

							    var milliseconds = timestamp2 - timestamp1
							    
 							    var seconds = milliseconds / 1000;
							    var minutes = Math.floor(seconds / 60);
							    var hours = Math.floor(minutes / 60) ;
							    var days = Math.floor(hours / 24) ;
							    
							    scope.minutes = minutes% 60;
							    scope.hours = hours% 24;
							    scope.days = days% 365;
							    
						   		$('#exampleModal').modal('show');
						   		scope.getData();	

						}else if(response.data.status==21){
	  		  				 
		  		        } else
						{
 							scope.message = response.data.message;
							var x = document.getElementById("snackbar_error")
			 				x.className = "show";
			 				setTimeout(function() {
								x.className = x.className.replace("show", "");					 
							}, 3000);
			 			    document.getElementById('overlid').style.visibility = "hidden";
			 			    $('#exampleModal').modal('hide');
							
						} 
					}, function (response) {
					});
            }); 
	   
	   
	   
//*****************************RESET BUTTON*********************//
		
		scope.btnreset= function(){
			  scope.filterModel= "";
			  $('#myDatepicker1').data("DateTimePicker").clear();
		   	  $('#myDatepicker2').data("DateTimePicker").clear();
			  scope.fromdate = null; 
		      scope.todate= null;
		      scope.pageNumber =0;
	  		  scope.lowerlimit = 0;
	  		  scope.upperlimit=10;
	  		  scope.pageSize= 10;
	  		  scope.totalrecord=0;
	  		  scope.totalPages =0;
		      scope.GetParking();
		};
		 
	
	
	  	/* scope.print = function () {
	        window.print();
	    } */
  
	}]);
</script>
</head>
<body class="" ng-controller="GetDataController" ng-init="GetParking()">
 
  <%@ include file="Sidebar.jsp"%>	
    
  
  
  <div class="content-wrap">
    
    <!-- main page content. the place to put widgets in. usually consists of .row > .col-lg-* > .widget.  -->
    <main id="contentt" class="content" role="main" class="ng-cloak">
      <!-- Page content -->
      
       <!--Heading  -->
     
       <!--Filter Form  -->
       <div class="row">
            <div class="col-lg-12">
                <section class="widget" style="margin-bottom: 15px;">
                       <header> <h5>Vehicle Information<span class="fw-semi-bold"></span> </h5></header>
                
                    <div class="widget-body">
                    <form id="alert_form" autocomplete="off">
						<!--To Date  -->
						<div class="row">
							 <div class="col-sm-3 col-md-3  col-xs-12">
									From
									<div class="row">
										<div class="col-sm-12 col-md-12 col-xs-12">
											<div class="form-group">
												<div>
													<div class='input-group date' id='myDatepicker1'>
												<input type='text' class="form-control" name="To_date"
													placeholder="From Date" ng-model="filterModel.To_date" />
												<span class="input-group-addon datetime"  > <span
													class="fa fa-calendar"></span>
												</span>
 											</div>
												
												</div>
											</div>
										</div>
									</div>
									
								</div> 
								<!--End date  -->
								 <div class="col-sm-3 col-md-3  col-xs-12">
									To
									<div class="row">
										<div class="col-sm-12 col-md-12 col-xs-12">
											<div class="form-group">
												<div>
													<div class='input-group date' id='myDatepicker2'>
												<input type='text' class="form-control" name="out_time"
													placeholder="To Date" ng-model="filterModel.out_date" />
												<span class="input-group-addon datetime"  > <span
													class="fa fa-calendar"></span>
												</span>
 											</div>
												</div>
											</div>
										</div>
									</div>
									
								</div> 
							    <!-- Vehicle Number -->
								<div class="col-sm-3 col-md-3  col-xs-12">
									 Vehicle Number
									<div class="row">
										<div class="col-sm-12 col-md-12 col-xs-12">
											<div class="form-group">
												<div>
												<input type="text" class="form-control  " name="vehicleNumber"	placeholder="Enter Vehicle Number" ng-model="filterModel.vehicleNumber"
											      id="vehicleNumber" onkeyup="alphaNumericChecker()" />
												</div>
											</div>
										</div>
									</div>
									
								</div>
								<!--Site Type  -->
								<div class="col-sm-3 col-md-3  col-xs-12">
									 Vehicle Class
									<div class="row">
										<div class="col-sm-12 col-md-12 col-xs-12">
											<div class="form-group">
												<div>
												  <select class="select2 form-control" name ="mVehicleClass" ng-model="filterModel.mVehicleClass"ng-options="s as s.className for s in vehiclelist track by s.classId"  id="mVehicleClass" >
									              <option value="" disabled selected>Select Vehicle Class  </option>
 									              </select>
												</div>
											</div>
										</div>
									</div>
									
								</div>
							
								<!--Vehicle Site  -->
								<div class="col-sm-3 col-md-3  col-xs-12">
								     Site
									<div class="row">
										<div class="col-sm-12 col-md-12 col-xs-12">
											<div class="form-group">
												<div>
												<select class="select2 form-control" name ="sitemodel"  ng-model="filterModel.mSiteModel"   ng-options="a as a.siteName for a in sitedata track by a.sid"  id="sitemodel" >
									            <option value="" disabled selected>Select Site  </option>
 									            </select>
												</div>
											</div>
										</div>
									</div>
									
								</div>
								<!--Enter By  -->
								<div class="col-sm-3 col-md-3  col-xs-12">
									Entry By
									<div class="row">
										<div class="col-sm-12 col-md-12 col-xs-12">
											<div class="form-group">
												<div>
												<select class="select2 form-control" name ="userType" ng-model="filterModel.userType" id="userType" >
													<option value="" disabled selected>Select User Type </option>
													<option value="10">Operator</option>
													<option value="11">User</option>
				 									</select>  
												</div>
											</div>
										</div>
									</div>
									
								</div>
								 <div class="col-sm-6 col-md-6 col-xs-12">
 						   <b>&nbsp;</b>
 						   <div class="row">
   							<div class="col-sm-12 col-md-12 col-xs-12">
   							<button class="btn btn-primary" ng-click="btnfilter(filterModel)"id="serach" style="width:110px; background-color: #002B49;">Search</button>
 							<button type="submit" id="btnreset" class="btn btn-danger" ng-click="btnreset()" style="width:110px;">Reset</button>
   							</div>
   							</div>
    					</div>
								</div>
								</form>
							
                    </div>
                    </section>
                    </div>
                    </div>
                
      <!-- Table Content -->
        <div class="row">
            <div class="col-lg-12">
                <section class="widget">
                  <!--   <header>
                         <h5>Vehicle Information<span class="fw-semi-bold"></span>
                        </h5>
                    </header> -->
                    <div class="widget-body">
                    <div class="table-responsive">
                      <table class="table table-striped dataTable  table-lg mt-lg mb-0" id="users">
                      <thead class=" text-primary">
                      <tr>
                                	<th data-visible="false">ID</th>
											<th style=" text-transform: revert; font-size: 15px;">Vehicle Number</th>
											<th style=" text-transform: revert; font-size: 15px;">Type</th>
											<th style=" text-transform: revert; font-size: 15px;">In-Time</th>
											<th style=" text-transform: revert; font-size: 15px;">Out-Time</th>
											<th style=" text-transform: revert; font-size: 15px;">Site</th>
											<th style=" text-transform: revert; font-size: 15px;">Vehicle Status</th>
											<th style=" text-transform: revert; font-size: 15px;">Entry-By</th>
											<th style=" text-transform: revert; font-size: 15px;">Action</th>
						 </tr>
                         </thead>
                       </table>
                       
                       	 <div ng-if="totalRecord!=0">
                       	 <div class="dataTables_info" role="status" aria-live="polite">Showing {{pageNumber*10+1}} to  <span ng-if="totalRecord<10"> {{totalRecord}} </span><span ng-if="totalRecord==10"> {{upperlimit}} </span><span ng-if="totalRecord>10"> {{upperlimit}} </span>of {{totalRecord}} entries</div>
                       </div>
                       	 <div ng-if="totalRecord==0">
                       	 <div class="dataTables_info" role="status" aria-live="polite"> Showing {{totalRecord}} to {{totalRecord}} of {{totalRecord}}</div>
                       </div>
                       
                        <div class="clearfix">
                     </div>
                    </div>
                    
                </section>
            </div>
        
        <!--payment info  -->
        
          <div id="print-content">
            <div class="modal " id="exampleModal"  aria-labelledby="exampleModalLabel" style="padding-right: 17px;" aria-modal="true" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                              <div class="modal-header"  style="border: none; margin:10px; padding: 0;">
                                  
                                    <h5><i class="fa fa-map-marker  text-info"></i> <b class="text-info">{{sitename}}</b></h5>
                                </div> 
                                <div class="modal-body" style="padding-top:0;">
                                   <div class="row">
                                   <div class="col-sm-5 ">
                                   <!-- <p style="font-weight: 500;">Site :</p> -->
                                   <p style="font-weight: 500;">Vehicle Number :</p>
                                   <p style="font-weight: 500;">In-Time :</p>
                                   <p style="font-weight: 500;" >Out-Time :</p>
                                   <p style="font-weight: 500;" >Total Time :</p>
                                   
                                    <p style="font-weight: 500;">Total Amount :</p>
                                   </div>
                                    
                                   <div class="col-sm-5 ">
                                   <!-- <P>{{sitename}}</P> -->
                                   <p>{{vehiclenumber}}</p>
                                   <p>{{formatDate(intime) | date:"dd MMM , yyyy hh:mm a"}}</p>
                                   <p>{{formatDate(outime) | date:"dd MMM , yyyy hh:mm a"}}</p>
                                   
                                   <p><span ng-if="days>0">{{days}} Day</span>&nbsp;<span ng-if="hours>0">{{hours}} Hour &nbsp;</span>&nbsp;<span ng-if="minutes>0">{{minutes}} Minute</span></p>
                                   
                                   <p> Rs. {{amount}}</p>
                                   </div>
                                 </div>
                             
                               <!--   Do you really want to exit vehicle <i class="fa fa-question-circle" style="color: #002B49;font-size: 19px;"aria-hidden="true"></i> -->  
                                </div>
                                <div class="modal-footer">
                                   <!--  <button type="button" class="btn btn-primary" id="print" ng-click= "print()"  style="background-color: #002B49;"> <span class="glyphicon glyphicon-print"></span> print</button> -->
                                    <button type="button" class="btn btn-secondary"   style="background-color: red;" data-dismiss="modal">close</button>
                                </div>
                                </div>
                            </div>
                        </div>
                        </div>
            
        </div>
        <div id="snackbar_success">{{response_message}} </div>
		<div id="snackbar_error">{{message}} </div>
		
		<!-- spinner -->
        <div class="loader-wrap" style="background-color: rgba(0, 0, 0, 0.4);" id="overlid">
		<i class="fa fa-spinner fa-spin"></i>
        </div>
        
        </main>
        </div>
              
        <div class="loader-wrap"id="overl">
            <i class="fa fa-circle-o-notch fa-spin-fast"></i>
        </div> 
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-pjax/jquery.pjax.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/popper.js/dist/umd/popper.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/dist/js/bootstrap.js"></script>
       <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap/js/dist/util.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/widgster/widgster.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/pace.js/pace.js" data-pace-options='{ "target": "body", "ghostTime": 1000 }'></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/hammerjs/hammer.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-hammerjs/jquery.hammer.js"></script>


        <!-- common app js -->
        <script src="${pageContext.request.contextPath}/resources/js/settings.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/app.js"></script>

        <!-- Page scripts -->
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.animator/jquery.flot.animator.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.selection.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.time.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.pie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.stack.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.crosshair.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot/jquery.flot.symbol.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/flot.dashes/jquery.flot.dashes.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap_calendar/bootstrap_calendar/js/bootstrap_calendar.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/vendors/moment/min/moment.min.js"></script>
       
        <!-- bootstrap-daterangepicker -->	
       <script src="${pageContext.request.contextPath}/resources/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
         
      
         <script>
       
       $('#myDatepicker1').datetimepicker({
   		
           format: 'YYYY-MM-DD' 
  		//format: 'YYYY-MM-DD  h:mm:ss a'
        
         });
	  	$('#myDatepicker2').datetimepicker({
	          format: 'YYYY-MM-DD'
	  	//	format: 'YYYY-MM-DD  h:mm:ss a'
	         });
	  	 
	     //TO CHECK INPUT FIELDS ONLY CONTAING ALPHA_NUMERIC
	  	  	 function alphaNumericChecker() {
	  			 
	           var fvehicleNumber = document.getElementById('vehicleNumber');
	           fvehicleNumber.value = fvehicleNumber.value.replace(/[^a-zA-Z0-9 ]+/, '');
	           }
       </script>
    </body>

</html>










